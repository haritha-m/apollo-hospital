<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;
use common\models\ServiceMainCategory;
AppAsset::register($this);
$action = Yii::$app->controller->action->id;
$controler = Yii::$app->controller->id;
$url_val = $controler . '/' . $action;
$contact_info = \common\models\ContactInfo::findOne(1);
$common_content = \common\models\CommonContent::findOne(1);
$sanjeevani = \common\models\Sanjeevani::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();
$service_category = \common\models\ServiceCategory::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_ASC])->all();
$service_excellence = \common\models\ServiceCategory::findOne(1);
$service_other = \common\models\ServiceCategory::findOne(2);


?>
<?php $this->beginPage() ?>


<?php
error_reporting(0);
session_start();
?>
<!doctype html>
<html lang="en" xmlns="//www.w3.org/1999/xhtml">
<!-- 
 ██╗███╗░░██╗████████╗███████╗██████╗░░██████╗███╗░░░███╗░█████╗░██████╗░████████╗
 ██║████╗░██║╚══██╔══╝██╔════╝██╔══██╗██╔════╝████╗░████║██╔══██╗██╔══██╗╚══██╔══╝
 ██║██╔██╗██║░░░██║░░░█████╗░░██████╔╝╚█████╗░██╔████╔██║███████║██████╔╝░░░██║░░░
 ██║██║╚████║░░░██║░░░██╔══╝░░██╔══██╗░╚═══██╗██║╚██╔╝██║██╔══██║██╔══██╗░░░██║░░░
 ██║██║░╚███║░░░██║░░░███████╗██║░░██║██████╔╝██║░╚═╝░██║██║░░██║██║░░██║░░░██║░░░
 ╚═╝╚═╝░░╚══╝░░░╚═╝░░░╚══════╝╚═╝░░╚═╝╚═════╝░╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░
 -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, shrink-to-fit=no, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php
    if (isset($this->params['other_meta_tags']) && $this->params['other_meta_tags'] != '') {
        echo $this->params['other_meta_tags'];
    }
    ?>



    <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->homeUrl ?>assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::$app->homeUrl ?>assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->homeUrl ?>assets/images/favicon-16x16.png">
    <link rel="manifest" href="<?= Yii::$app->homeUrl ?>assets/images/manifest.webmanifest">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#006b8e">

    <!-- FONTS --->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@200;300;400;600;700;900&family=Urbanist:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <!-- JQUERY --->
    <script src="https://code.jquery.com/jquery-3.6.2.min.js"
        integrity="sha256-2krYZKh//PcchRtd+H+VyyQoZ/e3EcrkxhM8ycwASPA=" crossorigin="anonymous"></script>

    <!-- LAZY LOAD --->
    <script type="text/javascript"
        src="//cdn.jsdelivr.net/npm/intersection-observer@0.7.0/intersection-observer.min.js">
    </script>
    <script async src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.8.3/dist/lazyload.min.js"></script>

    <!-- BOOTSTRAP --->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>

    <!-- INCLUDES -->
    <link rel="stylesheet preload" type="text/css" href="<?= Yii::$app->homeUrl ?>assets/css/app.min.css" as="style"
        media="all">
    <link rel="stylesheet preload" type="text/css" href="<?= Yii::$app->homeUrl ?>assets/css/style.css" as="style"
        media="all">
    <!-- Google tag (gtag.js) --> 
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-11052711271"></script> 
    <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-11052711271'); </script>
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
</head>

<body
    class="<?= $url_val == 'site/Quality_and_Safety' ? 'BannerH' : '' ?> <?= $url_val == 'site/Privacy_Policy' ? 'BannerH' : '' ?>">



    <?php $this->beginBody() ?>

    <header id="Header">
        <section id="HeaderMain">
            <div class="sp-container rigt">
                <div class="FRow main-nav">
                    <div class="LogoSec">
                        <a href="<?= Url::to(['site/index']); ?>">
                            <img src="<?= Yii::$app->homeUrl ?>assets/images/ApolloAdluxLogo.png" alt="logo" width="280"
                                height="143" />
                        </a>
                    </div>
                    <div class="rgtSd">
                        <div class="modal fade" id="HeaderMenu" tabindex="-1" aria-labelledby="HeaderMenuLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="ModalHeader">
                                        <div class="MenuIcon">
                                            <button type="button" data-bs-dismiss="modal" aria-label="Close">
                                                <div class="IconWrp">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="nav-item">
                                            <div class="accordion" id="MenuAccord">
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <a href="<?= Url::to(['site/doctors']); ?>"
                                                            class="accordion-button <?= $url_val == 'site/doctors' ? 'active' : '' ?>">
    
    
    
    
                                                            Doctors</a>
                                                    </div>
                                                </div>
                                                <!--<div class="accordion-item">-->
                                                <!--    <div class="accordion-header">-->
                                                <!--        <a href="<?= Url::to(['site/services']); ?>"-->
                                                <!--            class="accordion-button <?= $url_val == 'site/services' ? 'active' : '' ?>">-->
    
                                                <!--            Services</a>-->
                                                <!--    </div>-->
                                                <!--</div>-->
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <!--<a href="<?= Url::to(['site/packages']); ?>"  class="accordion-button <?= $url_val == 'site/packages' ? 'active' : '' ?>"> Packages &amp; Offers </a>-->
                                                        <button
                                                            class="accordion-button <?= $url_val == 'site/services' ? 'active' : '' ?>"
                                                            type="button" data-bs-toggle="collapse"
                                                            data-bs-target="#menuAccoSpecialities" aria-expanded="false"
                                                            aria-controls="collapseOne">
                                                            Our Specialities
                                                        </button>
                                                    </div>
                                                    <div id="menuAccoSpecialities" class="accordion-collapse collapse"
                                                        aria-labelledby="headingOne" data-bs-parent="#MenuAccord">
                                                        <div class="accordion-body">
                                                            <ul>
                                                                <?php foreach($service_category as $category){?>
                                                                <li>
                                                                   <?=$category->category_name?>
                                                                    <div class="accordian-body">
                                                                        <ul>
                                                                            
                                                                           <?php 
                                                                           $sub_cat = ServiceMainCategory::find()->where(['category'=>$category->id,'status'=>'1'])->all();
                                                                           foreach($sub_cat as $val) : ?>
                                                                           
                                                                           <li>
                                                                            
                                                                             <a href="<?= Url::to(['site/service-detail','id'=>$val->canonical_name]); ?>"><?=$val->title?></a>
                                                                          </li>
                                                                          <?php endforeach; ?>
                                                                        </ul>
                                                                </li>
                                                                <?php }?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <a href="<?= Url::to(['site/international']); ?>"
                                                            class="accordion-button <?= $url_val == 'site/international' ? 'active' : '' ?>">
    
    
    
    
                                                            International Patients</a>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <!--<a href="<?= Url::to(['site/packages']); ?>"  class="accordion-button <?= $url_val == 'site/packages' ? 'active' : '' ?>"> Packages &amp; Offers </a>-->
                                                        <button
                                                            class="accordion-button <?= $url_val == 'site/packages' ? 'active' : '' ?>"
                                                            type="button" data-bs-toggle="collapse"
                                                            data-bs-target="#menuAccoPackages" aria-expanded="false"
                                                            aria-controls="collapseOne">
                                                            Special Packages
                                                        </button>
                                                    </div>
                                                    <div id="menuAccoPackages" class="accordion-collapse collapse"
                                                        aria-labelledby="headingOne" data-bs-parent="#MenuAccord">
                                                        <div class="accordion-body">
                                                            <ul>
                                                                <li><a
                                                                        href="<?= Url::to(['site/packages']); ?>">Health Check-up Packages</a>
                                                                </li>
                                                                <li><a href="<?= Url::to(['site/offers']); ?>">Special Offers</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <a href="<?= Url::to(['site/insurance']); ?>"
                                                            class="accordion-button <?= $url_val == 'site/insurance' ? 'active' : '' ?>">
    
    
                                                            Insurance</a>
                                                    </div>
                                                </div>
                                                <!-- <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <a href="#"
                                                            class="accordion-button <?= $url_val == 'site/eservices' ? 'active' : '' ?>">
    
                                                            Patient Portals</a>
                                                    </div>
                                                </div> -->
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <!--<a href="<?= Url::to(['site/packages']); ?>"  class="accordion-button <?= $url_val == 'site/packages' ? 'active' : '' ?>"> Packages &amp; Offers </a>-->
                                                        <button
                                                            class="accordion-button"
                                                            type="button" data-bs-toggle="collapse"
                                                            data-bs-target="#menuAccoPatientPortal" aria-expanded="false"
                                                            aria-controls="collapseOne">
                                                            Patient Portals
                                                        </button>
                                                    </div>
                                                    <div id="menuAccoPatientPortal" class="accordion-collapse collapse"
                                                        aria-labelledby="headingOne" data-bs-parent="#MenuAccord">
                                                        <div class="accordion-body">
                                                            <ul>
                                                                <li><a
                                                                        href="#">Payment Portal</a>
                                                                </li>
                                                                <li><a href="#">Patient Records</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="accordion-item">
                                                    <div class="accordion-header">
                                                        <button
                                                            class="accordion-button <?= $url_val == 'site/media' || $url_val == 'site/media-details' || $url_val == 'site/media-gallery' || $url_val == 'site/media-gallery-details' ? 'active' : '' ?>"
                                                            type="button" data-bs-toggle="collapse"
                                                            data-bs-target="#menuAccoMedia" aria-expanded="false"
                                                            aria-controls="collapseThree">
                                                            Gallery
                                                        </button>
                                                    </div>
                                                    <div id="menuAccoMedia" class="accordion-collapse collapse"
                                                        aria-labelledby="headingThree" data-bs-parent="#MenuAccord">
                                                        <div class="accordion-body">
                                                            <ul>
                                                                <li><a href="<?= Url::to(['site/media']); ?>">Media</a>
                                                                </li>
                                                                <li><a
                                                                        href="<?= Url::to(['site/media-gallery']); ?>">Gallery</a>
                                                                </li>
                                                                <li><a
                                                                        href="<?= Url::to(['site/cme']); ?>">CME</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="DeskHeaderMenu" tabindex="-1" role="dialog"
                            aria-labelledby="DeskHeaderMenuLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="MenuIcon closeBtn">
                                            <button type="button" data-bs-dismiss="modal" aria-label="Close">
                                                <div class="IconWrp">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                                <div class="btnCnt">Menu</div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="sp-container rigt">
                                            <div class="Dflx">
                                                <div class="lftBx">
                                                    <div class="designBxElemntOuter">
                                                        <div class="elementOne"></div>
                                                        <div class="elementTwo"></div>
                                                    </div>
                                                    <div class="logoSec">
                                                        <a href="<?= Url::to(['site/index']); ?>">
                                                            <img src="<?= Yii::$app->homeUrl ?>assets/images/logoColor.png"
                                                                alt="logo" width="280" height="143">
                                                        </a>
                                                    </div>
                                                    <div class="menuListWrap">
                                                        <div class="accordion accordion-flush menuAccoWrap"
                                                            id="accordionMob">
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="headingOne">
                                                                    <a href="#!" class="accorBtn pointer">About Us</a>
                                                                    <button class="accordion-button" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#collapseOne" aria-expanded="true"
                                                                        aria-controls="collapseOne">
                                                                        About Us
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseOne"
                                                                    class="accordion-collapse collapse show"
                                                                    aria-labelledby="headingOne"
                                                                    data-bs-parent="#accordionMob">
                                                                    <div class="accordion-body">
                                                                        <ul class="itemList">
                                                                            <li><a href="<?= Url::to(['site/about']); ?>">About
                                                                                    Apollo Adlux</a></li>
                                                                            <li><a href="<?= Url::to(['site/vision']); ?>">Vision
                                                                                    And Mission</a></li>
                                                                            <li><a
                                                                                    href="<?= Url::to(['site/ourmessage']); ?>">Our
                                                                                    Message</a></li>
                                                                            <li><a
                                                                                    href="<?= Url::to(['site/corevalues']); ?>">Core
                                                                                    Values</a></li>
                                                                            <!-- <li><a
                                                                                    href="<?= Url::to(['site/promotors']); ?>">Promoters</a>
                                                                            </li> -->
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header" id="headingTwo">
                                                                    <a href="<?= Url::to(['site/sanjeevani']); ?>"
                                                                        class="accorBtn pointer">Sanjeevani</a>
                                                                    <button class="accordion-button collapsed" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#collapseTwo" aria-expanded="false"
                                                                        aria-controls="collapseTwo">
                                                                        Sanjeevani
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseTwo" class="accordion-collapse collapse"
                                                                    aria-labelledby="headingTwo"
                                                                    data-bs-parent="#accordionMob">
                                                                    <div class="accordion-body">
                                                                        <ul class="itemList">
                                                                            <?php
                                                                            if(!empty($sanjeevani))
                                                                            {
                                                                                foreach($sanjeevani as $data)
                                                                            {
                                                                                ?>
                                                                            <li><a
                                                                                    href="<?= Url::to(['site/sanjeevanidetails', 'name' => $data->canonical_name]); ?>"><?= $data->title?></a>
                                                                            </li>
                                                                            <?php
                                                                           }
                                                                       }
                                                                       ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="accordion-item">
                                                                <h2 class="accordion-header">
                                                                    <a href="javascript:void(0)"
                                                                        class="accorBtn pointer">Our Specialities</a>
                                                                    <button class="accordion-button collapsed" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#collapseSpecialities" aria-expanded="false"
                                                                        aria-controls="collapseSpecialities">
                                                                        Our Specialities
                                                                    </button>
                                                                </h2>
                                                                <div id="collapseSpecialities" class="accordion-collapse collapse"
                                                                    aria-labelledby="headingTwo"
                                                                    data-bs-parent="#accordionMob">
                                                                    <div class="accordion-body">
                                                                        <ul class="itemList">
                                                                <?php foreach($service_category as $category){?>
                                                                <li>
                                                                    <a href="<?= Url::to(['site/services', 'slug' => $category->canonical_name]) ?>"><?=$category->category_name?></a>
                                                                </li>
                                                                <?php }?>
                                                            </ul>
                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="accordion-item singleMenu">
                                                                <h2 class="accordion-header" id="headingFour">
                                                                    <a href="<?= Url::to(['site/media']); ?>"
                                                                        class="accorBtn pointer">Media</a>
                                                                    <button class="accordion-button collapsed" type="button"
                                                                        data-bs-toggle="collapse"
                                                                        data-bs-target="#collapseFour" aria-expanded="false"
                                                                        aria-controls="collapseFour">
                                                                        Media
                                                                    </button>
                                                                </h2>
    
                                                            </div> -->
    
                                                            <div class="accordion-item singleMenu">
                                                                <h2 class="accordion-header" id="headingFour">
                                                                    <a href="<?= Url::to(['site/visitors']); ?>"
                                                                        class="accorBtn pointer">Patients & Visitors</a>
                                                                    <a href="<?= Url::to(['site/visitors']); ?>"
                                                                        class="accordion-button">
                                                                        Patients & Visitors
                                                                    </a>
                                                                </h2>
                                                            </div>
    
    
                                                            <div class="accordion-item singleMenu">
                                                                <h2 class="accordion-header" id="headingFive">
                                                                    <a href="<?= Url::to(['site/quality']); ?>"
                                                                        class="accorBtn pointer">Quality & Safety</a>
                                                                    <a href="<?= Url::to(['site/quality']); ?>"
                                                                        class="accordion-button">
                                                                        Quality & Safety
                                                                    </a>
                                                                </h2>
    
                                                            </div>
    
    
                                                            <div class="accordion-item singleMenu">
                                                                <h2 class="accordion-header" id="headingSix">
                                                                    <a href="<?= Url::to(['site/conference',]); ?>"
                                                                        class="accorBtn pointer">Conferences</a>
                                                                    <a href="<?= Url::to(['site/conference']); ?>"
                                                                        class="accordion-button">
                                                                        Conferences
                                                                    </a>
                                                                </h2>
    
                                                            </div>
    
    
    
                                                            <div class="accordion-item singleMenu">
                                                                <h2 class="accordion-header" id="headingSeven">
                                                                    <a href="<?= Url::to(['site/career']); ?>"
                                                                        class="accorBtn pointer">Careers</a>
                                                                    <a href="<?= Url::to(['site/career']); ?>"
                                                                        class="accordion-button">
                                                                        Careers
                                                                    </a>
                                                                </h2>
    
                                                            </div>
    
                                                            <div class="accordion-item singleMenu">
                                                                <h2 class="accordion-header" id="headingSeven2">
                                                                    <a href="<?= Url::to(['site/contact']); ?>"
                                                                        class="accorBtn pointer">Contact Us</a>
                                                                    <a href="<?= Url::to(['site/contact']); ?>"
                                                                        class="accordion-button">
                                                                        Contact Us
                                                                    </a>
                                                                </h2>
    
                                                            </div>
    
                                                            <!--<div class="accordion-item mobMenu singleMenu">-->
                                                            <!--    <div class="accordion-header">-->
                                                            <!--        <a href="#!" class="accordion-button">Doctors</a>-->
                                                            <!--    </div>-->
                                                            <!--</div>-->
    
    
    
    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ritBx">
                                                    <img class="lazy innerBg" loading="lazy"
                                                        src="<?= Yii::$app->homeUrl ?>assets/images/menuRgtImg.jpg"
                                                        data-src="<?= Yii::$app->homeUrl ?>assets/images/menuRgtImg.jpg"
                                                        alt="doctor" width="800" height="1080">
                                                    <div class="cntWrap">
                                                        <h4 class="bxTle"><?= $contact_info->footer_caption?></h4>
                                                        <ul class="social">
                                                            <li>
                                                                <a href="<?= $contact_info->facebook_link?>"
                                                                    target="_blank">
                                                                    <svg viewBox="0 0 22 22">
                                                                        <defs>
                                                                            <clipPath id="clip-path">
                                                                                <rect id="Rectangle_93"
                                                                                    data-name="Rectangle 93" width="22"
                                                                                    height="22"
                                                                                    transform="translate(-0.392 -0.334)"
                                                                                    fill="#fff"></rect>
                                                                            </clipPath>
                                                                        </defs>
                                                                        <g id="Group_179" data-name="Group 179"
                                                                            transform="translate(0.392 0.334)"
                                                                            clip-path="url(#clip-path)">
                                                                            <path id="Path_142" data-name="Path 142"
                                                                                d="M19.206,0H2.837A2.837,2.837,0,0,0,0,2.837V19.21a2.836,2.836,0,0,0,2.837,2.833h8.938V13.519H8.912V10.182h2.863V7.728a4.008,4.008,0,0,1,4.28-4.4,24.2,24.2,0,0,1,2.566.129V6.436H16.869c-1.382,0-1.649.659-1.649,1.619v2.127h3.307L18.1,13.519H15.22v8.525h3.987a2.837,2.837,0,0,0,2.837-2.837V2.837A2.837,2.837,0,0,0,19.206,0"
                                                                                transform="translate(-0.439 -0.355)"></path>
                                                                        </g>
                                                                    </svg>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?= $contact_info->twitter_linlk?>"
                                                                    target="_blank">
                                                                    <svg viewBox="0 0 14.928 12.129">
                                                                        <g id="twitter_1_" data-name="twitter (1)"
                                                                            transform="translate(0 -48)">
                                                                            <g id="Group_11" data-name="Group 11"
                                                                                transform="translate(0 48)">
                                                                                <path id="Path_6" data-name="Path 6"
                                                                                    d="M14.928,49.436a6.381,6.381,0,0,1-1.763.483,3.043,3.043,0,0,0,1.346-1.692,6.116,6.116,0,0,1-1.941.741,3.06,3.06,0,0,0-5.294,2.093,3.151,3.151,0,0,0,.071.7,8.663,8.663,0,0,1-6.308-3.2,3.061,3.061,0,0,0,.94,4.09A3.022,3.022,0,0,1,.6,52.271v.034a3.075,3.075,0,0,0,2.452,3.007,3.055,3.055,0,0,1-.8.1,2.706,2.706,0,0,1-.579-.052,3.09,3.09,0,0,0,2.86,2.132A6.149,6.149,0,0,1,.732,58.8,5.733,5.733,0,0,1,0,58.756a8.616,8.616,0,0,0,4.7,1.373,8.651,8.651,0,0,0,8.711-8.709c0-.135,0-.266-.011-.4A6.105,6.105,0,0,0,14.928,49.436Z"
                                                                                    transform="translate(0 -48)"></path>
                                                                            </g>
                                                                        </g>
                                                                    </svg>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?= $contact_info->instagram_link?>"
                                                                    target="_blank">
                                                                    <svg viewBox="0 0 510.9 511">
                                                                        <path d="M509.6,150.2c-1.2-27.2-5.6-45.8-11.9-62c-6.5-17.2-16.5-32.5-29.5-45.3c-12.8-13-28.3-23.1-45.2-29.4
                                                                        c-16.3-6.3-34.8-10.7-62-11.9C333.6,0.3,324.9,0,255.5,0s-78.1,0.3-105.3,1.5c-27.2,1.2-45.8,5.6-62,11.9
                                                                        c-17.2,6.5-32.5,16.5-45.3,29.5c-13,12.8-23.1,28.3-29.4,45.2c-6.3,16.3-10.7,34.8-11.9,62C0.3,177.5,0,186.2,0,255.5
                                                                        s0.3,78.1,1.5,105.3c1.2,27.2,5.6,45.8,11.9,62c6.5,17.2,16.6,32.5,29.5,45.3c12.8,13,28.3,23.1,45.2,29.4
                                                                        c16.3,6.3,34.8,10.7,62,11.9c27.2,1.2,35.9,1.5,105.3,1.5s78.1-0.3,105.3-1.5c27.2-1.2,45.8-5.6,62-11.9
                                                                        c34.3-13.3,61.5-40.4,74.8-74.8c6.3-16.3,10.7-34.8,11.9-62c1.2-27.3,1.5-35.9,1.5-105.3S510.8,177.5,509.6,150.2z M463.6,358.9
                                                                        c-1.1,25-5.3,38.4-8.8,47.4c-8.6,22.3-26.3,39.9-48.5,48.5c-9,3.5-22.6,7.7-47.4,8.8c-27,1.2-35,1.5-103.2,1.5s-76.4-0.3-103.2-1.5
                                                                        c-25-1.1-38.4-5.3-47.4-8.8c-11.1-4.1-21.2-10.6-29.3-19.1c-8.5-8.3-15-18.3-19.1-29.3c-3.5-9-7.7-22.6-8.8-47.4
                                                                        c-1.2-27-1.5-35-1.5-103.2s0.3-76.4,1.5-103.2c1.1-25,5.3-38.4,8.8-47.4C60.7,94,67.2,84,75.8,75.8c8.3-8.5,18.3-15,29.3-19.1
                                                                        c9-3.5,22.6-7.7,47.4-8.8c27-1.2,35-1.5,103.2-1.5c68.3,0,76.4,0.3,103.2,1.5c25,1.1,38.4,5.3,47.4,8.8
                                                                        c11.1,4.1,21.2,10.6,29.3,19.1c8.5,8.3,15,18.3,19.1,29.4c3.5,9,7.7,22.6,8.8,47.4c1.2,27,1.5,35,1.5,103.2S464.8,331.9,463.6,358.9
                                                                        z"></path>
                                                                        <path
                                                                            d="M255.5,124.3c-72.5,0-131.3,58.8-131.3,131.3s58.8,131.3,131.3,131.3c72.5,0,131.3-58.8,131.3-131.3S328,124.3,255.5,124.3z
                                                                        M255.5,340.7c-47,0-85.2-38.1-85.2-85.2s38.1-85.2,85.2-85.2c47,0,85.2,38.1,85.2,85.2S302.6,340.7,255.5,340.7z">
                                                                        </path>
                                                                        <path d="M422.7,119.1c0,16.9-13.7,30.6-30.6,30.6c-16.9,0-30.6-13.7-30.6-30.6c0-16.9,13.7-30.6,30.6-30.6
                                                                        C408.9,88.4,422.7,102.2,422.7,119.1z"></path>
                                                                    </svg>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?= $contact_info->linkedin_link?>"
                                                                    target="_blank">
                                                                    <svg viewBox="0 0 21.266 20.327">
                                                                        <defs>
                                                                            <clipPath id="clip-path">
                                                                                <rect id="Rectangle_91"
                                                                                    data-name="Rectangle 91" width="21.266"
                                                                                    height="20.327"></rect>
                                                                            </clipPath>
                                                                        </defs>
                                                                        <g id="Group_175" data-name="Group 175"
                                                                            transform="translate(0)">
                                                                            <g id="Group_174" data-name="Group 174"
                                                                                clip-path="url(#clip-path)">
                                                                                <path id="Path_139" data-name="Path 139"
                                                                                    d="M2.579,0a2.376,2.376,0,1,0-.06,4.739h.03A2.377,2.377,0,1,0,2.579,0">
                                                                                </path>
                                                                                <rect id="Rectangle_90"
                                                                                    data-name="Rectangle 90" width="4.559"
                                                                                    height="13.715"
                                                                                    transform="translate(0.269 6.612)">
                                                                                </rect>
                                                                                <path id="Path_140" data-name="Path 140"
                                                                                    d="M229.666,189.108a5.711,5.711,0,0,0-4.108,2.311V189.43H221v13.715h4.559v-7.659a3.124,3.124,0,0,1,.15-1.113,2.5,2.5,0,0,1,2.339-1.667c1.65,0,2.309,1.258,2.309,3.1v7.337h4.558v-7.864c0-4.213-2.249-6.173-5.248-6.173"
                                                                                    transform="translate(-213.648 -182.818)">
                                                                                </path>
                                                                            </g>
                                                                        </g>
                                                                    </svg>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="https://api.whatsapp.com/send?phone=<?= $contact_info->whatsap_number?>"
                                                                    target="_blank">
                                                                    <svg viewBox="0 0 24.98 25.298">
                                                                        <g id="whatsapp"
                                                                            transform="translate(-54.283 -52.069)">
                                                                            <path id="Path_145" data-name="Path 145"
                                                                                d="M153.036,140.811c-.126-.071-.269-.151-.44-.255-.1-.058-.216-.137-.344-.221a3.415,3.415,0,0,0-1.576-.735.77.77,0,0,0-.278.05,2.689,2.689,0,0,0-.969.893,4.751,4.751,0,0,1-.356.415,7,7,0,0,1-3.4-3.4,4.72,4.72,0,0,1,.417-.358,2.694,2.694,0,0,0,.891-.966c.183-.475-.152-1.043-.685-1.855-.084-.128-.163-.248-.221-.344-.1-.173-.184-.315-.255-.441-.291-.516-.483-.858-1.2-.858a2.83,2.83,0,0,0-1.579.881,3,3,0,0,0-1.011,2.075,9.128,9.128,0,0,0,2.914,5.987l.01.007a9.116,9.116,0,0,0,5.988,2.909h0a2.99,2.99,0,0,0,2.074-1.006,2.819,2.819,0,0,0,.882-1.573C153.894,141.294,153.553,141.1,153.036,140.811Z"
                                                                                transform="translate(-80.912 -74.303)">
                                                                            </path>
                                                                            <path id="Path_146" data-name="Path 146"
                                                                                d="M79.263,64.656A12.817,12.817,0,0,0,66.514,52.069,12.066,12.066,0,0,0,54.421,63.2a12.687,12.687,0,0,0,1.96,7.815l-2.1,6.347h.071l6.093-2.345a12.6,12.6,0,0,0,6.683,1.9h0A12.049,12.049,0,0,0,79.263,64.656Zm-4.4,7.891a10.845,10.845,0,0,1-7.728,3.176,11.4,11.4,0,0,1-6.287-1.892.615.615,0,0,0-.578-.052L56,75.588l1.64-4.434a.615.615,0,0,0-.067-.557,11.161,11.161,0,0,1,1.212-14.124A10.846,10.846,0,0,1,66.514,53.3a11.588,11.588,0,0,1,11.52,11.374A10.838,10.838,0,0,1,74.859,72.547Z"
                                                                                transform="translate(0 0)"></path>
                                                                        </g>
                                                                    </svg>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <a href="mailto:<?= $contact_info->email?>"
                                                            class="mailLink"><?= $contact_info->email?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="searchBtnWrap">
                            <a href="#!" class="searchBtn" data-bs-toggle="modal" data-bs-target="#SearchModal">
                                <span class="btnIcon">
                                    <svg viewBox="0 0 19.225 20.268">
                                        <path id="search_FILL0_wght200_GRAD0_opsz48"
                                            d="M25.9,27.818,22.763,23.6a8.251,8.251,0,0,1-2.8,1.649,9.513,9.513,0,0,1-3.2.563,8.877,8.877,0,0,1-6.5-2.654A8.777,8.777,0,0,1,7.6,16.68a9.064,9.064,0,0,1,9.13-9.13,8.877,8.877,0,0,1,6.5,2.654A8.777,8.777,0,0,1,25.9,16.68a9.328,9.328,0,0,1-.583,3.238,8.523,8.523,0,0,1-1.669,2.8l3.177,4.18Zm-9.13-3.255A7.689,7.689,0,0,0,22.4,22.29a7.588,7.588,0,0,0,2.292-5.611A7.829,7.829,0,0,0,16.77,8.757,7.829,7.829,0,0,0,8.847,16.68a7.588,7.588,0,0,0,2.292,5.611A7.689,7.689,0,0,0,16.77,24.563Z"
                                            transform="translate(-7.6 -7.55)" fill="#fff" />
                                    </svg>
                                </span>
                            </a>
                        </div>
                        <div class="MenuIcon">
                            <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#DeskHeaderMenu">
                                <div class="IconWrp">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="btnCnt">Menu</div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </header>

    <div class="modal fade SearchModal" id="SearchModal" data-bs-backdrop="static" data-bs-keyboard="false"
        tabindex="-1" aria-labelledby="exampleModalLabel" aria-modal="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="container">
                    <div class="modal-header">
                        <button type="button" data-bs-dismiss="modal" aria-label="Close">
                            <svg viewBox="0 0 24 24">
                                <path fill="#3E4152" fillrule="evenodd"
                                    d="M20.25 11.25H5.555l6.977-6.976a.748.748 0 000-1.056.749.749 0 00-1.056 0L3.262 11.43A.745.745 0 003 12a.745.745 0 00.262.57l8.214 8.212a.75.75 0 001.056 0 .748.748 0 000-1.056L5.555 12.75H20.25a.75.75 0 000-1.5">
                                </path>
                            </svg>
                        </button>
                        <div class="searchBox">
                            <form action="" method="post">
                                <div class="form-group">
                                    <input required="" type="search" name="searchd" id="searchd" class="form-control"
                                        placeholder="What are you looking for ?">
                                    <button type="submit" class="sendBtn">
                                        <svg viewBox="0 0 512 512">
                                            <path d="M225.5,0C101.2,0,0,101.2,0,225.5c0,124.3,101.2,225.5,225.5,225.5c124.3,0,225.5-101.1,225.5-225.5
                                            C450.9,101.2,349.8,0,225.5,0z M225.5,409.3c-101.4,0-183.8-82.5-183.8-183.8S124.1,41.6,225.5,41.6s183.8,82.5,183.8,183.8
                                            S326.8,409.3,225.5,409.3z" />
                                            <path d="M466.9,437.4l-80.3-80.3c-8.1-8.1-21.3-8.1-29.4,0c-8.1,8.1-8.1,21.3,0,29.4l80.3,80.3c4.1,4.1,9.4,6.1,14.7,6.1
                                            c5.3,0,10.6-2,14.7-6.1C475,458.7,475,445.6,466.9,437.4z" />
                                        </svg>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-body">


                        <ul class="results">

                            <!--test-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="mobFixedRit">
        <ul>
            <li class="enquiriesItem">
                <a href="tel:<?=$contact_info->sidemenu_get_in_touch_phone_number?>" class="floatBx enquiries">
                    <div class="bxIcon">
                    <svg viewBox="0 0 43.789 48.776">
                                        <g id="call" transform="translate(-13.42)">
                                            <path id="Path_62" data-name="Path 62"
                                                d="M238.722,0a13.1,13.1,0,0,0-12.95,12.923,12.949,12.949,0,0,0,4.57,10.039,8.561,8.561,0,0,1-.745,1.5,2.224,2.224,0,0,0,1.669,3.39.848.848,0,0,0,.166-1.687.529.529,0,0,1-.4-.8,10.265,10.265,0,0,0,1.111-2.435.848.848,0,0,0-.29-.918,11.275,11.275,0,0,1-4.388-9.07,11.349,11.349,0,0,1,22.7.106c0,9.529-7.737,12.185-12.35,12.919a.848.848,0,1,0,.267,1.674c8.309-1.324,13.778-6.223,13.778-14.594A13.04,13.04,0,0,0,238.722,0Z"
                                                transform="translate(-194.652)" fill="#fff" />
                                            <path id="Path_63" data-name="Path 63"
                                                d="M291.3,67h-.848a2.546,2.546,0,0,0-2.543,2.543V70.92h-1.377a2.546,2.546,0,0,0-2.543,2.543v.848a2.546,2.546,0,0,0,2.543,2.543h1.377v1.377a2.546,2.546,0,0,0,2.543,2.543h.848a2.546,2.546,0,0,0,2.543-2.543V76.854h1.377a2.546,2.546,0,0,0,2.543-2.543v-.848a2.546,2.546,0,0,0-2.543-2.543h-1.377V69.543A2.546,2.546,0,0,0,291.3,67Zm3.92,5.616a.849.849,0,0,1,.848.848v.848a.849.849,0,0,1-.848.848H293a.848.848,0,0,0-.848.848v2.225a.849.849,0,0,1-.848.848h-.848a.849.849,0,0,1-.848-.848V76.006a.848.848,0,0,0-.848-.848h-2.225a.849.849,0,0,1-.848-.848v-.848a.849.849,0,0,1,.848-.848h2.225a.848.848,0,0,0,.848-.848V69.543a.849.849,0,0,1,.848-.848h.848a.849.849,0,0,1,.848.848v2.225a.848.848,0,0,0,.848.848Z"
                                                transform="translate(-246.704 -59.901)" fill="#fff" />
                                            <path id="Path_64" data-name="Path 64"
                                                d="M43.63,132.685a4.053,4.053,0,0,0-4.864.667,4.882,4.882,0,0,1-6.27,1.9c-4.5-1.678-8.489-7.735-5.29-11.27a8.209,8.209,0,0,1,1.171-1.007,4.059,4.059,0,0,0,.674-4.871A16.818,16.818,0,0,0,22.8,111.86a4.093,4.093,0,0,0-4.854.67c-10.245,9.94-1.549,26.4,10.6,33,.168.039,1.253.822,1.965.142a.848.848,0,0,0-.341-1.208c-11.694-5.82-20.53-21.513-11.041-30.719a2.39,2.39,0,0,1,2.831-.415,15.121,15.121,0,0,1,5.619,5.617,2.358,2.358,0,0,1-.351,2.78c-.165.116-.323.236-.474.36A26.932,26.932,0,0,0,21.08,115.8a.848.848,0,0,0-1.065,1.319,25.254,25.254,0,0,1,5.533,6.227c-5.326,7.241,8.47,19.7,14.46,11.162a2.353,2.353,0,0,1,2.78-.352,15.124,15.124,0,0,1,5.616,5.619,2.391,2.391,0,0,1-.414,2.831q-.186.192-.375.372a27.125,27.125,0,0,0-5.042-5.8.848.848,0,0,0-1.11,1.282,25.425,25.425,0,0,1,4.847,5.624,13.448,13.448,0,0,1-11.159,2.16A.848.848,0,1,0,34.8,147.9a14.821,14.821,0,0,0,14.411-4.116,4.093,4.093,0,0,0,.67-4.855,16.818,16.818,0,0,0-6.247-6.249Z"
                                                transform="translate(0 -99.548)" fill="#fff" />
                                        </g>
                                    </svg>
                    </div>
                    <div class="bxCnt">
                        <span>Get in touch</span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    <div class="fixedRit">
        <ul>
            <li class="enquiriesItem">
                <a href="tel:<?=$contact_info->sidemenu_get_in_touch_phone_number?>" class="floatBx enquiries">
                    <div class="bxIcon">
                    <svg viewBox="0 0 43.789 48.776">
                                        <g id="call" transform="translate(-13.42)">
                                            <path id="Path_62" data-name="Path 62"
                                                d="M238.722,0a13.1,13.1,0,0,0-12.95,12.923,12.949,12.949,0,0,0,4.57,10.039,8.561,8.561,0,0,1-.745,1.5,2.224,2.224,0,0,0,1.669,3.39.848.848,0,0,0,.166-1.687.529.529,0,0,1-.4-.8,10.265,10.265,0,0,0,1.111-2.435.848.848,0,0,0-.29-.918,11.275,11.275,0,0,1-4.388-9.07,11.349,11.349,0,0,1,22.7.106c0,9.529-7.737,12.185-12.35,12.919a.848.848,0,1,0,.267,1.674c8.309-1.324,13.778-6.223,13.778-14.594A13.04,13.04,0,0,0,238.722,0Z"
                                                transform="translate(-194.652)" fill="#fff" />
                                            <path id="Path_63" data-name="Path 63"
                                                d="M291.3,67h-.848a2.546,2.546,0,0,0-2.543,2.543V70.92h-1.377a2.546,2.546,0,0,0-2.543,2.543v.848a2.546,2.546,0,0,0,2.543,2.543h1.377v1.377a2.546,2.546,0,0,0,2.543,2.543h.848a2.546,2.546,0,0,0,2.543-2.543V76.854h1.377a2.546,2.546,0,0,0,2.543-2.543v-.848a2.546,2.546,0,0,0-2.543-2.543h-1.377V69.543A2.546,2.546,0,0,0,291.3,67Zm3.92,5.616a.849.849,0,0,1,.848.848v.848a.849.849,0,0,1-.848.848H293a.848.848,0,0,0-.848.848v2.225a.849.849,0,0,1-.848.848h-.848a.849.849,0,0,1-.848-.848V76.006a.848.848,0,0,0-.848-.848h-2.225a.849.849,0,0,1-.848-.848v-.848a.849.849,0,0,1,.848-.848h2.225a.848.848,0,0,0,.848-.848V69.543a.849.849,0,0,1,.848-.848h.848a.849.849,0,0,1,.848.848v2.225a.848.848,0,0,0,.848.848Z"
                                                transform="translate(-246.704 -59.901)" fill="#fff" />
                                            <path id="Path_64" data-name="Path 64"
                                                d="M43.63,132.685a4.053,4.053,0,0,0-4.864.667,4.882,4.882,0,0,1-6.27,1.9c-4.5-1.678-8.489-7.735-5.29-11.27a8.209,8.209,0,0,1,1.171-1.007,4.059,4.059,0,0,0,.674-4.871A16.818,16.818,0,0,0,22.8,111.86a4.093,4.093,0,0,0-4.854.67c-10.245,9.94-1.549,26.4,10.6,33,.168.039,1.253.822,1.965.142a.848.848,0,0,0-.341-1.208c-11.694-5.82-20.53-21.513-11.041-30.719a2.39,2.39,0,0,1,2.831-.415,15.121,15.121,0,0,1,5.619,5.617,2.358,2.358,0,0,1-.351,2.78c-.165.116-.323.236-.474.36A26.932,26.932,0,0,0,21.08,115.8a.848.848,0,0,0-1.065,1.319,25.254,25.254,0,0,1,5.533,6.227c-5.326,7.241,8.47,19.7,14.46,11.162a2.353,2.353,0,0,1,2.78-.352,15.124,15.124,0,0,1,5.616,5.619,2.391,2.391,0,0,1-.414,2.831q-.186.192-.375.372a27.125,27.125,0,0,0-5.042-5.8.848.848,0,0,0-1.11,1.282,25.425,25.425,0,0,1,4.847,5.624,13.448,13.448,0,0,1-11.159,2.16A.848.848,0,1,0,34.8,147.9a14.821,14.821,0,0,0,14.411-4.116,4.093,4.093,0,0,0,.67-4.855,16.818,16.818,0,0,0-6.247-6.249Z"
                                                transform="translate(0 -99.548)" fill="#fff" />
                                        </g>
                                    </svg>
                    </div>
                    <div class="bxCnt">
                        <span>Get in touch</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['site/doctors']); ?>" class="floatBx doctor">
                    <div class="bxIcon">
                        <svg viewBox="0 0 74 74" id ="doctor" width="512" height="512"><path d="M36.975,41.077c-9.019-.294-11.182-8.016-11.645-10.509a3.915,3.915,0,0,1-2.193-1.393,4.407,4.407,0,0,1-.821-3,4.138,4.138,0,0,1,.976-3.217,3.026,3.026,0,0,1,.729-.55,15.905,15.905,0,0,1-2.37-9.034c1.044-7.46,7.056-8.186,8.939-8.228a8.815,8.815,0,0,1,3.169-2.337,11.168,11.168,0,0,1,7.179-.4c4.662,1.217,7.771,3.551,9.246,6.938,2.1,4.814.258,10.355-.8,12.843a3.063,3.063,0,0,1,1.092.627,4.011,4.011,0,0,1,1.116,3.361,4.4,4.4,0,0,1-.82,3,3.921,3.921,0,0,1-2.194,1.393C48.115,33.061,45.957,40.783,36.975,41.077ZM25.692,24.007a1.353,1.353,0,0,0-.966.344,2.383,2.383,0,0,0-.409,1.789,1.027,1.027,0,0,1-.007.2,2.469,2.469,0,0,0,.419,1.625,2.283,2.283,0,0,0,1.6.758,1,1,0,0,1,.879.908c.033.372.92,9.157,9.769,9.447,8.8-.29,9.691-9.075,9.724-9.449a1,1,0,0,1,.879-.906,2.281,2.281,0,0,0,1.6-.758,2.474,2.474,0,0,0,.419-1.633,1.047,1.047,0,0,1-.007-.194,2.383,2.383,0,0,0-.451-1.836,1.517,1.517,0,0,0-1.086-.284A1,1,0,0,1,47,22.559c.037-.07,3.7-7.022,1.346-12.425-1.219-2.792-3.88-4.741-7.911-5.793a9.159,9.159,0,0,0-5.873.3,6.725,6.725,0,0,0-2.694,2.108.991.991,0,0,1-.887.4c-.259-.021-6.382-.417-7.342,6.428h0s-.277,5.445,2.877,8.745a1,1,0,0,1,.19,1.1.947.947,0,0,1-.947.591Z"/><path d="M47.881,24.031a1,1,0,0,1-1-.958c-.012-.259-.322-6.3-4.832-6.881a15.226,15.226,0,0,1-11.613-3.364c-1.275.959-4.213,3.874-3.648,10.091A1,1,0,0,1,24.8,23.1c-.829-9.121,5.028-12.287,5.277-12.417a1,1,0,0,1,1.154.164A13.355,13.355,0,0,0,41.857,14.2a1.118,1.118,0,0,1,.277-.01c5.084.529,6.637,5.991,6.747,8.8a1,1,0,0,1-.958,1.038Z"/><path d="M65.089,72H8.911a1,1,0,0,1-1-1V59.852a13.308,13.308,0,0,1,13.3-13.281h1.92a1,1,0,1,1,0,2h-1.92a11.306,11.306,0,0,0-11.3,11.281V70H64.089V59.852A11.294,11.294,0,0,0,52.808,48.571H50.75a1,1,0,0,1,0-2h2.058A13.3,13.3,0,0,1,66.089,59.852V71A1,1,0,0,1,65.089,72Z"/><path d="M40.342,72a1,1,0,0,1-.989-1.149L43.4,44.068a1,1,0,0,1,1.277-.808l6.984,2.105a1,1,0,0,1,.608,1.4l-2.776,5.57,1.61,1.807a1,1,0,0,1,.1,1.191L41.192,71.526A1,1,0,0,1,40.342,72Zm4.864-26.492L42.061,66.321l7.053-11.4-1.563-1.755a1,1,0,0,1-.149-1.111l2.548-5.114Z"/><path d="M33.543,72a1,1,0,0,1-.851-.474L22.678,55.336a1,1,0,0,1,.1-1.191l1.61-1.807-2.776-5.57a1,1,0,0,1,.608-1.4l6.984-2.105a1,1,0,0,1,1.276.808l4.049,26.783A1,1,0,0,1,33.543,72ZM24.771,54.918l7.053,11.4L28.679,45.508l-4.744,1.43,2.547,5.114a1,1,0,0,1-.148,1.111Z"/><path d="M37.017,53.355a54.215,54.215,0,0,1-6.469-.392,1,1,0,1,1,.238-1.985,50.417,50.417,0,0,0,12.307,0,1,1,0,0,1,.25,1.984A50.5,50.5,0,0,1,37.017,53.355Z"/><path d="M44.391,45.217a1,1,0,0,1-.8-.405c-1.486-2.009-1.755-5.991-1.78-6.438a1,1,0,0,1,.94-1.056,1.011,1.011,0,0,1,1.056.94c.061,1.052.431,4.067,1.391,5.365a1,1,0,0,1-.8,1.594Z"/><path d="M29.494,45.217a1,1,0,0,1-.8-1.594c.962-1.3,1.331-4.314,1.392-5.365a1,1,0,1,1,2,.116c-.025.447-.3,4.429-1.78,6.438A1,1,0,0,1,29.494,45.217Z"/><path d="M57.155,57.984a1,1,0,0,1-1-.971c-.187-6.512-2.663-8.561-2.687-8.581a1.007,1.007,0,0,1-.174-1.4.994.994,0,0,1,1.385-.192c.132.1,3.257,2.527,3.476,10.115a1,1,0,0,1-.97,1.028Z"/><path d="M57.16,63.888a3.953,3.953,0,1,1,3.952-3.953A3.957,3.957,0,0,1,57.16,63.888Zm0-5.906a1.953,1.953,0,1,0,1.952,1.953A1.955,1.955,0,0,0,57.16,57.982Z"/><path d="M16.082,57.984a1,1,0,0,1-1-.929c-.545-7.675,3.684-10.128,3.864-10.229a1,1,0,0,1,.983,1.742c-.152.09-3.3,2.033-2.853,8.346a1,1,0,0,1-.927,1.068Z"/><path d="M14.059,65.991a1,1,0,0,1-.8-.394A6.251,6.251,0,0,1,12.2,63.449a6.012,6.012,0,0,1,3.929-7.492,5.6,5.6,0,0,1,4.3.532,6.418,6.418,0,0,1,3.12,6.612,1,1,0,0,1-1.973-.328,4.422,4.422,0,0,0-2.126-4.54,3.619,3.619,0,0,0-2.778-.353,4.014,4.014,0,0,0-2.548,5.036,4.267,4.267,0,0,0,.726,1.47,1,1,0,0,1-.795,1.605Z"/><path d="M22.576,63.939c-.039,0-.079,0-.119-.006L21.33,63.8a1,1,0,0,1,.236-1.987l1.127.134a1,1,0,0,1-.117,1.993Z"/><path d="M14.059,66a1,1,0,0,1-.348-1.938l1.2-.443a1,1,0,0,1,.7,1.875l-1.2.444A1,1,0,0,1,14.059,66Z"/></svg>
                    </div>
                    <div class="bxCnt">
                        <span>Find a Doctor</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="tel:<?= $contact_info->footer_conatct_number?>" class="floatBx emergency">
                    <div class="bxIcon">
                        <svg id="emergency" viewBox="0 0 100 100"><g><path d="m50.236 38.224c-6.624 0-12.013 5.389-12.013 12.013 0 1.381 1.119 2.5 2.5 2.5s2.5-1.119 2.5-2.5c0-3.867 3.146-7.013 7.013-7.013 1.381 0 2.5-1.119 2.5-2.5s-1.119-2.5-2.5-2.5z"/><path d="m73.438 75.625h-2.188v-25.389c0-11.587-9.427-21.013-21.014-21.013s-21.013 9.426-21.013 21.013v25.389h-2.661c-3.963 0-7.188 3.225-7.188 7.188s3.226 7.187 7.189 7.187h46.875c3.963 0 7.188-3.225 7.188-7.188s-3.226-7.187-7.188-7.187zm-39.214-25.389c0-8.829 7.184-16.013 16.013-16.013 8.83 0 16.014 7.184 16.014 16.013v25.389h-32.027zm39.214 34.764h-46.875c-1.206 0-2.188-.981-2.188-2.188s.981-2.188 2.188-2.188h5.161 37.026 4.688c1.206 0 2.188.981 2.188 2.188s-.982 2.188-2.188 2.188z"/><path d="m50 24.375c1.381 0 2.5-1.119 2.5-2.5v-9.375c0-1.381-1.119-2.5-2.5-2.5s-2.5 1.119-2.5 2.5v9.375c0 1.381 1.119 2.5 2.5 2.5z"/><path d="m69.888 32.612c.64 0 1.279-.244 1.768-.732l6.629-6.629c.977-.977.977-2.559 0-3.535s-2.559-.977-3.535 0l-6.629 6.629c-.977.977-.977 2.559 0 3.535.487.488 1.127.732 1.767.732z"/><path d="m28.345 31.88c.488.488 1.128.732 1.768.732s1.279-.244 1.768-.732c.977-.977.977-2.559 0-3.535l-6.629-6.629c-.977-.977-2.559-.977-3.535 0s-.977 2.559 0 3.535z"/><path d="m24.375 50c0-1.381-1.119-2.5-2.5-2.5h-9.375c-1.381 0-2.5 1.119-2.5 2.5s1.119 2.5 2.5 2.5h9.375c1.381 0 2.5-1.119 2.5-2.5z"/><path d="m87.5 47.5h-9.375c-1.381 0-2.5 1.119-2.5 2.5s1.119 2.5 2.5 2.5h9.375c1.381 0 2.5-1.119 2.5-2.5s-1.119-2.5-2.5-2.5z"/></g></svg>
                    </div>
                    <div class="bxCnt">
                        <span>24/7 Emergency</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['site/services', 'slug' => $service_excellence->canonical_name]) ?>" class="floatBx excellence">
                    <div class="bxIcon">
                        <svg viewBox="0 0 15.456 18.819" id="excellence"><g transform="translate(-5.623 -2.243)"><path class="a" d="M23.61,2.25l1.07,2.016,2.289.278-1.68,1.514L25.626,8.3,23.61,7.038,21.594,8.3l.336-2.24L20.25,4.544,22.6,4.266Z" transform="translate(-5.89 -0.003)"/><path class="a" d="M17.529,9.806l-1.3-.334a4.7,4.7,0,1,1-3.378-5.732l.335-1.3A6.041,6.041,0,0,0,7.64,12.79v8.272l4.032-2.688L15.7,21.062V12.8a6.023,6.023,0,0,0,1.826-3Zm-3.17,8.745-2.688-1.792L8.984,18.551V13.7a6,6,0,0,0,5.376,0Z" transform="translate(0)"/></g></svg>
                    </div>
                    <div class="bxCnt">
                        <span>Centre of Excellence</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['site/services', 'slug' => $service_other->canonical_name]) ?>"
                    class="floatBx specialities">
                    <div class="bxIcon">
                        <svg viewBox="0 0 32.448 32.447" id="specialities">
                            <path id="Path_171" data-name="Path 171"
                                d="M31.614,2.913a4.086,4.086,0,0,0-3.289-1.639h-.552V.958a.951.951,0,1,0-1.9,0V3.493a.951.951,0,1,0,1.9,0V3.176h.552A2.218,2.218,0,0,1,30.458,6l-2.523,8.831a5.771,5.771,0,0,1-11.1,0L14.315,6a2.218,2.218,0,0,1,2.133-2.827H17v.317a.951.951,0,1,0,1.9,0V.958a.951.951,0,1,0-1.9,0v.317h-.552a4.119,4.119,0,0,0-3.961,5.251l2.523,8.831a7.614,7.614,0,0,0,2.757,4.017,7.773,7.773,0,0,0,.9.583L21.436,26.5v.686a3.367,3.367,0,1,1-6.734,0V20.667a5.45,5.45,0,0,0-10.9,0v2.25a4.753,4.753,0,1,0,1.9,0v-2.25a3.549,3.549,0,0,1,7.1,0v6.519a5.268,5.268,0,1,0,10.536,0V26.5l2.772-6.544a7.767,7.767,0,0,0,.9-.583,7.614,7.614,0,0,0,2.757-4.017l2.523-8.831a4.086,4.086,0,0,0-.672-3.612ZM7.6,27.574a2.852,2.852,0,1,1-2.852-2.852A2.855,2.855,0,0,1,7.6,27.574Zm14.782-3.7-1.3-3.059a7.743,7.743,0,0,0,2.592,0Z"
                                transform="translate(0 -0.007)" />
                        </svg>
                    </div>
                    <div class="bxCnt">
                        <span>Other Specialities</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['site/packages']); ?>" class="floatBx packages">
                    <div class="bxIcon">
                        <svg viewBox="0 0 35.399 43.153" id="packages">
                          <g id="checkup" transform="translate(-46)">
                            <path id="Path_3194" data-name="Path 3194" d="M78.239,0H49.161A3.164,3.164,0,0,0,46,3.161V39.993a3.164,3.164,0,0,0,3.161,3.161H78.239A3.164,3.164,0,0,0,81.4,39.993V3.161A3.164,3.164,0,0,0,78.239,0ZM56.114,3.793H71.285V5.226a.633.633,0,0,1-.632.632H56.746a.633.633,0,0,1-.632-.632ZM71.285,2.529H56.114V1.264H71.285Zm8.85,37.464a1.9,1.9,0,0,1-1.9,1.9H49.161a1.9,1.9,0,0,1-1.9-1.9V3.161a1.9,1.9,0,0,1,1.9-1.9H54.85V5.226a1.9,1.9,0,0,0,1.9,1.9H70.653a1.9,1.9,0,0,0,1.9-1.9V1.264h5.689a1.9,1.9,0,0,1,1.9,1.9Z" transform="translate(0 0)" />
                            <path id="Path_3195" data-name="Path 3195" d="M105.71,30h-3.793a.632.632,0,1,0,0,1.264h3.161V52.925a3.164,3.164,0,0,1-3.161,3.161H94.964a.632.632,0,0,0-.632.632v6.953a3.164,3.164,0,0,1-3.161,3.161H77.264V31.264h3.161a.632.632,0,1,0,0-1.264H76.632a.632.632,0,0,0-.632.632V67.464a.632.632,0,0,0,.632.632H91.171a4.411,4.411,0,0,0,3.12-1.29s10.765-10.764,10.769-10.77a4.41,4.41,0,0,0,1.282-3.111V30.632A.632.632,0,0,0,105.71,30ZM95.6,57.35h6.367l-6.369,6.369c0-.015,0-.031,0-.046Z" transform="translate(-27.471 -27.471)" />
                            <path id="Path_3196" data-name="Path 3196" d="M144.132,113.407h4.425v4.425a.632.632,0,0,0,.632.632h7.586a.632.632,0,0,0,.632-.632v-4.425h4.425a.632.632,0,0,0,.632-.632v-7.586a.632.632,0,0,0-.632-.632h-4.425v-4.425a.632.632,0,0,0-.632-.632h-7.586a.632.632,0,0,0-.632.632v4.425h-4.425a.632.632,0,0,0-.632.632v7.586A.632.632,0,0,0,144.132,113.407Zm.632-7.586h4.425a.632.632,0,0,0,.632-.632v-4.425h6.321v4.425a.632.632,0,0,0,.632.632H161.2v6.321h-4.425a.632.632,0,0,0-.632.632V117.2h-6.321v-4.425a.632.632,0,0,0-.632-.632h-4.425Z" transform="translate(-89.282 -91.114)" />
                          </g>
                        </svg>
                    </div>
                    <div class="bxCnt">
                        <span>Health Packages</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['site/insurance']); ?>" class="floatBx insurance">
                    <div class="bxIcon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="insurance" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                        	<g>
                        		<path d="M495.984,252.588c-17.119-14.109-44.177-15.319-61.936,3.74l-44.087,47.327c-5.7-18.319-22.809-31.658-42.977-31.658    h-78.675c-5.97,0-7.969-2.28-18.339-10.269c-39.538-34.468-98.924-34.358-138.342,0.33l-28.918,25.458    c-12.999-6.88-28.178-7.05-41.248-0.52L8.294,303.575c-7.41,3.71-10.409,12.719-6.71,20.129l89.995,179.989    c3.71,7.41,12.719,10.409,20.129,6.71l33.168-16.589c16.349-8.169,25.448-24.849,24.858-41.827h177.249    c32.868,0,64.276-15.699,83.995-41.997l72.006-96.014C516.953,295.366,514.743,268.077,495.984,252.588z M131.456,466.985    l-19.749,9.879L35.122,323.704l19.759-9.879c7.41-3.7,16.409-0.71,20.119,6.71l63.166,126.332    C141.866,454.276,138.866,463.275,131.456,466.985z M478.985,295.976L406.98,391.99c-14.089,18.789-36.518,29.998-59.996,29.998    H159.265l-56.207-112.423l28.388-24.988c28.248-24.849,70.846-24.849,99.094,0c16.639,14.649,26.988,17.419,37.768,17.419h78.675    c8.27,0,14.999,6.73,14.999,14.999s-6.73,14.999-14.999,14.999h-76.605c-8.28,0-14.999,6.72-14.999,14.999    s6.72,14.999,14.999,14.999h86.655c12.449,0,24.449-5.22,32.928-14.329l66.036-70.886c6.04-6.48,15.299-5.94,20.979-0.97    C482.915,281.006,483.556,289.896,478.985,295.976z"/>
                        	</g>
                        </g>
                        <g>
                        	<g>
                        		<path d="M315.385,102.367c10.269-10.769,16.599-25.328,16.599-41.358c0-33.018-26.678-60.996-59.996-60.996    c-33.068,0-60.996,27.928-60.996,60.996c0,15.539,6.09,30.208,17.149,41.478c-27.428,15.379-47.147,44.897-47.147,79.515v14.999    c0,8.279,6.72,14.999,14.999,14.999h150.991c8.279,0,14.999-6.72,14.999-14.999v-14.999    C361.982,148.064,343.315,118.086,315.385,102.367z M271.988,30.012c16.259,0,29.998,14.199,29.998,30.998    c0,16.539-13.459,29.998-29.998,29.998c-16.799,0-30.998-13.739-30.998-29.998C240.99,44.501,255.479,30.012,271.988,30.012z     M210.992,182.002c0-33.068,27.928-60.996,60.996-60.996c33.078,0,59.996,27.358,59.996,60.996H210.992z"/>
                        	</g>
                        </g>
                        </svg>
                    </div>
                    <div class="bxCnt">
                        <span>Insurance</span>
                    </div>
                </a>
            </li>
        </ul>
    </div>



    <div id="viewport">
        <?= $content ?>


        <footer class="main-foot">
            <div class="container">
                <div class="topSec">
                    <div class="flxBox">
                        <div class="item">
                            <div class="bxWrap">
                                <div class="bxIcon">
                                    <svg viewBox="0 0 43.789 48.776">
                                        <g id="call" transform="translate(-13.42)">
                                            <path id="Path_62" data-name="Path 62"
                                                d="M238.722,0a13.1,13.1,0,0,0-12.95,12.923,12.949,12.949,0,0,0,4.57,10.039,8.561,8.561,0,0,1-.745,1.5,2.224,2.224,0,0,0,1.669,3.39.848.848,0,0,0,.166-1.687.529.529,0,0,1-.4-.8,10.265,10.265,0,0,0,1.111-2.435.848.848,0,0,0-.29-.918,11.275,11.275,0,0,1-4.388-9.07,11.349,11.349,0,0,1,22.7.106c0,9.529-7.737,12.185-12.35,12.919a.848.848,0,1,0,.267,1.674c8.309-1.324,13.778-6.223,13.778-14.594A13.04,13.04,0,0,0,238.722,0Z"
                                                transform="translate(-194.652)" fill="#fff" />
                                            <path id="Path_63" data-name="Path 63"
                                                d="M291.3,67h-.848a2.546,2.546,0,0,0-2.543,2.543V70.92h-1.377a2.546,2.546,0,0,0-2.543,2.543v.848a2.546,2.546,0,0,0,2.543,2.543h1.377v1.377a2.546,2.546,0,0,0,2.543,2.543h.848a2.546,2.546,0,0,0,2.543-2.543V76.854h1.377a2.546,2.546,0,0,0,2.543-2.543v-.848a2.546,2.546,0,0,0-2.543-2.543h-1.377V69.543A2.546,2.546,0,0,0,291.3,67Zm3.92,5.616a.849.849,0,0,1,.848.848v.848a.849.849,0,0,1-.848.848H293a.848.848,0,0,0-.848.848v2.225a.849.849,0,0,1-.848.848h-.848a.849.849,0,0,1-.848-.848V76.006a.848.848,0,0,0-.848-.848h-2.225a.849.849,0,0,1-.848-.848v-.848a.849.849,0,0,1,.848-.848h2.225a.848.848,0,0,0,.848-.848V69.543a.849.849,0,0,1,.848-.848h.848a.849.849,0,0,1,.848.848v2.225a.848.848,0,0,0,.848.848Z"
                                                transform="translate(-246.704 -59.901)" fill="#fff" />
                                            <path id="Path_64" data-name="Path 64"
                                                d="M43.63,132.685a4.053,4.053,0,0,0-4.864.667,4.882,4.882,0,0,1-6.27,1.9c-4.5-1.678-8.489-7.735-5.29-11.27a8.209,8.209,0,0,1,1.171-1.007,4.059,4.059,0,0,0,.674-4.871A16.818,16.818,0,0,0,22.8,111.86a4.093,4.093,0,0,0-4.854.67c-10.245,9.94-1.549,26.4,10.6,33,.168.039,1.253.822,1.965.142a.848.848,0,0,0-.341-1.208c-11.694-5.82-20.53-21.513-11.041-30.719a2.39,2.39,0,0,1,2.831-.415,15.121,15.121,0,0,1,5.619,5.617,2.358,2.358,0,0,1-.351,2.78c-.165.116-.323.236-.474.36A26.932,26.932,0,0,0,21.08,115.8a.848.848,0,0,0-1.065,1.319,25.254,25.254,0,0,1,5.533,6.227c-5.326,7.241,8.47,19.7,14.46,11.162a2.353,2.353,0,0,1,2.78-.352,15.124,15.124,0,0,1,5.616,5.619,2.391,2.391,0,0,1-.414,2.831q-.186.192-.375.372a27.125,27.125,0,0,0-5.042-5.8.848.848,0,0,0-1.11,1.282,25.425,25.425,0,0,1,4.847,5.624,13.448,13.448,0,0,1-11.159,2.16A.848.848,0,1,0,34.8,147.9a14.821,14.821,0,0,0,14.411-4.116,4.093,4.093,0,0,0,.67-4.855,16.818,16.818,0,0,0-6.247-6.249Z"
                                                transform="translate(0 -99.548)" fill="#fff" />
                                        </g>
                                    </svg>
                                </div>
                                <div class="bxCnt">
                                    <div class="bxTle">For Queries</div>
                                    <a href="tel:<?= $contact_info->footer_conatct_number?>"
                                        class="bxDtls">+91 7594046144</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="bxWrap">
                                <div class="bxIcon">
                                    <svg viewBox="0 0 49.909 47.516">
                                        <g id="stethoscope" transform="translate(0 -11.508)">
                                            <g id="Group_32" data-name="Group 32" transform="translate(0 11.508)">
                                                <g id="Group_31" data-name="Group 31" transform="translate(0 0)">
                                                    <path id="Path_65" data-name="Path 65"
                                                        d="M49.849,38.133A5.823,5.823,0,0,0,44.917,33.2V31.69a7.342,7.342,0,0,0-7.07-7.569,7.37,7.37,0,0,0-7.07,7.343h.048l-.048.008V47.864c0,5.24-3.918,9.5-8.734,9.5s-8.734-4.261-8.734-9.5v-1.42c1.206,0,2.5-.874,2.5-3.327V41.025a11.8,11.8,0,0,0,9.15-11.146V28.145H23.291l-.035-10.826a4.163,4.163,0,0,0-3.422-4.072,2.482,2.482,0,1,0-.057,1.7,2.5,2.5,0,0,1,1.813,2.381l.037,10.821H19.963V29.9a6.615,6.615,0,0,1-4.788,6.206,7.564,7.564,0,0,1-4.575,0A6.611,6.611,0,0,1,5.823,29.9V28.145h-2.5V17.331a2.5,2.5,0,0,1,1.847-2.4,2.507,2.507,0,1,0-.052-1.689,4.159,4.159,0,0,0-3.459,4.088V28.145H0v1.73a11.8,11.8,0,0,0,9.15,11.15v2.092c0,2.453,1.289,3.327,2.5,3.327v1.42c0,6.155,4.664,11.159,10.4,11.159s10.4-5.01,10.4-11.159V31.512a5.692,5.692,0,0,1,5.407-5.725,5.679,5.679,0,0,1,5.407,5.906V33.2a5.823,5.823,0,1,0,6.595,4.931Zm-32.381-23.3A.832.832,0,1,1,18.3,14,.832.832,0,0,1,17.468,14.836ZM7.486,13.172A.832.832,0,1,1,6.654,14,.832.832,0,0,1,7.486,13.172Zm-5.823,16.7v-.067h2.5V29.9A8.265,8.265,0,0,0,10.1,37.691a9.256,9.256,0,0,0,5.588,0A8.266,8.266,0,0,0,21.627,29.9v-.095h1.664v.067c0,4.627-3.567,8.7-8.484,9.684a11.866,11.866,0,0,1-3.509.172,10.867,10.867,0,0,1-1.149-.174C5.23,38.574,1.664,34.5,1.664,29.875Zm9.982,14.906c-.749,0-.832-1.165-.832-1.664V41.352c.067.008.135.012.2.018.083.009.166.016.25.023.393.035.79.058,1.19.059H12.5c.4,0,.8-.024,1.186-.059.083-.007.171-.014.257-.023.066-.007.134-.01.2-.018v1.765c0,.5-.083,1.664-.832,1.664Zm32.44-1.664a4.159,4.159,0,1,1,4.159-4.159A4.159,4.159,0,0,1,44.086,43.117Z"
                                                        transform="translate(0 -11.508)" fill="#fff" />
                                                </g>
                                            </g>
                                            <g id="Group_34" data-name="Group 34" transform="translate(41.59 36.463)">
                                                <g id="Group_33" data-name="Group 33">
                                                    <path id="Path_66" data-name="Path 66"
                                                        d="M402.5,251.515a2.5,2.5,0,1,0,2.5,2.5A2.5,2.5,0,0,0,402.5,251.515Zm0,3.327a.832.832,0,1,1,.832-.832A.832.832,0,0,1,402.5,254.842Z"
                                                        transform="translate(-400 -251.515)" fill="#fff" />
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="bxCnt">
                                    <!-- <div class="bxTle">Apollo Adlux Hospital</div> -->
                                    <a href="<?= Url::to(['site/doctors']); ?>" class="bxDtls">Find a Doctor</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="bxWrap">
                                <div class="bxIcon">
                                    <svg viewBox="0 0 35.741 40.711">
                                        <path id="today_FILL0_wght200_GRAD0_opsz48"
                                            d="M19,33.975a4.432,4.432,0,0,1-3.295-1.368,4.506,4.506,0,0,1-1.34-3.267,4.432,4.432,0,0,1,1.368-3.295A4.506,4.506,0,0,1,19,24.7,4.432,4.432,0,0,1,22.3,26.073a4.506,4.506,0,0,1,1.34,3.267,4.432,4.432,0,0,1-1.368,3.295A4.506,4.506,0,0,1,19,33.975Zm-7.93,12.286A3.051,3.051,0,0,1,8,43.19v-30.1a3.051,3.051,0,0,1,3.071-3.071h4.635V5.55h2.01v4.468H34.191V5.55h1.9v4.468h4.579a3.051,3.051,0,0,1,3.071,3.071v30.1a3.051,3.051,0,0,1-3.071,3.071Zm0-1.731h29.6a1.441,1.441,0,0,0,1.34-1.34V22.806H9.731V43.19a1.441,1.441,0,0,0,1.34,1.34ZM9.731,21.075H42.01V13.089a1.441,1.441,0,0,0-1.34-1.34h-29.6a1.441,1.441,0,0,0-1.34,1.34Zm0,0v0Z"
                                            transform="translate(-8 -5.55)" fill="#fff" />
                                    </svg>
                                </div>
                                <div class="bxCnt">
                                    <!-- <div class="bxTle">Apollo Adlux Hospital</div> -->
                                    <a href="<?= Url::to(['site/schedule']); ?>" class="bxDtls">Book an Appointment</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="bxWrap">
                                <div class="bxIcon">
                                    <svg viewBox="0 0 32 40.857">
                                        <path id="health_and_safety_FILL0_wght200_GRAD0_opsz48"
                                            d="M24.229,32.893h3.543V27.179h5.714V23.636H27.771V17.921H24.229v5.714H18.514v3.543h5.714ZM26,47.007a21.326,21.326,0,0,1-11.457-8.429A23.979,23.979,0,0,1,10,24.321V12.093L26,6.15l16,5.943V24.321a23.979,23.979,0,0,1-4.543,14.257A21.326,21.326,0,0,1,26,47.007Zm0-1.886a19.082,19.082,0,0,0,10.229-7.8,22.543,22.543,0,0,0,4-13V13.35L26,7.979,11.771,13.35V24.321a22.542,22.542,0,0,0,4,13A19.082,19.082,0,0,0,26,45.122ZM26,26.607Z"
                                            transform="translate(-10 -6.15)" fill="#fff" />
                                    </svg>
                                </div>
                                <div class="bxCnt">
                                    <!-- <div class="bxTle">Apollo Adlux Hospital</div> -->
                                    <a href="<?= Url::to(['site/packages']); ?>" class="bxDtls">Health Packages</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="midSec">
                    <div class="Flx">
                        <div class="Col_">
                            <h3><?= $contact_info->footer_caption?></h3>
                        </div>
                        <div class="Col_">

                            <ul class="footUl">
                                <li><a href="<?= Url::to(['site/index']); ?>">Home</a></li>
                                <li><a href=<?= Url::to(['site/about']); ?>>About Us</a></li>
                                <li><a href="<?= Url::to(['site/medical']); ?>">Health Blogs</a></li>
                                <li><a href="<?= Url::to(['site/career']); ?>">Careers</a></li>
                                <li><a href="<?= Url::to(['site/cme']); ?>">CME</a></li>

                                <li><a href="<?= Url::to(['site/patient-testimonial']); ?>">Patient Testimonial</a></li>

                            </ul>
                        </div>
                        <div class="Col_">
                            <ul class="footUl">
                                <li><a href="<?= Url::to(['site/packages']); ?>">Health Packages</a></li>
                                <!-- <li><a href="<?= Url::to(['site/homecare']); ?>">Homecare</a></li> -->
                                <li><a href="<?= Url::to(['site/international']); ?>">International Patient</a></li>
                                <li><a href="<?= Url::to(['site/insurance']); ?>">Insurance & TPA</a></li>
                                <li><a href="<?= Url::to(['site/contact']); ?>">Contact Us</a></li>
                                <li><a href="<?= Url::to(['site/feedback']); ?>">Feedback</a></li>
                                <li><a href="<?= Url::to(['site/brochure']); ?>">Brochure</a></li>

                            </ul>
                        </div>
                        <div class="Col_">
                            <ul class="footUl">
                                <li><a href="<?= Url::to(['site/new-doctors']); ?>">Newly Joined Doctors</a></li>
                                <li><a href="<?= Url::to(['site/doctors']); ?>">Find a Doctor</a></li>
                                <li><a href="<?= Url::to(['site/media-gallery']); ?>">Gallery</a></li>
                                <li><a href="#!">Virtual Tour</a></li>
                                <li><a href="<?= Url::to(['site/privacy']); ?>">Privacy Policy</a></li>
                                

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="btmSec">
                    <div class="Flx">
                        <div class="itemWrap">
                            <div class="bxWrap">
                                <h5 class="bxTle">Stay Up To Date</h5>
                                <form action="" class="subsForm">
                                    <input type="text" placeholder="Enter email address" id="nemail" name="nemail"
                                        required>
                                    <button type="submit" class="subsBtn">
                                        <svg viewBox="0 0 31.219 31.219">
                                            <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                                d="M24.055,39.594,39.636,24.013,24.055,8.375l-.952.952L37.17,23.34H8.417v1.289H37.17L23.1,38.7Z"
                                                transform="translate(-8.417 -8.375)" />
                                        </svg>
                                    </button>

                                </form>
                                <div class="newsmsg"></div>
                            </div>
                        </div>
                        <div class="itemWrap">
                            <div class="bxWrap">
                                <h5 class="bxTle"><?= $contact_info->footer_get_title?></h5>
                                <?= $contact_info->conatct_number?>
                            </div>
                        </div>
                        <div class="itemWrap">
                            <div class="bxWrap">
                                <a href="mailto:<?= $contact_info->footer_email?>"
                                    class="mailLink"><?= $contact_info->footer_email?></a>
                                <ul class="social">
                                    <li>
                                        <a href="<?= $contact_info->facebook_link?>" target="_blank">
                                            <svg viewBox="0 0 22 22">
                                                <defs>
                                                    <clipPath id="clip-path">
                                                        <rect id="Rectangle_93" data-name="Rectangle 93" width="22"
                                                            height="22" transform="translate(-0.392 -0.334)"
                                                            fill="#fff" />
                                                    </clipPath>
                                                </defs>
                                                <g id="Group_179" data-name="Group 179"
                                                    transform="translate(0.392 0.334)" clip-path="url(#clip-path)">
                                                    <path id="Path_142" data-name="Path 142"
                                                        d="M19.206,0H2.837A2.837,2.837,0,0,0,0,2.837V19.21a2.836,2.836,0,0,0,2.837,2.833h8.938V13.519H8.912V10.182h2.863V7.728a4.008,4.008,0,0,1,4.28-4.4,24.2,24.2,0,0,1,2.566.129V6.436H16.869c-1.382,0-1.649.659-1.649,1.619v2.127h3.307L18.1,13.519H15.22v8.525h3.987a2.837,2.837,0,0,0,2.837-2.837V2.837A2.837,2.837,0,0,0,19.206,0"
                                                        transform="translate(-0.439 -0.355)" />
                                                </g>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $contact_info->twitter_linlk?>" target="_blank">
                                            <svg viewBox="0 0 14.928 12.129">
                                                <g id="twitter_1_" data-name="twitter (1)" transform="translate(0 -48)">
                                                    <g id="Group_11" data-name="Group 11" transform="translate(0 48)">
                                                        <path id="Path_6" data-name="Path 6"
                                                            d="M14.928,49.436a6.381,6.381,0,0,1-1.763.483,3.043,3.043,0,0,0,1.346-1.692,6.116,6.116,0,0,1-1.941.741,3.06,3.06,0,0,0-5.294,2.093,3.151,3.151,0,0,0,.071.7,8.663,8.663,0,0,1-6.308-3.2,3.061,3.061,0,0,0,.94,4.09A3.022,3.022,0,0,1,.6,52.271v.034a3.075,3.075,0,0,0,2.452,3.007,3.055,3.055,0,0,1-.8.1,2.706,2.706,0,0,1-.579-.052,3.09,3.09,0,0,0,2.86,2.132A6.149,6.149,0,0,1,.732,58.8,5.733,5.733,0,0,1,0,58.756a8.616,8.616,0,0,0,4.7,1.373,8.651,8.651,0,0,0,8.711-8.709c0-.135,0-.266-.011-.4A6.105,6.105,0,0,0,14.928,49.436Z"
                                                            transform="translate(0 -48)"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $contact_info->instagram_link?>" target="_blank">
                                            <svg viewBox="0 0 510.9 511">
                                                <path d="M509.6,150.2c-1.2-27.2-5.6-45.8-11.9-62c-6.5-17.2-16.5-32.5-29.5-45.3c-12.8-13-28.3-23.1-45.2-29.4
                                        c-16.3-6.3-34.8-10.7-62-11.9C333.6,0.3,324.9,0,255.5,0s-78.1,0.3-105.3,1.5c-27.2,1.2-45.8,5.6-62,11.9
                                        c-17.2,6.5-32.5,16.5-45.3,29.5c-13,12.8-23.1,28.3-29.4,45.2c-6.3,16.3-10.7,34.8-11.9,62C0.3,177.5,0,186.2,0,255.5
                                        s0.3,78.1,1.5,105.3c1.2,27.2,5.6,45.8,11.9,62c6.5,17.2,16.6,32.5,29.5,45.3c12.8,13,28.3,23.1,45.2,29.4
                                        c16.3,6.3,34.8,10.7,62,11.9c27.2,1.2,35.9,1.5,105.3,1.5s78.1-0.3,105.3-1.5c27.2-1.2,45.8-5.6,62-11.9
                                        c34.3-13.3,61.5-40.4,74.8-74.8c6.3-16.3,10.7-34.8,11.9-62c1.2-27.3,1.5-35.9,1.5-105.3S510.8,177.5,509.6,150.2z M463.6,358.9
                                        c-1.1,25-5.3,38.4-8.8,47.4c-8.6,22.3-26.3,39.9-48.5,48.5c-9,3.5-22.6,7.7-47.4,8.8c-27,1.2-35,1.5-103.2,1.5s-76.4-0.3-103.2-1.5
                                        c-25-1.1-38.4-5.3-47.4-8.8c-11.1-4.1-21.2-10.6-29.3-19.1c-8.5-8.3-15-18.3-19.1-29.3c-3.5-9-7.7-22.6-8.8-47.4
                                        c-1.2-27-1.5-35-1.5-103.2s0.3-76.4,1.5-103.2c1.1-25,5.3-38.4,8.8-47.4C60.7,94,67.2,84,75.8,75.8c8.3-8.5,18.3-15,29.3-19.1
                                        c9-3.5,22.6-7.7,47.4-8.8c27-1.2,35-1.5,103.2-1.5c68.3,0,76.4,0.3,103.2,1.5c25,1.1,38.4,5.3,47.4,8.8
                                        c11.1,4.1,21.2,10.6,29.3,19.1c8.5,8.3,15,18.3,19.1,29.4c3.5,9,7.7,22.6,8.8,47.4c1.2,27,1.5,35,1.5,103.2S464.8,331.9,463.6,358.9
                                        z"></path>
                                                <path
                                                    d="M255.5,124.3c-72.5,0-131.3,58.8-131.3,131.3s58.8,131.3,131.3,131.3c72.5,0,131.3-58.8,131.3-131.3S328,124.3,255.5,124.3z
                                        M255.5,340.7c-47,0-85.2-38.1-85.2-85.2s38.1-85.2,85.2-85.2c47,0,85.2,38.1,85.2,85.2S302.6,340.7,255.5,340.7z">
                                                </path>
                                                <path d="M422.7,119.1c0,16.9-13.7,30.6-30.6,30.6c-16.9,0-30.6-13.7-30.6-30.6c0-16.9,13.7-30.6,30.6-30.6
                                        C408.9,88.4,422.7,102.2,422.7,119.1z"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $contact_info->linkedin_link?>" target="_blank">
                                            <svg viewBox="0 0 21.266 20.327">
                                                <defs>
                                                    <clipPath id="clip-path">
                                                        <rect id="Rectangle_91" data-name="Rectangle 91" width="21.266"
                                                            height="20.327" />
                                                    </clipPath>
                                                </defs>
                                                <g id="Group_175" data-name="Group 175" transform="translate(0)">
                                                    <g id="Group_174" data-name="Group 174" clip-path="url(#clip-path)">
                                                        <path id="Path_139" data-name="Path 139"
                                                            d="M2.579,0a2.376,2.376,0,1,0-.06,4.739h.03A2.377,2.377,0,1,0,2.579,0" />
                                                        <rect id="Rectangle_90" data-name="Rectangle 90" width="4.559"
                                                            height="13.715" transform="translate(0.269 6.612)" />
                                                        <path id="Path_140" data-name="Path 140"
                                                            d="M229.666,189.108a5.711,5.711,0,0,0-4.108,2.311V189.43H221v13.715h4.559v-7.659a3.124,3.124,0,0,1,.15-1.113,2.5,2.5,0,0,1,2.339-1.667c1.65,0,2.309,1.258,2.309,3.1v7.337h4.558v-7.864c0-4.213-2.249-6.173-5.248-6.173"
                                                            transform="translate(-213.648 -182.818)" />
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://api.whatsapp.com/send?phone=<?= $contact_info->whatsap_number?>"
                                            target="_blank">
                                            <svg viewBox="0 0 24.98 25.298">
                                                <g id="whatsapp" transform="translate(-54.283 -52.069)">
                                                    <path id="Path_145" data-name="Path 145"
                                                        d="M153.036,140.811c-.126-.071-.269-.151-.44-.255-.1-.058-.216-.137-.344-.221a3.415,3.415,0,0,0-1.576-.735.77.77,0,0,0-.278.05,2.689,2.689,0,0,0-.969.893,4.751,4.751,0,0,1-.356.415,7,7,0,0,1-3.4-3.4,4.72,4.72,0,0,1,.417-.358,2.694,2.694,0,0,0,.891-.966c.183-.475-.152-1.043-.685-1.855-.084-.128-.163-.248-.221-.344-.1-.173-.184-.315-.255-.441-.291-.516-.483-.858-1.2-.858a2.83,2.83,0,0,0-1.579.881,3,3,0,0,0-1.011,2.075,9.128,9.128,0,0,0,2.914,5.987l.01.007a9.116,9.116,0,0,0,5.988,2.909h0a2.99,2.99,0,0,0,2.074-1.006,2.819,2.819,0,0,0,.882-1.573C153.894,141.294,153.553,141.1,153.036,140.811Z"
                                                        transform="translate(-80.912 -74.303)" />
                                                    <path id="Path_146" data-name="Path 146"
                                                        d="M79.263,64.656A12.817,12.817,0,0,0,66.514,52.069,12.066,12.066,0,0,0,54.421,63.2a12.687,12.687,0,0,0,1.96,7.815l-2.1,6.347h.071l6.093-2.345a12.6,12.6,0,0,0,6.683,1.9h0A12.049,12.049,0,0,0,79.263,64.656Zm-4.4,7.891a10.845,10.845,0,0,1-7.728,3.176,11.4,11.4,0,0,1-6.287-1.892.615.615,0,0,0-.578-.052L56,75.588l1.64-4.434a.615.615,0,0,0-.067-.557,11.161,11.161,0,0,1,1.212-14.124A10.846,10.846,0,0,1,66.514,53.3a11.588,11.588,0,0,1,11.52,11.374A10.838,10.838,0,0,1,74.859,72.547Z"
                                                        transform="translate(0 0)" />
                                                </g>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ftBottomWrap">
                <div class="container">
                    <div class="footBttmSec">
                        <div class="cRightSec">
                            <p class="copyright">Copyright © 2022 Apollo Adlux Hospital. All Rights Reserved.</p>
                        </div>
                        <div class="cLftSec">
                            <p class="designed">Designed By: <a href="https://www.intersmartsolution.com" target="_blank">Intersmart</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <a href="#" class="scrollup">
            <svg viewBox="0 0 10 37">
                <path
                    d="M5.478,0.179 L9.802,4.415 C10.066,4.674 10.066,5.093 9.802,5.351 C9.538,5.610 9.110,5.610 8.847,5.351 L5.676,2.245 L5.676,36.337 C5.676,36.702 5.373,36.998 5.000,36.998 C4.627,36.998 4.324,36.702 4.324,36.337 L4.324,2.245 L1.153,5.351 C0.889,5.610 0.462,5.610 0.198,5.351 C0.066,5.222 -0.000,5.052 -0.000,4.883 C-0.000,4.714 0.066,4.545 0.198,4.415 L4.522,0.179 C4.786,-0.080 5.214,-0.080 5.478,0.179 Z">
                </path>
            </svg>
            Top
        </a>

        <!-- AOS --->





        <script>
        $(document).ready(function() {

            $(document).on('click', '.sendBtn', function(e) {



                e.preventDefault();
                // var str = $(this).serialize();
                var keyword = $('#searchd').val();





                $.ajax({
                    type: "POST",
                    url: '<?= Yii::$app->homeUrl; ?>site/get-search-departments',
                    data: {
                        keyword: keyword,

                    },
                    dataType: 'json',
                    success: function(data) {
                        $('.results').empty();

                        var departments = data.departments


                        $.each(departments, function(key, value) {



                            $('.results').append('<li><a href="' + value
                                .service_link +
                                '" class="flxBx"><div class="imgBx"><img src="' +
                                value.p_image +
                                '" alt="" width="50px"></div> <div class="txtBx"><div class="name">' +
                                value.service_name + '</div></div></a></li>');
                        });
                        //$('.mainT').append('Search Result For  '+data.search);
                        // $('.suggestions').append(data.count+' Result Found.');
                    }
                });

            });





            $(document).on('submit', '.subsForm', function(e) {


                e.preventDefault();
                var str = $(this).serialize();
                $.ajax({
                    type: "POST",
                    url: '<?= Yii::$app->homeUrl; ?>ajax/news-letter',
                    data: str,
                    success: function(data) {

                        if (data == 1) {


                            $('.newsmsg').html(
                                '<div id="email-alert" style="color: #fff;background: #4b2163;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'
                            );
                        }

                        $('#nemail').val("");

                        setTimeout(function() {
                            $('#email-alert').remove();
                        }, 4000);
                    }
                });
            });


            

        });
        </script>




        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
        AOS.init({
            duration: 600,
        });
        </script>

        <!-- JARALLAX --->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jarallax/2.0.3/jarallax.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/2.0.3/jarallax.min.js"></script>

        <script async type="text/javascript" src="<?= Yii::$app->homeUrl ?>assets/js/app.js"></script>

        <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>