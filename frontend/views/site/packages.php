<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="packagesPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">


        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg jarallax-img" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>assets/images/offersPgBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/offersPgBnr.jpg" alt="bannerImage" width="1920"
            height="590">


        <?php
    }
    ?>
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="packagesSec" class="inrCntSec">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <div class="secTitleWrap center" data-aos="fade-up">
                        <h2 class="secTitle"><?= $common_content->package_title?></h2>
                        <h5 class="secSubTitle"><?= $common_content->package_description?></h5>
                    </div>
                </div>

                <?php
                if(!empty($packages))
                {
                    foreach($packages as $package)
                    {
                ?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-aos="fade-up" data-aos-delay="300">
                    <div class="pckgCard">
                        <div class="cImg">
                            <img class="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/packages/image<?= $package->id ?>.<?= $package->image ?>"
                                data-src="<?= Yii::$app->homeUrl ?>images/packages/image<?= $package->id ?>.<?= $package->image ?>"
                                loading="lazy" alt="package" width="340" height="250">
                        </div>
                        <div class="cCnt">
                            <!--<h5 class="dName"><?= $package->title?></h5>-->
                            <p class="dDisc lineControl"><?= $package->small_description?></p>
                            <div class="cBttm">
                                <div class="lftBtn">
                                    <a href="#!" class="docBtn hoveranim"><span>&#x20B9;
                                            <?= $package->amount?>/-</span></a>
                                </div>
                                <div class="rgtBtn">
                                    <a href="<?= Url::to(['site/packagedetails', 'name' => $package->canonical_name]); ?>"
                                        class="bookBtn docBtn hoveranim"><span>Know More <svg
                                                viewBox="0 0 31.219 31.219">
                                                <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                                    d="M24.055,39.594,39.636,24.013,24.055,8.375l-.952.952L37.17,23.34H8.417v1.289H37.17L23.1,38.7Z"
                                                    transform="translate(-8.417 -8.375)"></path>
                                            </svg></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>

</div>