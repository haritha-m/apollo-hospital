<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="NewsDetailPage">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl?>images/news/banner-images/banner<?=$news->id?>.<?=$news->banner_image?>"
            data-src="<?= Yii::$app->homeUrl?>images/news/banner-images/banner<?=$news->id?>.<?=$news->banner_image?>" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?=$news->banner_title?></h1>
                        <h4 class="bnrSubHd"><?=$news->banner_description?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="news-detail">
        <div class="container">
            <div class="row">
                <div class="News-Lft col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="news-detail-img_box" data-aos="fade-up">
                        <img class="lazy" loading="lazy" width="945" height="490" data-src="<?= Yii::$app->homeUrl ?>images/news/data<?= $news->id ?>.<?= $news->image1 ?>" src="<?= Yii::$app->homeUrl ?>images/news/data<?= $news->id ?>.<?= $news->image1 ?>">
                    </div>
                    <div class="news-detail-content_box">
                        <div class="news-detail-head" data-aos="fade-up">
                            <span class="news-dtl-post_date"> <?= date('M d, Y', strtotime($news->date)) ?></span>
                            <span class="news-dtl-post_ttl"># News</span>
                        </div>
                        <div class="news-detail-title" data-aos="fade-up">
                           <?= $news->title?>
                        </div>
                        <div class="news-detail-content" data-aos="fade-up">
                           <?= $news->detail_description?>
                        </div>
                    </div>
                </div>
                <div class="News-Rgt col-xl-4 col-lg-4">
                    <div class="news-dtl_ttl" data-aos="fade-up">
                    Case Study
                    </div>
                    <div class="row">
                         <?php
                if(!empty($news_all))
                {
                    foreach($news_all as $data)
                    {                               


                        ?>

                        <div class="col-xl-12 col-lg-12 col-md-4 col-sm-6 col-12" data-aos="fade-up">
                            <a href="<?= Url::to(['site/newsdetails', 'name' => $data->canonical_name]); ?>">
                                <div class="NewsBox">
                                    <div class="Lft_News">
                                        <div class="Img_Box">
                                            <img class="lazy" loading="lazy" width="150" height="102" data-src="<?= Yii::$app->homeUrl ?>images/news/image<?= $data->id ?>.<?= $data->image ?>" src="<?= Yii::$app->homeUrl ?>images/news/image<?= $data->id ?>.<?= $data->image ?>" >
                                        </div>
                                    </div>
                                    <div class="Rgt_News">
                                        <div class="SubTitle">
                                            <?= $data->title?>
                                        </div>
                                        <div class="News-flxbx">
                                            <div class="News-dtl-date"><?= date('M d, Y', strtotime($data->date)) ?></div>
                                            <div class="News-dtl-tag"># News</div>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>
                        <?php
                       
}
}
?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

