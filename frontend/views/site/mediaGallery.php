<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="mediaGalleryPage">

    <section id="innerBanner">


        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920"
            height="590">

        <?php
    }
    ?>
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="media-gallery">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="align-items-start">
                        <div class="nav nav-pills" id="v-pills-tab" role="tablist" data-aos="fade-up">
                            <?php if(!empty($photo_gallery)) { ?>
                            <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill"
                                data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home"
                                aria-selected="true">
                                Photo Gallery
                            </button>
                            <?php } ?>
                            <?php if(!empty($video_gallery)) { ?>
                            <button class="nav-link <?= !empty($photo_gallery) ? '' : 'active' ?>"
                                id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile"
                                type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                Video Gallery
                            </button>
                            <?php } ?>
                        </div>
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                                aria-labelledby="v-pills-home-tab" tabindex="0">
                                <div class="row">
                                    <?php
                                    if(!empty($photo_gallery))
                                    {
                                        foreach($photo_gallery  as $photo)
                                        {
                                            ?>


                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                                        <a href="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $photo->gallery_id ?>/photos/image<?= $photo->id ?>.<?= $photo->image ?>"
                                            data-fancybox="photos">
                                            <div class="photo-gallery">
                                                <img class="lazy" loading="lazy" width="353" height="353"
                                                    data-src="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $photo->gallery_id ?>/photos/image<?= $photo->id ?>.<?= $photo->image ?>"
                                                    src="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $photo->gallery_id ?>/photos/image<?= $photo->id ?>.<?= $photo->image ?>"
                                                    alt="<?= $photo->image_alt?>">
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>




                                </div>
                            </div>
                            <div class="tab-pane fade <?= !empty($photo_gallery) ? '' : 'show active' ?>"
                                id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab" tabindex="0">
                                <div class="row">
                                    <?php
                                    if(!empty($video_gallery))
                                    {
                                        foreach($video_gallery  as $video)
                                        {
                                            ?>


                                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                                        <a href="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $video->gallery_id ?>/videos/video<?= $video->id ?>.<?= $video->video ?>"
                                            data-fancybox="videos">
                                            <div class="video-gallery">
                                                <img class="lazy" loading="lazy" width="355" height="355"
                                                    data-src="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $video->gallery_id ?>/videos/thumb<?= $video->id ?>.<?= $video->image ?>"
                                                    src="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $video->gallery_id ?>/videos/thumb<?= $video->id ?>.<?= $video->image ?>"
                                                    alt="<?= $video->image_alt?>">
                                                <div class="video-icon">
                                                    <svg viewBox="0 0 55.572 55.572">
                                                        <g id="Group_209" data-name="Group 209">
                                                            <g id="Group_208" data-name="Group 208">
                                                                <path id="Path_174" data-name="Path 174"
                                                                    d="M27.786,0A27.786,27.786,0,1,0,55.572,27.786,27.817,27.817,0,0,0,27.786,0Zm0,53.257A25.471,25.471,0,1,1,53.257,27.786,25.5,25.5,0,0,1,27.786,53.257Z"
                                                                    fill="#fff" />
                                                                <path id="Path_175" data-name="Path 175"
                                                                    d="M204.294,156.577l-11.075-7.12a.791.791,0,0,0-1.219.665v14.239a.791.791,0,0,0,1.219.665l11.075-7.12a.791.791,0,0,0,0-1.33Z"
                                                                    transform="translate(-168.227 -129.456)"
                                                                    fill="#fff" />
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
}
}
?>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
        integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
        integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
    // fancybox
    $('[data-fancybox="photos"]').fancybox({
        buttons: [
            "zoom",
            "slideShow",
            "thumbs",
            "close"
        ],
        loop: true,
        protect: true
    });
    </script>

</div>