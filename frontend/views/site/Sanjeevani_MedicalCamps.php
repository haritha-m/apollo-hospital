<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="SanjeevaniMedicalCampPage">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= yii::$app->homeUrl ?>images/sanjeevani/banner-images/banner<?= $sanjeevani->id?>.<?= $sanjeevani->banner_image ?>"
            data-src="<?= yii::$app->homeUrl ?>images/sanjeevani/banner-images/banner<?= $sanjeevani->id?>.<?= $sanjeevani->banner_image ?>" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $sanjeevani->banner_title?></h1>
                        <h4 class="bnrSubHd"><?= $sanjeevani->banner_description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Sanjeevani">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="Main_Title" data-aos="fade-up">
                        <div class="Main-Title">
                            <?= $sanjeevani->title?>
                        </div>
                        <div class="Main-SubTitle">
                            <?= $sanjeevani->detail_title?>
                        </div>
                        <div class="Main-Cnt">
                            <?= $sanjeevani->detail_description?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php if(count($sanjeevani_gallery) > 0): ?>
    <section id="MedicalCamp">
        <div class="container">
            <div class="row">
                <div class="grid-section">
                    <div class="grid-products">
                        <?php foreach($sanjeevani_gallery as $sanjeevani_glr): ?>
                        <div class="product-grid" data-aos="fade-up">
                            <a href="<?= Url::to(['site/sanjeevanigallery', 'name' => $sanjeevani_glr->canonical_name]); ?>"
                                class="catGallBx">
                                <div class="photo-gallery">
                                    <img class="lazy" loading="lazy" width="353" height="353"
                                        data-src="<?= yii::$app->homeUrl ?>images/sanjeevani-gallery/<?= $sanjeevani_glr->sanjeevani_id ?>/<?= $sanjeevani_glr->id ?>/image<?= $sanjeevani_glr->id ?>.<?= $sanjeevani_glr->image ?>"
                                        src="<?= yii::$app->homeUrl ?>images/sanjeevani-gallery/<?= $sanjeevani_glr->sanjeevani_id ?>/<?= $sanjeevani_glr->id ?>/image<?= $sanjeevani_glr->id ?>.<?= $sanjeevani_glr->image ?>">
                                    <div class="ctryTleWrp">
                                        <div class="ctryTle"><?= $sanjeevani_glr->title ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
        integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
        integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
    // fancybox
    $('[data-fancybox="MedicalCamp-photos"]').fancybox({
        buttons: [
            "zoom",
            "slideShow",
            "thumbs",
            "close"
        ],
        loop: true,
        protect: true
    });
    </script>


</div>