<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="ConferenceDetailedsPage ">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd">Conference</h1>
                        <h4 class="bnrSubHd">These are the words that form the cornerstone of India’s most advanced medical institution</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Conference_Detailed">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="core_v-ttl" data-aos="fade-up">
                        <div class="core_title">
                            <?= $confernce->title ?>
                        </div>
                        <div class="core_subtitle">
                           <?= date('d M Y', strtotime($confernce->date)) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($confernce->date >= date('Y-m-d')): {?>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-12 col-12">
                    <div class="conference-dtl-img" data-aos="fade-up">
                        <img class="lazy" loading="lazy" width="730" height="460" data-src="<?= Yii::$app->homeUrl ?>images/conference/data<?= $confernce->id ?>.<?= $confernce->image1 ?>" src="<?= Yii::$app->homeUrl ?>images/conference/data<?= $confernce->id ?>.<?= $confernce->image1 ?>">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="conference-dtl-content" data-aos="fade-up">
                        <p><?= $confernce->detail_description ?></p>
                    </div>
                </div>
            </div>
            <?php } else:?>
            <div class="row">
                <?php foreach($gallery as $gal)
                    if($gal->conference_id == $confernce->id){?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="<?= Yii::$app->homeUrl ?>images/conference/gallery/<?= $gal->image ?>" data-fancybox="photos">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="730" height="460" data-src="<?= Yii::$app->homeUrl ?>images/conference/gallery/<?= $gal->image ?>" src="<?= Yii::$app->homeUrl ?>images/conference/gallery/<?= $gal->image ?>">
                            <div class="ctryTleWrp">
                                <!--<div class="ctryTle"><?= $confernce->title ?></div>-->
                            </div>
                        </div>
                    </a>
                </div>
                <?php }?>
            </div>
            <?php endif; ?>
        </div>
    </section>
    
    <?php if($confernce->date >= date('Y-m-d')): ?>
    <section id="queFormSec">
        <div class="container">
            <div class="queFormWrap">
                <div class="formOuter" data-aos="fade-up">
                    <form class="customForm">
                        <fieldset>
                            <h4 class="formTle">Registration</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                            </div>
                           <div class="form-group">
                                        <select class="form-select form-control" id="district" name="district_id"
                                            required="">
                                            <option selected disabled value="">Select Your District</option>
                                            <?php if(count($districts) > 0): ?>
                                            <?php foreach($districts as $district): ?>
                                            <option value="<?= $district->id ?>"><?= $district->name ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group panchayat" style="display: none;">
                                        <select class="form-select form-control" id="panchayat" name="panchayat_id">
                                            <option selected disabled value="">Select Your Panchayat</option>
                                        </select>
                                    </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Mobile" id="mobile"  name="mobile"required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="dept" name="dept" placeholder="Department" required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="desg" name='desg' placeholder="Designation" required>
                            </div>
                            <div class="btnWrap">
                                <button type="submit" class="btnSbmt hoveranim"><span>submit</span></button>
                                <div class="testmsg"></div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <?php
                if($confernce->image2 !='')
                {
                    ?>
                <div class="contntImg jarallax" data-jarallax data-type="scroll">
                    <img class="lazy jarallax-img" src="<?= Yii::$app->homeUrl ?>images/conference/register<?= $confernce->id ?>.<?= $confernce->image2 ?>" data-src="<?= Yii::$app->homeUrl ?>images/conference/register<?= $confernce->id ?>.<?= $confernce->image2 ?>" 
                    alt="<?= $confernce->image2_alt ?>" width="820" height="730">
                </div>
                <?php
}
?>


            </div>
        </div>
    </section>   
    <?php endif; ?>



</div>
<!-- fancybox -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
    integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
    integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
// fancybox
$('[data-fancybox="photos"]').fancybox({
    buttons: [
        "zoom",
        "slideShow",
        "thumbs",
        "close"
    ],
    loop: true,
    protect: true
});
</script>
<script>
    $(document).ready(function () {




        
        $(document).on('submit', '.customForm', function (e) {
           

            e.preventDefault();
            var str = $(this).serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::$app->homeUrl; ?>ajax/confernce-enquiry',
                data: str,
                success: function (data)
                {
                   

                    if (data == 1) {
                        
                      
                    $('.testmsg').html('<div id="email-alert" style="color: #fff;background: #4b2163;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>');
                    }
                    $('.customForm')[0].reset();
                    $('#name').val("");
                    $('#email').val("");
                    $('#email').val("");
                     $('#district_id').val("");
                $('#panchayat_id').val("");
                     $('#mobile').val("");
                    $('#dept').val("");
                     $('#desg').val("");
                     setTimeout(function () {
                        $('#email-alert').remove();
                    }, 4000);
                }
            });
        });
       
    });
      function onlyNumberKey(evt) {
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
        }
</script>
<script>
$(document).on('change', '#district', function(e) {
    $('#panchayat').empty().append(
        '<option selected disabled value="">Select Your Panchayat</option>')
    $('.panchayat').hide()
    $('#schedulingappointments-other').val('')
    $('.otherLocation').hide()
    $('#schedulingappointments-other').attr('required', false)
    $('#panchayat').attr('required', false)

    var district_id = $(this).val();
    if (district_id == 7 || district_id == 8) {
        $.ajax({
            type: "POST",
            url: '<?= yii::$app->homeUrl ?>ajax/get-panchayat',
            data: {
                id: district_id
            },
            dataType: 'json',
            success: function(data) {
                var panchayats = data.panchayats
                if (panchayats) {
                    $('#panchayat').empty().append(
                        '<option selected disabled value="">Select Your Panchayat</option>')
                    $.each(panchayats, function(key, value) {
                        $('#panchayat').append(
                            '<option value="' +
                            value.id + '">' + value.name + '</option>');
                    });
                    $('.panchayat').show()
                    $('#panchayat').attr('required', true)
                } else {
                    return false;
                }
            }
        });
    } else if (district_id == 15) {
        $('.otherLocation').show()
        $('#schedulingappointments-other').attr('required', true)
    }
});

function onlyNumberKey(evt) {
    // Only ASCII charactar in that range allowed 
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
$(document).ready(function() {
    $("#schedulingappointments-name").keypress(function(event) {
        var inputValue = event.which;
        if ((inputValue > 47 && inputValue < 58)) {
            event.preventDefault();
        }
    });
    var flag = <?= $flag ?>;

    if (flag == 1) {
        $('.testmsg').html(
            '<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'
        );
    }
    setTimeout(function() {
        $('#email-alert').remove();
    }, 4000);
});
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}
</script>