<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="CmePage mediaGalleryPage">

    <section id="innerBanner">


<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
               <img class="lazy innerBg" loading="lazy" src="?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920" height="590">



        <?php
    }
    ?> 






    


        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="cme-list">
        <div class="container">
            <div class="row">
               <?php
                if(!empty($confernce_data))
                {
                    foreach($confernce_data as $data)
                    {
                        ?>

                <!--<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-aos="fade-up">-->
                <!--    <a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>" class="blog-box">-->
                <!--        <div class="image_box">-->
                <!--            <img class="lazy" loading="lazy" width="330" height="235" data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>" -->
                <!--            src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">-->
                <!--        </div>-->
                        <!--<div class="content_box">-->
                         
                        <!--    <div class="details_sec">-->
                        <!--        <div class="post-title"><?= $data->title?></div>-->
                        <!--        <div class="post-description"><?= $data->small_description?></div>-->
                        <!--    </div>-->
                        <!--</div>-->
                <!--    </a>-->
                <!--</div>-->
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="330" height="235" data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>" src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">
                            <div class="ctryTleWrp">
                                <div class="ctryTle"><?= $data->title?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
        }
        ?>
                
            </div>
        </div>
    </section>

</div>

