<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($service->meta_title) && $service->meta_title != '') {
    $this->title = $services->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?> 
<div id="pageWrapper" class="servicesPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">


        <?php
            if($service->banner_image != '')
            {
                ?>


        <img class="lazy innerBg jarallax-img" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/services/banner-images/banner<?= $service->id ?>.<?= $service->banner_image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/services/banner-images/banner<?= $service->id ?>.<?= $service->banner_image ?>"
            alt="<?= $service->banner_alt ?>" width="1920" height="590">

        <?php
            }
            else{
                ?>


        <img class="lazy innerBg jarallax-img" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>assets/images/cardiacPgBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/cardiacPgBnr.jpg" alt="bannerImage" width="1920"
            height="590">
        <?php
            }
            
            ?>


        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">


                        <h1 class="bnrMainHd">

                            <?php
            if($service->banner_title != '')
            {
                echo $service->banner_title;
            }
            else{
                                echo $service->title;
            }
                ?>






                        </h1>
                        <h4 class="bnrSubHd">
                            <?php
                            
                             if($service->banner_description != '')
            {
                echo $service->banner_description;
            }
                            ?>

                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="serviceDtlsSec" class="inrCntSec">
        <div class="topSec">
            <div class="container">
                <div class="row">
                    <div class="designBxElemntOuter">
                        <div class="elementOne"></div>
                        <div class="elementTwo"></div>
                    </div>
                    <div class="designBxElemntOuter rgt">
                        <div class="elementOne"></div>
                        <div class="elementTwo"></div>
                    </div>
                    <div class="col-12 col-lg-7 col-md-12" data-aos="fade-up" data-aos-delay="300">
                        <div class="contentBx">
                            <div class="secTitleWrap">
                                <h2 class="secTitle"><?= $service->detail_title?></h2>
                            </div>
                            <?= $service->detail_description?>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 col-md-12" data-aos="fade-up" data-aos-delay="300">
                        <div class="contntImg">
                            <div class="circleElmt">
                                <img src="<?= Yii::$app->homeUrl ?>images/services/icon<?= $service->id ?>.<?= $service->icon ?>"
                                    alt="<?= $service->icon_alt ?>">
                            </div>
                            <div class="circleElmt">
                                <svg viewBox="0 0 43.909 37.348">
                                    <path id="Path_61" data-name="Path 61"
                                        d="M43.909,23.226A12.428,12.428,0,0,1,43.29,27.1c-2.035,9.149-19.623,20.289-20.37,20.756a.967.967,0,0,1-1.022,0A87.624,87.624,0,0,1,7.124,35.991a.959.959,0,1,1,1.4-1.316A85.678,85.678,0,0,0,22.41,45.91c2.911-1.9,17.329-11.673,19.033-19.311a10.87,10.87,0,0,0,.547-3.371,10.646,10.646,0,0,0-18.844-6.8.964.964,0,0,1-.738.348h0a.951.951,0,0,1-.738-.348,10.649,10.649,0,0,0-18.851,6.8,10.514,10.514,0,0,0,.527,3.28,11.483,11.483,0,0,0,1.276,3.087h6.424L13.1,26.15a.955.955,0,0,1,.806-.466.924.924,0,0,1,.823.438L18.094,31.3l3.748-10.131a.959.959,0,0,1,.9-.628h0a.958.958,0,0,1,.9.626l3.139,8.422h5.557a.96.96,0,0,1,0,1.919H26.112a.964.964,0,0,1-.9-.622l-2.464-6.625-3.512,9.5a.954.954,0,0,1-.795.619.937.937,0,0,1-.912-.431l-3.568-5.5-1.545,2.586a.961.961,0,0,1-.823.468H.96a.959.959,0,1,1,0-1.919H2.437A11.829,11.829,0,0,1,1.5,27.015a12.163,12.163,0,0,1-.6-3.787A12.569,12.569,0,0,1,22.406,14.39a12.565,12.565,0,0,1,21.5,8.836Z"
                                        transform="translate(0 -10.659)" fill="#fff" />
                                </svg>
                            </div>
                            <img class="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/services/data<?= $service->id ?>.<?= $service->image1 ?>"
                                data-src="<?= Yii::$app->homeUrl ?>images/services/data<?= $service->id ?>.<?= $service->image1 ?>"
                                alt="Heart Treatment" width="600" height="500">
                            <?php if($service->is_booking == 1){ ?>
                            <a href="<?= Url::to(['site/doctors']); ?>#<?= $service->canonical_name?>"
                                class="bookAppoiBtn">
                                <svg viewBox="0 0 55.633 55.633">
                                    <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                        d="M28.743,49.1,8.417,28.775l20.326-20.4,1.242,1.242L11.634,27.9h37.51v1.681H11.634L29.986,47.933Z"
                                        transform="translate(67.095 17.261) rotate(120)" />
                                </svg>
                                <p class="btnTxt">Book an Appointment</p>
                            </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!empty($service->facility_content)): ?>
        <div class="facilitySec">
            <div class="container">
                <div class="row">
                    <div class="cntWrp">
                        <div class="secTitleWrap">
                            
                            <h2 class="secTitle"> Scope of Services</h2>
                            <h6 class="secSubTitle"><?= $service->facility_description ?></h6>
                        </div>
                        <?= $service->facility_content ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php
        if(!empty($doctors))
        {
            ?>
        <div class="expertSec">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="secTitleWrap" data-aos="fade-up">
                            <h2 class="secTitle">Get to Know our Team of Experts</h2>
                        </div>
                    </div>

                    <div class="col-12" data-aos="fade-up">
                        <div class="owl-carousel docCarousel owl-theme">
                            <?php
                            foreach($doctors as $doctor)
                            {
                                ?>

                            <div class="item">
                                <div class="docCard">
                                    <div class="cImg">
                                        <img class="lazy"
                                            src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doctor->id ?>.<?= $doctor->image ?>"
                                            data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doctor->id ?>.<?= $doctor->image ?>"
                                            loading="lazy" alt="doctor" width="340" height="390">
                                    </div>
                                    <div class="cCnt">
                                        <h4 class="dName"><?= $doctor->name?></h4>
                                        <h6 class="dDprt"><?= $doctor->small_description?></h6>
                                        <p class="dDisc"><?= $doctor->education?></p>
                                    </div>
                                    <div class="cBttm">
                                        <div class="lftBtn">
                                            <a href="<?= Url::to(['site/doctordetails', 'name' => $doctor->canonical_name]); ?>"
                                                class="docBtn hoveranim"><span>Profile</span></a>
                                        </div>
                                        <div class="rgtBtn">
                                            <a href="<?= $doctor->appointment_link?>" class="bookBtn docBtn hoveranim"
                                                target="_blank"><span>Book An
                                                    Appointment</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
        <?php if(count($faqs) > 0): $i=0; ?>
        <div class="faqSecWrap">
            <div class="container">
                <div class="secTitleWrap center">
                    <h2 class="secTitle">FAQs</h2>
                    <h4 class="secSubTitle"><?= $service->faq_description?></h4>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="accordion accordion-flush accoWrap" id="faqAccordion" data-aos="fade-up">
                            <?php foreach($faqs as $faq): $i++ ?>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading<?= $i ?>">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse<?= $i ?>" aria-expanded="true"
                                        aria-controls="collapse<?= $i ?>">
                                        <?= $faq->question ?>
                                    </button>
                                </h2>
                                <div id="collapse<?= $i ?>"
                                    class="accordion-collapse collapse <?= $i == 1 ? 'show' : '' ?>"
                                    aria-labelledby="heading<?= $i ?>" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <?= $faq->answer ?>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>

</div>

<!-- OWL_SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
$('.docCarousel').owlCarousel({
    loop: true,
    rewind: true,
    nav: false,
    dots: false,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        480: {
            items: 2,
            loop: $('.docCarousel .item').lenght > 2 ? true : false,
        },
        768: {
            items: 3,
            loop: $('.docCarousel .item').lenght > 3 ? true : false,
        },
        1000: {
            items: 4,
            loop: $('.docCarousel .item').lenght > 4 ? true : false,
            margin: 15
        },
        1200: {
            items: 4,
            loop: $('.docCarousel .item').lenght > 4 ? true : false,
            margin: 30,
        }
    }
})
</script>