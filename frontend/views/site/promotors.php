<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="promotorsPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
       



<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                  <img class="lazy innerBg jarallax-img" src="<?= Yii::$app->homeUrl ?>assets/images/prmtrsPgBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/prmtrsPgBnr.jpg" alt="bannerImage" width="1920" height="590">


        <?php
    }
    ?> 









        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="promotorsCntSec">

           <?php
    $other_doctors = explode(',', $promotors->doctor_id);
    if (!empty($promotors->doctor_id)) {
        ?>
       
                <?php
                $i=0;

                foreach ($other_doctors as $other_doctor) {
                    $i++;
                    $doctor = common\models\Doctors::find()->where(['id' => $other_doctor])->one();
                    if (!empty($doctor)) {
                            $service = \common\models\Services::find()->where(['id' => $doctor->department])->one();

                        ?>

        <div class="prmtrSecWrap">
            <div class="container">
                <div class="row">



                    <div class="col-xl-5 col-lg-5 col-12">
                        <div class="prmtrWrap">
                            <div class="cImg">
                                <img class="lazy" src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doctor->id ?>.<?= $doctor->image ?>"
                                    data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doctor->id ?>.<?= $doctor->image ?>" loading="lazy" alt="<?= $doctor->image_alt ?>" width="340"
                                    height="390">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                        <div class="cntWrap">
                            <div class="topSec" data-aos="fade-up">
                                <h2 class="dName"> <?= $doctor->name?></h2>
                                <h6 class="dDprt"><?= $service->title?></h6>
                                <?= $doctor->promote_description?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="accordion accordion-flush accoWrap" id="docAccordion" data-aos="fade-up">

                           <?php
                           if($doctor->fellowship_description != '')
                           {
                            ?>

                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                       Fellowship and certification
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="headingOne" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                       <?= $doctor->fellowship_description?>
                                    </div>
                                </div>
                            </div>
                            <?php
}

 if($doctor->expertise_description != '')
                           {
?>




                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                       Expertise
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                          <?= $doctor->expertise_description?>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        if($doctor->languages_description != '')
                           {
                        ?>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Languages Known
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse"
                                    aria-labelledby="headingThree" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                            <?= $doctor->languages_description?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>








                                          


                                   <?php
                    }
                }
            }
                ?>


















    
    </section>
</div>

