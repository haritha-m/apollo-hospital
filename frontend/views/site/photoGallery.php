<?php include "./includes/header.php"?>
<div id="pageWrapper" class="mediaGalleryPage photoGallery">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="assets/images/newsMediaBg.jpg"
            data-src="assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd">Photo Gallery</h1>
                        <h4 class="bnrSubHd">These are the words that form the cornerstone of India’s most advanced
                            medical institution</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="media-gallery">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-01.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-01.jpg" src="assets/images/photo-gallery-01.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-02.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-02.jpg" src="assets/images/photo-gallery-02.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-03.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-03.jpg" src="assets/images/photo-gallery-03.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-04.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-04.jpg" src="assets/images/photo-gallery-04.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-05.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-05.jpg" src="assets/images/photo-gallery-05.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-06.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-06.jpg" src="assets/images/photo-gallery-06.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-07.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-07.jpg" src="assets/images/photo-gallery-07.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-08.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-08.jpg" src="assets/images/photo-gallery-08.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-01.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-01.jpg" src="assets/images/photo-gallery-01.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-02.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-02.jpg" src="assets/images/photo-gallery-02.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-03.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-03.jpg" src="assets/images/photo-gallery-03.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-04.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-04.jpg" src="assets/images/photo-gallery-04.jpg">
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-05.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-05.jpg" src="assets/images/photo-gallery-05.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-06.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-06.jpg" src="assets/images/photo-gallery-06.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-07.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-07.jpg" src="assets/images/photo-gallery-07.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="assets/images/photo-gallery-08.jpg" data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="assets/images/photo-gallery-08.jpg" src="assets/images/photo-gallery-08.jpg">
                        </div>
                        <div class="gallDisc">
                            <h6 class="disc">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h6>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
        integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
        integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
    // fancybox
    $('[data-fancybox="photos"]').fancybox({
        buttons: [
            "zoom",
            "slideShow",
            "thumbs",
            "close"
        ],
        loop: true,
        protect: true
    });
    </script>

</div>

<?php include "./includes/footer.php"?>