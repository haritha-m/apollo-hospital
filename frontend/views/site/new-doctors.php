<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}
?>
<div id="pageWrapper" class="doctorsPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
        <?php if(!empty($banner_images)) { ?>
        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">
        <?php } else { ?>
        <img class="lazy innerBg jarallax-img" src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" loading="lazy" alt="bannerImage"
            width="1920" height="590">
        <?php } ?>
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="doctorsSec" class="inrCntSec">
        <div class="container">
            <?php if(count($doctors) > 0) { ?>
            <div class="row docCarouselSec">
                <div class="col-12" data-aos="fade-up">
                    <div class="owl-carousel docCarousel owl-theme">
                        <?php foreach($doctors as  $doc) { ?>
                        <div class="item">
                            <div class="docCard">
                                <div class="cImg">
                                    <img class="lazy"
                                        src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>"
                                        data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>"
                                        loading="lazy" alt="doctor" width="340" height="390">
                                </div>
                                <div class="cCnt">
                                    <h4 class="dName"><?= $doc->name?></h4>
                                    <h6 class="dDprt"><?= $doc->small_description?></h6>
                                    <p class="dDisc"><?= $doc->education?></p>
                                </div>
                                <div class="cBttm">
                                    <div class="lftBtn">
                                        <a href="<?= Url::to(['site/doctordetails', 'name' => $doc->canonical_name]); ?>"
                                            class="docBtn hoveranim"><span>Profile</span></a>
                                    </div>
                                    <div class="rgtBtn">
                                        <a href="<?= $doc->appointment_link ?>" class="bookBtn docBtn hoveranim"
                                            target="_blank"><span>Book An Appointment</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="row docCarouselSec">
                <div class="col-12">
                    <div class="text-center">No results found.</div>
                </div>
            </div>
            <?php  } ?>
        </div>
    </section>

</div>

<!-- OWL_SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
$('.docCarousel').owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2,
                loop: $('.item img').lenght > 2 ? true : false,
            },
            768: {
                items: 3,
                loop: $('.item img').lenght > 3 ? true : false,
            },
            1000: {
                items: 4,
                margin: 15,
                loop: $('.item img').lenght > 4 ? true : false,
            },
            1200: {
                items: 4,
                margin: 30,
                loop: $('.item img').lenght > 4 ? true : false,
            }
        }
})
</script>