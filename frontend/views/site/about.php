<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="AboutPage">

    <section id="innerBanner">


      



<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
              <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/aboutBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/aboutBg.jpg" alt="bannerImage" width="1920" height="590">



        <?php
    }
    ?> 


        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Breadcrumb-Nav">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <ul class="breadcrumb">
                        <li><a href=""><?= $banner_images->title ?></a></li>
                        <li class="active">About Apollo Adlux</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="about_us">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade-up">
                    <div class="about-image">
                        <img class="lazy" loading="lazy" width="750" height="580" data-src="<?= Yii::$app->homeUrl ?>images/about/abt<?= $about->id ?>.<?= $about->image ?>" src="<?= Yii::$app->homeUrl ?>images/about/abt<?= $about->id ?>.<?= $about->image ?>" alt="<?= $about->image_alt ?>">
                        <div class="Icon_1 rotating">
                            <svg viewBox="0 0 44 44">
                                <g id="Group_1876" data-name="Group 1876" transform="translate(-0.382 0.776)">
                                    <rect id="Rectangle_9" data-name="Rectangle 9" width="22" height="22" transform="translate(0.382 21.224)" fill="#ffbe00"/>
                                    <rect id="Rectangle_13" data-name="Rectangle 13" width="22" height="22" transform="translate(22.382 -0.776)" fill="#006b8e"/>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade-up">
                    <div class="about-details">
                        <div class="about-ttl"><?=$about->main_title?></div>
                        <div class="about-content">
                            <?=$about->main_description?>
                        </div>
                        <div class="number_count">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                                    <div class="counter" data-aos="fade-up">
                                        <div class="contertTxt"><span data-count="<?=$about->count1?>">0</span>+</div>
                                        <div class="Txt"><?=$about->count1_title?></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6" data-aos="fade-up">
                                    <div class="counter">
                                        <div class="contertTxt"><span data-count="<?=$about->count2?>">0</span>+</div>
                                        <div class="Txt"><?=$about->count2_title?></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6" data-aos="fade-up">
                                    <div class="counter">
                                        <div class="contertTxt"><span data-count="<?=$about->count3?>">0</span>+</div>
                                        <div class="Txt"><?=$about->count3_title?></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6" data-aos="fade-up">
                                    <div class="counter">
                                        <div class="contertTxt"><span><?=$about->count4?></span></div>
                                        <div class="Txt"><?=$about->count4_title?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    <section id="our_values">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/vision/common<?= $vision->id ?>.<?= $vision->image3 ?>" src="<?= Yii::$app->homeUrl ?>images/vision/common<?= $vision->id ?>.<?= $vision->image3 ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?=$vision->common_title?></div>
                            <div class="value_DtL">
                                <?=$vision->common_description?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/vision']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/our-message/chc<?= $our_message->id ?>.<?= $our_message->image2 ?>" src="<?= Yii::$app->homeUrl ?>images/our-message/chc<?= $our_message->id ?>.<?= $our_message->image2 ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?=$our_message->common_title?></div>
                            <div class="value_DtL">
                                <?=$our_message->common_description?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/ourmessage']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/core-values/common<?= $core_values->id ?>.<?= $core_values->image ?>" src="<?= Yii::$app->homeUrl ?>images/core-values/common<?= $core_values->id ?>.<?= $core_values->image ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?=$core_values->common_title?></div>
                            <div class="value_DtL">
                              <?=$core_values->common_description?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/corevalues']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<script>
//   counter js  

$(document).ready(function() {

var controller = new ScrollMagic.Controller();

var scene = new ScrollMagic.Scene({
        triggerElement: '#about',
        triggerHook: 0.6
    })
    .setClassToggle('#about', 'isVisible')
    .on("enter", function() {
        $('.counter .contertTxt span').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');

            $({
                countNum: $this.text()
            }).animate({
                countNum: countTo
            }, {

                duration: 2000,
                easing: 'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }

            });

        });
    })
    .on("leave", function() {
        $('.contertTxt span').each(function() {
            $(this).text(0);
        });
    })
    .addTo(controller);

});   


</script>

    <!-- SCROLL_MAGIC -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/ScrollMagic.min.js"
        integrity="sha512-8E3KZoPoZCD+1dgfqhPbejQBnQfBXe8FuwL4z/c8sTrgeDMFEnoyTlH3obB4/fV+6Sg0a0XF+L/6xS4Xx1fUEg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</div>

