<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?> <div id="pageWrapper" class="servicesPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">






        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg jarallax-img" loading="lazy" src="assets/images/servicesBnr.jpg"
            data-src="assets/images/servicesBnr.jpg" alt="bannerImage" width="1920" height="590">

        <?php
    }
    ?>











        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="servicesSec" class="inrCntSec">
        <div class="container">
            <div class="row">
                <?php
                if(!empty($services))
                {
                    foreach($services as $service)
                    {
                        ?>

                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-aos="fade-up">
                    <a href="<?= Url::to(['site/servicedetails', 'name' => $service->canonical_name]); ?>"
                        class="servCard">
                        <div class="cImg">
                            <img class="lazy" loading="lazy" width="330" height="235"
                                data-src="<?= Yii::$app->homeUrl ?>images/services/image<?= $service->id ?>.<?= $service->image ?>"
                                src="<?= Yii::$app->homeUrl ?>images/services/image<?= $service->id ?>.<?= $service->image ?>">
                            <div class="hoverRdMr">
                                <div class="rdMrIcon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="55.633" height="55.633"
                                        viewBox="0 0 55.633 55.633">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                            d="M28.743,49.1,8.417,28.775l20.326-20.4,1.242,1.242L11.634,27.9h37.51v1.681H11.634L29.986,47.933Z"
                                            transform="translate(67.095 17.261) rotate(120)" fill="#fff" />
                                    </svg>
                                    <p class="rdMrTxt">View More</p>
                                </div>
                            </div>
                        </div>
                        <div class="cCnt">
                            <h3 class="cTle"><?= $service->title?></h3>
                            <p class="cDisc"><?= $service->small_description?></p>
                        </div>
                    </a>
                </div>
                <?php
               }
           }
           ?>
            </div>
        </div>
    </section>
</div>