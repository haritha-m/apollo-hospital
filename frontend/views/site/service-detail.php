<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($details->meta_title) && $details->meta_title != '') {
    $this->title = $details->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}
use common\models\Doctors;
use common\models\ServiceMainCategory;
?>
<div id="pageWrapper" class="servicesPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
        <img class="lazy innerBg jarallax-img" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/services/service-main-category/banner-images/banner<?= $details->id ?>.<?= $details->banner_image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/services/service-main-category/banner-images/banner<?= $details->id ?>.<?= $details->banner_image ?>" alt="<?=$details->title?>" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?=$details->title?></h1>
                        <h4 class="bnrSubHd"><?=$details->caption?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="serviceDtlsSec"  >
        <div class="topSec">
            <div class="container">
                <div class="row">
                    <div class="designBxElemntOuter">
                        <div class="elementOne"></div>
                        <div class="elementTwo"></div>
                    </div>

                    <div class="col-12 col-lg-6 col-md-12" data-aos="fade-up" data-aos-delay="300">
                        <div class="contentBx">
                            <div class="designBxElemntOuter rgt">
                                <div class="elementOne"></div>
                                <div class="elementTwo"></div>
                            </div>
                            <div class="secTitleWrap">
                                <h2 class="secTitle"><?=$details->inner_title?></h2>
                            </div>
                            <?=$details->description?>

                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-md-12" data-aos="fade-up" data-aos-delay="300">
                        <div class="contactForm" data-aos="fade-up">
                            <div class="designBxElemntOuter rgt">
                                <div class="elementOne"></div>
                                <div class="elementTwo"></div>
                            </div>
                            <div class="mainTxt"><?=$details->form_title?></div>
                            <form class="customForm">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" pattern="[a-zA-Z\s]+" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Xyz@gmail.com /Xyz@gmail.in" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <select class="form-select form-control" id="district" name="district_id"
                                            required="">
                                            <option selected disabled value="">Select Your District</option>
                                            <?php if(count($districts) > 0): ?>
                                            <?php foreach($districts as $district): ?>
                                            <option value="<?= $district->id ?>"><?= $district->name ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                        </div>
                                        <div class="form-group panchayat" style="display: none;">
                                        <select class="form-select form-control" id="panchayat" name="panchayat_id">
                                            <option selected disabled value="">Select Your Panchayat</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" onkeypress="return onlyNumberKey(event)"  required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <textarea class="form-control" placeholder="Query"  id="message" name="message"style="height: 80px"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="btnWrap">
                                            <button type="submit" class="btnSbmt hoveranim"><span>submit</span></button>
                                        </div>
                                    </div>
                                    <div class="testmsg"></div>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <?php if(!empty($services)): ?>
        <div class="services">
            <div class="container">
                <div class="secTitleWrap" data-aos="fade-up">
                    <h2 class="secTitle"><?=$details->service_title?></h2>
                </div>
                <div class="flxBx">
                    <?php 
                    foreach($services as $service) : 
                    
                    ?>
                    <div class="item" data-aos="fade-up">
                        <div class="serviceBx">
                            <div class="lftBx">
                                <div class="icon">
                                <img src="<?= Yii::$app->homeUrl ?>images/services/icon<?= $service->id ?>.<?= $service->icon ?>"
                                    alt="<?= $service->icon_alt ?>">

                                </div>
                            </div>
                            <div class="rtSec">
                                <div class="mainTxt"><?=$service->title?></div>
                                <p><?=$service->small_description?></p>
                                <div class="BtnWrp">
                                    <a href="<?= Url::to(['site/servicedetails', 'name' => $service->canonical_name]); ?>" class="MoreBtn">
                                        <div class="Txt">View More</div>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                                            <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                                transform="translate(54.108 12.508) rotate(120)" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    endforeach;
               ?>
                </div>
            </div> 
        </div>
       <?php endif;
         if($doctors) : 
        ?>
        <div class="expertSec">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="secTitleWrap" data-aos="fade-up">
                            <h2 class="secTitle"><?=$details->doctors_list_title?></h2>
                        </div>
                    </div>
                    <div class="col-12" data-aos="fade-up">
                        <div class="owl-carousel docCarousel owl-theme">
                      <?php 
                    
                      foreach($doctors as $doc) : ?>
                            <div class="item">
                                <div class="docCard">
                                    <div class="cImg">
                                        <img class="lazy" src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>"
                                            data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>" loading="lazy" alt="doctor"
                                            width="340" height="390">
                                    </div>
                                    <div class="cCnt">
                                        <h4 class="dName"><?= $doc->name?></h4>
                                        <h6 class="dDprt"><?= $doc->small_description?></h6>
                                        <p class="dDisc"><?= $doc->education?></p>
                                    </div>
                                <div class="cBttm">
                                    <div class="lftBtn">
                                        <?php if($doc->view_label == 1){ ?>
                                        <a  href="<?= Url::to(['site/doctordetails', 'name' => $doc->canonical_name]); ?>"
                                            class="docBtn hoveranim"><span>Profile</span></a>
                                        <?php  }  else{ ?>
                                            <div class="docBtn hoveranim"><span>Profile</span></div>
                                            <?php } ?>
                                    </div>
                                    <div class="rgtBtn">
                                        <?php if($doc->view_label == 1 && !empty( $doc->appointment_link) &&  $doc->appointment_link !=NULL && $doc->appointment_link !="#"){ ?>
                                        <a href="<?= $doc->appointment_link ?>" class="bookBtn docBtn hoveranim"
                                            target="_blank"><span>Book An Appointment</span></a>
                                            <?php }else{ ?>
                                            <div class="bookBtn docBtn hoveranim"
                                            target="_blank"><span>Book An Appointment</span></div>
                                            <?php }?>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <?php endforeach; 
                            
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php  endif; ?>
    </section>

</div>

<!-- OWL_SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
$('.docCarousel').owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
        },
        480: {
            items: 2
        },
        768: {
            items: 3
        },
        1000: {
            items: 4,
            margin: 15
        },
        1200: {
            items: 4,
            margin: 30,
        }
    }
})


    $(document).ready(function () {




        
        $(document).on('submit', '.customForm', function (e) {
           

            e.preventDefault();
            var str = $(this).serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::$app->homeUrl; ?>ajax/contact-enquiry',
                data: str,
                success: function (data)
                {

                    if (data == 1) {
                        
                      
                    $('.testmsg').html('<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>');
                    }
                    $('.customForm')[0].reset();
                    $('#name').val("");
                    $('#email').val("");
                     $('#mobile').val("");
                      $('#district_id').val("");
                $('#panchayat_id').val("");
                    $('#message').val("");
                     setTimeout(function () {
                        $('#email-alert').remove();
                    }, 4000);
                }
            });
        });
       
    });
      function onlyNumberKey(evt) {
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
        }
</script>
<script>
$(document).on('change', '#district', function(e) {
    $('#panchayat').empty().append(
        '<option selected disabled value="">Select Your Panchayat</option>')
    $('.panchayat').hide()
    $('#schedulingappointments-other').val('')
    $('.otherLocation').hide()
    $('#schedulingappointments-other').attr('required', false)
    $('#panchayat').attr('required', false)

    var district_id = $(this).val();
    if (district_id == 7 || district_id == 8) {
        $.ajax({
            type: "POST",
            url: '<?= yii::$app->homeUrl ?>ajax/get-panchayat',
            data: {
                id: district_id
            },
            dataType: 'json',
            success: function(data) {
                var panchayats = data.panchayats
                if (panchayats) {
                    $('#panchayat').empty().append(
                        '<option selected disabled value="">Select Your Panchayat</option>')
                    $.each(panchayats, function(key, value) {
                        $('#panchayat').append(
                            '<option value="' +
                            value.id + '">' + value.name + '</option>');
                    });
                    $('.panchayat').show()
                    $('#panchayat').attr('required', true)
                } else {
                    return false;
                }
            }
        });
    } else if (district_id == 15) {
        $('.otherLocation').show()
        $('#schedulingappointments-other').attr('required', true)
    }
});
</script>

