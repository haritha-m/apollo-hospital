<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="OurMessagePage">

    <section id="innerBanner">
      

<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                        <img class="lazy innerBg" loading="lazy" src="assets/images/aboutBg.jpg"
            data-src="assets/images/aboutBg.jpg" alt="bannerImage" width="1920" height="590">

        <?php
    }
    ?> 








        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Breadcrumb-Nav">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <ul class="breadcrumb">
                        <li><a href="">About Us</a></li>
                        <li class="active">Our Message</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="our_message">
        <div class="container">
            <div class="our-message">
                <div class="Left-Side" data-aos="fade-up">
                    <div class="message_Img">
                        <img class="lazy" loading="lazy" width="560" height="630" data-src="<?= Yii::$app->homeUrl ?>images/our-message/cho<?= $our_message->id ?>.<?= $our_message->image ?>" src="<?= Yii::$app->homeUrl ?>images/our-message/cho<?= $our_message->id ?>.<?= $our_message->image ?>" alt="<?= $our_message->image_alt?>">
                        <div class="Icon_1">
                            <svg viewBox="0 0 44 44">
                                <g id="Group_1876" data-name="Group 1876" transform="translate(-0.382 0.776)">
                                    <rect id="Rectangle_9" data-name="Rectangle 9" width="22" height="22" transform="translate(0.382 21.224)" fill="#ffbe00"/>
                                    <rect id="Rectangle_13" data-name="Rectangle 13" width="22" height="22" transform="translate(22.382 -0.776)" fill="#006b8e"/>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="Right-Side" data-aos="fade-up">
                    <div class="Icon_2">
                        <svg viewBox="0 0 69.897 59.912">
                            <g id="quote_1_" data-name="quote (1)" transform="translate(0 -29.235)">
                                <path id="Path_1092" data-name="Path 1092" d="M233.882,29.235V59.191h19.971a19.994,19.994,0,0,1-19.971,19.971v9.985a29.989,29.989,0,0,0,29.956-29.956V29.235Z" transform="translate(-193.941)" fill="#006b8e"/>
                                <path id="Path_1093" data-name="Path 1093" d="M0,59.191H19.971A19.994,19.994,0,0,1,0,79.162v9.985A29.989,29.989,0,0,0,29.956,59.191V29.235H0Z" transform="translate(0 0)" fill="#006b8e"/>
                            </g>
                        </svg>
                    </div>
                    <div class="message_Details">
                        <div class="message-name"><?= $our_message->name?></div>
                        <div class="message-tagline"><?= $our_message->position?></div>
                        <div class="message-content">
                            <?= $our_message->description?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="our-message">
                <div class="Left-Side" data-aos="fade-up">
                    <div class="message_Img">
                        <img class="lazy" loading="lazy" width="560" height="630" data-src="<?= Yii::$app->homeUrl ?>images/our-message/cht<?= $our_message->id ?>.<?= $our_message->image1 ?>" src="<?= Yii::$app->homeUrl ?>images/our-message/cht<?= $our_message->id ?>.<?= $our_message->image1 ?>" alt="<?= $our_message->image1_alt?>">
                    </div>
                </div>
                <div class="Right-Side" data-aos="fade-up">
                    <div class="message_Details">
                        <div class="message-name"><?= $our_message->name1?></div>
                        <div class="message-tagline"><?= $our_message->position1?></div>
                        <div class="message-content">
                           <?= $our_message->description1?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="our_values">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/vision/common<?= $vision->id ?>.<?= $vision->image3 ?>" src="<?= Yii::$app->homeUrl ?>images/vision/common<?= $vision->id ?>.<?= $vision->image3 ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?= $vision->common_title ?></div>
                            <div class="value_DtL">
                              <?= $vision->common_description ?>
                            </div>
                            <div class="value_Btn">
                                <a href="" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/about/abt<?= $about->id ?>.<?= $about->image ?>" src="<?= Yii::$app->homeUrl ?>images/about/abt<?= $about->id ?>.<?= $about->image ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?=$about->main_title?></div>
                            <div class="value_DtL">
                                <?=$about->common_description?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/about']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">-->
                <!--    <div class="value_box">-->
                <!--        <div class="imG-Box">-->
                <!--            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/our-message/chc<?= $our_message->id ?>.<?= $our_message->image2 ?>" src="<?= Yii::$app->homeUrl ?>images/our-message/chc<?= $our_message->id ?>.<?= $our_message->image2 ?>" alt="<?= $our_message->image2_alt?>">-->
                <!--        </div>-->
                <!--        <div class="CnT_Box">-->
                <!--            <div class="value_TtL"><?= $our_message->common_title?></div>-->
                <!--            <div class="value_DtL">-->
                <!--               <?= $our_message->common_description?>-->
                <!--            </div>-->
                <!--            <div class="value_Btn">-->
                <!--                <a href="<?= Url::to(['site/ourmessage']); ?>" class="btn-block">-->
                <!--                    <span class="KnowMore_Btn">Know More</span>-->
                <!--                    <svg viewBox="0 0 31.219 31.219">-->
                <!--                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>-->
                <!--                    </svg>-->

                <!--                </a>-->
                <!--            </div>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/core-values/common<?= $core_values->id ?>.<?= $core_values->image ?>" src="<?= Yii::$app->homeUrl ?>images/core-values/common<?= $core_values->id ?>.<?= $core_values->image ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?= $core_values->common_title ?></div>
                            <div class="value_DtL">
                               <?= $core_values->common_description ?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/corevalues']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

