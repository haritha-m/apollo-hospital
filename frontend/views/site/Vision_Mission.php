<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="VisionMissionPage">

    <section id="innerBanner">
      



<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                  <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/aboutBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/aboutBg.jpg" alt="bannerImage" width="1920" height="590">


        <?php
    }
    ?> 


        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Breadcrumb-Nav">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <ul class="breadcrumb">
                        <li><a href="">About Us</a></li>
                        <li class="active">Vision and Mission</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="vision-mission">
        <div class="container">
            <div class="visionmission-sec">
                <div class="Lft-side" data-aos="fade-up">
                    <div class="Vision">
                        <div class="v_m-ttl"><?= $vision->vision_title?></div>
                        <div class="v_m-content"><?= $vision->vision_description?></div>
                    </div>
                    <div class="Mission">
                        <div class="v_m-ttl"><?= $vision->mission_title?></div>
                        <div class="v_m-content">
                            <?= $vision->mission_description?>
                        </div>
                    </div>
                </div>
                <div class="Rgt-side" data-aos="fade-up">
                    <div class="v_m-ImgBox3">
                        <img class="lazy" loading="lazy" width="280" height="200" data-src="<?= Yii::$app->homeUrl ?>images/vision/first<?= $vision->id ?>.<?= $vision->image ?>" src="<?= Yii::$app->homeUrl ?>images/vision/first<?= $vision->id ?>.<?= $vision->image ?>" alt="<?= $vision->image_alt ?>">
                        <div class="shine"></div>
                    </div>
                    <div class="vision_mission-img">
                        <div class="v_m-ImgBox1">
                            <img class="lazy" loading="lazy" width="415" height="290" data-src="<?= Yii::$app->homeUrl ?>images/vision/second<?= $vision->id ?>.<?= $vision->image1 ?>" src="<?= Yii::$app->homeUrl ?>images/vision/second<?= $vision->id ?>.<?= $vision->image1 ?>" alt="<?= $vision->image1_alt ?>">
                            <div class="shine"></div>
                        </div>
                        <div class="v_m-ImgBox2">
                            <img class="lazy" loading="lazy" width="310" height="240" data-src="<?= Yii::$app->homeUrl ?>images/vision/third<?= $vision->id ?>.<?= $vision->image2 ?>" src="<?= Yii::$app->homeUrl ?>images/vision/third<?= $vision->id ?>.<?= $vision->image2 ?>" alt="<?= $vision->image2_alt ?>">
                            <div class="shine"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="our_values">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/about/abt<?= $about->id ?>.<?= $about->image ?>" src="<?= Yii::$app->homeUrl ?>images/about/abt<?= $about->id ?>.<?= $about->image ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?=$about->main_title?></div>
                            <div class="value_DtL">
                               <?=$about->common_description?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/about']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/our-message/chc<?= $our_message->id ?>.<?= $our_message->image2 ?>" src="<?= Yii::$app->homeUrl ?>images/our-message/chc<?= $our_message->id ?>.<?= $our_message->image2 ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?= $our_message->common_title ?></div>
                            <div class="value_DtL">
                                <?= $our_message->common_description ?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/ourmessage']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up">
                    <div class="value_box">
                        <div class="imG-Box">
                            <img class="lazy" loading="lazy" width="440" height="310" data-src="<?= Yii::$app->homeUrl ?>images/core-values/common<?= $core_values->id ?>.<?= $core_values->image ?>" src="<?= Yii::$app->homeUrl ?>images/core-values/common<?= $core_values->id ?>.<?= $core_values->image ?>">
                        </div>
                        <div class="CnT_Box">
                            <div class="value_TtL"><?= $core_values->common_title ?></div>
                            <div class="value_DtL">
                               <?= $core_values->common_description ?>
                            </div>
                            <div class="value_Btn">
                                <a href="<?= Url::to(['site/corevalues']); ?>" class="btn-block">
                                    <span class="KnowMore_Btn">Know More</span>
                                    <svg viewBox="0 0 31.219 31.219">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40" d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z" transform="translate(39.636 39.594) rotate(-180)"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</div>

