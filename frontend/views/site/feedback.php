<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="feedbackPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">




<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                    <img class="lazy innerBg jarallax-img" src="assets/images/careerPgBnr.jpg"
            data-src="assets/images/careerPgBnr.jpg" alt="bannerImage" width="1920" height="590">

        <?php
    }
    ?> 



       



        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="fbckSec">
        <div class="designBxElemntOuter lft">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="designBxElemntOuter btmLft">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="designBxElemntOuter topRgt">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="designBxElemntOuter rgt">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="container">
            <div class="queFormWrap">
                <div class="formOuter">
                         <form action="#" class="customForm">
                        <fieldset>
                            <h4 class="formTle">Let Us Know<br> Your Feedback</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" name="name" pattern="[a-zA-Z\s]+" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Xyz@gmail.com /Xyz@gmail.in" required>
                            </div>
                                 <div class="form-group">
                                        <select class="form-select form-control" id="district" name="district_id"
                                            required="">
                                            <option selected disabled value="">Select Your District</option>
                                            <?php if(count($districts) > 0): ?>
                                            <?php foreach($districts as $district): ?>
                                            <option value="<?= $district->id ?>"><?= $district->name ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group panchayat" style="display: none;">
                                        <select class="form-select form-control" id="panchayat" name="panchayat_id">
                                            <option selected disabled value="">Select Your Panchayat</option>
                                        </select>
                                    </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" nkeypress="return onlyNumberKey(event)" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Feedback" id="message" name="message" style="height: 80px" required></textarea>
                            </div>
                            <div class="btnWrap">
                                <button type="submit" class="btnSbmt hoveranim"><span>submit</span></button>
                                <div class="testmsg"></div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="contntImg jarallax" data-jarallax data-type="scroll">
                    <img class="lazy jarallax-img" src="assets/images/fbckFormImg.jpg" data-src="assets/images/fbckFormImg.jpg"
                        alt="feedback form" width="720" height="620">
                </div>
            </div>
    </section>

</div>


<script>
    $(document).ready(function () {




        
        $(document).on('submit', '.customForm', function (e) {
          
            e.preventDefault();
            var str = $(this).serialize();
              console.log(str);
            $.ajax({
                type: "POST",
                url: '<?= Yii::$app->homeUrl; ?>ajax/feedback',
                data: str,
                success: function (data)
                {

                    if (data == 1) {

                        
                      
                    $('.testmsg').html('<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>');
                    }
                     $('#name').val("");
                    $('#email').val("");
                     $('#mobile').val("");
                    $('#message').val("");
                    $('#district').val("");
                    $('#panchayat').val("");
                    
                    
                    
                    setTimeout(function () {
                        $('#email-alert').remove();
                    }, 4000);
                }
            });
           

        });
       
    });
       function onlyNumberKey(evt) {
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
        }
</script>
<script>
$(document).on('change', '#district', function(e) {
    $('#panchayat').empty().append(
        '<option selected disabled value="">Select Your Panchayat</option>')
    $('.panchayat').hide()
    $('#schedulingappointments-other').val('')
    $('.otherLocation').hide()
    $('#schedulingappointments-other').attr('required', false)
    $('#panchayat').attr('required', false)

    var district_id = $(this).val();
    if (district_id == 7 || district_id == 8) {
        $.ajax({
            type: "POST",
            url: '<?= yii::$app->homeUrl ?>ajax/get-panchayat',
            data: {
                id: district_id
            },
            dataType: 'json',
            success: function(data) {
                var panchayats = data.panchayats
                if (panchayats) {
                    $('#panchayat').empty().append(
                        '<option selected disabled value="">Select Your Panchayat</option>')
                    $.each(panchayats, function(key, value) {
                        $('#panchayat').append(
                            '<option value="' +
                            value.id + '">' + value.name + '</option>');
                    });
                    $('.panchayat').show()
                    $('#panchayat').attr('required', true)
                } else {
                    return false;
                }
            }
        });
    } else if (district_id == 15) {
        $('.otherLocation').show()
        $('#schedulingappointments-other').attr('required', true)
    }
});

function onlyNumberKey(evt) {
    // Only ASCII charactar in that range allowed 
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
$(document).ready(function() {
    $("#schedulingappointments-name").keypress(function(event) {
        var inputValue = event.which;
        if ((inputValue > 47 && inputValue < 58)) {
            event.preventDefault();
        }
    });
    var flag = <?= $flag ?>;

    if (flag == 1) {
        $('.testmsg').html(
            '<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'
        );
    }
    setTimeout(function() {
        $('#email-alert').remove();
    }, 4000);
});
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}
</script>
