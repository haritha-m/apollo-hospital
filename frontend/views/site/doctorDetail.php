<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}
    $service = \common\models\Services::find()->where(['id' => $doctor->department])->one();

?><div id="pageWrapper" class="doctorsPage">
    <section id="innerSmBnr" class="jarallax" data-jarallax data-type="scroll">
        <img class="lazy innerBg jarallax-img" src="<?= Yii::$app->homeUrl ?>assets/images/doctorDetailBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/doctorDetailBnr.jpg" loading="lazy" alt="bannerImage" width="1920" height="200">
    </section>

    <section id="doctorDtlsSec">
        <div class="designBxElemntOuter lft">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="designBxElemntOuter rgt">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="lftWrap col-xl-4 col-lg-4">
                    <div class="bookAppoiWrap">
                        <div class="cImg">
                            <img class="lazy" src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doctor->id ?>.<?= $doctor->image ?>" data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doctor->id ?>.<?= $doctor->image ?>"
                                loading="lazy" alt="doctor" width="340" height="390">
                        </div>
                        <div class="btnWrap">


                            <!--<button type="button" class="btnAppoi hoveranim"><span>Book an Appointment</span></button>-->


<a href="<?= $doctor->appointment_link ?>" class="btnAppoi hoveranim" target="_blank"><span>Book An Appointment</span></a>



                        </div>
                    </div>
                </div>
                <div class="rgtWrap col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="cntWrap">
                        <div class="topSec" data-aos="fade-up">
                            <h2 class="dName"><?= $doctor->name ?></h2>
                            <h6 class="dDprt"><?= $service->title?></h6>
                           <!-- <p class="dDisc">Etiam sapien sem magna at vitae pulvinar congue augue egestas pretium neque
                                id viverra suscipit egestas meagna porta ratione mollis risus lectus porta rutrum arcu
                                aenean primis in augue luc luctus neque purus ipsum neque dolor primis suscipit in magna
                                dignissim orttitor hendrerit diamIn at mauris vel nisl convallis porta at vitae dui. Nam
                                lacus ligula, vulputate molestie</p>-->
                                <?= $doctor->detail_description ?>
                        </div>
                        <div class="accordion accordion-flush accoWrap" id="docAccordion" data-aos="fade-up">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Fellowship and Certification
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="headingOne" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <?= $doctor->fellowship_description ?>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                       Expertise
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                    data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <?= $doctor->expertise_description ?>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Languages Known
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse"
                                    aria-labelledby="headingThree" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                       <?= $doctor->languages_description ?>
                                    </div>
                                </div>
                            </div>
                        </div>

<?php
if(!empty($gallery))
{
    ?>
                        <div class="vdoSec" data-aos="fade-up">
                            <h4 class="secTle">Videos</h4>
                            <div class="row">
                                <?php
                                foreach($gallery as $gal)
                                {
                                ?>

                                <div class="col-md-4 col-sm-6 col-12" data-aos="fade-up">
                                    <a href="<?= $gal->video ?>" class="docVdoCard" data-fancybox="videos">
                                        <div class="vdoThumb">
                                            <img class="lazy" loading="lazy" width="355" height="355"
                                                data-src="http://img.youtube.com/vi/<?= $gal->image; ?>/hqdefault.jpg"
                                                src="http://img.youtube.com/vi/<?= $gal->image; ?>/hqdefault.jpg">
                                            <div class="video-icon">
                                                <svg viewBox="0 0 30.902 21.637">
                                                    <g id="youtube_1_" data-name="youtube (1)"
                                                        transform="translate(0 0)">
                                                        <path id="Path_992" data-name="Path 992"
                                                            d="M30.266,3.386A3.872,3.872,0,0,0,27.542.662C25.124,0,15.451,0,15.451,0S5.778,0,3.36.637A3.951,3.951,0,0,0,.636,3.386,40.8,40.8,0,0,0,0,10.818a40.646,40.646,0,0,0,.636,7.433A3.872,3.872,0,0,0,3.36,20.975c2.443.662,12.091.662,12.091.662s9.673,0,12.091-.637a3.872,3.872,0,0,0,2.724-2.724,40.8,40.8,0,0,0,.636-7.433,38.723,38.723,0,0,0-.637-7.458Zm0,0"
                                                            fill="red" />
                                                        <path id="Path_993" data-name="Path 993"
                                                            d="M204.969,111.75l8.044-4.633-8.044-4.633Zm0,0"
                                                            transform="translate(-192.598 -96.299)" fill="#fff" />
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="vdoCnt">
                                            <h6 class="vTle"><?= $gal->title?></h6>
                                        </div>
                                    </a>
                                </div>
                               <?php
                           }
                           ?>
                            </div>
                        </div>
<?php


}
?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

    <!-- fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


