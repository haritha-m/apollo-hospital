<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="doctorsPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
        <img class="lazy innerBg jarallax-img" src="assets/images/newsMediaBg.jpg" data-src="assets/images/newsMediaBg.jpg"
            loading="lazy" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd">Meet the Doctors</h1>
                        <h3 class="bnrSubHd">These are the words that form the cornerstone of India’s most advanced
                            medical institution</h3>
                    </div>
                    <div class="col-lg-6">
                       

                         <form action="search" method="GET" class="searchForm">

                           
                              <input type="text" name="search" id="search" placeholder="Search Doctor Department Wise"
            class="form-control"  value="<?= $search?>"required>


                            <button type="submit" class="searchBtn">
                                <svg viewBox="0 0 19.225 20.268">
                                    <path id="search_FILL0_wght200_GRAD0_opsz48"
                                        d="M25.9,27.818,22.763,23.6a8.251,8.251,0,0,1-2.8,1.649,9.513,9.513,0,0,1-3.2.563,8.877,8.877,0,0,1-6.5-2.654A8.777,8.777,0,0,1,7.6,16.68a9.064,9.064,0,0,1,9.13-9.13,8.877,8.877,0,0,1,6.5,2.654A8.777,8.777,0,0,1,25.9,16.68a9.328,9.328,0,0,1-.583,3.238,8.523,8.523,0,0,1-1.669,2.8l3.177,4.18Zm-9.13-3.255A7.689,7.689,0,0,0,22.4,22.29a7.588,7.588,0,0,0,2.292-5.611A7.829,7.829,0,0,0,16.77,8.757,7.829,7.829,0,0,0,8.847,16.68a7.588,7.588,0,0,0,2.292,5.611A7.689,7.689,0,0,0,16.77,24.563Z"
                                        transform="translate(-7.6 -7.55)" />
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="doctorsSec" class="inrCntSec">
        <div class="container">
            <?php
            if(!empty($department))
            {
                foreach($department as $dept)
                {
                    
                    if(empty($doctors)){
                       $doctors = \common\models\Doctors::find()->where(['status' => 1,'department'=>$dept->id])->orderBy(['sort_order' => SORT_DESC])->all();
                    }
                    if(!empty($doctors))
                    {
                    ?>

            <div class="row docCarouselSec">
                <div class="col-12">
                    <div class="secTitleWrap" data-aos="fade-up">
                        <h2 class="secTitle"><?= $dept->title?></h2>
                    </div>
                </div>

                <div class="col-12" data-aos="fade-up">
                    <div class="owl-carousel docCarousel owl-theme">
                        <?php
                        foreach($doctors as  $doc)
                        {
                             if($doc->department == $dept->id)
                    {
                            ?>

                        <div class="item">
                            <div class="docCard">
                                <div class="cImg">
                                    <img class="lazy" src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>"
                                        data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>" loading="lazy" alt="doctor" width="340"
                                        height="390">
                                </div>
                                <div class="cCnt">
                                    <h4 class="dName"><?= $doc->name?></h4>
                                    <h6 class="dDprt"><?= $dept->title?></h6>
                                    <p class="dDisc"><?= $doc->small_description?></p>
                                </div>
                                <div class="cBttm">
                                    <div class="lftBtn">
                                        <a href="<?= Url::to(['site/doctordetails', 'name' => $doc->canonical_name]); ?>" class="docBtn hoveranim"><span>Profile</span></a>
                                    </div>
                                    <div class="rgtBtn">
                                        <a href="<?= $doc->appointment_link ?>" class="bookBtn docBtn hoveranim" target="_blank"><span>Book An Appointment</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }

}
?>
                      
                    </div>
                </div>
            </div>

<?php
}
}
}
?>

           
        </div>
    </section>

</div>

    <!-- OWL_SLIDER -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
        integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $('.docCarousel').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2,
                loop: $('.item img').lenght > 2 ? true : false,
            },
            768: {
                items: 3,
                loop: $('.item img').lenght > 3 ? true : false,
            },
            1000: {
                items: 4,
                margin: 15,
                loop: $('.item img').lenght > 4 ? true : false,
            },
            1200: {
                items: 4,
                margin: 30,
                loop: $('.item img').lenght > 4 ? true : false,
            }
        }
    })
    </script>

