<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="insurancePage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
        <img class="lazy innerBg jarallax-img" src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="insurcCntSec">
        <div class="designBxElemntOuter lft">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="designBxElemntOuter rgt">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="lftWrap col-xl-7 col-lg-7 col-12" data-aos="fade-up">
                    <div class="contentBx">
                        <div class="secTitleWrap">
                            <h2 class="secTitle"><?= $insurance->title?></h2>
                        </div>
                       <?= $insurance->description?>
                    </div>
                </div>
                <div class="rgtWrap col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12" data-aos="fade-up">
                    <div class="contntImg">
                        <img class="lazy" src="<?= Yii::$app->homeUrl ?>images/insurance/common<?= $insurance->id ?>.<?= $insurance->image ?>"
                            data-src="<?= Yii::$app->homeUrl ?>images/insurance/common<?= $insurance->id ?>.<?= $insurance->image ?>" alt="<?= $insurance->image_alt ?>" width="800" height="600">
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
if(!empty($partners))
{
    ?>
    <section id="insurcPrtnrSec">
        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-up">
                    <div class="secTitleWrap center">
                        <h2 class="secTitle">Insurance Partners</h2>
                    </div>
                </div>
                <div class="col-12">
                    <div class="insurcPrtnrCarousel">

<?php
foreach($partners as $partner)
{
    ?>

                        <div class="item" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?= Yii::$app->homeUrl ?>images/insurance/partners/partner<?= $partner->id ?>.<?= $partner->image ?>" class="lazy" data-src="<?= Yii::$app->homeUrl ?>images/insurance/partners/partner<?= $partner->id ?>.<?= $partner->image ?>" width="260" height="50" alt="<?= $partner->image_alt ?>">
                        </div>
                     
<?php

}
?>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php
}
?>
</div>

