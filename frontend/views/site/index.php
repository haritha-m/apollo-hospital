<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="homePage">
    <section id="MainBanner">
        <a href="#welSec" class="Scroll">
            <div class="ScrollWrap">
                <div class="Txt">Scroll</div>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                    <path d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                        transform="translate(39.636 39.594) rotate(-180)" />
                </svg>
            </div>
        </a>
        <!--<div id="bannerCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel">-->
        <div id="bannerCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <div class="container">
                    <?php
                    if(!empty($sliders))
                    {
                        $i=0;
                        $j=0;
                     
                        foreach($sliders as $slider)
                        {
                            $i++;
                            ?>
                    <button type="button" data-bs-target="#bannerCarousel" data-bs-slide-to="<?=$j?>"
                        class="<?= $i == 1 ? 'active' : '' ?>" aria-current="<?= $i == 1 ? 'true' : '' ?>"
                        aria-label="Slide <?=$i?>"></button>

                    <?php
                   $j++;
               }
           }
           ?>


                </div>
            </div>
            <div class="carousel-inner">
                <?php
                    if(!empty($sliders))
                    {
                        $i=0;
                     
                        foreach($sliders as $slider)
                        {
                            $i++;
                            ?>

                <a href="<?=$slider->link ?>">
                    <div class="carousel-item <?= $i == 1 ? 'active' : '' ?>">
                        <!--<img loading="lazy"-->
                        <!--    src="<?= Yii::$app->homeUrl ?>images/sliders/slider<?= $slider->id ?>.<?= $slider->image ?>"-->
                        <!--    data-src="<?= Yii::$app->homeUrl ?>images/sliders/slider<?= $slider->id ?>.<?= $slider->image ?>"-->
                        <!--    class="bnrSldImg lazy" alt="<?= $slider->image_alt ?>">-->
                        <picture>
                            <source media="(max-width: 768px)" data-srcset="<?= Yii::$app->homeUrl ?>images/sliders/<?= $slider->image1 ?> 2x" />
                            <img width="1920" height="720" alt="banner" loading="lazy" class="lazy bnrSldImg" src="<?= Yii::$app->homeUrl ?>images/sliders/<?= $slider->image ?>" />
                        </picture>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1 class="bnrMainHd"><?= $slider->title?></h1>
                                <h4 class="bnrSubHd"><?= $slider->description?></h4>
                            </div>
                        </div>
                    </div>
                </a>
                <?php
          }
      }
      ?>

            </div>
        </div>
    </section>

    <section id="welSec">
        <div class="container">
            <div class="FlxTitleWrp">
                <div class="secLogoWrap">
                    <a href="javascript:void(0)">
                        <img src="<?= Yii::$app->homeUrl ?>assets/images/logoColor.png" alt="logo" width="280" height="143" />
                    </a>
                </div>
                <div class="FlxMainTitle" data-aos="fade-up"><?=$about->title?></div>
                <div class="FlxTitle">
                    <div class="TitleRow" data-aos="fade-up">
                        <div class="secTitleWrap">
                            <div class="secTitle">
                                <?=$about->sub_title?>
                            </div>
                        </div>
                        <div class="secLogoWrap">
                            <a href="javascript:void(0)">
                                <img src="<?= Yii::$app->homeUrl ?>assets/images/logoColor.png" alt="logo" width="280" height="143" />
                            </a>
                        </div>
                    </div>
                    <div class="InfoWrp" data-aos="fade-up" data-aos-delay="200">
                        <?=$about->description?>
                    </div>
                    <div class="BtnWrp" data-aos="fade-up" data-aos-delay="400">
                        <a href="<?= Url::to(['site/about']); ?>" class="MoreBtn">
                            <div class="Txt">Know More story</div>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                                <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                    d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                    transform="translate(54.108 12.508) rotate(120)" />
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="AbtVideoSec" data-aos="fade-up">
        <div class="container">
            <div class="VidPoster jarallax" data-jarallax data-type="scroll">
                <img loading="lazy"
                    src="<?= Yii::$app->homeUrl ?>images/about/homethumb<?= $about->id ?>.<?= $about->image ?>" 
                    class="bnrSldImg lazy jarallax-img" alt="<?= $about->image_alt ?>">
                <div class="BoxFooter">
                    <a href="<?= Yii::$app->homeUrl ?>images/about/homevideo<?= $about->id ?>.<?= $about->video ?>"
                        data-fancybox="OurVideo" class="PlayBtn">
                        <div class="Round">
                            <div class="Icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65.454 65.454">
                                    <g fill="none" stroke="#fff" stroke-width="1">
                                        <circle cx="32.727" cy="32.727" r="32.727" stroke="none" />
                                        <circle cx="32.727" cy="32.727" r="32.227" fill="none" />
                                    </g>
                                    <path d="M8.794,0l8.794,14.757H0Z" transform="translate(42.18 23.937) rotate(90)"
                                        fill="#ffbe00" />
                                </svg>
                            </div>
                        </div>
                        Watch Our Video
                    </a>
                    <div class="CounterGrid">
                        <div class="Itm">
                            <div class="CountFlx">
                                <div class="count" data-count="<?= $about->count1 ?>">0</div>
                                +
                            </div>
                            <div class="Txt"><?= $about->count1_title ?></div>
                        </div>
                        <div class="Itm">
                            <div class="CountFlx">
                                <div class="count" data-count="<?= $about->count2 ?>">0</div>
                                +
                            </div>
                            <div class="Txt"><?= $about->count2_title ?></div>
                        </div>
                        <div class="Itm">
                            <div class="CountFlx">
                                <div class="count" data-count="<?= $about->count3 ?>">0</div>
                                +
                            </div>
                            <div class="Txt"><?= $about->count3_title ?></div>
                        </div>
                        <div class="Itm">
                            <div class="CountFlx">
                                <?= $about->count4 ?>
                            </div>
                            <div class="Txt"><?= $about->count4_title ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="OurQuality">
        <div class="container">
            <div class="secTitleWrap center" data-aos="fade-up" data-aos-delay="200">
                <div class="secTitle"><?= $common_content->medical_care_quality_title ?></div>
                <div class="secSubTitle"><?= $common_content->medical_care_quality_sub_title ?></div>
            </div>
            <div class="QualityFlx">
                <div class="GridItm lft" data-aos="fade-right">
                    <div class="GridFlx">
                        <?php
                        if(!empty($medical_care))
                        {
                            foreach($medical_care as $medical)
                            {
                                ?>


                        <div class="Box">
                            <div class="Icon">
                                <img loading="lazy"
                                    src="<?= Yii::$app->homeUrl ?>images/home/media-quality/image<?= $medical->id ?>.<?= $medical->image ?>" 
                                    class="lazy" width="60" height="60" alt="<?= $medical->image_alt ?>">
                            </div>
                            <div class="Title">
                                <?= $medical->title?>
                            </div>
                            <div class="Info">
                                <?= $medical->description?>
                            </div>
                        </div>
                        <?php
                      }
                  }
                  ?>



                    </div>
                </div>
                <div class="GridItm ImgGrid" data-aos="zoom-in">
                    <svg class="GridShape Shape1" viewBox="0 0 120 120">
                        <g transform="translate(-1353 -1850)">
                            <rect id="Rectangle1" data-name="Rectangle 9" width="60" height="60"
                                transform="translate(1353 1850)" />
                            <rect id="Rectangle2" data-name="Rectangle 13" width="60" height="60"
                                transform="translate(1413 1910)" />
                        </g>
                    </svg>
                    <svg class="GridShape Shape2" viewBox="0 0 120 120">
                        <g transform="translate(-1353 -1850)">
                            <rect id="Rectangle1" data-name="Rectangle 9" width="60" height="60"
                                transform="translate(1353 1850)" />
                            <rect id="Rectangle2" data-name="Rectangle 13" width="60" height="60"
                                transform="translate(1413 1910)" />
                        </g>
                    </svg>

                    <img loading="lazy"
                        src="<?= Yii::$app->homeUrl ?>images/common-content/medical<?= $common_content->id ?>.<?= $common_content->image ?>" 
                        class="bnrSldImg lazy" width="906" height="604" alt="<?= $common_content->image_alt ?>">
                </div>
                <div class="GridItm rit" data-aos="fade-left" data-aos-delay="400">
                    <div class="GridFlx">

                        <?php
                        if(!empty($medical_care1))
                        {
                            foreach($medical_care1 as $medical)
                            {
                                ?>


                        <div class="Box">
                            <div class="Icon">
                                <img loading="lazy"
                                    src="<?= Yii::$app->homeUrl ?>images/home/media-quality/image<?= $medical->id ?>.<?= $medical->image ?>" 
                                    class="lazy" width="60" height="60" alt="<?= $medical->image_alt ?>">
                            </div>
                            <div class="Title">
                                <?= $medical->title?>
                            </div>
                            <div class="Info">
                                <?= $medical->description?>
                            </div>
                        </div>
                        <?php
}
}
?>




                    </div>
                </div>
            </div>
        </div>
    </section>

<section id="OurDepartments">
        <div class="container">
            <div class="FlxTitleWrp">
                <?php if(!empty($common_content->department_title)) : ?>
                <div class="FlxMainTitle" data-aos="fade-up"><?= $common_content->department_title ?></div>
                <?php endif; ?>
                <div class="FlxTitle">
                    <div class="TitleRow" data-aos="fade-up">
                        <div class="secTitleWrap">
                            <div class="secTitle">
                                <?= $common_content->department_sub_title ?>
                            </div>
                            <div class="BtnWrp">
                                <a href="<?= Url::to(['site/services', 'slug' => $category->canonical_name]) ?>" class="MoreBtn">
                                    <div class="Txt">All Our Centres Of Excellence</div>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                            d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                            transform="translate(54.108 12.508) rotate(120)" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="InfoWrp" data-aos="fade-up" data-aos-delay="200">
                        <p>
                            <?= $common_content->department_sub_description ?>
                        </p>
                    </div>
                    <div class="BtnWrp" data-aos="fade-up" data-aos-delay="400">
                        <div class="Navigation lft">
                            <div class="OwlPrev arrows DepartmentSlideNav">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                                    <path
                                        d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />
                                </svg>
                                Previous
                            </div>
                            <div class="OwlNext arrows DepartmentSlideNav">
                                Next
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                                    <path
                                        d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                        transform="translate(39.636 39.594) rotate(-180)" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sp-container rigt" data-aos="fade-up">
            <div class="DepartmentSlide owl-carousel">
                
                        <?php
                                
                                    $services =  \common\models\Services::find()->where(['category_id'=> 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

                                    if(isset($services)){ 

                                        foreach($services as $service){
                                     ?>

                <a href="<?= Url::to(['site/servicedetails', 'name' => $service->canonical_name]); ?>"
                    class="DepartmentBox">


                    <div class="ImgBox">
                        <img loading="lazy"
                            src="<?= Yii::$app->homeUrl ?>images/services/image<?= $service->id ?>.<?= $service->image ?>" 
                            class="lazy" width="440" height="218" alt="<?= $service->image_alt ?>">
                    </div>
                    <div class="CntnBox">
                        <div class="ServTitle"><?= $service->title?></div>
                        <div class="ServInfo">
                            <p>
                                <?= $service->small_description?>
                            </p>
                        </div>
                    </div>
                </a>
                <?php
         }
        }
     
     ?>



            </div>
            <!--<div class="Navigation lft">-->
            <!--    <div class="OwlPrev arrows DepartmentSlideNav">-->
            <!--        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">-->
            <!--            <path-->
            <!--                d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />-->
            <!--        </svg>-->
            <!--        Previous-->
            <!--    </div>-->
            <!--    <div class="OwlNext arrows DepartmentSlideNav">-->
            <!--        Next-->
            <!--        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">-->
            <!--            <path-->
            <!--                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"-->
            <!--                transform="translate(39.636 39.594) rotate(-180)" />-->
            <!--        </svg>-->
            <!--    </div>-->
            <!--</div>-->
        </div>
    </section>
    
     <section id="OurDepartments">
        <div class="container">
            <div class="FlxTitleWrp">
                <?php if(!empty($common_content->specialities_title)) : ?>
                <div class="FlxMainTitle" data-aos="fade-up"><?= $common_content->specialities_title ?></div>
                <?php endif; ?>
                <div class="FlxTitle">
                    <div class="TitleRow" data-aos="fade-up">
                        <div class="secTitleWrap">
                            <div class="secTitle">
                                <?= $common_content->specialities_sub_title ?>
                            </div>
                            <div class="BtnWrp">
                                <a href="<?= Url::to(['site/services', 'slug' => $service_categories->canonical_name]) ?>" class="MoreBtn">
                                    <div class="Txt">All Other Specialities</div>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                            d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                            transform="translate(54.108 12.508) rotate(120)" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="InfoWrp" data-aos="fade-up" data-aos-delay="200">
                        <p>
                            <?= $common_content->specialities_description ?>
                        </p>
                    </div>
                    <div class="BtnWrp" data-aos="fade-up" data-aos-delay="400">
                        <div class="Navigation lft">
                            <div class="OwlPrev arrows SpecialitiesSlideNav">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                                    <path
                                        d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />
                                </svg>
                                Previous
                            </div>
                            <div class="OwlNext arrows SpecialitiesSlideNav">
                                Next
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                                    <path
                                        d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                        transform="translate(39.636 39.594) rotate(-180)" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sp-container rigt" data-aos="fade-up">
            <div class="SpecialitiesSlide owl-carousel">
                
                        <?php
                                
                                    $services =  \common\models\Services::find()->where(['category_id'=> 2])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

                                    if(isset($services)){ 

                                        foreach($services as $service){
                                     ?>

                <a href="<?= Url::to(['site/servicedetails', 'name' => $service->canonical_name]); ?>"
                    class="DepartmentBox">


                    <div class="ImgBox">
                        <img loading="lazy"
                            src="<?= Yii::$app->homeUrl ?>images/services/image<?= $service->id ?>.<?= $service->image ?>" 
                            class="lazy" width="440" height="218" alt="<?= $service->image_alt ?>">
                    </div>
                    <div class="CntnBox">
                        <div class="ServTitle"><?= $service->title?></div>
                        <div class="ServInfo">
                            <p>
                                <?= $service->small_description?>
                            </p>
                        </div>
                    </div>
                </a>
                <?php
         }
        }
     
     ?>



            </div>
            <!--<div class="Navigation lft">-->
            <!--    <div class="OwlPrev arrows DepartmentSlideNav">-->
            <!--        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">-->
            <!--            <path-->
            <!--                d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />-->
            <!--        </svg>-->
            <!--        Previous-->
            <!--    </div>-->
            <!--    <div class="OwlNext arrows DepartmentSlideNav">-->
            <!--        Next-->
            <!--        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">-->
            <!--            <path-->
            <!--                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"-->
            <!--                transform="translate(39.636 39.594) rotate(-180)" />-->
            <!--        </svg>-->
            <!--    </div>-->
            <!--</div>-->
        </div>
    </section>

    <section id="OurFeatures">
        <div class="container">
            <div class="FeaturesSlide owl-carousel">
                <a href="<?= Url::to(['site/visitors']); ?>" class="FeaturesBox Bg1" data-aos="fade-up">
                    <div class="IconWrp">
                        <svg id="Arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)"></path>
                        </svg>
                        <div class="Icon">
                            <img loading="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/home/visitors<?= $visitors->id ?>.<?= $visitors->image ?>" 
                                class="lazy" width="75" height="75" alt="<?= $visitors->image_alt ?>">
                        </div>
                    </div>
                    <div class="CntntWrp">
                        <div class="Title"><?= $visitors->home_title?></div>
                        <div class="Info">
                            <?= $visitors->home_description?>
                        </div>
                    </div>
                </a>
                <a href="<?= Url::to(['site/quality']); ?>" class="FeaturesBox Bg2" data-aos="fade-up"
                    data-aos-delay="200">
                    <div class="IconWrp">
                        <svg id="Arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)"></path>
                        </svg>
                        <div class="Icon">
                            <img loading="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/home/safety<?= $visitors->id ?>.<?= $visitors->image ?>" 
                                class="lazy" width="75" height="75" alt="<?= $visitors->image_alt ?>">
                        </div>
                    </div>
                    <div class="CntntWrp">
                        <div class="Title"><?= $quality->home_title?></div>
                        <div class="Info">
                            <?= $quality->home_description?>
                        </div>
                    </div>
                </a>
                <a href="<?= Url::to(['site/international']); ?>" class="FeaturesBox Bg3" data-aos="fade-up"
                    data-aos-delay="400">
                    <div class="IconWrp">
                        <svg id="Arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)"></path>
                        </svg>
                        <div class="Icon">
                            <img loading="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/common-content/visitors<?= $common_content->id ?>.<?= $common_content->image2 ?>" 
                                class="lazy" width="75" height="75" alt="<?= $common_content->image2_alt ?>">
                        </div>
                    </div>
                    <div class="CntntWrp">
                        <div class="Title"><?= $common_content->home_title?></div>
                        <div class="Info">
                            <?= $common_content->home_description?>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <section id="PackagesSec">
        <div class="container">
            <div class="secTitleWrap center" data-aos="fade-up">
                <div class="secTitle">Packages & Offers</div>
            </div>
            <div class="PackagesSlide owl-carousel" data-aos="fade-up">
                <?php
                if(!empty($offers))
                {
                    foreach($offers as $offer)
                    {
                        ?>



                <a href="<?= Url::to(['site/offerdetails', 'name' => $offer->canonical_name]); ?>" class="PackageBox">
                    <div class="ImgBox">
                        <img loading="lazy"
                            src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $offer->id ?>.<?= $offer->image ?>" 
                            class="lazy" width="75" height="75" alt="<?= $offer->image_alt ?>">
                    </div>
                    <div class="Info">
                        <?= $offer->title?>
                    </div>
                </a>
                <?php
            }
        }
        if (!empty($packages)) {
                    foreach ($packages as $package) {
                        ?>



                <a href="<?= Url::to(['site/packagedetails', 'name' => $package->canonical_name]); ?>"
                    class="PackageBox">
                    <div class="ImgBox">
                        <img loading="lazy"
                            src="<?= Yii::$app->homeUrl ?>images/packages/image<?= $package->id ?>.<?= $package->image ?>" 
                            class="lazy" width="75" height="75" alt="package">
                    </div>
                    <div class="Info">
                        <?= $package->small_description ?>
                    </div>
                </a>
                <?php
                    }
                }
        ?>


            </div>
            <div class="Navigation" data-aos="fade-up" data-aos-delay="400">
                <div class="OwlPrev arrows PackagesSlideNav">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                        <path
                            d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />
                    </svg>
                    Previous
                </div>
                <div class="OwlNext arrows PackagesSlideNav">
                    Next
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                        <path
                            d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                            transform="translate(39.636 39.594) rotate(-180)" />
                    </svg>
                </div>
            </div>
        </div>
    </section>

    <section id="HappyToHelp" class="jarallax" data-jarallax data-type="scroll">
        <img alt="Happy To Help image" width="1920" height="1028" loading="lazy" class="lazy jarallax-img"
            src="<?= Yii::$app->homeUrl ?>images/common-content/nation<?= $common_content->id ?>.<?= $common_content->image1 ?>">
        <div class="container">
            <div class="CntnBoxWrpr">
                <div class="TitleWrpr" data-aos="fade-left">
                    <div class="Title">
                        <?= $common_content->hospital_nation_title ?>
                    </div>
                    <div class="Tagline"><?= $common_content->hospital_nation_sub_title ?></div>
                </div>
                <div class="ActionWrpr">
                    <a href="<?= Url::to(['site/schedule']); ?>" class="Box Bg1" data-aos="fade-left">
                        <svg id="Arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)"></path>
                        </svg>
                        <div class="Txt">
                            Book an<br>
                            Appointment
                        </div>
                    </a>
                    <a href="<?= Url::to(['site/doctors']); ?>" class="Box Bg2" data-aos="fade-left"
                        data-aos-delay="200">
                        <svg id="Arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)"></path>
                        </svg>
                        <div class="Txt">
                            Find<br>
                            A Doctor
                        </div>
                    </a>
                    <a href="<?= Url::to(['site/new-doctors']); ?>" class="Box Bg3" data-aos="fade-left"
                        data-aos-delay="400">
                        <svg id="Arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)"></path>
                        </svg>
                        <div class="Txt">
                            Newly Joined Doctors
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section id="DualSec">
        <div class="container">
            <div class="FlxRow">
                <div class="lSide" data-aos="fade-up" data-aos-delay="400">
                    <div class="secTitleWrap">
                        <div class="secTitle">Events</div>
                    </div>
                    <div class="GalGrid">
                        <div class="GalRow GalRow1">
                            <?php
                            if(!empty($photo_gallery))
                            {
                                foreach($photo_gallery as $gallery)
                                {
                                    ?>

                            <div class="GalItm">
                                <a href="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $gallery->gallery_id ?>/photos/image<?=$gallery->id?>.<?= $gallery->image ?>"
                                    data-fancybox="MediaGall" class="GalBox">
                                    <img loading="lazy"
                                        src="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $gallery->gallery_id ?>/photos/image<?=$gallery->id?>.<?= $gallery->image ?>" 
                                        class="lazy" width="265" height="235" alt="<?= $gallery->image_alt ?>">
                                </a>
                            </div>

                            <?php
                          }}
                          ?>



                        </div>


                        <div class="GalRow GalRow2">
                            <div class="GalItm">
                                <a href="<?= Yii::$app->homeUrl ?>images/media-gallery/videos/video<?= $video_gallery->id ?>.<?= $video_gallery->video ?>"
                                    data-fancybox="videos" class="GalBox">
                                    <img loading="lazy"
                                        src="http://img.youtube.com/vi/<?= $video_gallery->image; ?>/hqdefault.jpg" 
                                        class="lazy" width="265" height="235" alt="<?= $video_gallery->image_alt ?>">
                                    <div class="video-icon">
                                        <svg viewBox="0 0 55.572 55.572">
                                            <g id="Group_209" data-name="Group 209">
                                                <g id="Group_208" data-name="Group 208">
                                                    <path id="Path_174" data-name="Path 174"
                                                        d="M27.786,0A27.786,27.786,0,1,0,55.572,27.786,27.817,27.817,0,0,0,27.786,0Zm0,53.257A25.471,25.471,0,1,1,53.257,27.786,25.5,25.5,0,0,1,27.786,53.257Z"
                                                        fill="#fff" />
                                                    <path id="Path_175" data-name="Path 175"
                                                        d="M204.294,156.577l-11.075-7.12a.791.791,0,0,0-1.219.665v14.239a.791.791,0,0,0,1.219.665l11.075-7.12a.791.791,0,0,0,0-1.33Z"
                                                        transform="translate(-168.227 -129.456)" fill="#fff" />
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                </a>
                            </div>



                            <div class="GalItm pad0">
                                <div class="GalItmRow">
                                    <?php
                                      if(!empty($photo_gallery1))
                            {
                                foreach($photo_gallery1 as $gallery)
                                {
                                    ?>

                                    <div class="GalItm">
                                        <a href="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $gallery->gallery_id ?>/photos/image<?= $gallery->id ?>.<?= $gallery->image ?>"
                                            data-fancybox="MediaGall" class="GalBox">
                                            <img loading="lazy"
                                                src="<?= Yii::$app->homeUrl ?>images/media-gallery/<?= $gallery->gallery_id ?>/photos/image<?= $gallery->id ?>.<?= $gallery->image ?>" 
                                                class="lazy" width="265" height="235"
                                                alt="<?= $video_gallery->image_alt ?>">
                                        </a>
                                    </div>

                                    <?php
                                   }
                               }
                               ?>


                                    <div class="GalItm">
                                        <a href="<?= Url::to(['site/media-gallery']); ?>" class="GalBtnBox">
                                            <svg id="Arrow" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 42.646 42.646">
                                                <path
                                                    d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                                    transform="translate(54.108 12.508) rotate(120)"></path>
                                            </svg>
                                            <div class="Txt">
                                                View all Events
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="RSide" data-aos="fade-up" data-aos-delay="400">
                    <div class="secTitleWrap">
                        <div class="secTitle">Case Study</div>
                    </div>
                    <div class="BlogSlide">
                        <div class="newsSlide">
                            <?php
                            if(!empty($news))
                            {
                                foreach($news as $data)
                                {
                                    ?>

                            <div class="item">
                                <a href="<?= Url::to(['site/newsdetails', 'name' => $data->canonical_name]); ?>"
                                    class="NewsBox">
                                    <div class="Lft_News">
                                        <div class="Img_Box">
                                            <img class="lazy" loading="lazy" width="190" height="140" 
                                                src="<?= Yii::$app->homeUrl ?>images/news/image<?= $data->id ?>.<?= $data->image ?>"
                                                alt="<?= $data->image_alt ?>">
                                        </div>
                                    </div>
                                    <div class="Rgt_News">
                                        <div class="SubTitle">
                                            <?= $data->title?>
                                        </div>
                                        <p class="SubDisc"> <?= $data->small_description?></p>
                                        <div class="News-flxbx">
                                            <div class="News-dtl-date">
                                                <?= date('M d, Y', strtotime($data->date)) ?></div>
                                            <div class="News-dtl-tag"># News</div>
                                        </div>
                                    </div>
                                </a>
                            </div>


                            <?php
                            }
                        }
                        ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      
    <?php if(!empty($media)) { ?>
    <section id="blogSec">
        <div class="container">
            <div class="secTitleWrap" data-aos="fade-up">
                <div class="secTitle">Blogs</div>
                <div class="Navigation" data-aos="fade-up" data-aos-delay="400">
                    <div class="OwlPrev arrows blogSlideNav">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                            <path
                                d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />
                        </svg>
                        Previous
                    </div>
                    <div class="OwlNext arrows blogSlideNav">
                        Next
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                            <path
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(39.636 39.594) rotate(-180)" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="blogSlide owl-carousel" data-aos="fade-up">
                <?php foreach($media as $data) { ?>
                <div class="item">
                    <a href="<?= Url::to(['site/medicaldetails', 'name' => $data->canonical_name]); ?>"
                        class="blog-box">
                        <div class="image_box">
                            <img class="lazy" loading="lazy" width="330" height="235"
                                src="<?= Yii::$app->homeUrl ?>images/media-blogs/image<?= $data->id ?>.<?= $data->image ?>" >
                        </div>
                        <div class="content_box">
                            <div class="title_box">
                                <span class="post-date"><?= date('M d, Y', strtotime($data->date)) ?></span>
                                <span class="post-ttl"># Blog</span>
                            </div>
                            <div class="details_sec">
                                <div class="post-title"><?= $data->title?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div>
            <div class="Navigation" data-aos="fade-up" data-aos-delay="400">
                <div class="OwlPrev arrows blogSlideNav">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                        <path
                            d="M15.581,0,0,15.581,15.581,31.219l.952-.952L2.466,16.254H31.219V14.965H2.466L16.534.9Z" />
                    </svg>
                    Previous
                </div>
                <div class="OwlNext arrows blogSlideNav">
                    Next
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.219 31.219">
                        <path
                            d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                            transform="translate(39.636 39.594) rotate(-180)" />
                    </svg>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>

    <!-- SCROLL_MAGIC -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/ScrollMagic.min.js"></script>

    <!-- touchSwipe -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>

    <!-- JARALLAX --->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jarallax/2.0.3/jarallax.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/2.0.3/jarallax.min.js"></script>

    <!-- OWL-->
    <link rel="stylesheet" type="text/css"
        href="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
    </script>

    <!-- SLICK-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>

    <!-- FANCYBOX-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
        integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
        integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
    $(".carousel").swipe({
        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
            if (direction == 'left') $(this).carousel('next');
            if (direction == 'right') $(this).carousel('prev');
        },
        allowPageScroll: "vertical"
    })

    // ABOUT SECTION
    var controller = new ScrollMagic.Controller();
    var scene = new ScrollMagic.Scene({
            triggerElement: '#AbtVideoSec',
            triggerHook: 0.8
        })
        .on("enter", function() {
            $('.count').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');

                $({
                    countNum: $this.text()
                }).animate({
                    countNum: countTo
                }, {

                    duration: 2000,
                    easing: 'linear',
                    step: function() {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        $this.text(this.countNum);
                        //alert('finished');
                    }

                });

            });

        })
        .reverse(false)
        .addTo(controller);

    // DEPARTMENTS SECTION
    $('.DepartmentSlide').owlCarousel({
        loop: true,
        rewind: true,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        responsiveClass: true,
        nav: false,
        dots: false,
        items: 1.2,
        margin: 10,
        responsive: {
            468: {
                items: 1.6,
                loop: $('.DepartmentSlide .item').length > 1 ? true : false,
                margin: 10,
            },
            576: {
                items: 2.2,
                loop: $('.DepartmentSlide .item').length > 2 ? true : false,
                margin: 10,
            },
            768: {
                items: 2.5,
                loop: $('.DepartmentSlide .item').length > 2 ? true : false,
                margin: 25,
            },
            992: {
                items: 3.2,
                loop: $('.DepartmentSlide .item').length > 3 ? true : false,
                margin: 30,
            },
            1200: {
                items: 3.5,
                loop: $('.DepartmentSlide .item').length > 3 ? true : false,
                margin: 35,
            },
            1551: {
                items: 3.5,
                loop: $('.DepartmentSlide .item').length > 3 ? true : false,
                margin: 55,
            }
        }
    });
    var DepartmentSlide = $('.DepartmentSlide');

    $('.DepartmentSlideNav.OwlNext').click(function() {
        DepartmentSlide.trigger('next.owl.carousel');
    });

    $('.DepartmentSlideNav.OwlPrev').click(function() {
        DepartmentSlide.trigger('prev.owl.carousel');
    });
    $('.SpecialitiesSlide').owlCarousel({
        loop: true,
        rewind: true,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        responsiveClass: true,
        nav: false,
        dots: false,
        items: 1.2,
        margin: 10,
        responsive: {
            468: {
                items: 1.6,
                loop: $('.SpecialitiesSlide .item').length > 1 ? true : false,
                margin: 10,
            },
            576: {
                items: 2.2,
                loop: $('.SpecialitiesSlide .item').length > 2 ? true : false,
                margin: 10,
            },
            768: {
                items: 2.5,
                loop: $('.SpecialitiesSlide .item').length > 2 ? true : false,
                margin: 25,
            },
            992: {
                items: 3.2,
                loop: $('.SpecialitiesSlide .item').length > 3 ? true : false,
                margin: 30,
            },
            1200: {
                items: 3.5,
                loop: $('.SpecialitiesSlide .item').length > 3 ? true : false,
                margin: 35,
            },
            1551: {
                items: 3.5,
                loop: $('.SpecialitiesSlide .item').length > 3 ? true : false,
                margin: 55,
            }
        }
    });
    var SpecialitiesSlide = $('.SpecialitiesSlide');

    $('.SpecialitiesSlideNav.OwlNext').click(function() {
        SpecialitiesSlide.trigger('next.owl.carousel');
    });

    $('.SpecialitiesSlideNav.OwlPrev').click(function() {
        SpecialitiesSlide.trigger('prev.owl.carousel');
    });

    // OUR FEATURES
    $('.FeaturesSlide').owlCarousel({
        loop: true,
        rewind: true,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        responsiveClass: true,
        nav: false,
        dots: false,
        items: 1.2,
        margin: 10,
        responsive: {
            468: {
                items: 1.6,
                loop: $('.FeaturesSlide .item').length > 1 ? true : false,
                margin: 10,
            },
            576: {
                items: 2.2,
                loop: $('.FeaturesSlide .item').length > 2 ? true : false,
                margin: 10,
            },
            768: {
                items: 2.2,
                loop: $('.FeaturesSlide .item').length > 2 ? true : false,
                margin: 20,
                autoplay: false,
            },
            880: {
                items: 3,
                loop: $('.FeaturesSlide .item').length > 3 ? true : false,
                margin: 20,
                autoplay: false,
            },
            992: {
                items: 3,
                loop: $('.FeaturesSlide .item').length > 3 ? true : false,
                margin: 30,
                autoplay: false,
            },
            1200: {
                items: 3,
                loop: $('.FeaturesSlide .item').length > 3 ? true : false,
                margin: 40,
                autoplay: false,
            }
        }
    });

    // OUR OFFERS & PACKAGES
    $('.PackagesSlide').owlCarousel({
        loop: true,
        rewind: true,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        responsiveClass: true,
        nav: false,
        dots: false,
        items: 1.2,
        margin: 10,
        responsive: {
            468: {
                items: 1.6,
                loop: $('.PackagesSlide .item').length > 1 ? true : false,
                margin: 10,
            },
            576: {
                items: 2.2,
                loop: $('.PackagesSlide .item').length > 2 ? true : false,
                margin: 10,
            },
            768: {
                items: 2.2,
                loop: $('.PackagesSlide .item').length > 2 ? true : false,
                margin: 20,
            },
            880: {
                items: 3,
                loop: $('.PackagesSlide .item').length > 3 ? true : false,
                margin: 20,
            },
            992: {
                items: 3,
                loop: $('.PackagesSlide .item').length > 4 ? true : false,
                margin: 20,
            },
            1200: {
                items: 3,
                loop: $('.PackagesSlide .item').length > 4 ? true : false,
                margin: 40,
            }
        }
    });
     var PackagesSlide = $('.PackagesSlide');

    $('.PackagesSlideNav.OwlNext').click(function() {
        PackagesSlide.trigger('next.owl.carousel');
    });

    $('.PackagesSlideNav.OwlPrev').click(function() {
        PackagesSlide.trigger('prev.owl.carousel');
    });
    // BLOG
    $('.blogSlide').owlCarousel({
        loop: true,
        rewind: true,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        responsiveClass: true,
        nav: false,
        dots: false,
        items: 1.2,
        margin: 10,
        responsive: {
            468: {
                items: 1.6,
                loop: $('.blogSlide .item').length > 1 ? true : false,
                margin: 10,
            },
            576: {
                items: 2.2,
                loop: $('.blogSlide .item').length > 2 ? true : false,
                margin: 10,
            },
            768: {
                items: 2.2,
                loop: $('.blogSlide .item').length > 2 ? true : false,
                margin: 20,
            },
            880: {
                items: 3,
                loop: $('.blogSlide .item').length > 3 ? true : false,
                margin: 20,
            },
            992: {
                items: 4,
                loop: $('.blogSlide .item').length > 4 ? true : false,
                margin: 20,
            },
            1200: {
                items: 4,
                loop: $('.blogSlide .item').length > 4 ? true : false,
                margin: 40,
            }
        }
    });
    var blogSlide = $('.PackagesSlide, .blogSlide');

    $('.blogSlideNav.OwlNext').click(function() {
        blogSlide.trigger('next.owl.carousel');
    });

    $('.blogSlideNav.OwlPre').click(function() {
        blogSlide.trigger('prev.owl.carousel');
    });

    // LATEST NEWS 
    $('.newsSlide').slick({
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 1000,
        responsive: [{
                breakpoint: 992,
                settings: {
                    vertical: false,
                    verticalSwiping: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    vertical: false,
                    verticalSwiping: false,
                }
            },
            {
                breakpoint: 468,
                settings: {
                    slidesToShow: 1,
                    vertical: false,
                    verticalSwiping: false,
                }
            }
        ]
    });
    </script>

</div>