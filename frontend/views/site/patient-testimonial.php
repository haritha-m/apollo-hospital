<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="testimonialPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
        <img class="lazy innerBg jarallax-img" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="testimonial">
        <div class="container">
            <div class="secTitleWrap">
                <h2 class="secTitle">Testimonial Videos</h2>
            </div>
            <div class="row">
                <?php 
                foreach($testimonials as $testimonial) : ?>
                <div class="col-lg-3 col-md-4 col-6">
                    <a href="<?=$testimonial->video_link?>" target="_blank">
                        <div class="testimonialBx">
                            <div class="imgBox">
                                <img src="<?= Yii::$app->homeUrl ?>images/testimonial/<?=$testimonial->thump_image?>"
                                    data-src="<?= Yii::$app->homeUrl ?>images/testimonial/<?=$testimonial->thump_image?>"
                                    width="350" height="250" class="lazy" loading="lazy" alt="">
                                <div class="icon">
                                    <!-- <img src="<?= Yii::$app->homeUrl ?>assets/images/youtube.png" alt=""> -->
                                    <svg viewBox="0 0 30.902 21.637">
                                        <g id="youtube_1_" data-name="youtube (1)" transform="translate(0 0)">
                                            <path id="Path_992" data-name="Path 992"
                                                d="M30.266,3.386A3.872,3.872,0,0,0,27.542.662C25.124,0,15.451,0,15.451,0S5.778,0,3.36.637A3.951,3.951,0,0,0,.636,3.386,40.8,40.8,0,0,0,0,10.818a40.646,40.646,0,0,0,.636,7.433A3.872,3.872,0,0,0,3.36,20.975c2.443.662,12.091.662,12.091.662s9.673,0,12.091-.637a3.872,3.872,0,0,0,2.724-2.724,40.8,40.8,0,0,0,.636-7.433,38.723,38.723,0,0,0-.637-7.458Zm0,0"
                                                fill="red" />
                                            <path id="Path_993" data-name="Path 993"
                                                d="M204.969,111.75l8.044-4.633-8.044-4.633Zm0,0"
                                                transform="translate(-192.598 -96.299)" fill="#fff" />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <div class="info">
                                <div class="name"><?=$testimonial->name?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>


            </div>
        </div>
    </section>


</div>