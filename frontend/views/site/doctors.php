<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\ServiceMainCategory;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}
use yii\widgets\ListView;
use common\models\Doctors;

?>
<div id="pageWrapper" class="doctorsPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
        <?php
        if (!empty($banner_images)) {
            ?>
            <img class="lazy innerBg" loading="lazy"
                src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
                data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
                alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">
            <?php
        } else {
            ?>
            <img class="lazy innerBg jarallax-img" src="assets/images/newsMediaBg.jpg"
                data-src="assets/images/newsMediaBg.jpg" loading="lazy" alt="bannerImage" width="1920" height="590">
            <?php
        }
        ?>
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd">
                            <?= $banner_images->title ?>
                        </h1>
                        <h3 class="bnrSubHd">
                            <?= $banner_images->description ?>
                        </h3>
                    </div>
                    <div class="col-lg-6">
                        <div class="searchBx">

                            <form class="searchForm">


                                <input type="text" name="search" id="search" placeholder="Search Doctor / Department"
                                    class="form-control" required
                                    value="<?= Yii::$app->getRequest()->getQueryParam('search') ?>">


                                <button type="submit" class="searchBtn">
                                    <svg viewBox="0 0 19.225 20.268">
                                        <path id="search_FILL0_wght200_GRAD0_opsz48"
                                            d="M25.9,27.818,22.763,23.6a8.251,8.251,0,0,1-2.8,1.649,9.513,9.513,0,0,1-3.2.563,8.877,8.877,0,0,1-6.5-2.654A8.777,8.777,0,0,1,7.6,16.68a9.064,9.064,0,0,1,9.13-9.13,8.877,8.877,0,0,1,6.5,2.654A8.777,8.777,0,0,1,25.9,16.68a9.328,9.328,0,0,1-.583,3.238,8.523,8.523,0,0,1-1.669,2.8l3.177,4.18Zm-9.13-3.255A7.689,7.689,0,0,0,22.4,22.29a7.588,7.588,0,0,0,2.292-5.611A7.829,7.829,0,0,0,16.77,8.757,7.829,7.829,0,0,0,8.847,16.68a7.588,7.588,0,0,0,2.292,5.611A7.689,7.689,0,0,0,16.77,24.563Z"
                                            transform="translate(-7.6 -7.55)" />
                                    </svg>
                                </button>
                            </form>
                            <div class="sugBxWrp">
                                <ul class="results">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="doctor">
        <div class="container">

            <div class="FilterHeaderTab">
                <div class="item">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#filterAccodionModal">
                        <svg viewBox="0 0 372.1 372.2">
                            <path d="M69.7,59.2c0,13.5,0.1,27-0.1,40.5c0,3.2,1,4.9,4,6.4c18.8,9.6,29.3,25,29.3,46.3c0.1,21.5-10.4,37.1-29.5,46.7
                        c-3.5,1.7-3.8,3.9-3.8,7.1c0,47.6,0,95.3,0,142.9c0,2.8,0,5.7-0.5,8.4c-1.5,9-9.1,15.1-18.4,14.8c-8.8-0.3-16-6.7-17.1-15.7
                        c-0.4-3.9-0.4-7.9-0.4-11.8c0-46,0-91.9,0.1-137.9c0-4.3-1.2-6.5-5.1-8.6C10.1,188.7,0,173.5,0,152.6c0-21.1,10.1-36.5,28.6-46.1
                        c3.8-2,4.9-4.1,4.9-8.1c-0.2-26.3-0.2-52.5,0-78.8c0-10.9,6.3-18.4,15.6-19.4c11.5-1.3,20.2,6.4,20.5,18.5
                        c0.3,11.4,0.1,22.9,0.1,34.3C69.7,55,69.7,57.1,69.7,59.2C69.7,59.2,69.7,59.2,69.7,59.2z" />
                            <path d="M302.4,279.2c0-24.2-0.1-48.4,0.1-72.6c0-4-0.9-6.2-4.8-8.2c-18.6-9.6-28.6-25.1-28.6-46.1c0-21.1,10.3-36.4,28.9-46
                        c3.7-1.9,4.6-4,4.6-7.7c-0.1-26.3-0.1-52.5,0-78.8c0.1-12.5,7.9-20.4,19.4-19.8c8.3,0.5,15.3,6.7,16.4,15c0.5,4.1,0.5,8.2,0.5,12.4
                        c0,23.6,0.1,47.3-0.1,70.9c0,4,1,6.1,4.9,8.1c18.6,9.6,28.6,25.1,28.6,46.1c0,20.8-10,36.1-28.3,45.7c-4,2.1-5.2,4.3-5.2,8.6
                        c0.1,48.4,0.1,96.8,0.1,145.2c0,12.7-7.7,20.6-19.1,20.1c-8.2-0.4-15-6.1-16.6-14.2c-0.5-2.6-0.6-5.2-0.6-7.8
                        C302.4,326.5,302.4,302.9,302.4,279.2z" />
                            <path d="M168,98c0-26.6-0.1-53.3,0-79.9c0-9.5,6.6-16.9,15.4-17.9c9.6-1.1,17.7,4.2,20.1,13.3c0.6,2.3,0.6,4.8,0.6,7.2
                        c0,52.2,0.1,104.3,0,156.5c0,3.8,0.9,5.8,4.6,7.7c18.7,9.5,28.9,25,29,45.9c0.1,21.2-10.3,36.8-29.1,46.4c-3.4,1.7-4.5,3.4-4.5,7.2
                        c0.2,23.3,0.2,46.5,0,69.8c0,9.4-6.7,16.9-15.4,17.9c-9.4,1.1-17.8-4.3-20.1-13.3c-0.6-2.3-0.6-4.8-0.6-7.2c0-22.3-0.1-44.7,0.1-67
                        c0-3.6-0.8-5.5-4.3-7.3c-18.9-9.5-29.3-25.1-29.3-46.3c0-21.4,10.7-37.1,29.8-46.5c3.2-1.6,3.7-3.4,3.7-6.4
                        C167.9,151.3,168,124.7,168,98z" />
                        </svg>
                        Filter
                    </button>
                </div>

            </div>
            <?php
            $form = ActiveForm::begin([
                'action' => ['/site/doctors'],
                'id' => 'doctor-filter',
                'method' => 'get'
            ]);
            ?>
            <input type="hidden" name="search" class="search">
            <div class="flxBox">
                <div class="lftSec">
                    <div class="modal fade" id="filterAccodionModal" tabindex="-1" role="dialog"
                        aria-labelledby="filterAccodionModal" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="ModalHeader">
                                    <div class="title">
                                        <div class="icon">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                        FILTERS
                                    </div>
                                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="flxBx">
                                        <div class="lft">
                                            <div class="Txt">Filters</div>
                                        </div>
                                        <div class="rt">
                                            <a href="<?= Url::to(['site/doctors'])?>" class="clrBtn">
                                                <span>Clear All</span>
                                            </a>
                                        </div>
                                    </div>
                                    <?php $i = 0; ?>
                                    <?php

                                    foreach ($categories as $category):


                                        ?>
                                        <div class="accordion" id="filterAccodion">
                                            <div class="accordion-item">
                                                <div class="accordion-header">
                                                    <button class="btn btn-link" type="button" data-bs-toggle="collapse"
                                                        data-bs-target="#Category" aria-expanded="true"
                                                        aria-controls="CategoryFilter">
                                                        <?= $category->category_name ?>
                                                    </button>
                                                </div>

                                                <div id="Category" class="collapse show" aria-labelledby="headingOne"
                                                    data-bs-parent="#filterAccodion1">
                                                    <div class="accordion-body">
                                                        <ul>
                                                            <?php
                                                            $sub_cat = ServiceMainCategory::find()->where(['category' => $category->id, 'status' => '1'])->orderBy('sort_order ASC')->all();
                                                            foreach ($sub_cat as $value):
                                                                $i++;
                                                                ?>
                                                                <li>
                                                                    <?php if (!empty(Yii::$app->getRequest()->getQueryParam('category'))): ?>
                                                                        <input type="checkbox" id="category<?= $i ?>"
                                                                            name="category[]" value="<?= $value->canonical_name; ?>"
                                                                            <?= in_array($value->canonical_name, Yii::$app->getRequest()->getQueryParam('category')) ? 'checked' : '' ?>>
                                                                    <?php else: ?>
                                                                        <input type="checkbox" id="category<?= $i ?>"
                                                                            name="category[]"
                                                                            value="<?= $value->canonical_name; ?>">
                                                                    <?php endif; ?>
                                                                    <label for="category<?= $i ?>"><?= $value->title ?>

                                                                    </label>
                                                                </li>
                                                                <?php

                                                            endforeach;

                                                            ?>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn clear">Clear</button>
                                    <button type="button" class="btn">Apply</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="rtSec">
                    <div class="topHeader">
                        <div class="pageInfo">
                            <?= 'All Doctors' ?>
                        </div>
                        <div class="num">
                            <div class="Txt">

                                <?= $dataProvider->count ?> Numbers
                            </div>
                        </div>
                    </div>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'div',
                            'class' => 'rowBox',

                        ],

                        'itemView' => function ($model) {


                        return $this->render('doctors-list', [
                            'model' => $model,

                        ]);
                    },
                        'pager' => [
                            'options' => ['class' => 'pagination'],
                            'prevPageLabel' => '<',
                            // Set the label for the "previous" page button
                            'nextPageLabel' => '>',
                            // Set the label for the "next" page button
                            'firstPageLabel' => 'First',
                            // Set the label for the "first" page button
                            'lastPageLabel' => 'Last',
                            // Set the label for the "last" page button
                            'nextPageCssClass' => '>',
                            // Set CSS class for the "next" page button
                            'prevPageCssClass' => '<',
                            // Set CSS class for the "previous" page button
                            'firstPageCssClass' => '<<',
                            // Set CSS class for the "first" page button
                            'lastPageCssClass' => '>>',
                            // Set CSS class for the "last" page button
                            'maxButtonCount' => 5,
                            // Set maximum number of page buttons that can be displayed
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    if ($('input.checkbox_check').is(':checked')) {
                        jQuery('#doctor-filter').submit();
                    }

                    $("input[name='category[]']").change(function () {
                        $("#doctor-filter").submit();
                    });
                    $('.summary').hide()

                    let keyupTimer;
                    var last_keyword;
                    var innerClick;
                    $("#search").bind("keyup click", function (event) {
                        var keyword = $(this).val();
                        if (innerClick != 1 || (keyword != last_keyword || last_keyword == undefined)) {
                            innerClick = 1;
                            last_keyword = $(this).val();
                            clearTimeout(keyupTimer);
                            keyupTimer = setTimeout(function () {
                                if (keyword != '') {
                                    $.ajax({
                                        url: '<?= yii::$app->homeUrl ?>ajax/get-search-products',
                                        type: "post",
                                        data: {
                                            keyword: keyword
                                        },
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data.success) {
                                                var data = data.data
                                                console.log(data.name)
                                                if (data) {
                                                    $('.sugBxWrp').show();
                                                    $('.sugBxWrp .results').empty();
                                                    $.each(data, function (key, value) {
                                                        $('.sugBxWrp .results').append(
                                                            '<li class="doctorNme">' +
                                                            value.name +
                                                            '</li>');
                                                    });
                                                } else {
                                                    $('.sugBxWrp').show();
                                                    $('.sugBxWrp .results').empty().append(
                                                        '<li>No results found!</li>');
                                                }
                                            } else if(data.s_success) {
                                                
                                            }
                                            else {
                                                $('.sugBxWrp').show();
                                                $('.sugBxWrp .results').empty().append(
                                                    '<li>No results found!</li>');
                                            }
                                        },
                                        error: function () {
                                            return false
                                        }
                                    });
                                }
                            }, 500);
                        }
                        if (event.which === 13) {
                            event.preventDefault();
                            $('#doctor-filter').submit();
                        }
                    });
                    document.getElementById('search').addEventListener('input', (e) => {
                        $('.sugBxWrp').hide();
                        $('.sugBxWrp .results').empty();
                    })
                    $(document).on('click', '.doctorNme', function (e) {
                        var name = $(this).text();
                        $('.search').val(name)
                        $("#doctor-filter").submit();
                    })
                });
            </script>
        </div>
    </section>

</div>

<style>
    .pagination {
        display: flex;
        justify-content: center;
        margin-top: 20px;
    }

    .pagination li {
        list-style: none;
        margin: 0 5px;
    }

    .pagination a,
    .pagination span {
        padding: 5px 10px;
        border: 1px solid #ccc;
        background-color: #f9f9f9;
        color: #333;
        text-decoration: none;
        border-radius: 3px;
    }

    .pagination .active a {
        background-color: #007bff;
        color: #fff;
        border-color: #007bff;
    }
</style>