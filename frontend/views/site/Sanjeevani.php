<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="SanjeevaniPage">

    <section id="innerBanner">





        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg" loading="lazy" src="assets/images/sanjeevaniBg.jpg"
            data-src="assets/images/sanjeevaniBg.jpg" alt="bannerImage" width="1920" height="590">


        <?php
    }
    ?>











        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Sanjeevani">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="Main_Title" data-aos="fade-up">
                        <div class="Main-Title">
                            <?= $sanjeevani_common->title?>
                        </div>
                        <div class="Main-SubTitle">
                            <?= $sanjeevani_common->caption?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="sanjeevani-Box">
        <div class="container">
            <div class="row">
                <?php
                if(!empty($sanjeevani))
                {
                    $i=0;
                    foreach($sanjeevani as $data)
                    {
                        $i++;
                        ?>



                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" data-aos="fade-up">
                    <a href="<?= Url::to(['site/sanjeevanidetails', 'name' => $data->canonical_name]); ?>">
                        <div class="Sanjeevani_Box-<?=$i?>">
                            <div class="IconBox">
                                <div class="Icn_1">
                                    <img class="lazy"
                                        src="<?= Yii::$app->homeUrl ?>images/sanjeevani/image<?= $data->id ?>.<?= $data->image ?>"
                                        data-src="<?= Yii::$app->homeUrl ?>images/sanjeevani/image<?= $data->id ?>.<?= $data->image ?>"
                                        loading="lazy" alt="doctor" width="340" height="390">
                                </div>
                                <div class="Values">
                                    <div class="value_1 count" data-count="<?= $data->value?>">0</div>
                                    <div class="value_Txt"><?= $data->value_text?></div>
                                </div>
                                <div class="value_Arrow">
                                    <svg viewBox="0 0 42.646 42.646">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                            d="M15.581,31.219,0,15.638,15.581,0l.952.952L2.466,14.965H31.219v1.289H2.466L16.534,30.323Z"
                                            transform="translate(42.646 15.61) rotate(120)" fill="#fff" />
                                    </svg>
                                </div>
                            </div>
                            <div class="Content_Box">
                                <div class="Cnt_ttL"><?= $data->title?></div>
                                <div class="Cnt_DtL">
                                    <?= $data->small_description?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>


                <?php
            }
        }
        ?>

            </div>
        </div>
    </section>

    <section id="sanjeevani_Content">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="Cnt_DtL" data-aos="fade-up">
                        <?= $sanjeevani_common->description?>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>

<!-- SCROLL_MAGIC -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/ScrollMagic.min.js"></script>
<script>
    // COUNTER
    var controller = new ScrollMagic.Controller();
    var scene = new ScrollMagic.Scene({
            triggerElement: '#sanjeevani-Box',
            triggerHook: 0.8
        })
        .on("enter", function() {
            $('.count').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');
                $({
                    countNum: $this.text()
                }).animate({
                    countNum: countTo
                }, {
                    duration: 2000,
                    easing: 'linear',
                    step: function() {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        $this.text(this.countNum);
                        //alert('finished');
                    }
                });
            });
        })
        .reverse(false)
        .addTo(controller);
</script>

