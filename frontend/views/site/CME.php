<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="CmePage">

    <section id="innerBanner">


<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
               <img class="lazy innerBg" loading="lazy" src="?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920" height="590">



        <?php
    }
    ?> 






    


        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="cme-list">
        <div class="container">
            <div class="row">
                <?php
                if(!empty($cme))
                {
                    foreach($cme as $data)
                    {

                        ?>

                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-aos="fade-up">
                    <a href="<?= Url::to(['site/cmedetails', 'name' => $data->canonical_name]); ?>" class="blog-box">
                        <div class="image_box">
                            <img class="lazy" loading="lazy" width="330" height="235" data-src="<?= Yii::$app->homeUrl ?>images/cme/image<?= $data->id ?>.<?= $data->image ?>" src="<?= Yii::$app->homeUrl ?>images/cme/image<?= $data->id ?>.<?= $data->image ?>">
                        </div>
                        <div class="content_box">
                            <!--<div class="title_box">-->
                            <!--    <span class="post-date">-->
                            <!--     <?= date('M d, Y', strtotime($data->date)) ?>-->
                            <!--</span>-->
                            <!--    <span class="post-ttl"># News</span>-->
                            <!--</div>-->
                            <div class="details_sec">
                                <div class="post-title"><?= $data->title?></div>
                                <!--<div class="post-description"><?= $data->small_description?></div>-->
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
        }
        ?>
                
            </div>
        </div>
    </section>

</div>

