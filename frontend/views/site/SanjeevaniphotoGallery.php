<div id="pageWrapper" class="mediaGalleryPage photoGallery">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920"
            height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $sanjeevani->title ?></h1>
                        <h4 class="bnrSubHd"><?= $sanjeevani->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php $path = Yii::getAlias('@paths') . '/sanjeevani-gallery/' . $sanjeevani->sanjeevani_id . '/' . $sanjeevani->id . '/gallery' ;
        if (count(glob("{$path}/*")) > 0) { $k = 0; ?>
    <section id="media-gallery">
        <div class="container">
            <div class="row">
                <?php foreach (glob("{$path}/*") as $file) {
                    $k++;
                    $arry = explode('/', $file);
                    $img_nmee = end($arry);
                    $img_nmees = explode('.', $img_nmee);
                    if (isset($img_nmees['1']) && $img_nmees['1'] != '') {
                ?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="<?= Yii::$app->homeUrl . 'images/sanjeevani-gallery/' . $sanjeevani->sanjeevani_id . '/' . $sanjeevani->id . '/gallery/' . end($arry) ?>"
                        data-fancybox="photos" data-caption="Lorem ipsum dolor sit amet consectetur adipisicing elit.">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="<?= Yii::$app->homeUrl . 'images/sanjeevani-gallery/' . $sanjeevani->sanjeevani_id . '/' . $sanjeevani->id . '/gallery/' . end($arry) ?>"
                                src="<?= Yii::$app->homeUrl . 'images/sanjeevani-gallery/' . $sanjeevani->sanjeevani_id . '/' . $sanjeevani->id . '/gallery/' . end($arry) ?>">
                        </div>
                    </a>
                </div>
                <?php } } ?>
            </div>
        </div>
    </section>
    <?php } ?>
    <!-- fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"
        integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
        integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
    // fancybox
    $('[data-fancybox="photos"]').fancybox({
        buttons: [
            "zoom",
            "slideShow",
            "thumbs",
            "close"
        ],
        loop: true,
        protect: true
    });
    </script>

</div>