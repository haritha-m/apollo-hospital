<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="PrivacyPolicyPage">

    <section id="innerSmBnr">
     

<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                   <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/qualitySafetyBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/qualitySafetyBg.jpg" alt="bannerImage" width="1920" height="200">

        <?php
    }
    ?> 









            
    </section>

    <section id="Privacy_Policy">
        <div class="designBxElemntOuter">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="Quality_Icon">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="container">
            <h1><?= $privacy->title?></h1>
            <?= $privacy->description?>
           
         
            
        </div>
        
    </section>

</div>

