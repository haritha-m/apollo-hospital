<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="doctorsPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
            <?php
            if(!empty($banner_images))
            {
                ?>
        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">
            <?php
            }
            else{
            ?>
        <img class="lazy innerBg jarallax-img" src="assets/images/newsMediaBg.jpg"
            data-src="assets/images/newsMediaBg.jpg" loading="lazy" alt="bannerImage" width="1920" height="590">
           <?php
           }
           ?>
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                    <div class="col-lg-6">


                        <form action="search" method="GET" class="searchForm">


                            <input type="text" name="search" id="search" placeholder="Search Doctor / Department"
                                class="form-control" required>


                            <button type="submit" class="searchBtn">
                                <svg viewBox="0 0 19.225 20.268">
                                    <path id="search_FILL0_wght200_GRAD0_opsz48"
                                        d="M25.9,27.818,22.763,23.6a8.251,8.251,0,0,1-2.8,1.649,9.513,9.513,0,0,1-3.2.563,8.877,8.877,0,0,1-6.5-2.654A8.777,8.777,0,0,1,7.6,16.68a9.064,9.064,0,0,1,9.13-9.13,8.877,8.877,0,0,1,6.5,2.654A8.777,8.777,0,0,1,25.9,16.68a9.328,9.328,0,0,1-.583,3.238,8.523,8.523,0,0,1-1.669,2.8l3.177,4.18Zm-9.13-3.255A7.689,7.689,0,0,0,22.4,22.29a7.588,7.588,0,0,0,2.292-5.611A7.829,7.829,0,0,0,16.77,8.757,7.829,7.829,0,0,0,8.847,16.68a7.588,7.588,0,0,0,2.292,5.611A7.689,7.689,0,0,0,16.77,24.563Z"
                                        transform="translate(-7.6 -7.55)" />
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="doctorsSec" class="inrCntSec">
        <div class="container">
            <?php
            if(!empty($department))
            {
                $i=0;
                foreach($department as $dept)
                {
                    $i++;
                      $doctors = \common\models\Doctors::find()->where(['status' => 1,'department'=>$dept->id])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
                    if(!empty($doctors))
                    {
                    ?>

            <div class="row docCarouselSec Sec<?= $i ?>" id="<?= $dept->canonical_name?>">
                <div class="col-12">
                    <div class="secTitleWrap" data-aos="fade-up">
                        <h2 class="secTitle"><?= $dept->title?></h2>
                    </div>
                </div>

                <div class="col-12" data-aos="fade-up">
                    <div class="owl-carousel docCarousel Slide<?= $i ?> owl-theme">
                        <?php
                        foreach($doctors as  $doc)
                        {
                            ?>

                        <div class="item">
                            <div class="docCard">
                                <div class="cImg">
                                    <img class="lazy"
                                        src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>"
                                        data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $doc->id ?>.<?= $doc->image ?>"
                                        loading="lazy" alt="doctor" width="340" height="390">
                                </div>
                                <div class="cCnt">
                                    <h4 class="dName"><?= $doc->name?></h4>
                                    <h6 class="dDprt"><?= $doc->small_description?></h6>
                                    <p class="dDisc"><?= $doc->education?></p>
                                </div>

                                <div class="cBttm">
                                    <div class="lftBtn">
                                        <?php if($doc->view_label == 1){ ?>
                                        <a href="<?= Url::to(['site/doctordetails', 'name' => $doc->canonical_name]); ?>"
                                            class="docBtn hoveranim"><span>Profile</span></a>
                                        <?php  }  else{ ?>
                                        <div class="docBtn hoveranim"><span>Profile</span></div>
                                        <?php } ?>
                                    </div>
                                    <div class="rgtBtn">
                                        <?php if($doc->view_label == 1 && !empty( $doc->appointment_link) &&  $doc->appointment_link !=NULL && $doc->appointment_link !="#"){ ?>
                                        <a href="<?= $doc->appointment_link ?>" class="bookBtn docBtn hoveranim"
                                            target="_blank"><span>Book An Appointment</span></a>
                                        <?php }else{ ?>
                                        <div class="bookBtn docBtn hoveranim" target="_blank"><span>Book An
                                                Appointment</span></div>
                                        <?php }?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php

}
?>

                    </div>
                </div>
            </div>

            <?php
}
}
}
?>


        </div>
    </section>

</div>

<!-- OWL_SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- SCROLL_MAGIC -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/ScrollMagic.min.js"
    integrity="sha512-8E3KZoPoZCD+1dgfqhPbejQBnQfBXe8FuwL4z/c8sTrgeDMFEnoyTlH3obB4/fV+6Sg0a0XF+L/6xS4Xx1fUEg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
var controller = new ScrollMagic.Controller();
var numberOfScenes = <?= count($department) ?>;

for (var i = 1; i <= numberOfScenes; i++) {
    var scene = new ScrollMagic.Scene({
            triggerElement: '.Sec' + i,
            triggerHook: 0.8
        })
        .setClassToggle('isVisible')
        .on("enter", function(index) {
            return function() {
                $(".Slide" + index).owlCarousel({
                    loop: false,
                    rewind: true,
                    nav: false,
                    dots: false,
                    margin: 10,
                    startPosition: 0,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                        },
                        480: {
                            items: 2,
                        },
                        768: {
                            items: 3,
                        },
                        1000: {
                            items: 4,
                            margin: 15,
                        },
                        1200: {
                            items: 4,
                            margin: 30,
                        }
                    }
                });
            };
        }(i))
        .on("leave", function(index) {
            return function(event) {
                $(".Slide" + index).trigger('refresh.owl.carousel');
            };
        }(i))
        .reverse(true)
        .addTo(controller);
}

$("#search").autocomplete({
    source: function(request, response) {
        $.ajax({
            url: '<?=Yii::$app->homeUrl?>site/search',
            dataType: "json",
            data: {
                search: request.term
            },
            success: function(data) {
                response(data);
            }
        });
    },
    minLength: 2,
    select: function(event, ui) {
        // Handle the selected doctor or department
    }
});


$(document).ready(function() {

    $.fn.visible = function(partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

})

// $(window).scroll(function(event) {

//     $(".docCarouselSec").each(function(i, el) {
//         var el = $(el);
//         if (el.visible(true)) {
//             el.addClass("LFTgX");
//         } else {
//             el.removeClass("LFTgX");
//         }
//     });
//     return function() {
//         alert('return');
//     }

// });
</script>