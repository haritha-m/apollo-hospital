<?php 
use yii\helpers\Url;
?>
<div class="item">
                            <div class="docCard">
                                <div class="cImg">
                                    <img class="lazy" src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $model->id ?>.<?= $model->image ?>"
                                        data-src="<?= Yii::$app->homeUrl ?>images/doctors/doctor<?= $model->id ?>.<?= $model->image ?>" loading="lazy" alt="doctor" width="340"
                                        height="390">
                                </div>
                                <div class="cCnt">
                                    <h4 class="dName"> <a href="<?= Url::to(['site/doctordetails', 'name' => $model->canonical_name]); ?>"
                                            ><?= $model->name?></a></h4>
                                    <h6 class="dDprt"><?= $model->small_description?></h6>
                                    <p class="dDisc"><?= $model->education?></p>
                                </div>
                                <div class="cBttm">
                                    <div class="lftBtn">
                                        <?php if($model->view_label == 1){ ?>
                                        <a href="<?= Url::to(['site/doctordetails', 'name' => $model->canonical_name]); ?>"
                                            class="docBtn hoveranim"><span>Profile</span></a>
                                        <?php  }  else{ ?>
                                        <div class="docBtn hoveranim"><span>Profile</span></div>
                                        <?php } ?>
                                    </div>
                                    <div class="rgtBtn">
                                        <?php if($model->view_label == 1 && !empty( $model->appointment_link) &&  $model->appointment_link !=NULL && $model->appointment_link !="#"){ ?>
                                        <a href="<?= $model->appointment_link ?>" class="bookBtn docBtn hoveranim"
                                            target="_blank"><span>Book An Appointment</span></a>
                                        <?php }else{ ?>
                                        <div class="bookBtn docBtn hoveranim" target="_blank"><span>Book An
                                                Appointment</span></div>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>