<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="QualityAndSafetyPage">

    <section id="innerBanner">



        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/qualitySafetyBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/qualitySafetyBg.jpg" alt="bannerImage" width="1920"
            height="200">


        <?php
    }
    ?>












        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>


    </section>

    <section id="Quality_and_Safety">
        <div class="designBxElemntOuter">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="Quality_Icon">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="container">
            <h1><?= $quality->title?></h1>
            <?= $quality->description?>
        </div>
       
  <?php 
  foreach($files as $file) : ?>
  <div class="pdf-box">
    <a href="<?= Yii::$app->homeUrl ?>images/quality-safety/files<?= $file->id ?>.<?= $file->files ?>" target="_blank">
      <h3><?=$file->title?></h3>
    </a>
  </div>
  <?php endforeach; ?>
    </section>

</div>
<style>
  .pdf-box {
    width: 23%; /* Adjust as needed */
    margin: 1%;
    padding: 10px;
    border: 1px solid #ccc;
    display: inline-block;
    box-sizing: border-box;
    text-align: center;
  }
</style>