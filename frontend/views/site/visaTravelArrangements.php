<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="internationalPatientsPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">
       




<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                  <img class="lazy innerBg jarallax-img" src="<?= Yii::$app->homeUrl ?>assets/images/internationalPatientsBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/internationalPatientsBnr.jpg" loading="lazy" alt="bannerImage" width="1920"
            height="590">

        <?php
    }
    ?> 












        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                    <div class="col-lg-6">
                         <form action="search" method="GET" class="searchForm">

                           
                              <input type="text" name="search" id="search" placeholder="Search Doctor / Department"
            class="form-control" required>
                            <button type="submit" class="searchBtn">
                                <svg viewBox="0 0 19.225 20.268">
                                    <path id="search_FILL0_wght200_GRAD0_opsz48"
                                        d="M25.9,27.818,22.763,23.6a8.251,8.251,0,0,1-2.8,1.649,9.513,9.513,0,0,1-3.2.563,8.877,8.877,0,0,1-6.5-2.654A8.777,8.777,0,0,1,7.6,16.68a9.064,9.064,0,0,1,9.13-9.13,8.877,8.877,0,0,1,6.5,2.654A8.777,8.777,0,0,1,25.9,16.68a9.328,9.328,0,0,1-.583,3.238,8.523,8.523,0,0,1-1.669,2.8l3.177,4.18Zm-9.13-3.255A7.689,7.689,0,0,0,22.4,22.29a7.588,7.588,0,0,0,2.292-5.611A7.829,7.829,0,0,0,16.77,8.757,7.829,7.829,0,0,0,8.847,16.68a7.588,7.588,0,0,0,2.292,5.611A7.689,7.689,0,0,0,16.77,24.563Z"
                                        transform="translate(-7.6 -7.55)" />
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="visaTravelcntSec" class="inrCntSec">
        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-up" data-aos-delay="300">
                    <div class="treatCard largeCard">
                        <div class="cCnt">
                           
                            <?= $visa_info->description?>
                        </div>
                        <div class="cIcon">
                            <div class="iconBg">
                                <svg viewBox="0 0 332.365 223.607">
                                    <path id="Path_397" data-name="Path 397"
                                        d="M15200.911,1878s-29.147,48-4.6,69.861,55.993,18.025,44.1,60.212,24.545,111.219,97.412,89.358,77.853-47.172,120.423-22.244,60.595,13.423,60.595,13.423V1878Z"
                                        transform="translate(-15186.478 -1878)" />
                                </svg>
                            </div>
                            <img src="<?= Yii::$app->homeUrl ?>images/insurance/visa<?= $visa_info->id ?>.<?= $visa_info->image ?>" data-src="<?= Yii::$app->homeUrl ?>images/insurance/visa<?= $visa_info->id ?>.<?= $visa_info->image ?>"
                                class="lazy" width="505" height="340" loading="lazy"
                                 alt="<?= $visa_info->image_alt ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

