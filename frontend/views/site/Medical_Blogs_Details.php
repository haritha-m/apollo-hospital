<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($media->meta_title) && $media->meta_title != '') {
    $this->title = $media->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="MedicalBlogsDetailsPage">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd">Medical Blogs</h1>
                        <h4 class="bnrSubHd">These are the words that form the cornerstone of India’s most advanced medical institution</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="news-detail">
        <div class="container">
            <div class="row">
                <div class="News-Lft col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="news-detail-img_box" data-aos="fade-up">
                        <img class="lazy" loading="lazy" width="945" height="490" data-src="<?= Yii::$app->homeUrl ?>images/media-blogs/data<?= $media->id ?>.<?= $media->image1 ?>" src="<?= Yii::$app->homeUrl ?>images/media-blogs/data<?= $media->id ?>.<?= $media->image1 ?>">
                    </div>
                    <div class="news-detail-content_box">
                        <div class="news-detail-head" data-aos="fade-up">
                            <span class="news-dtl-post_date"><?= date('M d, Y', strtotime($media->date)) ?></span>
                        </div>
                        <div class="news-detail-title" data-aos="fade-up">
                           <?= $media->title?>
                        </div>
                        <div class="news-detail-content" data-aos="fade-up">
                           
                                                         <?= $media->detail_description?>

                        </div>
                    </div>
                </div>
                <div class="News-Rgt col-xl-4 col-lg-4">
                    <div class="news-dtl_ttl" data-aos="fade-up">
                        Our Latest Blogs
                    </div>
                    <div class="row">
                        <?php
                  if(!empty($media_all))
                {
                    foreach($media_all as $data)
                    {

                        ?>

                            <div class="col-xl-12 col-lg-12 col-md-4 col-sm-6 col-12" data-aos="fade-up">
                                <a href="<?= Url::to(['site/medicaldetails', 'name' => $data->canonical_name]); ?>">
                                    <div class="NewsBox">
                                        <div class="Lft_News">
                                            <div class="Img_Box">
                                                <img class="lazy" loading="lazy" width="150" height="102" data-src="<?= Yii::$app->homeUrl ?>images/media-blogs/image<?= $data->id ?>.<?= $data->image ?>" src="<?= Yii::$app->homeUrl ?>images/media-blogs/image<?= $data->id ?>.<?= $data->image ?>" >
                                            </div>
                                        </div>
                                        <div class="Rgt_News">
                                            <div class="SubTitle">
                                                <?=$data->title?>
                                            </div>
                                            <div class="News-flxbx">
                                                <div class="News-dtl-date"><?= date('M d, Y', strtotime($data->date)) ?></div>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </div>
                            <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

