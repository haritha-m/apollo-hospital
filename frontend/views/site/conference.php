<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="conferencePage mediaGalleryPage">

    <section id="innerBanner">







        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg" loading="lazy" src="assets/images/conference-banner.jpg"
            data-src="assets/images/conference-banner.jpg" alt="bannerImage" width="1920" height="590">


        <?php
    }
    ?>











        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
               
                        <h1 class="bnrMainHd"><?= $banner_images->title?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Conference Section  -->
    <!--<section id="conferenceSec">-->
    <!--    <div class="container">-->
    <!--        <div class="headingSec">-->
    <!--            <div class="lftSec" data-aos="fade-right">-->
    <!--                <div class="mainHeading"><?= $confernce_common->title?></div>-->
    <!--            </div>-->
    <!--            <div class="ritSec" data-aos="fade-left">-->
    <!--                <p><?= $confernce_common->sub_title?>-->
    <!--                </p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="infoBx" data-aos="fade-up">-->
    <!--            <?= $confernce_common->description?>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- Conference Section  -->


    <!-- Events Section  -->
    <section id="EventSec">
        <div class="container">
            <div class="headingMain" data-aos="fade-up">Upcoming Events</div>
        </div>
        <div class="custom_container">
            <div class="owl-carousel owl-theme eventsSlider" data-aos="fade-up">
                <?php
                if(!empty($confernce))
                {
                    foreach($confernce as $data)
                    {
                        ?>


                <!--<a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>" class="item">-->
                    
                <!--        <div class="imgBx">-->
                <!--            <img src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>"-->
                <!--                alt="<?= $data->image_alt ?>" width="439" height="218" class="lazy" loading="lazy"-->
                <!--                data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">-->
                <!--        </div>-->
                <!--    <div class="evntBx">-->
                <!--        <div class="title"><?= $data->title?></div>-->
                <!--        <div class="date"> <?= date('d M Y', strtotime($data->date)) ?></div>-->
                <!--    </div>-->
                <!--</a>-->
                 <div class="item">
                    <!--<a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>"-->
                    <!--    class="conferenceBx">-->
                    <!--    <div class="evntBx">-->
                    <!--    <div class="imgBx">-->
                    <!--        <img src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>"-->
                    <!--            alt="<?= $data->image_alt ?>" width="439" height="218" class="lazy" loading="lazy"-->
                    <!--            data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">-->
                    <!--    </div>-->
                    <!--        <div class="title"><?= $data->title?></div>-->
                    <!--        <div class="infoBx">-->
                    <!--            <p><?= $data->small_description?></p>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</a>-->
                    <a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>">
                        <div class="photo-gallery">
                            <img src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>"
                                alt="<?= $data->image_alt ?>" width="439" height="218" class="lazy" loading="lazy"
                                data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">
                            <div class="ctryTleWrp">
                                <div class="ctryTle"><?= $data->title?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
            }
        }
        ?>



            </div>
        </div>
    </section>
    <!-- Events Section  -->

    <!-- Conference Slider Section  -->
    <section id="ConferenceSliderSec">
        <div class="container">
            <div class="flxTitleWrp">
                <div class="headingMain" data-aos="fade-up">Past Conferences</div>
                <div class="BtnWrp" data-aos="fade-up" data-aos-delay="400">
                    <a href="<?= Url::to(['site/conferencelisting']); ?>" class="MoreBtn">
                        <div class="Txt">View All</div>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.646 42.646">
                            <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                d="M24,39.594,8.417,24.013,24,8.375l.952.952L10.883,23.34H39.636v1.289H10.883L24.951,38.7Z"
                                transform="translate(54.108 12.508) rotate(120)" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
        <div class="custom_container">
            <div class="owl-carousel owl-theme conferenceSlider" data-aos="fade-up">
                <?php
                if(!empty($confernce_data))
                {
                    foreach($confernce_data as $data)
                    {
                        ?>

                <div class="item">
                    <a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>">
                        <div class="photo-gallery">
                            <img src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>"
                                alt="<?= $data->image_alt ?>" width="439" height="218" class="lazy" loading="lazy"
                                data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">
                            <div class="ctryTleWrp">
                                <div class="ctryTle"><?= $data->title?></div>
                            </div>
                        </div>
                    </a>
                    <!--<a href="<?= Url::to(['site/conferncedetails', 'name' => $data->canonical_name]); ?>"-->
                    <!--    class="conferenceBx">-->
                    <!--    <div class="imgBx">-->
                    <!--        <img src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>"-->
                    <!--            alt="<?= $data->image_alt ?>" width="439" height="218" class="lazy" loading="lazy"-->
                    <!--            data-src="<?= Yii::$app->homeUrl ?>images/conference/image<?= $data->id ?>.<?= $data->image ?>">-->
                    <!--    </div>-->
                        <!--<div class="cntBx">-->
                        <!--    <div class="title"><?= $data->title?></div>-->
                        <!--    <div class="infoBx">-->
                        <!--        <p><?= $data->small_description?></p>-->
                        <!--    </div>-->
                        <!--</div>-->
                    <!--</a>-->
                </div>
                <?php
            }
        }
        ?>


            </div>
        </div>
    </section>
    <!-- Conference Slider Section  -->




</div>

<!-- owl carousel  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script>
//Events Slider
$('.eventsSlider').owlCarousel({
    loop: true,
    rewind: true,
    lazyLoad: true,
    autoplay: false,
    smartSpeed: 1000,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    responsiveClass: true,
    nav: true,
    navText: ["<div class='nav-button owl-prev'><img src='assets/images/arrow-left-yellow.svg'>Previous </div>",
        "<div class='nav-button owl-next'>Next<img src='assets/images/arrow-right-yellow.svg'></div>"
    ],
    dots: false,
    margin: 20,
    responsive: {
        0: {
            items: 1.4,
            loop: $('.eventsSlider .item').length > 1 ? true : false,
        },
        468: {
            items: 1.8,
            loop: $('.eventsSlider .item').length > 1 ? true : false,
        },
        576: {
            items: 2.3,
            loop: $('.eventsSlider .item').length > 2 ? true : false,
        },
        767: {
            items: 2.5,
            loop: $('.eventsSlider .item').length > 2 ? true : false,
        },
        992: {
            items: 3.5,
            loop: $('.eventsSlider .item').length > 3 ? true : false,
            margin: 26,
        },
        1200: {
            items: 3.5,
            loop: $('.eventsSlider .item').length > 3 ? true : false,
            margin: 35,
        },
        1441: {
            items: 3.5,
            loop: $('.eventsSlider .item').length > 3 ? true : false,
            margin: 45,
        },
        1551: {
            items: 3.5,
            loop: $('.eventsSlider .item').length > 3 ? true : false,
            margin: 58,
        },
    }
})

//Conference Slider
$('.conferenceSlider').owlCarousel({
    loop: true,
    rewind: true,
    lazyLoad: true,
    autoplay: false,
    smartSpeed: 1000,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    responsiveClass: true,
    nav: false,
    dots: false,
    margin: 20,
    responsive: {
        0: {
            items: 1.4,
            loop: $('.conferenceSlider .item').length > 1 ? true : false,
        },
        468: {
            items: 1.8,
            loop: $('.conferenceSlider .item').length > 1 ? true : false,
        },
        576: {
            items: 2.3,
            loop: $('.conferenceSlider .item').length > 2 ? true : false,
            loop: true,
        },
        767: {
            items: 2.5,
            loop: $('.conferenceSlider .item').length > 2 ? true : false,
        },
        992: {
            items: 2.8,
            loop: $('.conferenceSlider .item').length > 2 ? true : false,
            margin: 26,
        },
        1200: {
            items: 3.2,
            loop: $('.conferenceSlider .item').length > 3 ? true : false,
            margin: 35,
        },
        1441: {
            items: 3.5,
            loop: $('.conferenceSlider .item').length > 3 ? true : false,
            margin: 45,
        },
        1551: {
            items: 3.5,
            loop: $('.conferenceSlider .item').length > 3 ? true : false,
            margin: 58,
        },
    }
})
</script>