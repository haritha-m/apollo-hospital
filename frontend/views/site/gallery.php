<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>

<div id="pageWrapper" class="galleryPage mediaGalleryPage">

    <section id="innerBanner">
        <?php if(!empty($banner_images)) { ?>
        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">
        <?php } else { ?>
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920"
            height="590">
        <?php } ?>
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php if(count($gallery) > 0): ?>
    <section id="galleryCato">
        <div class="container">
            <div class="row">
                <?php foreach($gallery as $image): 
                    $photo_gallery = \common\models\PhotoGallery::find()->where(['gallery_id' => $image->id,'status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
                    $video_gallery = \common\models\VideoGallery::find()->where(['gallery_id' => $image->id,'status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
                    if(count($photo_gallery) > 0 || count($video_gallery) > 0):
                ?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="<?= Url::to(['site/media-gallery-details', 'name' => $image->canonical_name]); ?>">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353"
                                data-src="<?= yii::$app->homeUrl ?>images/gallery/image<?= $image->id ?>.<?= $image->image ?>"
                                src="<?= yii::$app->homeUrl ?>images/gallery/image<?= $image->id ?>.<?= $image->image ?>">
                            <div class="ctryTleWrp">
                                <div class="ctryTle"><?= $image->title ?></div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php endif; endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>

</div>