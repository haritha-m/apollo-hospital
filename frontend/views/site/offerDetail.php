<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="offersPage">
    <section id="innerSmBnr" class="jarallax" data-jarallax data-type="scroll">
        <img class="lazy innerBg jarallax-img" src="<?= Yii::$app->homeUrl ?>assets/images/doctorDetailBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/doctorDetailBnr.jpg" loading="lazy" alt="bannerImage"
            width="1920" height="200">
    </section>

    <section id="packageDtlsSec" class="inrCntSec">
        <div class="topSec secWrap">
            <div class="designBxElemntOuter lft">
                <div class="elementOne"></div>
                <div class="elementTwo"></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="lftWrap col-xl-6 col-lg-6" data-aos="fade-up">
                        <!--<div class="contentBx">-->
                        <!--    <div class="secTitleWrap">-->
                        <!--        <h2 class="secTitle"><?= $offer->sub_title1 ?></h2>-->
                        <!--    </div>-->
                        <!--    <?= $offer->sub_title1_description ?>-->
                        <!--    <a href="#appointmentForm" class="pckgprice">-->
                        <!--            Book Appointment-->
                        <!--    </a>-->
                        <!--</div>-->
                        <div class="formOuter">
                            <form class="customForm">
                                <fieldset>
                                    <h4 class="formTle">Request an Appointment</h4>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="name" name="name"
                                                    placeholder="Name" required>
                                                <input type="hidden" class="form-control" id="offer" name="offer"
                                                    placeholder="Name" value="<?= $offer->title?>">

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="email" name="email"
                                                    placeholder="Email"
                                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                                    title="Xyz@gmail.com /Xyz@gmail.in" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                        <select class="form-select form-control" id="district" name="district_id"
                                            required="">
                                            <option selected disabled value="">Select Your District</option>
                                            <?php if(count($districts) > 0): ?>
                                            <?php foreach($districts as $district): ?>
                                            <option value="<?= $district->id ?>"><?= $district->name ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group panchayat" style="display: none;">
                                        <select class="form-select form-control" id="panchayat" name="panchayat_id">
                                            <option selected disabled value="">Select Your Panchayat</option>
                                        </select>
                                    </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="mobile" name="mobile"
                                                    placeholder="Mobile" onkeypress="return onlyNumberKey(event)"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Message"
                                                    style="height: 80px" id="message" name="message"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="btnWrap">
                                                <button type="submit"
                                                    class="btnSbmt hoveranim"><span>submit</span></button>
                                                <div class="testmsg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="rgtWrap col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade-up">
                        <div class="contntImg">
                            <img class="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $offer->id ?>.<?= $offer->image ?>"
                                data-src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $offer->id ?>.<?= $offer->image ?>"
                                alt="<?= $offer->image_alt ?>" width="800" height="600">
                            <div class="designBxElemntOuter">
                                <div class="elementOne"></div>
                                <div class="elementTwo"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="bttmSec secWrap" id="appointmentForm">-->
        <!--    <div class="container">-->
        <!--        <div class="row">-->
        <!--            <div class="rgtWrap col-12" data-aos="fade-up">-->
        <!--                <div class="formOuter">-->
        <!--                    <form class="customForm">-->
        <!--                        <fieldset>-->
        <!--                            <h4 class="formTle">Request an Appointment</h4>-->
        <!--                            <div class="row">-->
        <!--                                <div class="col-sm-6">-->
        <!--                                    <div class="form-group">-->
        <!--                                        <input type="text" class="form-control" id="name" name="name"-->
        <!--                                            placeholder="Name" required>-->
        <!--                                        <input type="hidden" class="form-control" id="offer" name="offer"-->
        <!--                                            placeholder="Name" value="<?= $offer->title?>">-->

        <!--                                    </div>-->
        <!--                                </div>-->
        <!--                                <div class="col-sm-6">-->
        <!--                                    <div class="form-group">-->
        <!--                                        <input type="email" class="form-control" id="email" name="email"-->
        <!--                                            placeholder="Email"-->
        <!--                                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"-->
        <!--                                            title="Xyz@gmail.com /Xyz@gmail.in" required>-->
        <!--                                    </div>-->
        <!--                                </div>-->
        <!--                                <div class="col-sm-6">-->
        <!--                                     <div class="form-group">-->
        <!--                                <select class="form-select form-control" id="district" name="district_id"-->
        <!--                                    required="">-->
        <!--                                    <option selected disabled value="">Select Your District</option>-->
        <!--                                    <?php if(count($districts) > 0): ?>-->
        <!--                                    <?php foreach($districts as $district): ?>-->
        <!--                                    <option value="<?= $district->id ?>"><?= $district->name ?></option>-->
        <!--                                    <?php endforeach; ?>-->
        <!--                                    <?php endif; ?>-->
        <!--                                </select>-->
        <!--                            </div>-->
        <!--                            <div class="form-group panchayat" style="display: none;">-->
        <!--                                <select class="form-select form-control" id="panchayat" name="panchayat_id">-->
        <!--                                    <option selected disabled value="">Select Your Panchayat</option>-->
        <!--                                </select>-->
        <!--                            </div>-->
        <!--                                </div>-->
        <!--                                <div class="col-sm-6">-->
        <!--                                    <div class="form-group">-->
        <!--                                        <input type="text" class="form-control" id="mobile" name="mobile"-->
        <!--                                            placeholder="Mobile" onkeypress="return onlyNumberKey(event)"-->
        <!--                                            required>-->
        <!--                                    </div>-->
        <!--                                </div>-->
        <!--                                <div class="col-12">-->
        <!--                                    <div class="form-group">-->
        <!--                                        <textarea class="form-control" placeholder="Message"-->
        <!--                                            style="height: 80px" id="message" name="message"></textarea>-->
        <!--                                    </div>-->
        <!--                                </div>-->
        <!--                                <div class="col-sm-6">-->
        <!--                                    <div class="btnWrap">-->
        <!--                                        <button type="submit"-->
        <!--                                            class="btnSbmt hoveranim"><span>submit</span></button>-->
        <!--                                        <div class="testmsg"></div>-->
        <!--                                    </div>-->
        <!--                                </div>-->
        <!--                            </div>-->
        <!--                        </fieldset>-->
        <!--                    </form>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
        <div class="mrPckgSec">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="secTitleWrap" data-aos="fade-up">
                            <h2 class="secTitle">Related Offers</h2>
                        </div>
                    </div>
                    <div class="col-12" data-aos="fade-up">
                        <div class="owl-carousel pckgCarousel owl-theme">
                            <?php
                            if(!empty($offer_all))
                            {
                                foreach($offer_all as $data)
                                {
                                    ?>

                            <a href="<?= Url::to(['site/offerdetails', 'name' => $data->canonical_name]); ?>"
                                class="item">
                                <div class="pckgCard">
                                    <div class="cImg">
                                        <img class="lazy"
                                            src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $data->id ?>.<?= $data->image ?>"
                                            data-src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $data->id ?>.<?= $data->image ?>"
                                            loading="lazy" alt="<?= $data->image_alt ?>" width="340" height="250">
                                    </div>
                                    <div class="cCnt">
                                        <h5 class="dName"><?= $data->title?></h5>
                                    </div>
                                </div>
                            </a>
                            <?php
                          }
                      }
                      ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<!-- OWL_SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
    integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
    integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
$('.pckgCarousel').owlCarousel({
    loop: true,
    rewind: true,
    nav: false,
    dots: false,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            loop: $('.pckgCarousel .item').length > 1 ? true : false,
        },
        480: {
            items: 2,
            loop: $('.pckgCarousel .item').length > 2 ? true : false,
        },
        768: {
            items: 3,
            loop: $('.pckgCarousel .item').length > 3 ? true : false,
        },
        1000: {
            items: 4,
            loop: $('.pckgCarousel .item').length > 4 ? true : false,
            margin: 15,
        },
        1200: {
            items: 4,
            loop: $('.pckgCarousel .item').length > 4 ? true : false,
            margin: 30,
        }
    }
})
</script>









<script>
$(document).ready(function() {





    $(document).on('submit', '.customForm', function(e) {


        e.preventDefault();
        var str = $(this).serialize();
        $.ajax({
            type: "POST",
            url: '<?= Yii::$app->homeUrl; ?>ajax/offer-enquiry',
            data: str,
            success: function(data) {

                if (data == 1) {


                    $('.testmsg').html(
                        '<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'
                    );
                }
                $('.customForm')[0].reset();
                $('#name').val("");
                $('#email').val("");
                $('#mobile').val("");
                $('#district_id').val("");
                $('#panchayat_id').val("");
                $('#message').val("");
                setTimeout(function() {
                    $('#email-alert').remove();
                }, 4000);
            }
        });
    });

});

function onlyNumberKey(evt) {
    // Only ASCII charactar in that range allowed 
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
</script>
<script>
$(document).on('change', '#district', function(e) {
    $('#panchayat').empty().append(
        '<option selected disabled value="">Select Your Panchayat</option>')
    $('.panchayat').hide()
    $('#schedulingappointments-other').val('')
    $('.otherLocation').hide()
    $('#schedulingappointments-other').attr('required', false)
    $('#panchayat').attr('required', false)

    var district_id = $(this).val();
    if (district_id == 7 || district_id == 8) {
        $.ajax({
            type: "POST",
            url: '<?= yii::$app->homeUrl ?>ajax/get-panchayat',
            data: {
                id: district_id
            },
            dataType: 'json',
            success: function(data) {
                var panchayats = data.panchayats
                if (panchayats) {
                    $('#panchayat').empty().append(
                        '<option selected disabled value="">Select Your Panchayat</option>')
                    $.each(panchayats, function(key, value) {
                        $('#panchayat').append(
                            '<option value="' +
                            value.id + '">' + value.name + '</option>');
                    });
                    $('.panchayat').show()
                    $('#panchayat').attr('required', true)
                } else {
                    return false;
                }
            }
        });
    } else if (district_id == 15) {
        $('.otherLocation').show()
        $('#schedulingappointments-other').attr('required', true)
    }
});

function onlyNumberKey(evt) {
    // Only ASCII charactar in that range allowed 
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
$(document).ready(function() {
    $("#schedulingappointments-name").keypress(function(event) {
        var inputValue = event.which;
        if ((inputValue > 47 && inputValue < 58)) {
            event.preventDefault();
        }
    });
    var flag = <?= $flag ?>;

    if (flag == 1) {
        $('.testmsg').html(
            '<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'
        );
    }
    setTimeout(function() {
        $('#email-alert').remove();
    }, 4000);
});
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}
</script>