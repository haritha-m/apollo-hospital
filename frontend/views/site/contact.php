<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="contactPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">



<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                 <img class="lazy innerBg jarallax-img" src="assets/images/contactPgBnr.jpg"
            data-src="assets/images/contactPgBnr.jpg" alt="bannerImage" width="1920" height="590">


        <?php
    }
    ?> 


    


        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h3 class="bnrSubHd"><?= $banner_images->description ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="getTouchSec">
        <div class="designBxElemntOuter lft">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="designBxElemntOuter rgt">
            <div class="elementOne"></div>
            <div class="elementTwo"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="lftSide" data-aos="fade-up" data-aos-delay="300">
                    <div class="contentBx">
                        <div class="secTitleWrap">
                            <h2 class="secTitle"><?= $contact_info->title?></h2>
                        </div>
                        <p><?= $contact_info->caption?></p>
                    </div>
                </div>
                <div class="rgtSide">
                    <div class="gTouchCardWrap">
                        <div class="gTouchCard" data-aos="fade-up" data-aos-delay="300">
                            <div class="cardIcon">
                                <div class="iconBg"></div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="31" height="23.613"
                                    viewBox="0 0 31 23.613">
                                    <path id="Path_986" data-name="Path 986"
                                        d="M28.275,61H2.725A2.729,2.729,0,0,0,0,63.725V81.889a2.729,2.729,0,0,0,2.725,2.725H28.275A2.729,2.729,0,0,0,31,81.889V63.725A2.729,2.729,0,0,0,28.275,61ZM27.9,62.816,15.558,75.158,3.11,62.816ZM1.816,81.513V64.092l8.748,8.673ZM3.1,82.8l8.753-8.753,3.067,3.041a.908.908,0,0,0,1.282,0l2.991-2.991L27.9,82.8Zm26.083-1.284-8.706-8.706L29.184,64.1Z"
                                        transform="translate(0 -61)" fill="#b38d4f" />
                                </svg>
                            </div>
                            <div class="cardCnt">
                                <h5 class="cntTle">Email</h5>
                                <a href="mailto:<?= $contact_info->email?>
"><?= $contact_info->email?></a>
                            </div>
                        </div>
                        <div class="gTouchCard" data-aos="fade-up" data-aos-delay="300">
                            <div class="cardIcon">
                                <div class="iconBg"></div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24.777" height="24.777"
                                    viewBox="0 0 24.777 24.777">
                                    <path id="Path_981" data-name="Path 981"
                                        d="M24.98,18.835c-.044-.036-5-3.565-6.333-3.344-.645.114-1.013.554-1.753,1.435-.119.142-.406.482-.627.723a10.29,10.29,0,0,1-1.364-.555,11.315,11.315,0,0,1-5.221-5.221,10.289,10.289,0,0,1-.555-1.364c.243-.222.583-.509.728-.631.876-.735,1.316-1.1,1.43-1.75C11.52,6.791,7.979,1.841,7.943,1.8A1.891,1.891,0,0,0,6.534,1C5.1,1,1,6.316,1,7.212c0,.052.075,5.341,6.6,11.976,6.627,6.515,11.916,6.59,11.968,6.59.9,0,6.212-4.1,6.212-5.534a1.892,1.892,0,0,0-.8-1.409Zm-5.506,5.286c-.717-.061-5.16-.647-10.71-6.1C3.285,12.445,2.715,7.994,2.658,7.3A22.344,22.344,0,0,1,6.544,2.711c.033.033.077.083.133.147A29.23,29.23,0,0,1,9.629,7.866a9.817,9.817,0,0,1-.839.752A8.286,8.286,0,0,0,7.541,9.793a.826.826,0,0,0-.141.62,9.43,9.43,0,0,0,.8,2.181,12.975,12.975,0,0,0,5.986,5.985,9.406,9.406,0,0,0,2.181.8.826.826,0,0,0,.62-.141,8.321,8.321,0,0,0,1.179-1.254c.259-.309.605-.721.737-.837A29.028,29.028,0,0,1,23.918,20.1c.069.058.117.1.149.131a22.329,22.329,0,0,1-4.594,3.888Z"
                                        transform="translate(-1 -1)" fill="#b38d4f" />
                                    <path id="Path_982" data-name="Path 982"
                                        d="M16.826,8.652a4.96,4.96,0,0,1,4.955,4.955.826.826,0,0,0,1.652,0A6.615,6.615,0,0,0,16.826,7a.826.826,0,0,0,0,1.652Z"
                                        transform="translate(-3.611 -2.045)" fill="#b38d4f" />
                                    <path id="Path_983" data-name="Path 983"
                                        d="M16.826,3.652a9.1,9.1,0,0,1,9.085,9.085.826.826,0,0,0,1.652,0A10.749,10.749,0,0,0,16.826,2a.826.826,0,0,0,0,1.652Z"
                                        transform="translate(-3.611 -1.174)" fill="#b38d4f" />
                                </svg>
                            </div>
                            <div class="cardCnt">
                                <h5 class="cntTle">Contact no</h5>
                                <?= $contact_info->conatct_number?>
                            </div>
                        </div>
                        <div class="gTouchCard" data-aos="fade-up" data-aos-delay="300">
                            <div class="cardIcon">
                                <div class="iconBg"></div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.747" height="33.011"
                                    viewBox="0 0 27.747 33.011">
                                    <path id="Path_975" data-name="Path 975"
                                        d="M16.974,34.554a1.154,1.154,0,0,1-.826-.343L7.169,25.233a13.874,13.874,0,1,1,19.61,0L17.8,34.211a1.154,1.154,0,0,1-.826.343Zm0-30.663A11.535,11.535,0,0,0,8.821,23.58l8.153,8.153,8.153-8.153A11.535,11.535,0,0,0,16.974,3.892Z"
                                        transform="translate(-3.1 -1.544)" fill="#b38d4f" />
                                    <path id="Path_976" data-name="Path 976"
                                        d="M14.1,17.941A5.846,5.846,0,1,1,19.941,12.1,5.846,5.846,0,0,1,14.1,17.941Zm0-9.353A3.507,3.507,0,1,0,17.6,12.1,3.507,3.507,0,0,0,14.1,8.588Z"
                                        transform="translate(-0.222 1.087)" fill="#b38d4f" />
                                </svg>
                            </div>
                            <div class="cardCnt">
                                <h5 class="cntTle">Address</h5>
                                <p><?= $contact_info->address?></p>
                            </div>
                        </div>
                        <div class="gTouchCard" data-aos="fade-up" data-aos-delay="300">
                            <div class="cardIcon">
                                <div class="iconBg"></div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="35.917" height="35.892"
                                    viewBox="0 0 35.917 35.892">
                                    <path id="Path_989" data-name="Path 989"
                                        d="M32.986,8.845A17.673,17.673,0,0,0,.61,13.108,17.676,17.676,0,0,0,24,34.188a.69.69,0,0,0-.5-1.289A16.188,16.188,0,0,1,7.93,30.758l.619-3.229.3-.1a2.151,2.151,0,0,0,1.379-2.707l-.219-.673a.762.762,0,0,1,0-.476A2.148,2.148,0,0,0,8.379,20.8l-1.559-.3L3.814,18.207a.691.691,0,0,0-.631-.107l-1.761.572a16.279,16.279,0,0,1,0-1.885c.069.065.115.109.143.138a.724.724,0,0,0,.675.346,10.03,10.03,0,0,0,1.237-.141c.583-.086,1.162-.183,1.168-.184a.69.69,0,0,0,.456-1.07l-.847-1.242,1.213-.867a.691.691,0,0,0,.2-.218l1.88-3.282L8.3,9.454a2.917,2.917,0,0,0,.563-3.093A3.176,3.176,0,0,0,7.6,4.88a16.271,16.271,0,0,1,9.888-3.485L16.259,2.941l-4.572,1.6a.69.69,0,0,0-.41.388L10,8.028a.691.691,0,0,0,.076.664L11.941,11.3a6.563,6.563,0,0,0-.838,1.039c-.136.193-.265.376-.354.478s-.155.176-.231.262a6.462,6.462,0,0,0-1.111,1.541,4.115,4.115,0,0,0,.341,4.16,3.875,3.875,0,0,0,3.659,1.57,5.376,5.376,0,0,0,.675-.136c.592-.146.78-.161.969.017.092.087.1.1.1.4a2.684,2.684,0,0,0,.062.684,2.182,2.182,0,0,0,.584.953,1.547,1.547,0,0,1,.25.319,1.719,1.719,0,0,1-.084,1.428l-.054.164a3.875,3.875,0,0,0,.507,2.771c.125.275.242.534.32.763.621,1.842,1.086,2.264,1.46,2.434a1.439,1.439,0,0,0,.6.128,3.861,3.861,0,0,0,2.511-1.51,2.122,2.122,0,0,0,.427-1.005,1.1,1.1,0,0,1,.088-.309,1.809,1.809,0,0,1,.226-.291,2.4,2.4,0,0,0,.565-.938,1.52,1.52,0,0,1,.469-.735l.152-.164a2.214,2.214,0,0,0,.432-2.575c-.211-.681.158-1.117,1.051-1.948a7.459,7.459,0,0,0,1.1-1.181,1.491,1.491,0,0,0,.292-1.383c-.25-.579-.885-.677-1.395-.755a2.288,2.288,0,0,1-.6-.141,3.264,3.264,0,0,1-.941-1.426c-.05-.112-.1-.223-.15-.33-.09-.192-.179-.424-.274-.67a5.793,5.793,0,0,0-1.021-1.9,4.41,4.41,0,0,0-1.981-.88c-.244-.066-.474-.128-.625-.184a.69.69,0,0,0-.4-.025,1.792,1.792,0,0,0-.736.3.858.858,0,0,0-.246.331,7.9,7.9,0,0,1-.814-.5l-.013-.009a.979.979,0,0,0-.062-.665c-.386-.816-1.666-.752-1.919-.731-.143.012-.325.018-.517.025-.3.011-.635.023-.965.06l.017-.047a1.766,1.766,0,0,1,1.658-1.166h.393a.69.69,0,1,0,0-1.381h-.393a3.152,3.152,0,0,0-2.673,1.491L11.422,8.2l1.013-2.456L16.9,4.179a.691.691,0,0,0,.311-.221L19.2,1.466A16.284,16.284,0,0,1,31.791,9.535a16.114,16.114,0,0,1,1.627,3.952l-.293.261a1.844,1.844,0,0,0-.615,1.342c0,.006,0,.011,0,.017L32.033,13.8a1.724,1.724,0,0,0-.311-.533l-.851-1a1.73,1.73,0,0,0-1.318-.607H28.513a1,1,0,0,0-.832,1.555l.105.158a7.653,7.653,0,0,1-2.28,1.354l-1.427-2.734v-.61a.69.69,0,0,0-.214-.5L22.58,9.656a.691.691,0,0,0-.248-.152L21.175,9.1a.69.69,0,1,0-.458,1.3l1.017.357.964.92v.484a.691.691,0,0,0,.078.32L24.563,15.9a.69.69,0,0,0,.851.328l.52-.192a9.037,9.037,0,0,0,3.24-2.074.69.69,0,0,0,.086-.871l-.037-.055h.33a.351.351,0,0,1,.268.123l.851,1a.351.351,0,0,1,.063.108l.939,2.581a.69.69,0,0,0,1.137.252l.447-.447a2.156,2.156,0,0,0,.571-1.038,16.351,16.351,0,0,1-6.071,14.87.69.69,0,1,0,.853,1.086A17.676,17.676,0,0,0,32.986,8.845ZM3.265,19.525,6.113,21.69a.69.69,0,0,0,.286.128l1.716.335a.767.767,0,0,1,.582.992,2.134,2.134,0,0,0,0,1.333l.219.673a.768.768,0,0,1-.493.967l-.686.223a.69.69,0,0,0-.465.527l-.552,2.883a16.475,16.475,0,0,1-3.145-3.923,16.206,16.206,0,0,1-2.006-5.75ZM7.29,8.512l-.812.87a.693.693,0,0,0-.094.128l-1.845,3.22L2.9,13.9a.69.69,0,0,0-.169.951l.613.9c-.353.053-.671.1-.881.119l-.127-.119C2.171,15.6,1.943,15.4,1.6,15.1A16.321,16.321,0,0,1,6.466,5.863,1.8,1.8,0,0,1,7.585,6.887,1.532,1.532,0,0,1,7.29,8.512Zm5.63,3.773a4.428,4.428,0,0,1,1.545-.217c.209-.007.407-.014.583-.029a1.878,1.878,0,0,1,.294,0,.69.69,0,0,0,.318.859c.13.069.31.19.5.317a3.979,3.979,0,0,0,1.726.8,1.146,1.146,0,0,0,.938-.391l.029-.029a.74.74,0,0,0,.185-.226c.11.032.226.063.345.1a4.6,4.6,0,0,1,1.338.495,4.9,4.9,0,0,1,.737,1.451c.1.269.2.522.312.757.047.1.094.2.141.31A4.312,4.312,0,0,0,23.357,18.5a2.854,2.854,0,0,0,1.149.35l.15.024a6.677,6.677,0,0,1-.876.919c-.855.8-1.919,1.787-1.429,3.367.231.746.242.827-.13,1.231l-.142.153a2.837,2.837,0,0,0-.778,1.263,1.078,1.078,0,0,1-.281.439,3.083,3.083,0,0,0-.386.512,2.238,2.238,0,0,0-.25.734.845.845,0,0,1-.144.4,4.025,4.025,0,0,1-.872.771,1.139,1.139,0,0,1-.579.23,4.926,4.926,0,0,1-.744-1.622c-.1-.3-.237-.6-.371-.893a2.96,2.96,0,0,1-.45-1.775l.052-.158a3.016,3.016,0,0,0,.046-2.391,2.49,2.49,0,0,0-.5-.716,1.284,1.284,0,0,1-.264-.35,1.493,1.493,0,0,1-.023-.349A1.713,1.713,0,0,0,16,19.228a2.224,2.224,0,0,0-2.245-.349,4.331,4.331,0,0,1-.5.1,2.481,2.481,0,0,1-2.377-1,2.734,2.734,0,0,1-.214-2.764A5.243,5.243,0,0,1,11.551,14c.08-.09.161-.181.242-.274.135-.155.283-.365.44-.588A4.992,4.992,0,0,1,12.919,12.285Z"
                                        transform="translate(0.287 0.286)" fill="#b38d4f" stroke="#b38d4f"
                                        stroke-width="0.5" />
                                </svg>
                            </div>
                            <div class="cardCnt">
                                <h5 class="cntTle">International Patients</h5>
                               <?= $contact_info->international_patients?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="queFormSec">
        <div class="container">
            <div class="queFormWrap">
                <div class="formOuter">
                    <form class="customForm">
                        <fieldset>
                            <h4 class="formTle"><?= $contact_info->enquiry_title?></h4>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" pattern="[a-zA-Z\s]+" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Xyz@gmail.com /Xyz@gmail.in" required>
                            </div>
                             <div class="form-group">
                                        <select class="form-select form-control" id="district" name="district_id"
                                            required="">
                                            <option selected disabled value="">Select Your District</option>
                                            <?php if(count($districts) > 0): ?>
                                            <?php foreach($districts as $district): ?>
                                            <option value="<?= $district->id ?>"><?= $district->name ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-group panchayat" style="display: none;">
                                        <select class="form-select form-control" id="panchayat" name="panchayat_id">
                                            <option selected disabled value="">Select Your Panchayat</option>
                                        </select>
                                    </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" onkeypress="return onlyNumberKey(event)"  required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Query"  id="message" name="message"style="height: 80px"></textarea>
                            </div>
                            <div class="btnWrap">
                                <button type="submit" class="btnSbmt hoveranim"><span>submit</span></button>
                                <div class="testmsg"></div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="contntImg jarallax" data-jarallax data-type="scroll">
                    <img class="lazy jarallax-img" src="assets/images/queFormImg.jpg" data-src="assets/images/queFormImg.jpg"
                        alt="contact form" width="720" height="620">
                </div>
            </div>
    </section>

    <section id="loctnSec">
        <div class="sp-container lft">
            <div class="row">
                <div class="mapWrap col-xl-7 col-lg-6 col-12">
                    <iframe
                        src="<?= $contact_info->map_link?>"
                        width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="loctnListWrap col-xl-5 col-lg-6 col-12">
                    <div class="contentBx">
                        <div class="secTitleWrap">
                            <h2 class="secTitle"><?= $contact_info->near_by_place_title?></h2>
                        </div>
                        <p><?= $contact_info->near_by_place_caption?></p>
                        <?php
                        if(!empty($locations))
                        {
                            foreach($locations as $loc)
                            {
                                ?>

                        <div class="nearLctnCard" data-aos="fade-up" data-aos-delay="300">
                            <div class="cardIcon">
                                <img src="assets/images/airport.png" alt="airport">
                            </div>
                            <div class="cardCnt">
                                <h5 class="cntTle"><?= $loc->title?></h5>
                                <p><?= $loc->sub_title?></p>
                            </div>
                        </div>
                       <?php
                   }
               }
               ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>





<script>
    $(document).ready(function () {




        
        $(document).on('submit', '.customForm', function (e) {
           

            e.preventDefault();
            var str = $(this).serialize();
            $.ajax({
                type: "POST",
                url: '<?= Yii::$app->homeUrl; ?>ajax/contact-enquiry',
                data: str,
                success: function (data)
                {

                    if (data == 1) {
                        
                      
                    $('.testmsg').html('<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>');
                    }
                    $('.customForm')[0].reset();
                    $('#name').val("");
                    $('#email').val("");
                     $('#mobile').val("");
                      $('#district_id').val("");
                $('#panchayat_id').val("");
                    $('#message').val("");
                     setTimeout(function () {
                        $('#email-alert').remove();
                    }, 4000);
                }
            });
        });
       
    });
      function onlyNumberKey(evt) {
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
        }
</script>
<script>
$(document).on('change', '#district', function(e) {
    $('#panchayat').empty().append(
        '<option selected disabled value="">Select Your Panchayat</option>')
    $('.panchayat').hide()
    $('#schedulingappointments-other').val('')
    $('.otherLocation').hide()
    $('#schedulingappointments-other').attr('required', false)
    $('#panchayat').attr('required', false)

    var district_id = $(this).val();
    if (district_id == 7 || district_id == 8) {
        $.ajax({
            type: "POST",
            url: '<?= yii::$app->homeUrl ?>ajax/get-panchayat',
            data: {
                id: district_id
            },
            dataType: 'json',
            success: function(data) {
                var panchayats = data.panchayats
                if (panchayats) {
                    $('#panchayat').empty().append(
                        '<option selected disabled value="">Select Your Panchayat</option>')
                    $.each(panchayats, function(key, value) {
                        $('#panchayat').append(
                            '<option value="' +
                            value.id + '">' + value.name + '</option>');
                    });
                    $('.panchayat').show()
                    $('#panchayat').attr('required', true)
                } else {
                    return false;
                }
            }
        });
    } else if (district_id == 15) {
        $('.otherLocation').show()
        $('#schedulingappointments-other').attr('required', true)
    }
});

function onlyNumberKey(evt) {
    // Only ASCII charactar in that range allowed 
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
$(document).ready(function() {
    $("#schedulingappointments-name").keypress(function(event) {
        var inputValue = event.which;
        if ((inputValue > 47 && inputValue < 58)) {
            event.preventDefault();
        }
    });
    var flag = <?= $flag ?>;

    if (flag == 1) {
        $('.testmsg').html(
            '<div id="email-alert" style="color: #fff;background: #006b8e;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'
        );
    }
    setTimeout(function() {
        $('#email-alert').remove();
    }, 4000);
});
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}
</script>
