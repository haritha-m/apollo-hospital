<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($cme->meta_title) && $cme->meta_title != '') {
    $this->title = $cme->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="CmeDetailsPage mediaGalleryPage">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/newsMediaBg.jpg" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="bnrMainHd">Continuing Medical Education (CME)</h1>
                        <h4 class="bnrSubHd">To share and update best medical practices around the world, as well as inside our medical fraternity</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="media-gallery">
        <div class="container">
            <div class="row">
                            <?php
                            
                            if(!empty($gallery))
                            {
                                
                            foreach($gallery as $gal)
                            {
                                if($gal->type == 2)
                                {
                                ?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="<?= Yii::$app->homeUrl ?>images/cme/gallery/video<?= $gal->id ?>.<?= $gal->video ?>" data-fancybox="photos" data-caption="Lorem ipsum">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353" data-src="<?= Yii::$app->homeUrl ?>images/cme/gallery/thumb<?= $gal->id ?>.<?= $gal->image1 ?>" src="<?= Yii::$app->homeUrl ?>images/cme/gallery/thumb<?= $gal->id ?>.<?= $gal->image1 ?>">
                        </div>
                        <!--<div class="gallDisc">-->
                        <!--    <h6 class="disc">Lorem ipsum</h6>-->
                        <!--</div>-->
                    </a>
                </div>
                <?php
                                }
                                else{
                                    ?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6" data-aos="fade-up">
                    <a href="<?= Yii::$app->homeUrl ?>images/cme/gallery/image<?= $gal->id ?>.<?= $gal->image ?>" data-fancybox="photos" data-caption="Lorem ipsum">
                        <div class="photo-gallery">
                            <img class="lazy" loading="lazy" width="353" height="353" data-src="<?= Yii::$app->homeUrl ?>images/cme/gallery/image<?= $gal->id ?>.<?= $gal->image ?>"src="<?= Yii::$app->homeUrl ?>images/cme/gallery/image<?= $gal->id ?>.<?= $gal->image ?>">
                        </div>
                        <!--<div class="gallDisc">-->
                        <!--    <h6 class="disc">Lorem ipsum</h6>-->
                        <!--</div>-->
                    </a>
                </div>
                <?php
                                }
                            }
                            }
                            ?>
            </div>
        </div>
    </section>

    <!-- fancybox -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        // fancybox
        $('[data-fancybox="photos"]').fancybox({
            buttons: [
                "zoom",
                "slideShow",
                "thumbs",
                "close"
            ],
            loop: true,
            protect: true
        });
    </script>   

</div>

