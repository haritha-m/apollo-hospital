<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="offersPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">

        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg jarallax-img" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>assets/images/offersPgBnr.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/offersPgBnr.jpg" alt="bannerImage" width="1920"
            height="590">

        <?php
    }
    ?>


















        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="offersSec" class="inrCntSec">
        <div class="container">
            <div class="row">
                <?php
                if(!empty($offers))
                {
                    foreach($offers as $offer)
                    {
                        ?>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-aos="fade-up" data-aos-delay="300">
                    <a href="<?= Url::to(['site/offerdetails', 'name' => $offer->canonical_name]); ?>" class="ofrCard">
                        <div class="cImg">
                            <img class="lazy" loading="lazy" width="340" height="340"
                                data-src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $offer->id ?>.<?= $offer->image ?>"
                                src="<?= Yii::$app->homeUrl ?>images/offers/image<?= $offer->id ?>.<?= $offer->image ?>">
                        </div>
                        <div class="cCnt">
                             <div class="Date"><?= date('F d, Y', strtotime($offer->date)) ?></div>
                            <h5 class="cTle"><?= $offer->title ?></h5>
                        </div>
                    </a>
                </div>
                <?php
           }
       }
       ?>
            </div>
        </div>
    </section>

</div>