<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="careerPage">
    <section id="innerBanner" class="jarallax" data-jarallax data-type="scroll">



        <?php
            if(!empty($banner_images))
            {
                ?>



        <img class="lazy innerBg" loading="lazy"
            src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





        <?php
    }
    else{
        ?>
        <img class="lazy innerBg jarallax-img" loading="lazy" src="assets/images/careerPgBnr.jpg"
            data-src="assets/images/careerPgBnr.jpg" alt="bannerImage" width="1920" height="590">



        <?php
    }
    ?>



        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $career->title?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="careerSec" class="inrCntSec">
        <div class="topSec secWrap">
            <div class="designBxElemntOuter lft">
                <div class="elementOne"></div>
                <div class="elementTwo"></div>
            </div>
            <div class="designBxElemntOuter rgt">
                <div class="elementOne"></div>
                <div class="elementTwo"></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="lftWrap col-xl-6 col-lg-6" data-aos="fade-up">
                        <div class="contentBx">
                            <!--<div class="secTitleWrap">-->
                            <!--    <h2 class="secTitle"></h2>-->
                            <!--</div>-->
                            <?= $career->description?>
                        </div>
                    </div>
                    <div class="rgtWrap col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade-up">
                        <div class="contntImg">
                            <img class="lazy"
                                src="<?= Yii::$app->homeUrl ?>images/career/common<?= $career->id ?>.<?= $career->image ?>"
                                data-src="<?= Yii::$app->homeUrl ?>images/career/common<?= $career->id ?>.<?= $career->image ?>"
                                alt="<?= $career->image_alt ?>" width="800" height="600">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bttmSec secWrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-12 mx-auto" data-aos="fade-up">
                        <div class="secTitleWrap center">
                            <h2 class="secTitle">We're hiring now!</h2>
                        </div>
                        <div class="accordion accordion-flush accoWrap" id="carrAccordion" data-aos="fade-up">

                            <?php
                          if(!empty($career_list))
                          {
                            $i=0;
                            foreach($career_list as $list)
                            {
                                $i++;
                                ?>

                            <div class="accordion-item">
                                <div class="accordion-header" id="headingOne<?=$i?>">
                                    <button class="accordion-button <?= $i == 1 ? '' : 'collapsed' ?>" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#collapseOne<?=$i?>"
                                        aria-expanded="false" aria-controls="collapseOne<?=$i?>">
                                        <!--<div class="accordion-header" id="headingOne<?=$i?>">-->
                                        <!--    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"-->
                                        <!--        data-bs-target="#collapseOne<?=$i?>" aria-expanded="false" aria-controls="collapseOne<?=$i?>">-->
                                        <div class="jobTle"><?= $list->title?></div>
                                        <!--<div class="jobLctn">-->
                                        <!--    <svg viewBox="0 0 17.8 21.177">-->
                                        <!--        <g id="pin_2_" data-name="pin (2)" transform="translate(-3.1 -1.543)">-->
                                        <!--            <path id="Path_975" data-name="Path 975"-->
                                        <!--                d="M12,22.72a.74.74,0,0,1-.53-.22L5.71,16.74a8.9,8.9,0,1,1,12.58,0L12.53,22.5a.74.74,0,0,1-.53.22ZM12,3.05A7.4,7.4,0,0,0,6.77,15.68L12,20.91l5.23-5.23A7.4,7.4,0,0,0,12,3.05Z" />-->
                                        <!--            <path id="Path_976" data-name="Path 976"-->
                                        <!--                d="M12,13.75A3.75,3.75,0,1,1,15.75,10,3.75,3.75,0,0,1,12,13.75Zm0-6A2.25,2.25,0,1,0,14.25,10,2.25,2.25,0,0,0,12,7.75Z" />-->
                                        <!--        </g>-->
                                        <!--    </svg>-->
                                        <!--    <p><?= $list->location?></p>-->
                                        <!--</div>-->
                                        <!--<div class="jobExp">-->
                                        <!--    <svg viewBox="0 0 20.342 17.878">-->
                                        <!--        <path id="portfolio"-->
                                        <!--            d="M19.75,2.384H14.343v-.6A1.79,1.79,0,0,0,12.555,0H7.787A1.79,1.79,0,0,0,6,1.788v.6H.6a.6.6,0,0,0-.6.6V16.09a1.79,1.79,0,0,0,1.788,1.788H18.554a1.79,1.79,0,0,0,1.788-1.788V2.99a.579.579,0,0,0-.592-.606Zm-12.558-.6a.6.6,0,0,1,.6-.6h4.768a.6.6,0,0,1,.6.6v.6H7.191ZM18.919,3.576,17.068,9.128a.6.6,0,0,1-.565.408H13.151v-.6a.6.6,0,0,0-.6-.6H7.787a.6.6,0,0,0-.6.6v.6H3.839a.6.6,0,0,1-.565-.408L1.423,3.576Zm-6.96,5.959v1.192H8.383V9.535ZM19.15,16.09a.6.6,0,0,1-.6.6H1.788a.6.6,0,0,1-.6-.6V6.652L2.143,9.5a1.785,1.785,0,0,0,1.7,1.222H7.191v.6a.6.6,0,0,0,.6.6h4.768a.6.6,0,0,0,.6-.6v-.6H16.5A1.785,1.785,0,0,0,18.2,9.5l.951-2.852Zm0,0"-->
                                        <!--            transform="translate(0)" />-->
                                        <!--    </svg>-->
                                        <!--    <p><?= $list->experience?></p>-->
                                        <!--</div>-->
                                    </button>
                                    <button type="button" class="aplyBtn" data-bs-toggle="modal"
                                        data-bs-target="#applyModal" data-val="<?= $list->title?>">APPLY NOW</button>
                                </div>
                                <div id="collapseOne<?=$i?>" class="accordion-collapse collapse"
                                    aria-labelledby="headingOne<?=$i?>" data-bs-parent="#carrAccordion">
                                    <div class="accordion-body">
                                        <?php
                                        if($list->about_the_role != '')
                                        {
                                            ?>


                                        <h6 class="discTle">About the Role</h6>
                                        <?= $list->about_the_role?>
                                        <?php
                                        }
                                        ?>

                                        <?php
                                        if($list->what_you_will_do != '')
                                        {
                                            ?>



                                        <?php
                                        }
                                        ?>




                                        <?php
                                        if($list->what_you_wiil_need != '')
                                        {
                                            ?>
                                        <h6 class="discTle">Qualification and experience</h6>
                                        <?= $list->what_you_wiil_need?>
                                        <?php
                                    }
                                    ?>
                                        <button type="button" class="aplyBtn" data-bs-toggle="modal"
                                            data-bs-target="#applyModal" data-val="<?= $list->title?>">APPLY
                                            NOW</button>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>






                        </div>
                        
                            <div class="accordion-body">
                                Apply for
                                <button  class="apply-button" type="button" class="aplyBtn" data-bs-toggle="modal"
                                    data-bs-target="#applyModal" data-val="null">APPLY NOW</button>
                            </div>
                        </div>
                   
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade aplyModal" id="applyModal" tabindex="-1" aria-labelledby="applyModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="formOuter">


                        <form class="customForm" method="post" enctype="multipart/form-data"
                            onsubmit="return validateForm()">

                            <fieldset>
                                <h4 class="formTle">Apply Now</h4>
                                <div class="form-group">



                                    <input type="hidden" id="careerenquiry-job_title" class="form-control"
                                        name="CareerEnquiry[job_title]" value="">

                                    <input type="text" id="careerenquiry-name" class="form-control"
                                        name="CareerEnquiry[name]" required="" pattern="[a-zA-Z\s]+" placeholder="Name"
                                        aria-required="true" aria-invalid="true" required>




                                    <div class="error-msg d-none">input error message</div>
                                </div>
                                <div class="form-group">

                                    <input type="text" id="careerenquiry-email" class="form-control"
                                        name="CareerEnquiry[email]" required="" placeholder="Email" aria-required="true"
                                        aria-invalid="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                        title="Xyz@gmail.com /Xyz@gmail.in" required>


                                </div>
                                <div class="form-group">

                                    <input type="text" id="careerenquiry-mobile" class="form-control"
                                        name="CareerEnquiry[mobile]" required="" placeholder="Mobile"
                                        aria-required="true" aria-invalid="true"
                                        onkeypress="return onlyNumberKey(event)" required>


                                </div>
                                <div class="form-group">
                                    <div class="fileUploadInput">
                                        <label for="file-upload" class="form-control">
                                            <i class="fa fa-cloud-upload"></i> Upload Resume
                                        </label>





                                        <input id="file-upload" name="CareerEnquiry[image]" style="display:none;"
                                            type="file" accept=".pdf, .doc, .docx">



                                        <div class="icon">
                                            <span>upload</span>
                                        </div>
                                    </div>
                                    <span id="errorMessage"></span>
                                </div>
                                <!--<div class="form-group">-->
                                <!--<select class="form-select form-control" id="careerenquiry-country" name="CareerEnquiry[country]" >-->
                                <!--        <option selected disabled value="">Select Your Country</option>-->
                                <!--        <option value="India">India</option>-->
                                <!--        <option value="India2">India2</option>-->
                                <!--        <option value="India3">India3</option>-->
                                <!--    </select>-->



                                <!--</div>-->
                                <div class="btnWrap">
                                    <button type="submit" value="submit"
                                        class="btnSbmt full  hoveranim"><span>submit</span></button>
                                </div>
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- success -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!--<img src="/assets/images/check.png" class="img-fluid" />-->
                <p id="success" align="centre">Your Application completed Successfully</p>
                <button type="button" class="btn btn-default closes" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>
<style>
    button {
  border: none;
  padding: 10px 20px;
  cursor: pointer;
  font-size: 16px;
}

/* Apply button styles */
.apply-button {
  background-color: #4CAF50; /* Green background color */
  color: white; /* Text color */
  border-radius: 5px; /* Rounded corners */
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2); /* Drop shadow */
  transition: background-color 0.3s ease-in-out; /* Transition effect on hover */
}

.apply-button:hover {
  background-color: #45a049; /* Darker green background color on hover */
}
</style>
<script>
$(document).ready(function() {

    var flag = <?php echo $flag?>;

    if (flag == 1) {
        //     $('.testmsg').html('<div id="email-alert" style="color: #fff;background: #2f333c;font-weight: 100;margin-bottom:10px;border: 1px solid black;margin-top: 7px;padding: 12px 15px;">Thank you for contacting us. We will get back to you as soon as possible.</div>'); 
        //   setTimeout(function () {
        //                         $('#email-alert').remove();
        //                     }, 4000);

        //                   alert(flag);
        $('#myModal').modal("show");
    }


    $(".closes").click(function() {
        $("#myModal").modal("hide");

    });

    $(".aplyBtn").click(function() {
        $("#careerenquiry-job_title").val($(this).attr('data-val'));

    });





});

function onlyNumberKey(evt) {
    // Only ASCII charactar in that range allowed 
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
</script>


<script>
function validateForm() {
    var fileUploadField = document.getElementById('file-upload');
    var errorMessage = document.getElementById('errorMessage');
    console.log(fileUploadField)
    if (fileUploadField.value === '') {
        errorMessage.textContent = 'Please select a file';
        errorMessage.style.color = 'red';
        return false; // Prevent form submission
    }

    return true; // Allow form submission
}
</script>



<script>
jQuery(function($) {
    $("#carrAccordion .accordion-header .accordion-button").click(function() {
        $('.accordion-header').removeClass('Current');
        if ($(this).attr('aria-expanded') == 'true') {
            $(this).closest('.accordion-header').addClass('Current');
        }
    });
});
</script>