<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?>
<div id="pageWrapper" class="PatientAndVisitorsPage">

    <section id="innerBanner">
       



<?php
            if(!empty($banner_images))
            {
                ?>
            


 <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>"
            data-src="<?= Yii::$app->homeUrl ?>images/banner-images/banner-images<?= $banner_images->id ?>.<?= $banner_images->image ?>" alt="<?= $banner_images->alt_tag ?>" width="1920" height="590">





            <?php
    }
    else{
        ?>
                    <img class="lazy innerBg" loading="lazy" src="assets/images/aboutBg.jpg"
            data-src="assets/images/aboutBg.jpg" alt="bannerImage" width="1920" height="590">

        <?php
    }
    ?> 








        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd"><?= $banner_images->title ?></h1>
                        <h4 class="bnrSubHd"><?= $banner_images->description ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Patient_Visitors">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="Main_Title" data-aos="fade-up">
                        <div class="Main-Title">
                            <?= $visitors->main_title?>
                        </div>
                        <div class="Main-SubTitle">
                            <!--<?=  $visitors->sub_caption?>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Patient-Visitors">
        <div class="container">
            <div class="row">
                <div class="Patient-Lft col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="Patient">
                        <div class="P_title" data-aos="fade-up"><?= $visitors->sub_title1?></div>
                        <div class="P_List" data-aos="fade-up">
                           <?= $visitors->sub_title1_description?>
                        </div>
                    </div>
                </div>
                <div class="Patient-Rgt col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="Visitors">
                        <div class="V_title" data-aos="fade-up"><?= $visitors->sub_title2?></div>
                        <div class="V_List" data-aos="fade-up">
                           <?= $visitors->sub_title2_description?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Patient-Full">
            <div class="container">
                <div class="Visitors">
                    <div class="V_title" data-aos="fade-up"><?= $visitors->sub_title3?></div>
                    <div class="V_List" data-aos="fade-up">
                      <?= $visitors->sub_title3_description?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

<script>
//   counter js  

$(document).ready(function() {

var controller = new ScrollMagic.Controller();

var scene = new ScrollMagic.Scene({
        triggerElement: '#about',
        triggerHook: 0.6
    })
    .setClassToggle('#about', 'isVisible')
    .on("enter", function() {
        $('.counter .contertTxt span').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');

            $({
                countNum: $this.text()
            }).animate({
                countNum: countTo
            }, {

                duration: 2000,
                easing: 'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }

            });

        });
    })
    .on("leave", function() {
        $('.contertTxt span').each(function() {
            $(this).text(0);
        });
    })
    .addTo(controller);

});   


</script>

    <!-- SCROLL_MAGIC -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.8/ScrollMagic.min.js"
        integrity="sha512-8E3KZoPoZCD+1dgfqhPbejQBnQfBXe8FuwL4z/c8sTrgeDMFEnoyTlH3obB4/fV+6Sg0a0XF+L/6xS4Xx1fUEg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</div>

