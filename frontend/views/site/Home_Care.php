<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

if (isset($meta_tags->meta_title) && $meta_tags->meta_title != '') {
    $this->title = $meta_tags->meta_title;
} else {
    $this->title = 'Apollo Adlux Hospital';
}

?><div id="pageWrapper" class="HomeCarePage">

    <section id="innerBanner">
        <img class="lazy innerBg" loading="lazy" src="<?= Yii::$app->homeUrl ?>assets/images/homeCareBg.jpg"
            data-src="<?= Yii::$app->homeUrl ?>assets/images/homeCareBg.jpg" alt="bannerImage" width="1920" height="590">
        <div class="bannerCntnt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="bnrMainHd">Home Care</h1>
                        <h4 class="bnrSubHd">These are the words that form the cornerstone of India’s most advanced medical institution</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="Home_Care">
        <div class="container">
            <div class="row">
                <?php
                if(!empty($home_care))
                {
                    foreach($home_care as $care)
                    {
                        ?>

                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-aos="fade-up">
                    <a href="" class="blog-box">
                        <div class="image_box">
                            <img class="lazy" loading="lazy" width="342" height="218" src="<?= Yii::$app->homeUrl ?>images/home-care/image<?= $care->id ?>.<?= $care->image ?>" data-src="<?= Yii::$app->homeUrl ?>images/home-care/image<?= $care->id ?>.<?= $care->image ?>">
                            <div class="hoverRdMr">
                                <div class="rdMrIcon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="55.633" height="55.633"
                                        viewBox="0 0 55.633 55.633">
                                        <path id="arrow_back_FILL0_wght100_GRAD-25_opsz40"
                                            d="M28.743,49.1,8.417,28.775l20.326-20.4,1.242,1.242L11.634,27.9h37.51v1.681H11.634L29.986,47.933Z"
                                            transform="translate(67.095 17.261) rotate(120)" fill="#fff" />
                                    </svg>
                                    <p class="rdMrTxt">Book Now</p>
                                </div>
                            </div>
                        </div>
                        <div class="content_box">
                            <div class="details_sec">
                                <div class="post-title"><?= $care->title?></div>
                                <div class="post-description">
                                    <?= $care->description?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php
                }
            }
            ?>
            </div>
        </div>
    </section>

</div>

