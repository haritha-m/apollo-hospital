<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '/CMS/apollo-hospital',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '' => 'site/index',
                'service-detail/<id>' =>'site/service-detail',
                'about-us' => 'site/about',
                'contact-us' => 'site/contact',
                
                'services/<name>'=>'site/servicedetails',
                'packages'=>'site/packages',
                'packages/<name>'=>'site/packagedetails',
                'cme'=>'site/cme',
                'cmedetails/<name>'=>'site/cmedetails',
                'offerdetails'=>'site/offerdetails',
                'offers/<name>' => 'site/offerdetails',
                'offers'=>'site/offers',
                'international-patients'=>'site/international',
                'patients-visitors'=>'site/visitors',
                'privacy-policy'=>'site/privacy',
                'quality-safety'=>'site/quality',
                'home-care'=>'site/homecare',
                'medical-blog'=>'site/medical',
                'medicalblog/<name>'=>'site/medicaldetails',
                'news'=>'site/news',
                'news/<name>'=>'site/newsdetails',
                'new-doctors'=>'site/new-doctors',
                'doctors'=>'site/doctors',
                'doctors/<name>'=>'site/doctordetails',
                'insurance'=>'site/insurance',
                'visa-travel-arrangements'=>'site/visatravel',
                'laboratory-services'=>'site/laboratory',
                'facilities'=>'site/facilities',
                'our-message'=>'site/ourmessage',
                'vision-mission'=>'site/vision',
                'core-values'=>'site/corevalues',
                'career'=>'site/career',
                'feedback'=>'site/feedback',
                'schedule'=>'site/schedule',
                'media-gallery'=>'site/media-gallery',
                'media-gallery/<name>'=>'site/media-gallery-details',
                'press-release'=>'site/media',
                'press-release/<name>'=>'site/media-details',
                'promotors'=>'site/promotors',
                'sanjeevani'=>'site/sanjeevani',
                'sanjeevani/<name>'=>'site/sanjeevanidetails',
                'sanjeevani/gallery/<name>'=>'site/sanjeevanigallery',
                // 'cme/<title>'=>'site/conference',
                'conference'=>'site/conference',
                'conferncedetails/<name>'=>'site/conferncedetails',
                'search' => 'site/search',
                'conferencelisting'=>'site/conferencelisting',
                'brochure'=>'site/brochure',
                'service/<slug>' =>'site/services',
                

            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
    ],
    'params' => $params,
];