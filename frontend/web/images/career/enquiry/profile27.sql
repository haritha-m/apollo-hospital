-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2023 at 10:41 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csi_collage`
--

-- --------------------------------------------------------

--
-- Table structure for table `anti_raging`
--

CREATE TABLE `anti_raging` (
  `id` int(11) NOT NULL,
  `title1` text DEFAULT NULL,
  `content1` text DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `CB` int(11) DEFAULT NULL,
  `UB` int(11) DEFAULT NULL,
  `DOC` timestamp NULL DEFAULT NULL,
  `DOU` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `anti_raging`
--

INSERT INTO `anti_raging` (`id`, `title1`, `content1`, `sort_order`, `status`, `CB`, `UB`, `DOC`, `DOU`) VALUES
(1, 'Nos autem non oblectationem videmuserram, mihi crede ea lanx et maria deprimet.', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n\r\n<p>Ergo in iis adolescentibus bonam spem esse dicemus et magnam indolem, quos suis commodis inservituros et quicquid ipsis expediat facturos arbitrabimur Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Quibus rebus vita consentiens virtutibusque respondens recta et honesta et constans et naturae congruens existimari potest. Etenim semper illud extra est, quod arte comprehenditur. Hi autem ponunt illi quidem prima naturae, sed ea seiungunt a finibus et a summa bonorum; Audi, ne longe abeam, moriens quid dicat Epicurus, ut intellegas facta eius cum dictis discrepare: Epicurus Hermarcho salutem. Quamquam id quidem, infinitum est in hac urbe; Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas Ita graviter et severe voluptatem secrevit a bono. Cur igitur, inquam, res tam dissimiles eodem nomine appellas Atqui perspicuum</p>\r\n', 1, 1, 1, 1, '2023-05-11 18:30:00', '2023-05-12 11:06:01'),
(2, 'Terram, mihi crede, ea lanx et maria deprimet.', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n\r\n<p>Ergo in iis adolescentibus bonam spem esse dicemus et magnam indolem, quos suis commodis inservituros et quicquid ipsis expediat facturos arbitrabimur Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Quibus rebus vita consentiens virtutibusque respondens recta et honesta et constans et naturae congruens existimari potest. Etenim semper illud extra est, quod arte comprehenditur. Hi autem ponunt illi quidem prima naturae, sed ea seiungunt a finibus et a summa bonorum; Audi, ne longe abeam, moriens quid dicat Epicurus, ut intellegas facta eius cum dictis discrepare: Epicurus Hermarcho salutem. Quamquam id quidem, infinitum est in hac urbe; Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas Ita graviter et severe voluptatem secrevit a bono. Cur igitur, inquam, res tam dissimiles eodem nomine appellas Atqui perspicuum</p>\r\n', 2, 1, 1, 1, '2023-05-11 18:30:00', '2023-05-17 06:48:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anti_raging`
--
ALTER TABLE `anti_raging`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anti_raging`
--
ALTER TABLE `anti_raging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
