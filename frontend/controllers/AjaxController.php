<?php

namespace frontend\controllers;

use common\models\Districts;
use common\models\Doctors;
use common\models\Panchayat;
use common\models\Country;
use yii\helpers\Json;
use common\models\ServiceMainCategory;

use Yii;

class AjaxController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }




    /*
     * contact Enquiry submission through jquery
     */



    public function actionContactEnquiry()
    {

        if (Yii::$app->request->isAjax) {

            $model = new \common\models\ContactEnquiry();
            $model->name = $_POST['name'];
            $model->email = $_POST['email'];
            $model->district_id = $_POST['district_id'];
            $model->panchayat_id = isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';
            $model->phone = $_POST['mobile'];
            $model->message = $_POST['message'];
            $model->date = date("Y-m-d");



            if ($model->save(false)) {

                $this->sendContactMail($model);

                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }






    /*
     * Contact enquiry mail function
     */

    public function sendContactMail($model)
    {

        $message = Yii::$app->mailer->compose('enquiry-mail', ['model' => $model])
            ->setFrom(['admin@apollohospitals.com' => 'Apollo Adlux Hospital'])
            ->setTo('marketing_kochi@apollohospitals.com')
            ->setBcc('haritha@intersmart.in')
            ->setSubject('Contact Enquiry From  Apollo Adlux Hospital');
        $message->send();

        return TRUE;
    }



    public function actionFeedback()
    {

        if (Yii::$app->request->isAjax) {

            $model = new \common\models\Feedback();
            $model->name = $_POST['name'];
            $model->email = $_POST['email'];
            $model->mobile = $_POST['mobile'];
            $model->feedback_message = $_POST['message'];
            $model->district_id = $_POST['district_id'];
            $model->panchayat_id = isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';



            $model->date = date("Y-m-d");



            if ($model->save(false)) {

                $this->sendFeedBackMail($model);

                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }

    public function sendFeedBackMail($model)
    {

        $message = Yii::$app->mailer->compose('feedback-mail', ['model' => $model])
            ->setFrom(['admin@apollohospitals.com' => 'Apollo Adlux Hospital'])
            ->setTo('Feedback_kochi@apollohospitals.com')
            ->setBcc('haritha@intersmart.in')
            ->setSubject('Feedback From  Apollo Adlux Hospital');
        $message->send();

        return TRUE;
    }



    public function actionConfernceEnquiry()
    {

        if (Yii::$app->request->isAjax) {

            $model = new \common\models\ConferenceEnquiry();

            $model->name = $_POST['name'];
            $model->email = $_POST['email'];
            $model->mobile = $_POST['mobile'];
            $model->district_id = $_POST['district_id'];
            $model->panchayat_id = isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';
            $model->department = $_POST['dept'];
            $model->designation = $_POST['desg'];
            $model->confernce = $_POST['desg'];
            $model->date = date("Y-m-d");



            if ($model->save(false)) {

                // $this->sendContactMail($model);

                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }




    public function actionNewsLetter()
    {

        if (Yii::$app->request->isAjax) {

            $model = new \common\models\NewsLetter();

            $model->email = $_POST['nemail'];




            $model->dare = date("Y-m-d");



            if ($model->save(false)) {



                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }




    public function actionPackageEnquiry()
    {

        if (Yii::$app->request->isAjax) {
            $model = new \common\models\PackageEnquiry();
            $model->name = $_POST['name'];
            $model->email = $_POST['email'];
            $model->district_id = $_POST['district_id'];
            $model->panchayat_id = isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';
            $model->mobile = $_POST['mobile'];
            $model->message = $_POST['message'];
            $model->package = $_POST['package'];
            $model->other = $_POST['other'];
            $model->date = date("Y-m-d");



            if ($model->save(false)) {

                // $this->sendContactMail($model);

                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }

    public function actionOfferEnquiry()
    {

        if (Yii::$app->request->isAjax) {

            $model = new \common\models\OfferEnquiry();
            $model->name = $_POST['name'];
            $model->email = $_POST['email'];
            $model->district_id = $_POST['district_id'];
            $model->panchayat_id = isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';
            $model->mobile = $_POST['mobile'];
            $model->message = $_POST['message'];
            $model->offer = $_POST['offer'];
            $model->date = date("Y-m-d");

            if ($model->save(false)) {
                // $this->sendContactMail($model);
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }
    /**
     * get panchayat
     */
    public function actionGetPanchayat()
    {
        if (yii::$app->request->isAjax) {
            $panchayats = [];
            $district = Districts::findOne($_POST['id']);
            if (isset($district)) {
                $panchayats = Panchayat::find()->where(['district_id' => $district->id, 'status' => 1])->all();
            }
            return \yii\helpers\Json::encode([
                'panchayats' => $panchayats
            ]);
        }
    }

    public function actionSchedulingAppointments()
    {

        if (Yii::$app->request->isAjax) {
            $model = new \common\models\SchedulingAppointments();
            $model->name = $_POST['name'];
            $model->email = $_POST['email'];
            $model->mobile = $_POST['mobile'];
            $model->country_id = $_POST['country_id'];
            $model->date = date("Y-m-d");

            if ($model->save(false)) {
                // $this->sendContactMail($model);
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }


    public function actionGetSearchProducts()
    {
        if (Yii::$app->request->isAjax) {
            $success = false;
            $s_success = false;
            $data = [];

            // keyword search
            if (!empty($_POST["keyword"])) {
                if (!ctype_space($_POST["keyword"])) {
                    $doctors = Doctors::find()->where(['like', 'name', trim(preg_replace('!\s+!', ' ', $_POST["keyword"]))])->all();
                    if (!empty($doctors)) {
                        $success = true;
                        foreach ($doctors as $doctor) {
                            $data[] = [
                                'name' => $doctor->name
                            ];
                        }

                    } 
                  $services = ServiceMainCategory::find()->where(['like', 'title', trim(preg_replace('!\s+!', ' ', $_POST["keyword"]))])->all();
                  
                  if (!empty($services)) {
                    $success = true;
                    foreach ($services as $service) {
                        $data[] = [
                            'name' => $service->title
                        ];
                    }

                
                    }
                }
            }
            return \yii\helpers\Json::encode([
                'success' => $success,
                's_success' => $s_success,
                'data' => $data
            ]);
        }
    }

}