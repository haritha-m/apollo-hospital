<?php

namespace frontend\controllers;

use common\models\ServiceFaq;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use common\models\QualitySafetyFiles;
use common\models\ServiceMainCategory;
use common\models\Services;

use common\models\Doctors;
use common\models\BannerImages;
use common\models\ConferenceGallery;
use common\models\ServiceCategory;
use common\models\DoctorsSearch;
use common\models\MetaTags;
use common\models\Testimonial;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'contact-enquiry'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $service_category = \common\models\ServiceCategory::findOne(1);
        $service_categories = \common\models\ServiceCategory::findOne(2);
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 1])->one();
        $sliders = \common\models\Sliders::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_ASC])->all();
        $about = \common\models\HomeAbout::findOne(1);
        $common_content = \common\models\CommonContent::findOne(1);
        $services = \common\models\Services::find()->where(['status' => 1, 'is_home' => 1])->orderBy('title ASC')->all();
        $offers = \common\models\Offers::find()->where(['status' => 1])->orderBy(['date' => SORT_DESC])->all();
        $packages = \common\models\Packages::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();
        $news = \common\models\News::find()->where(['status' => 1])->orderBy(['date' => SORT_ASC])->limit(5)->all();
        $photo_gallery = \common\models\PhotoGallery::find()->where(['status' => 1, 'home_display_mode' => 1])->orderBy(['DOU' => SORT_ASC])->limit(3)->all();
        $media = \common\models\MediaBlog::find()->where(['status' => 1])->orderBy(['date' => SORT_DESC])->all();

        $photo_gallery1 = \common\models\PhotoGallery::find()->where(['status' => 1, 'home_display_mode' => 1])->orderBy(['DOU' => SORT_DESC])->limit(3)->all();
        $video_gallery = \common\models\VideoGallery::find()->where(['status' => 1])->orderBy(['DOU' => SORT_DESC])->one();
        $medical_care = \common\models\MedicalCareQuality::find()
            ->where(['status' => 1])
            ->andwhere(['between', 'id', 1, 3])
            ->orderBy(['sort_order' => SORT_ASC])->all();
        $medical_care1 = \common\models\MedicalCareQuality::find()
            ->where(['status' => 1])
            ->andwhere(['between', 'id', 4, 6])
            ->orderBy(['sort_order' => SORT_ASC])->all();
        $quality = \common\models\QualitySafety::findOne(1);
        $visitors = \common\models\PatientsVisitors::findOne(1);





        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'index',
            [
                'meta_tags' => $meta_tags,
                'sliders' => $sliders,
                'about' => $about,
                'common_content' => $common_content,
                'services' => $services,
                'offers' => $offers,
                'news' => $news,
                'photo_gallery' => $photo_gallery,
                'photo_gallery1' => $photo_gallery1,
                'video_gallery' => $video_gallery,
                'medical_care' => $medical_care,
                'medical_care1' => $medical_care1,
                'quality' => $quality,
                'visitors' => $visitors,
                'packages' => $packages,
                'media' => $media,
                'category' => $service_category,
                'service_categories' => $service_categories
            ]
        );


    }


    /**
     * Displays about.
     *
     * @return mixed
     */
    public function actionBrochure()
    {

        $banner_images = \common\models\BannerImages::find()->where(['id' => 1])->one();
        $brochure = \common\models\Brochure::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();

        return $this->render('brochure', [

            'banner_images' => $banner_images,
            'brochure' => $brochure
        ]);
    }



    public function actionAbout()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 2])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 1])->one();
        $about = \common\models\About::findOne(1);
        $vision = \common\models\VisionMission::findOne(1);
        $our_message = \common\models\OurMessage::findOne(1);
        $core_values = \common\models\CoreValuesContent::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'about',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'about' => $about,
                'vision' => $vision,
                'our_message' => $our_message,
                'core_values' => $core_values
            ]
        );


    }


    /**
     * Displays our vision mision.
     *
     * @return mixed
     */



    public function actionVision()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 3])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 2])->one();
        $vision = \common\models\VisionMission::findOne(1);
        $about = \common\models\About::findOne(1);
        $our_message = \common\models\OurMessage::findOne(1);
        $core_values = \common\models\CoreValuesContent::findOne(1);
        $about = \common\models\About::findOne(1);
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Vision_Mission',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'vision' => $vision,
                'our_message' => $our_message,
                'core_values' => $core_values,
                'about' => $about
            ]
        );
    }
    /**
     * Displays our message.
     *
     * @return mixed
     */



    public function actionOurmessage()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 4])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 3])->one();
        $our_message = \common\models\OurMessage::findOne(1);
        $about = \common\models\About::findOne(1);
        $vision = \common\models\VisionMission::findOne(1);
        $core_values = \common\models\CoreValuesContent::findOne(1);


        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Our_Message',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'our_message' => $our_message,
                'about' => $about,
                'vision' => $vision,
                'core_values' => $core_values
            ]
        );
    }




    /**
     * Displays our vision mision.
     *
     * @return mixed
     */



    public function actionCorevalues()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 5])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 4])->one();
        $core_content = \common\models\CoreValuesContent::findOne(1);
        $core_list1 = \common\models\CoreValues::find()
            ->where(['status' => 1])
            ->andwhere(['between', 'id', 1, 3])
            ->orderBy(['sort_order' => SORT_ASC])->limit(3)->all();
        $vision = \common\models\VisionMission::findOne(1);
        $our_message = \common\models\OurMessage::findOne(1);
        $about = \common\models\About::findOne(1);
        $core_list2 = \common\models\CoreValues::find()
            ->where(['status' => 1])
            ->andwhere(['between', 'id', 4, 5])
            ->orderBy(['sort_order' => SORT_ASC])->limit(2)->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Core_Values',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'core_content' => $core_content,
                'core_list1' => $core_list1,
                'core_list2' => $core_list2,
                'vision' => $vision,
                'our_message' => $our_message,
                'about' => $about,
            ]
        );
    }






    /**
     * Displays services.
     *
     * @return mixed
     */


    public function actionServices($slug)
    {

        $service_category = \common\models\ServiceCategory::find()->where(['canonical_name' => $slug])->one();
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 8])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 7])->one();
        // $services = \common\models\Services::find()->where(['status' => 1])->orderBy('title ASC')->all();
        $services = \common\models\Services::find()->where(['status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->andWhere(['category_id' => $service_category->id])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'services',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'services' => $services, 'service_category' => $service_category]
        );


    }



    /**
     * Displays services details.
     *
     * @return mixed
     */


    public function actionServicedetails($name)
    {

        $service = \common\models\Services::find()->where(['canonical_name' => $name])->one();
        if (isset($service)) {
            $doctors = \common\models\Doctors::find()->where(['status' => 1, 'department' => $service->id])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
            $faqs = ServiceFaq::find()->where(['status' => 1, 'service_id' => $service->id])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $service->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $service->meta_description]);
            \Yii::$app->view->registerMetaTag(['name' => 'meta_title', 'content' => $service->meta_title]);
            $this->view->params['other_meta_tags'] = $service->other_meta_tags;

            return $this->render(
                'serviceDetail',
                ['service' => $service, 'doctors' => $doctors, 'faqs' => $faqs]
            );
        } else {
            return $this->render('error');
        }

    }

    /**
     * Displays Package.
     *
     * @return mixed
     */

    public function actionPackages()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 10])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 9])->one();
        $packages = \common\models\Packages::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();
        $common_content = \common\models\CommonContent::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'packages',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'packages' => $packages, 'common_content' => $common_content]
        );


    }



    /**
     * Displays package Details.
     *
     * @return mixed
     */


    public function actionPackagedetails($name)
    {
        $package = \common\models\Packages::find()->where(['canonical_name' => $name])->one();

        if (isset($package)) {
            $package_all = \common\models\Packages::find()->where(['status' => 1])->andWhere(['!=', 'id', $package->id])->orderBy(['id' => SORT_DESC])->all();
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $package->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $package->meta_description]);
            $this->view->params['other_meta_tags'] = $package->other_meta_tags;
            $districts = \common\models\Districts::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
            $flag = 0;
            $model = new \common\models\SchedulingAppointments();
            if ($model->load(Yii::$app->request->post())) {
                $model->date = date('Y-m-d');

                // $image = UploadedFile::getInstance($model, 'profile');
                // $model->profile = $image->extension;

                if ($model->validate() && $model->save()) {
                    //   $this->Uploadapp($image,$model);

                    $model = new \common\models\CareerEnquiry();
                    $flag = 1;
                }
            }
            return $this->render(
                'packageDetail',
                [
                    'package' => $package,
                    'package_all' => $package_all,
                    'flag' => $flag,
                    'districts' => $districts,

                ]
            );

        } else {

            return $this->render('error');
        }


    }


    public function actionSchedule()
    {

        $international = \common\models\InternationalPatients::findone(1);
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 21])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 20])->one();
        $country = \common\models\Country::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
        // $flag           = 0;
        $model = new \common\models\SchedulingAppointments();
        if ($model->load(Yii::$app->request->post())) {
            $model->date = date('Y-m-d');

            // $image = UploadedFile::getInstance($model, 'profile');
            // $model->profile = $image->extension;

            if ($model->validate() && $model->save()) {
                //   $this->Uploadapp($image,$model);

                $model = new \common\models\SchedulingAppointments();
                // $flag = 1;
            }
        }

        // \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        // \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        // $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render('schedulingAppointments', [
            'international' => $international,
            'banner_images' => $banner_images,
            'meta_tags' => $meta_tags,
            'country' => $country,
            'model' => $model,
            // 'flag'          =>  $flag
        ]);
    }


    // \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
// \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
// $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;


    /**
     * Displays cme.
     *
     * @return mixed
     */



    /**
     * Displays cme Details.
     *
     * @return mixed
     */
    public function actionCmedetails($name)
    {
        $cme = \common\models\Cme::find()->where(['canonical_name' => $name])->one();
        if (isset($cme)) {
            $cme_all = \common\models\Cme::find()->where(['status' => 1])->andWhere(['!=', 'id', $cme->id])->orderBy(['id' => SORT_DESC])->all();

            $gallery = \common\models\CmeGallery::find()->where(['status' => 1, 'cme_id' => $cme->id])->orderBy(['sort_order' => SORT_ASC])->all();

            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $cme->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $cme->meta_description]);
            $this->view->params['other_meta_tags'] = $cme->other_meta_tags;
            return $this->render(
                'CME-Detail',
                ['cme' => $cme, 'cme_all' => $cme_all, 'gallery' => $gallery]
            );

        } else {

            return $this->render('error');
        }

    }



    /**
     * Displays offers.
     *
     * @return mixed
     */

    public function actionOffers()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 11])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 10])->one();
        $offers = \common\models\Offers::find()->where(['status' => 1])->orderBy(['date' => SORT_DESC])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'offers',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'offers' => $offers]
        );


    }
    /**
     * Displays offer Details.
     *
     * @return mixed
     */
    public function actionOfferdetails($name)
    {
        $offer = \common\models\Offers::find()->where(['canonical_name' => $name])->one();
        if (isset($offer)) {
            $offer_all = \common\models\Offers::find()->where(['status' => 1])->andWhere(['!=', 'id', $offer->id])->orderBy(['id' => SORT_DESC])->all();
            $districts = \common\models\Districts::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
            $flag = 0;
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $offer->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $offer->meta_description]);
            $this->view->params['other_meta_tags'] = $offer->other_meta_tags;
            return $this->render('offerDetail', [
                'offer' => $offer,
                'offer_all' => $offer_all,
                'districts' => $districts,
                'flag' => $flag,
            ]);
        } else {

            return $this->render('error');
        }


    }

    /**
     * Displays offers.
     *
     * @return mixed
     */

    public function actionInternational()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 9])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 8])->one();
        $international = \common\models\InternationalPatients::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();
        $common_content = \common\models\CommonContent::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'internationalPatients',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'international' => $international,
                'common_content' => $common_content
            ]
        );


    }



    /**
     * Displays Patients_and_Visitors.
     *
     * @return mixed
     */

    public function actionVisitors()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 13])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 12])->one();
        $visitors = \common\models\PatientsVisitors::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Patients_and_Visitors',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'visitors' => $visitors]
        );


    }


    /**
     * Displays privacy-policy.
     *
     * @return mixed
     */

    public function actionPrivacy()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 23])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 22])->one();
        $privacy = \common\models\PrivacyPolicy::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Privacy_Policy',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'privacy' => $privacy]
        );


    }




    /**
     * Displays quality.
     *
     * @return mixed
     */

    public function actionQuality()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 14])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 13])->one();
        $quality = \common\models\QualitySafety::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        $files = QualitySafetyFiles::find()->where(['status' => '1'])->orderBy('sort_order ASC')->all();
        return $this->render(
            'Quality_and_Safety',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'quality' => $quality, 'files' => $files]
        );


    }

    public function actionHomecare()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 20])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 19])->one();
        $home_care = \common\models\HomeCare::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Home_Care',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'home_care' => $home_care]
        );


    }


    /**
     * Displays mediablog .
     *
     * @return mixed
     */




    public function actionMedical()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 18])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 17])->one();
        $media = \common\models\MediaBlog::find()->where(['status' => 1])->orderBy(['date' => SORT_DESC])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Medical-Blogs',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'media' => $media]
        );


    }

    /**
     * Displays mediablog Details.
     *
     * @return mixed
     */
    public function actionMedicaldetails($name)
    {
        $media = \common\models\MediaBlog::find()->where(['canonical_name' => $name])->one();

        if (isset($media)) {

            $media_all = \common\models\MediaBlog::find()->where(['status' => 1])->andWhere(['!=', 'id', $media->id])->orderBy(['id' => SORT_DESC])->all();
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $media->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $media->meta_description]);
            $this->view->params['other_meta_tags'] = $media->other_meta_tags;
            return $this->render(
                'Medical_Blogs_Details',
                ['media' => $media, 'media_all' => $media_all]
            );
        } else {
            return $this->render('error');
        }

    }


    /**
     * Displays news .
     *
     * @return mixed
     */


    public function actionNews()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 25])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 24])->one();
        $news = \common\models\News::find()->where(['status' => 1])->orderBy(['date' => SORT_DESC])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'News-Listing',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'news' => $news]
        );


    }

    /**
     * Displays news Details.
     *
     * @return mixed
     */
    public function actionNewsdetails($name)
    {
        $news = \common\models\News::find()->where(['canonical_name' => $name])->one();

        if (isset($news)) {

            $news_all = \common\models\News::find()->where(['status' => 1])->andWhere(['!=', 'id', $news->id])->orderBy(['id' => SORT_DESC])->all();
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $news->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $news->meta_description]);
            $this->view->params['other_meta_tags'] = $news->other_meta_tags;
            return $this->render(
                'News-Detail',
                ['news' => $news, 'news_all' => $news_all]
            );

        } else {
            return $this->render('error');
        }

    }

    public function actionNewDoctors()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 7])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 6])->one();
        $doctors = \common\models\Doctors::find()->where(['status' => 1])->Where(['>=', 'joined_date', date('Y-m-d', strtotime('today - 30 days'))])->orderBy(['joined_date' => SORT_DESC])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'new-doctors',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'doctors' => $doctors,
            ]
        );


    }

    public function actionDoctors()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 7])->one();
        $banner_images = BannerImages::find()->where(['id' => 6])->one();
        $department = Services::find()->where(['status' => 1])->orderBy(['sort_doctors' => SORT_ASC])->all();
        $categories = ServiceCategory::find()->where(['status' => '1'])->orderBy('sort_order ASC')->all();

        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;

        $searchModel = new DoctorsSearch(['status' => '1']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!empty($_GET["category"])) {
            $category = ServiceMainCategory::find()->where(['in', 'canonical_name', $_GET["category"]])->select('id');
            if ($category) {
                $dataProvider->query->andWhere(['category' => $category]);
            }
        }
        
        if (!empty($_GET["search"])) {
            $dataProvider->query->andWhere(['like', 'name', $_GET["search"]]);
        }
        $dataProvider->query->orderBy('sort_order ASC');


        return $this->render('doctors', [
            'meta_tags' => $meta_tags,
            'banner_images' => $banner_images,
            'department' => $department,
            'categories' => $categories,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);


    }

    /**
     * Displays news Details.
     *
     * @return mixed
     */
    public function actionDoctordetails($name)
    {
        $doctor = \common\models\Doctors::find()->where(['canonical_name' => $name])->one();

        if (isset($doctor)) {

            $gallery = \common\models\DoctorsVideoGallery::find()->where(['status' => 1, 'doctor_id' => $doctor->id])->orderBy(['sort_order' => SORT_DESC])->all();

            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $doctor->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $doctor->meta_description]);
            $this->view->params['other_meta_tags'] = $doctor->other_meta_tags;
            return $this->render(
                'doctorDetail',
                ['doctor' => $doctor, 'gallery' => $gallery]
            );

        } else {

            return $this->render('error');

        }

    }



    public function actionInsurance()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 12])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 11])->one();
        $insurance = \common\models\Insurance::findOne(1);
        $partners = \common\models\InsurancePartners::find()->where(['status' => 1,])->orderBy(['sort_order' => SORT_DESC])->all();

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'insurance',
            ['insurance' => $insurance, 'partners' => $partners, 'meta_tags' => $meta_tags, 'banner_images' => $banner_images]
        );


    }



    /**
     * Displays VisaTravelArrangements.
     *
     * @return mixed
     */


    public function actionVisatravel()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 27])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 26])->one();
        $visa_info = \common\models\VisaTravelArrangements::findOne(1);
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'visaTravelArrangements',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'visa_info' => $visa_info]
        );
    }



    /**
     * Displays VisaTravelArrangements.
     *
     * @return mixed
     */


    public function actionLaboratory()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 28])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 27])->one();
        $content = \common\models\LaboratoryServicesContent::findOne(1);
        $services = \common\models\LaboratoryServices::find()->where(['status' => 1,])->orderBy(['sort_order' => SORT_DESC])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'laboratoryServices',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'content' => $content, 'services' => $services]
        );
    }







    public function actionFacilities()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 26])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 25])->one();
        $facilities = \common\models\Facilities::find()->where(['status' => 1,])->orderBy(['sort_order' => SORT_DESC])->all();

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'facilities',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'facilities' => $facilities]
        );
    }



    /**
     * Displays career.
     *
     * @return mixed
     */




    public function actionCareer()
    {

        $career = \common\models\CareerCommonContent::findone(1);
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 16])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 15])->one();
        $career_list = \common\models\Careers::find()->where(['status' => 1,])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

        $flag = 0;
        $model = new \common\models\CareerEnquiry();
        if ($model->load(Yii::$app->request->post())) {


            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            $model->date = date("Y-m-d");

            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
                //$this->sendApplicationDetails($model);
                $model = new \common\models\CareerEnquiry();
                $flag = 1;
            }
        }

        // \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        // \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        // $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render('career', [
            'career' => $career,
            'banner_images' => $banner_images,
            'meta_tags' => $meta_tags,
            'career_list' => $career_list,
            'model' => $model
            ,
            'flag' => $flag

        ]);

    }
    /**
     * upload cv.
     *
     * @return mixed
     */
    //    public function Uploadapp($image,$model) {
//     if (!empty($image)) {
//       $path = Yii::$app->basePath . '/../frontend/web/images/schedule';
//       FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
//       $name = 'profile' . $model->id;
//       $image->saveAs($path . '/' . $name . '.' . $image->extension);
//     }

    //   }


    /**
     * upload cv.
     *
     * @return mixed
     */
    public function Upload($image, $model)
    {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/career/enquiry';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'profile' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }

    }



    /**
     * Displays feedback.
     *
     * @return mixed
     */
    public function actionFeedback()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 24])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 23])->one();
        $contact_info = \common\models\ContactInfo::findOne(1);
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        $districts = \common\models\Districts::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
        $flag = 0;
        return $this->render(
            'feedback',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'contact_info' => $contact_info,
                'districts' => $districts,
                'flag' => $flag,
            ]
        );
    }










    /**
     * Displays media gallery.
     *
     * @return mixed
     */

    public function actionMediaGallery()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 22])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 21])->one();
        $gallery = \common\models\Gallery::find()->where(['status' => 1,])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'gallery',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'gallery' => $gallery
            ]
        );
    }


    /**
     * Displays media gallery.
     *
     * @return mixed
     */

    public function actionMediaGalleryDetails($name)
    {
        $gallery = \common\models\Gallery::find()->where(['canonical_name' => $name])->one();
        if (isset($gallery)) {
            $meta_tags = \common\models\MetaTags::find()->where(['id' => 37])->one();
            $banner_images = \common\models\BannerImages::find()->where(['id' => 36])->one();
            $photo_gallery = \common\models\PhotoGallery::find()->where(['gallery_id' => $gallery->id, 'status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
            $video_gallery = \common\models\VideoGallery::find()->where(['gallery_id' => $gallery->id, 'status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
            $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
            return $this->render(
                'mediaGallery',
                [
                    'meta_tags' => $meta_tags,
                    'banner_images' => $banner_images,
                    'photo_gallery' => $photo_gallery,
                    'video_gallery' => $video_gallery
                ]
            );
        } else {
            return $this->redirect(['error']);
        }

    }




    /**
     * Displays promotors.
     *
     * @return mixed
     */


    public function actionPromotors()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 6])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 5])->one();
        $promotors = \common\models\Promotors::findOne(1);
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'promotors',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'promotors' => $promotors]
        );
    }


    /**
     * Displays sanjeevani .
     *
     * @return mixed
     */


    public function actionSanjeevani()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 17])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 16])->one();
        $sanjeevani = \common\models\Sanjeevani::find()->where(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();
        $sanjeevani_common = \common\models\SanjeevaniCommonContent::findOne(1);

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'Sanjeevani',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'sanjeevani_common' => $sanjeevani_common, 'sanjeevani' => $sanjeevani]
        );


    }

    /**
     * Displays sanjeevani Details.
     *
     * @return mixed
     */
    public function actionSanjeevanidetails($name)
    {
        $sanjeevani = \common\models\Sanjeevani::find()->where(['canonical_name' => $name])->one();
        if (isset($sanjeevani)) {
            $sanjeevani_gallery = \common\models\SanjeevaniGallery::find()->where(['sanjeevani_id' => $sanjeevani->id, 'status' => 1])->all();
            $banner_images = \common\models\BannerImages::find()->where(['id' => 38])->one();
            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $sanjeevani->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $sanjeevani->meta_description]);
            $this->view->params['other_meta_tags'] = $sanjeevani->other_meta_tags;
            return $this->render(
                'Sanjeevani_MedicalCamps',
                [
                    'sanjeevani' => $sanjeevani,
                    'sanjeevani_gallery' => $sanjeevani_gallery,
                    'banner_images' => $banner_images,
                ]
            );
        } else {
            return $this->redirect(['error']);
        }
    }

    /**
     * Displays sanjeevani Gallery.
     *
     * @return mixed
     */
    public function actionSanjeevanigallery($name)
    {
        $sanjeevani_gallery = \common\models\SanjeevaniGallery::find()->where(['canonical_name' => $name])->one();
        if (isset($sanjeevani_gallery)) {
            return $this->render(
                'SanjeevaniphotoGallery',
                ['sanjeevani' => $sanjeevani_gallery]
            );
        } else {
            return $this->redirect(['error']);
        }
    }

    /**
     * Displays Confernce .
     *
     * @return mixed
     */


    public function actionConference($title = null)
    {
        $title = 'Conference';

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 15])->one();
        $confernce = \common\models\Conference::find()->where(['status' => 1])->andWhere(['>=', 'date', date("Y-m-d")])->orderBy(['date' => SORT_DESC])->all();
        $confernce_data = \common\models\Conference::find()->where(['status' => 1])->andWhere(['<', 'date', date("Y-m-d")])->orderBy(['date' => SORT_DESC])->all();
        $confernce_common = \common\models\ConferenceCommonContent::findOne(1);
        $banner_images = \common\models\BannerImages::find()->where(['id' => 14])->one();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'conference',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'confernce_common' => $confernce_common,
                'confernce' => $confernce,
                'confernce_data' => $confernce_data,
                'title' => $title,
                'meta_tags' => $meta_tags

            ]
        );


    }

    public function actionConferencelisting()
    {


        $banner_images = \common\models\BannerImages::find()->where(['id' => 14])->one();
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 15])->one();
        $confernce = \common\models\Conference::find()->where(['status' => 1])->andWhere(['>=', 'date', date("Y-m-d")])->orderBy(['date' => SORT_DESC])->all();
        $confernce_data = \common\models\Conference::find()->where(['status' => 1])->andWhere(['<', 'date', date("Y-m-d")])->orderBy(['date' => SORT_DESC])->all();
        return $this->render('conference_listing', ['confernce' => $confernce, 'banner_images' => $banner_images, 'confernce_data' => $confernce_data]);
    }


    public function actionCme()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 19])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 18])->one();
        $cme = \common\models\Cme::find()->where(['status' => 1])->orderBy(['date' => SORT_DESC])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'CME',
            ['meta_tags' => $meta_tags, 'banner_images' => $banner_images, 'cme' => $cme]
        );


    }

    /**
     * Displays Confernce Details.
     *
     * @return mixed
     */
    public function actionConferncedetails($name)
    {
        $districts = \common\models\Districts::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 14])->one();
        $con = \common\models\Conference::findOne(2);
        $flag = 0;
        $confernce = \common\models\Conference::find()->where(['canonical_name' => $name])->one();
        $gallery = \common\models\ConferenceGallery::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $confernce->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $confernce->meta_description]);
        $this->view->params['other_meta_tags'] = $confernce->other_meta_tags;
        return $this->render(
            'Conference_Detailed',
            ['confernce' => $confernce, 'districts' => $districts, 'flag' => $flag, 'con' => $con, 'banner_images' => $banner_images, 'gallery' => $gallery]
        );


    }

    /**
     * Displays doctors search.
     *
     * @return mixed
     */



    public function actionSearch()
    {

        $search = $_POST['search'] ?? NULL;
        $doctors = \common\models\Doctors::find()->Where(['LIKE', 'name', $search])->andWhere(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->all();
        $doctors_depids = \common\models\Doctors::find()->Where(['LIKE', 'name', $search])->andWhere(['status' => 1])->orderBy(['sort_order' => SORT_DESC])->select('department')->distinct();
        $department = \common\models\Services::find()->Where(['LIKE', 'title', $search])->all();
        if (count($doctors) > 0) {
            $department = \common\models\Services::find()->Where(['in', 'id', $doctors_depids])->all();
        }

        //$insights_all=\common\models\Insights::find()->orderBy(['date' => SORT_DESC])->all();
        //$insights_count=\common\models\Insights::find()->where(['LIKE', 'title', $search])->count();

        return $this->render('doctorSearch', [
            'department' => $department,
            'doctors' => $doctors,
            'search' => $search,




        ]);
    }



    /**
     * Displays header search.
     *
     * @return mixed
     */








    public function actionGetSearchDepartments()
    {
        if (Yii::$app->request->isAjax) {

            $flag = 0;
            $departments = [];


            $search = $_POST['keyword'] ?? null;


            $stocks = \common\models\Services::find()
                ->Where(['LIKE', 'title', $search])

                ->all();
            $count = count($stocks);



            foreach ($stocks as $stock) {
                $p_image = 'localhost/CMS/apollo-hospital/images/services/image' . $stock->id . '.' . $stock->image;
                $service_link = $stock->canonical_name;

                $departments[] = [

                    'service_name' => $stock->title,

                    'p_image' => $p_image,
                    'service_link' => $service_link



                ];
            }




            return \yii\helpers\Json::encode([
                'flag' => $flag,
                'departments' => $departments,
                'count' => $count,
                'search' => $search

            ]);
        }
    }

    /**
     * Displays contact.
     *
     * @return mixed
     */


    public function actionContact()
    {

        $meta_tags = \common\models\MetaTags::find()->where(['id' => 29])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 34])->one();
        $contact_info = \common\models\ContactInfo::findOne(1);
        $districts = \common\models\Districts::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
        $flag = 0;
        $locations = \common\models\NearByLoactions::find()->where(['status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'contact',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'contact_info' => $contact_info,
                'locations' => $locations,
                'districts' => $districts,
                'flag' => $flag,
            ]
        );
    }

    public function actionMedia()
    {
        $meta_tags = \common\models\MetaTags::find()->where(['id' => 30])->one();
        $banner_images = \common\models\BannerImages::find()->where(['id' => 35])->one();
        $gallery = \common\models\MediaGallery::find()->where(['status' => 1,])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
        return $this->render(
            'media',
            [
                'meta_tags' => $meta_tags,
                'banner_images' => $banner_images,
                'gallery' => $gallery
            ]
        );
    }

    /**
     * Displays media gallery.
     *
     * @return mixed
     */

    public function actionMediaDetails($name)
    {
        $gallery = \common\models\MediaGallery::find()->where(['canonical_name' => $name])->one();
        if (isset($gallery)) {
            $meta_tags = \common\models\MetaTags::find()->where(['id' => 30])->one();
            $banner_images = \common\models\BannerImages::find()->where(['id' => 35])->one();
            $photo_gallery = \common\models\MediaPhotoGallery::find()->where(['media_gallery_id' => $gallery->id, 'status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();
            $video_gallery = \common\models\MediaVideoGallery::find()->where(['media_gallery_id' => $gallery->id, 'status' => 1])->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')])->all();

            \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
            \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
            $this->view->params['other_meta_tags'] = $meta_tags->other_meta_tags;
            return $this->render(
                'mediaDetails',
                [
                    'meta_tags' => $meta_tags,
                    'banner_images' => $banner_images,
                    'photo_gallery' => $photo_gallery,
                    'video_gallery' => $video_gallery
                ]
            );
        } else {
            return $this->redirect(['error']);
        }

    }
    /**
     * Displays serviceDetails.
     *
     * @return mixed
     */
    public function actionServiceDetail($id)
    {

        $details = ServiceMainCategory::find()->where(['canonical_name' => $id])->orderBy('sort_order ASC')->one();
        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $details->meta_keyword]);
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $details->meta_description]);
        $services = Services::find()->where(['category_id' => $details->id, 'status' => '1'])->orderBy('sort_order ASC')->all();
        $this->view->params['other_meta_tags'] = $details->other_meta_tags;
        $doctors = Doctors::find()->where(['category' => $details->id, 'status' => '1'])->orderBy('sort_order ASC')->all();
        $districts = \common\models\Districts::find()->where(['status' => 1])->orderBy('sort_order ASC')->all();
        return $this->render('service-detail', [
            'details' => $details,
            'services' => $services,
            'doctors' => $doctors,
            'districts' => $districts,

        ]);
    }

     /**
     * Displays Patient testimonials.
     *
     * @return mixed
     */
    public function actionPatientTestimonial()
    {
        $meta_tags = MetaTags::find()->where(['id' => 30])->one();
        $banner_images = BannerImages::find()->where(['id' => 35])->one();
        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tags->meta_keyword]);
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tags->meta_description]);
        $testimonials = Testimonial::find()->where(['type'=>'1','status'=>'1'])->all();
        return $this->render('patient-testimonial',[
            'meta_tags' => $meta_tags,
            'banner_images' => $banner_images,
            'testimonials' => $testimonials,
        ]);
    }
    public function actionError()
    {
        return $this->render('error', [
        ]);
    }





}