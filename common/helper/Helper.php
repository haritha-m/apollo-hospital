<?php

namespace common\helper;

use common\models\SiteConfig;
use common\models\Cms;
use common\models\AboutStory;
use common\models\AboutExpertise;
use common\models\AboutUs;
use common\models\PageBanner;


class Helper
{
    public static function getSiteConfig($key)
    {
        $response = SiteConfig::find()->where(['label' => $key])->limit('1')->one();
        return $response->value;
    }
    public static function getCmsData($key)
    {
        $response = Cms::find()->where(['label' => $key])->limit('1')->one();
        return $response->value;
    }
    public static function getAboutStory($key)
    {
        $response = AboutStory::find()->where(['label' => $key])->limit('1')->one();
        return $response->value;
    }
    public static function getAboutExpertise($key)
    {
        $response = AboutExpertise::find()->where(['label' => $key])->limit('1')->one();
        return $response->value;
    }
    public static function getAboutUs($key)
    {
        $response = AboutUs::find()->where(['label' => $key])->limit('1')->one();
        return $response->value;
    }
    public static function getPageBanner($key)
    {
        $response = PageBanner::find()->where(['page_name' => $key])->limit('1')->one();
        return $response->image;
    }

    public static function slugify($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}       