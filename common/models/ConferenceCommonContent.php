<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "conference_common_content".
 *
 * @property int $id
 * @property string $title
 * @property string $sub_title
 * @property string $description
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class ConferenceCommonContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conference_common_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title',], 'required'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'sub_title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'description' => 'Description',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
