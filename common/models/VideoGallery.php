<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "video_gallery".
 *
 * @property int $id
 * @property string $video
 * @property string $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class VideoGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort_order', 'status', 'CB', 'UB','gallery_id'], 'integer'],
            [['video','image'],'string'],
            [['DOC', 'DOU'], 'safe'],
            // [['image','video'], 'required', 'on' => 'create'],
            // [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
            [['image_alt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video' => 'Video',
            'image' => 'Thumb Image',
            'image_alt' => 'Thumb Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}