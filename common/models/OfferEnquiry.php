<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "offer_enquiry".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $country
 * @property string $message
 * @property string $offer
 * @property string $date
 */
class OfferEnquiry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offer_enquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['district_id','panchayat_id'],'integer'],
            [['date'], 'safe'],
            [['name', 'email', 'mobile', 'offer'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'district_id' => 'district_id',
            'panchayat_id'=> 'panchayat_id',
            'message' => 'Message',
            'offer' => 'Offer',
            'date' => 'Date',
        ];
    }
}
