<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "promotors".
 *
 * @property int $id
 * @property string $doctor_id
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Promotors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promotors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU','doctor_id'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_id' => 'Doctor',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
