<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AboutQuality;

/**
 * AboutQualitySearch represents the model behind the search form about `common\models\AboutQuality`.
 */
class AboutQualitySearch extends AboutQuality
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title1', 'title1_sub', 'CB', 'UB'], 'integer'],
            [['title1_description', 'image1', 'image1_alt', 'title2', 'title2_description', 'title3', 'title3_description', 'image2', 'image2_alt', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AboutQuality::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'title1' => $this->title1,
            'title1_sub' => $this->title1_sub,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'title1_description', $this->title1_description])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image1_alt', $this->image1_alt])
            ->andFilterWhere(['like', 'title2', $this->title2])
            ->andFilterWhere(['like', 'title2_description', $this->title2_description])
            ->andFilterWhere(['like', 'title3', $this->title3])
            ->andFilterWhere(['like', 'title3_description', $this->title3_description])
            ->andFilterWhere(['like', 'image2', $this->image2])
            ->andFilterWhere(['like', 'image2_alt', $this->image2_alt]);

        return $dataProvider;
    }
}
