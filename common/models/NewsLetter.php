<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_letter".
 *
 * @property int $id
 * @property string $email
 * @property string $dare
 */
class NewsLetter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_letter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'dare'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'dare' => 'Date',
        ];
    }
}
