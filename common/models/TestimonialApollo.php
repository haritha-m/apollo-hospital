<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "testimonial_apollo".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $video_link
 * @property string|null $thumb_link
 * @property int|null $sort_order
 * @property int|null $status
 * @property int|null $CB
 * @property int|null $UB
 * @property string|null $DOC
 * @property string|null $DOU
 */
class TestimonialApollo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'testimonial_apollo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['video_link', 'thumb_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'video_link' => 'Video Link',
            'thumb_link' => 'Thumb Link',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'Cb',
            'UB' => 'Ub',
            'DOC' => 'Doc',
            'DOU' => 'Dou',
        ];
    }
}
