<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "laboratory_services_content".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $image_alt
 * @property string $image1
 * @property string $image1_alt
 * @property string $get_in_touch_title
 * @property string $get_in_touch_caption
 * @property string $email
 * @property string $contact_number
 * @property string $address
 * @property string $international_patients
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class LaboratoryServicesContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'laboratory_services_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'get_in_touch_title', 'get_in_touch_caption', 'email', 'contact_number', 'address', 'international_patients'], 'required'],
            [['description'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'image_alt', 'image1_alt', 'get_in_touch_title', 'get_in_touch_caption', 'email', 'contact_number', 'address', 'international_patients'], 'string', 'max' => 255],
              [['image'], 'required', 'on' => 'create'],
            [['image','image1'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'image1' => 'Image(Laboratory Services)',
            'image1_alt' => 'Image Alt(Laboratory Services)',
            'get_in_touch_title' => 'Get In Touch Title',
            'get_in_touch_caption' => 'Get In Touch Caption',
            'email' => 'Email',
            'contact_number' => 'Contact Number',
            'address' => 'Address',
            'international_patients' => 'International Patients',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
