<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContactInfo;

/**
 * ContactInfoSearch represents the model behind the search form about `common\models\ContactInfo`.
 */
class ContactInfoSearch extends ContactInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'CB', 'UB'], 'integer'],
            [['title', 'caption', 'email', 'conatct_number', 'address', 'international_patients', 'enquiry_title', 'image', 'image_alt', 'near_by_place_title', 'near_by_place_caption', 'map_link', 'whatsap_number', 'footer_conatct_number', 'footer_email', 'footer_caption', 'footer_get_title', 'footer_get_caption', 'facebook_link', 'twitter_linlk', 'instagram_link', 'linkedin_link', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'caption', $this->caption])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'conatct_number', $this->conatct_number])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'international_patients', $this->international_patients])
            ->andFilterWhere(['like', 'enquiry_title', $this->enquiry_title])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'near_by_place_title', $this->near_by_place_title])
            ->andFilterWhere(['like', 'near_by_place_caption', $this->near_by_place_caption])
            ->andFilterWhere(['like', 'map_link', $this->map_link])
            ->andFilterWhere(['like', 'whatsap_number', $this->whatsap_number])
            ->andFilterWhere(['like', 'footer_conatct_number', $this->footer_conatct_number])
            ->andFilterWhere(['like', 'footer_email', $this->footer_email])
            ->andFilterWhere(['like', 'footer_caption', $this->footer_caption])
            ->andFilterWhere(['like', 'footer_get_title', $this->footer_get_title])
            ->andFilterWhere(['like', 'footer_get_caption', $this->footer_get_caption])
            ->andFilterWhere(['like', 'facebook_link', $this->facebook_link])
            ->andFilterWhere(['like', 'twitter_linlk', $this->twitter_linlk])
            ->andFilterWhere(['like', 'instagram_link', $this->instagram_link])
            ->andFilterWhere(['like', 'linkedin_link', $this->linkedin_link]);

        return $dataProvider;
    }
}
