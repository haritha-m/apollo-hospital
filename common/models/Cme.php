<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cme".
 *
 * @property int $id
 * @property string $title
 * @property string $small_description
 * @property string $image
 * @property string $alt_image
 * @property string $date
 * @property string $detail_description
 * @property string $canonical_name
 * @property string $image1
 * @property string $image1_alt
 * @property int $status
 * @property string $banner_image
 * @property string $banner_alt
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string $other_meta_tags
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Cme extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cme';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'date', 'detail_description', 'canonical_name','detail_title'], 'required'],
            [['small_description', 'detail_description', 'meta_title', 'meta_keyword', 'meta_description', 'other_meta_tags'], 'string'],
            [['status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'alt_image', 'date', 'canonical_name', 'image1_alt', 'banner_alt'], 'string', 'max' => 255],
 [['image'], 'required', 'on' => 'create'],
            [['image','banner_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'small_description' => 'Small Description',
            'image' => 'Image',
            'alt_image' => 'Alt Image',
            'date' => 'Date',
            'detail_title'=>'Detail Page Title',
            'detail_description' => 'Detail Page Description',
            'canonical_name' => 'Canonical Name',
            'image1' => 'Image1',
            'image1_alt' => 'Image1 Alt',
            'status' => 'Status',
            'banner_image' => 'Banner Image',
            'banner_alt' => 'Banner Alt',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'other_meta_tags' => 'Other Meta Tags',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
