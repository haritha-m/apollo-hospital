<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "offers".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'sub_title1', 'sub_title1_description', 'sub_title2', 'sub_title2_description',], 'required'],
            [['sub_title1_description', 'sub_title2_description', 'meta_title', 'meta_description', 'meta_keyword', 'other_meta_tags','amount'], 'string'],
            [[ 'CB', 'UB','sort_order','status'], 'integer'],
            [['canonical_name'], 'unique'],
            [['date', 'DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'image_alt', 'sub_title1', 'sub_title2'], 'string', 'max' => 255],
            [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],        
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}