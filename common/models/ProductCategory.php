<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $description
 * @property string $image
 * @property int $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'description','home_description'], 'required'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'description','image_alt','image1_alt'], 'string', 'max' => 255],
    [['image'], 'required', 'on' => 'create'],
            [['image','image1'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'home_description'=>'Home Description',
            'description' => 'Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'image1' => 'Home Image',
            'image1_alt' => 'Home Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
