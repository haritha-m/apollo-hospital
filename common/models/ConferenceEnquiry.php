<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "conference_enquiry".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $country
 * @property string $department
 * @property string $designation
 * @property string $confernce
 * @property string $date
 */
class ConferenceEnquiry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conference_enquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'mobile', 'department', 'designation', 'confernce', 'date'], 'string', 'max' => 255],
            [['district_id','panchayat_id'],'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'district_id'=>'District_id',
            'panchayat_id' => 'Panchayat_id',
            'department' => 'Department',
            'designation' => 'Designation',
            'confernce' => 'Confernce',
            'date' => 'Date',
        ];
    }
}
