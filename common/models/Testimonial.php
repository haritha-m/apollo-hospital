<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "testimonial".
 *
 * @property int $id
 * @property string $name
 * @property string $thump_image
 * @property string $video_link
 * @property int $status
 * @property int $sort_order
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Testimonial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'testimonial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'video_link'], 'required'],
            [['video_link'], 'string'],
            [['status', 'sort_order', 'CB', 'UB','type','video_type'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['name', 'thump_image'], 'string', 'max' => 555],
            [['thump_image'], 'required', 'on' => 'create'],
            [['thump_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'thump_image' => 'Thump Image',
            'video_link' => 'Video Link',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
