<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HomeAbout;

/**
 * HomeAboutSearch represents the model behind the search form about `common\models\HomeAbout`.
 */
class HomeAboutSearch extends HomeAbout
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'count1', 'count2', 'count3', 'count4', 'CB', 'UB'], 'integer'],
            [['title', 'sub_title', 'description', 'video', 'image', 'image_alt', 'count1_title', 'count2_title', 'count3_title', 'count4_title', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HomeAbout::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'count1' => $this->count1,
            'count2' => $this->count2,
            'count3' => $this->count3,
            'count4' => $this->count4,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'sub_title', $this->sub_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'count1_title', $this->count1_title])
            ->andFilterWhere(['like', 'count2_title', $this->count2_title])
            ->andFilterWhere(['like', 'count3_title', $this->count3_title])
            ->andFilterWhere(['like', 'count4_title', $this->count4_title]);

        return $dataProvider;
    }
}
