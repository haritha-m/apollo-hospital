<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "doctors".
 *
 * @property int $id
 * @property string $name
 * @property string $canonical_name
 * @property int $department
 * @property string $small_description
 * @property string $image
 * @property string $image_alt
 * @property string $detail_description
 * @property string $fellowship_title
 * @property string $fellowship_description
 * @property string $expertise_title
 * @property string $expertise_description
 * @property string $languages_title
 * @property string $languages_description
 * @property int $sort_order
 * @property int $status
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $other_meta_tags
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Doctors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'canonical_name', 'department', 'small_description', 'detail_description', 'fellowship_description', 'expertise_description', 'languages_description', 'joined_date','education','category'], 'required'],
            [['canonical_name', 'detail_description', 'fellowship_description', 'expertise_description', 'languages_description', 'meta_title', 'meta_description', 'meta_keyword', 'other_meta_tags','promote_description','education','appointment_link'], 'string'],
            [['department', 'sort_order','view_label', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU', 'joined_date'], 'safe'],
            [['name', 'small_description', 'image_alt', 'fellowship_title', 'expertise_title', 'languages_title'], 'string', 'max' => 255],
              [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'joined_date' => 'Date joined',
            'canonical_name' => 'Canonical Name',
            'department' => 'Department',
            'small_description' => 'Designation',
            'education'=>'Education',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'detail_description' => 'Detail Description',
            'fellowship_title' => 'Fellowship Title',
            'fellowship_description' => 'Fellowship Description',
            'expertise_title' => 'Expertise Title',
            'expertise_description' => 'Expertise Description',
            'languages_title' => 'Languages Title',
            'languages_description' => 'Languages Description',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'view_label'=>'Profile & Book Appoinment Visibility ',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'other_meta_tags' => 'Other Meta Tags',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
            'promote_description'=>'Description(Promotor Page)',
            'appointment_link'=>'Appointment Link',
        ];
    }
}
