<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "laboratory_services".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class LaboratoryServices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'laboratory_services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'image_alt'], 'string', 'max' => 255],
             [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
