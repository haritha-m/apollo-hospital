<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about_quality".
 *
 * @property int $id
 * @property int $title1
 * @property int $title1_sub
 * @property string $title1_description
 * @property string $image1
 * @property string $image1_alt
 * @property string $title2
 * @property string $title2_description
 * @property string $title3
 * @property string $title3_description
 * @property string $image2
 * @property string $image2_alt
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class AboutQuality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_quality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title1', 'title1_sub', 'title1_description', 'title2', 'title2_description', 'title3','title3_description'], 'required'],
            [['CB', 'UB'], 'integer'],
            [['title1_description', 'title2_description'], 'string'],
            [['DOC', 'DOU'], 'safe'],
            [['image1_alt', 'title2', 'title3','image2_alt'], 'string', 'max' => 255],
            [['image1','image2'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title1' => 'Title1',
            'title1_sub' => 'Title1 Sub Title',
            'title1_description' => 'Title1 Description',
            'image1' => 'Title1 Image',
            'image1_alt' => 'Title1 Image1 Alt',
            'title2' => 'Title2',
            'title2_description' => 'Title2 Description',
            'title3' => 'Title3',
            'title3_description' => 'Title3 Description',
            'image2' => 'Title3 Image',
            'image2_alt' => 'Title3 Image',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
