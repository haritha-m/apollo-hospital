<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_main_category".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $caption
 * @property string $banner_image
 * @property string $inner_title
 * @property string $description
 * @property string $form_title
 * @property string $doctors_list_title
 * @property string $service_title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $other_meta_tags
 * @property int $status
 * @property int $sort_order
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class ServiceMainCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_main_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'caption', 'inner_title', 'description', 'form_title', 'doctors_list_title', 'service_title'], 'required'],
            [['description', 'meta_description', 'meta_keyword', 'other_meta_tags'], 'string'],
            [['status', 'sort_order', 'CB', 'UB','category'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'caption', 'banner_image', 'inner_title', 'form_title', 'doctors_list_title', 'service_title', 'meta_title'], 'string', 'max' => 555],
            [['banner_image'], 'required', 'on' => 'create'],
            [['banner_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'caption' => 'Caption',
            'banner_image' => 'Banner Image',
            'inner_title' => 'Inner Title',
            'description' => 'Description',
            'form_title' => 'Form Title',
            'doctors_list_title' => 'Doctors List Title',
            'service_title' => 'Service Title',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'other_meta_tags' => 'Other Meta Tags',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
