<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "quality_safety_files".
 *
 * @property int $id
 * @property string $title
 * @property string $files
 * @property int $status
 * @property int $sort_order
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class QualitySafetyFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quality_safety_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'sort_order', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'files'], 'string', 'max' => 255],
            [['files'], 'required', 'on' => 'create'],
            [['files'], 'file', 'extensions' => 'pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'files' => 'Files',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
