<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "media_blog".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $small_description
 * @property string $image
 * @property string $image_alt
 * @property string $date
 * @property string $detail_description
 * @property string $image1
 * @property string $image1_alt
 * @property string $banner_title
 * @property string $banner_description
 * @property string $banner_image
 * @property string $banner_alt
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $other_meta_tags
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class MediaBlog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'small_description', 'date', 'detail_description'], 'required'],
            [['small_description', 'detail_description', 'banner_description', 'meta_title', 'meta_description', 'meta_keyword', 'other_meta_tags'], 'string'],
            [['status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'image_alt', 'date', 'image1_alt', 'banner_title', 'banner_alt'], 'string', 'max' => 255],
[['image','image1'], 'required', 'on' => 'create'],
            [['image','image1','banner_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'small_description' => 'Small Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'date' => 'Date',
            'detail_description' => 'Detail Description',
            'image1' => 'Image1',
            'image1_alt' => 'Image1 Alt',
            'banner_title' => 'Banner Title',
            'banner_description' => 'Banner Description',
            'banner_image' => 'Banner Image',
            'banner_alt' => 'Banner Alt',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'other_meta_tags' => 'Other Meta Tags',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
