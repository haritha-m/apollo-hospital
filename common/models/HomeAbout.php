<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "home_about".
 *
 * @property int $id
 * @property string $title
 * @property string $sub_title
 * @property string $description
 * @property string $video
 * @property string $image
 * @property string $image_alt
 * @property string $count1_title
 * @property int $count1
 * @property string $count2_title
 * @property int $count2
 * @property string $count3_title
 * @property int $count3
 * @property string $count4_title
 * @property int $count4
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class HomeAbout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'home_about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'sub_title', 'description', 'count1_title', 'count1', 'count2_title', 'count2', 'count3_title', 'count3', 'count4_title', 'count4'], 'required'],
            [['description'], 'string'],
            [['count1', 'count2', 'count3','CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'sub_title', 'image_alt', 'count1_title', 'count2_title', 'count3_title', 'count4_title'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],
            [['video'], 'file', 'extensions' => 'mp4,jpg, png, jpeg, webp,svg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'description' => 'Description',
            'video' => 'Video',
            'image' => 'Thumb Image',
            'image_alt' => 'Thumb Image Alt',
            'count1_title' => 'Count1 Title',
            'count1' => 'Count1',
            'count2_title' => 'Count2 Title',
            'count2' => 'Count2',
            'count3_title' => 'Count3 Title',
            'count3' => 'Count3',
            'count4_title' => 'Count4 Title',
            'count4' => 'Count4',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
