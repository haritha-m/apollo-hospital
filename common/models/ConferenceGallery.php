<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "conference_gallery".
 *
 * @property int $id
 * @property int|null $conference_id
 * @property string|null $image
 * @property string|null $image_alt
 * @property string|null $title
 * @property int|null $sort_order
 * @property int|null $status
 * @property int|null $CB
 * @property int|null $UB
 * @property string|null $DOC
 * @property string|null $DOU
 */
class ConferenceGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conference_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['conference_id', 'sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['image_alt', 'title'], 'string'],
            [['image'],'required','on'=>'create'],
            [['DOC', 'DOU'], 'safe'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'conference_id' => 'Conference ID',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'title' => 'Title',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'Cb',
            'UB' => 'Ub',
            'DOC' => 'Doc',
            'DOU' => 'Dou',
        ];
    }
}
