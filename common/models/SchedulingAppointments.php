<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "scheduling_appointments".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $district_id
 * @property string $panchayat_id
 * @property string $other
 * @property string $date
 */
class SchedulingAppointments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'scheduling_appointments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email','mobile','country_id'], 'required'],
            [['country_id'], 'integer'],
            [['name', 'email', 'mobile', 'date',], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'country_id' => 'country_id',
            'date' => 'Date',
        ];
    }
}