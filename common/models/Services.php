<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $small_description
 * @property int $image
 * @property int $image_alt
 * @property string $detail_title
 * @property string $detail_description
 * @property string $image1
 * @property string $image1_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'small_description', 'detail_title','is_booking', 'detail_description','is_home','category_id','main_category'], 'required'],
            [['small_description', 'detail_description','faq_description', 'facility_description', 'facility_content'], 'string'],
            [['sort_doctors','category_id','sort_order','is_booking','is_home', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU','meta_title','meta_description','meta_keyword','other_meta_tags'], 'safe'],
            [['title', 'canonical_name', 'detail_title', 'image1_alt','image_alt','banner_title','banner_description','banner_alt', 'icon', 'icon_alt'], 'string', 'max' => 255],
             [['image','image1', 'icon'], 'required', 'on' => 'create'],
            [['image','image1','banner_image', 'icon'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sort_doctors' => 'Sort Order for Doctors Page',
            'canonical_name' => 'Canonical Name',
            'small_description' => 'Small Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'detail_title' => 'Detailed Page Title',
            'detail_description' => 'Detailed page Description',
            'image1' => 'Detailed Page Image',
            'image1_alt' => 'Detailed Page Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'is_booking' => 'Booking Availability',
            'is_home' => 'Home Visibility',
            'banner_title'=>'Banner Title',
            'banner_description'=>'Banner Description',
            'banner_image'=>'Banner Image',
            'banner_alt'=>'Banner Alt',
            'meta_title'=>'Meta Title',
            'meta_description'=>'Meta Description',
            'meta_keyword'=>'Meta Keyword',
            'other_meta_tags'=>'Other Meta Tags',
            'book_an_ppointment_link'=>'Book An Appointment Link',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
