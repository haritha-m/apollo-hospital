<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "home_certifications".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class HomeCertifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'home_certifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'sort_order'], 'required'],
            [['description'], 'string'],
            [['sort_order', 'status', 'CB', 'UB','verify_display_mode'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'image_alt','verify_link'], 'string'],
             [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'verify_link'=>'Verification Link',
            'verify_display_mode'=>'Verify Display Mode',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
