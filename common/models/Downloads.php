<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "downloads".
 *
 * @property int $id
 * @property string $brochure
 * @property string $brochure_alt_tag
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Downloads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'downloads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['brochure_alt_tag'], 'string', 'max' => 255],
             [['brochure'], 'required', 'on' => 'create'],
            [['brochure'], 'file', 'extensions' => 'jpg, png, jpeg, webp,pdf,doc'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title'=>'Title',
            'brochure' => 'Brochure',
            'brochure_alt_tag' => 'Brochure Alt Tag',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
