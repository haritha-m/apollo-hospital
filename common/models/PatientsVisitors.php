<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "patients_visitors".
 *
 * @property int $id
 * @property string $main_title
 * @property string $sub_caption
 * @property string $sub_title1
 * @property string $sub_title1_description
 * @property string $sub_title2
 * @property string $sub_title2_description
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class PatientsVisitors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patients_visitors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_title', 'sub_caption', 'sub_title1', 'sub_title1_description', 'sub_title2', 'sub_title2_description','sub_title3','sub_title3_description','home_description','home_title'], 'required'],
            [['sub_title1_description', 'sub_title2_description','image_alt','sub_title3_description','sub_title3'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['main_title', 'sub_caption', 'sub_title1', 'sub_title2'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_title' => 'Main Title',
            'sub_caption' => 'Sub Caption',
            'sub_title1' => 'Sub Title1',
            'sub_title1_description' => 'Sub Title1 Description',
            'sub_title2' => 'Sub Title2',
            'sub_title2_description' => 'Sub Title2 Description',
            'home_title'=>'Home Title',
            'home_description'=>'Home Description',
            'image'=>'Icon (Home Page)',
            'image_alt'=>'Icon Alt (Home Page)',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
