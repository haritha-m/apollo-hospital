<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "quality_safety".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class QualitySafety extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quality_safety';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description','home_title','home_description'], 'required'],
            [['description','image_alt'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title'], 'string', 'max' => 255],
         [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'home_title'=>'Home Title',
            'home_description'=>'Home Description',
            'image'=>'Icon (Home Page)',
            'image_alt'=>'Icon Alt (Home Page)',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
