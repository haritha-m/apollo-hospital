<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CmeGallery;

/**
 * CmeGallerySearch represents the model behind the search form about `common\models\CmeGallery`.
 */
class CmeGallerySearch extends CmeGallery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cme_id', 'type', 'sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['image', 'image_alt', 'video', 'image1', 'image1_alt', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmeGallery::find()->where(['cme_id'=>$_GET['cme_id']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cme_id' => $this->cme_id,
            'type' => $this->type,
            'sort_order' => $this->sort_order,
            'status' => $this->status,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image1_alt', $this->image1_alt]);

        return $dataProvider;
    }
}
