<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\About;

/**
 * AboutSearch represents the model behind the search form about `common\models\About`.
 */
class AboutSearch extends About
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'count1', 'count2', 'count3', 'count4', 'CB', 'UB'], 'integer'],
            [['main_title', 'main_description', 'image', 'image_alt', 'count1_title', 'count2_title', 'count3_title', 'count4_title', 'common_title', 'common_description', 'image1', 'image1_alt', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = About::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'count1' => $this->count1,
            'count2' => $this->count2,
            'count3' => $this->count3,
            'count4' => $this->count4,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'main_title', $this->main_title])
            ->andFilterWhere(['like', 'main_description', $this->main_description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'count1_title', $this->count1_title])
            ->andFilterWhere(['like', 'count2_title', $this->count2_title])
            ->andFilterWhere(['like', 'count3_title', $this->count3_title])
            ->andFilterWhere(['like', 'count4_title', $this->count4_title])
            ->andFilterWhere(['like', 'common_title', $this->common_title])
            ->andFilterWhere(['like', 'common_description', $this->common_description])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image1_alt', $this->image1_alt]);

        return $dataProvider;
    }
}
