<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vision_mission".
 *
 * @property int $id
 * @property string $mission_title
 * @property string $mission_description
 * @property string $vision_title
 * @property string $vision_description
 * @property string $image
 * @property string $image_alt
 * @property string $image1
 * @property string $image1_alt
 * @property string $image2
 * @property string $image2_alt
 * @property string $common_title
 * @property string $common_description
 * @property string $image3
 * @property string $image3_alt
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class VisionMission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vision_mission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mission_title', 'mission_description', 'vision_title', 'vision_description', 'common_title', 'common_description'], 'required'],
            [['mission_description', 'vision_description'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['mission_title', 'vision_title', 'image_alt', 'image1_alt', 'image2_alt', 'common_title', 'common_description', 'image3_alt'], 'string', 'max' => 255],
            [['image','image1','image2','image3'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mission_title' => 'Mission Title',
            'mission_description' => 'Mission Description',
            'vision_title' => 'Vision Title',
            'vision_description' => 'Vision Description',
            'image' => 'Image1',
            'image_alt' => 'Image1 Alt',
            'image1' => 'Image2',
            'image1_alt' => 'Image2 Alt',
            'image2' => 'Image3',
            'image2_alt' => 'Image3 Alt',
            'common_title' => 'Common Title',
            'common_description' => 'Common Description',
            'image3' => 'common Image',
            'image3_alt' => 'Common Image Alt',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
