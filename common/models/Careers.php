<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "careers".
 *
 * @property int $id
 * @property string $title
 * @property string $location
 * @property string $experience
 * @property string $about_the_role
 * @property string $what_you_will_do
 * @property string $what_you_wiil_need
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Careers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'careers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'location', 'experience','about_the_role',  'what_you_wiil_need'], 'required'],
            [['about_the_role', 'what_you_will_do', 'what_you_wiil_need'], 'string'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'location', 'experience'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'location' => 'Location',
            'experience' => 'Experience',
            'about_the_role' => 'About The Role',
            'what_you_will_do' => 'What You Will Do',
            'what_you_wiil_need' => 'What You Wiil Need',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
