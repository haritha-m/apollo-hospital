<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sliders".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property int $sort_order
 * @property string $link
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Sliders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sliders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            
            [['description','image_alt','image1_alt'], 'string'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU','sub_title','link'], 'safe'],
            [['title', 'link'], 'string', 'max' => 255],
            [['image','image1'], 'required', 'on' => 'create'],
            [['image','image1'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sub_title'=>'Expert Link Title',
            'description' => 'Description',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
            'link' => 'Link',
            'image_alt'=>'Image Alt',
            'image1'=>'Mobile Slider',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
