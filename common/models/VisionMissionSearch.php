<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VisionMission;

/**
 * VisionMissionSearch represents the model behind the search form about `common\models\VisionMission`.
 */
class VisionMissionSearch extends VisionMission
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'CB', 'UB'], 'integer'],
            [['mission_title', 'mission_description', 'vision_title', 'vision_description', 'image', 'image_alt', 'image1', 'image1_alt', 'image2', 'image2_alt', 'common_title', 'common_description', 'image3', 'image3_alt', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisionMission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'mission_title', $this->mission_title])
            ->andFilterWhere(['like', 'mission_description', $this->mission_description])
            ->andFilterWhere(['like', 'vision_title', $this->vision_title])
            ->andFilterWhere(['like', 'vision_description', $this->vision_description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image1_alt', $this->image1_alt])
            ->andFilterWhere(['like', 'image2', $this->image2])
            ->andFilterWhere(['like', 'image2_alt', $this->image2_alt])
            ->andFilterWhere(['like', 'common_title', $this->common_title])
            ->andFilterWhere(['like', 'common_description', $this->common_description])
            ->andFilterWhere(['like', 'image3', $this->image3])
            ->andFilterWhere(['like', 'image3_alt', $this->image3_alt]);

        return $dataProvider;
    }
}
