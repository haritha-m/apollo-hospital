<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "our_message".
 *
 * @property int $id
 * @property string $name
 * @property string $position
 * @property string $description
 * @property string $image
 * @property string $image_alt
 * @property string $name1
 * @property string $position1
 * @property string $description1
 * @property string $image1
 * @property string $image1_alt
 * @property string $common_title
 * @property string $common_description
 * @property string $image2
 * @property string $image2_alt
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class OurMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'description', 'name1', 'position1', 'description1', 'common_title', 'common_description'], 'required'],
            [['description', 'description1', 'DOC'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOU'], 'safe'],
            [['name', 'position', 'image_alt', 'name1', 'position1', 'image1_alt', 'common_title', 'common_description', 'image2', 'image2_alt'], 'string', 'max' => 255],
            [['image','image1','image2'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name(Founder 1)',
            'position' => 'Position(Founder 1)',
            'description' => 'Description(Founder 1)',
            'image' => 'Image(Founder 1)',
            'image_alt' => 'Image Alt(Founder 1)',
            'name1' => 'Name(Founder 2)',
            'position1' => 'Position(Founder 2)',
            'description1' => 'Description(Founder 2)',
            'image1' => 'Image(Founder 2)',
            'image1_alt' => 'Image Alt(Founder 2)',
            'common_title' => 'Common Title',
            'common_description' => 'Common Description',
            'image2' => 'Common Image',
            'image2_alt' => 'Common Image Alt',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
