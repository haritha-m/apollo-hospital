<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "laboratory_contant".
 *
 * @property int $id
 * @property string $section1_title
 * @property string $section1_discription
 * @property string $section1_image
 * @property string $section1_image_alt
 * @property string $section2_title
 * @property string $section2_image
 * @property string $section2_image_alt
 * @property string $section3_title
 * @property string $section3_discription
 * @property string $section3_email
 * @property string $section3_phone_number1
 * @property string $section3_phone_number2
 * @property string $section3_address
 * @property string $section3_inp
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class LaboratoryContant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'laboratory_contant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['section1_title', 'section1_discription', 'section2_title', 'section3_title', 'section3_discription', 'section3_inp'], 'string'],
            [['section1_title', 'section1_discription', 'section2_title', 'section3_title', 'section3_discription', 'section3_inp','section1_image_alt','section2_image_alt','section3_email', 'section3_phone_number1', 'section3_phone_number2', 'section3_address'],'required'],
            [['section1_image','section2_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp, svg'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['section1_image', 'section1_image_alt', 'section2_image', 'section2_image_alt', 'section3_email', 'section3_phone_number1', 'section3_phone_number2', 'section3_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section1_title' => 'Section1 Title',
            'section1_discription' => 'Section1 Discription',
            'section1_image' => 'Section1 Image',
            'section1_image_alt' => 'Section1 Image Alt',
            'section2_title' => 'Section2 Title',
            'section2_image' => 'Section2 Image',
            'section2_image_alt' => 'Section2 Image Alt',
            'section3_title' => 'Section3 Title',
            'section3_discription' => 'Section3 Discription',
            'section3_email' => 'Section3 Email',
            'section3_phone_number1' => 'Section3 Phone Number1',
            'section3_phone_number2' => 'Section3 Phone Number2',
            'section3_address' => 'Section3 Address',
            'section3_inp' => 'Section3 Inp',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
