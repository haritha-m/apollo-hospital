<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_images".
 *
 * @property int $id
 * @property string $page
 * @property string $image
 * @property string $description
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class BannerImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description','alt_tag','title'], 'string'],
            [['status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['page'], 'string', 'max' => 32],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],    
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page' => 'Page',
            'image' => 'Banner Image',
            'alt_tag'=>'Banner Alt TAg',
            'title'=>'Title',
            'description' => 'Description',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
