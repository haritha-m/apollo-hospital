<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sanjeevani_gallery".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class SanjeevaniGallery extends \yii\db\ActiveRecord
{
    public $gallery;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sanjeevani_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sanjeevani_id','title', 'canonical_name'], 'required'],
            [['sanjeevani_id', 'sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['description'], 'string'],
            [['title', 'canonical_name', 'image', 'image_alt'], 'string', 'max' => 255],
            [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
            [['gallery'], 'file',  'extensions' => 'jpg, gif, png, jpeg, webp, mp4', 'maxFiles' => 100, 'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}