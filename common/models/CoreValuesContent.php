<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "core_values_content".
 *
 * @property int $id
 * @property string $main_title
 * @property string $main_description
 * @property string $common_title
 * @property string $common_description
 * @property string $image
 * @property string $image_alt
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class CoreValuesContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_values_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_title', 'main_description', 'common_title', 'common_description'], 'required'],
            [['main_description', 'common_description'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['main_title', 'common_title', 'image_alt'], 'string', 'max' => 255],
             [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_title' => 'Main Title',
            'main_description' => 'Main Description',
            'common_title' => 'Common Title',
            'common_description' => 'Common Description',
            'image' => 'Common Image',
            'image_alt' => 'Common Image Alt',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
