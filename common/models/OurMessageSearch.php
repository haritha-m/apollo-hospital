<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OurMessage;

/**
 * OurMessageSearch represents the model behind the search form about `common\models\OurMessage`.
 */
class OurMessageSearch extends OurMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'CB', 'UB'], 'integer'],
            [['name', 'position', 'description', 'image', 'image_alt', 'name1', 'position1', 'description1', 'image1', 'image1_alt', 'common_title', 'common_description', 'image2', 'image2_alt', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OurMessage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'name1', $this->name1])
            ->andFilterWhere(['like', 'position1', $this->position1])
            ->andFilterWhere(['like', 'description1', $this->description1])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image1_alt', $this->image1_alt])
            ->andFilterWhere(['like', 'common_title', $this->common_title])
            ->andFilterWhere(['like', 'common_description', $this->common_description])
            ->andFilterWhere(['like', 'image2', $this->image2])
            ->andFilterWhere(['like', 'image2_alt', $this->image2_alt])
            ->andFilterWhere(['like', 'DOC', $this->DOC]);

        return $dataProvider;
    }
}
