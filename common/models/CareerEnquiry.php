<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "career_enquiry".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $country
 * @property string $image
 * @property string $image_alt
 * @property string $date
 */
class CareerEnquiry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'career_enquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'mobile','image',], 'required'],

            [['name', 'email', 'mobile', 'country', 'date','job_title'], 'string', 'max' => 255],
            [['image_alt'], 'string', 'max' => 25],
            [['image'], 'file', 'extensions' => 'pdf,doc'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'country' => 'Country',
            'image' => 'Resume',
            'image_alt' => 'Image Alt',
            'date' => 'Date',
        ];
    }
}
