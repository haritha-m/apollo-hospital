<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "doctors_video_gallery".
 *
 * @property int $id
 * @property int $doctor_id
 * @property string $title
 * @property string $video
 * @property int $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class DoctorsVideoGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctors_video_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id','sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['title','video'], 'required'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'image_alt','image'], 'string', 'max' => 255],
            [['video'], 'string'],
            [['image'], 'required', 'on' => 'create'],
            // [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],
            // [['video'], 'file', 'extensions' => 'mp4'],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_id' => 'Doctor ID',
            'title' => 'Title',
            'video' => 'Video',
            'image' => 'Youtube ID for Thumb Image',
            'image_alt' => 'Thumb Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
