<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blogs".
 *
 * @property int $id
 * @property string $title
 * @property string $small_description
 * @property string $image
 * @property string $date
 * @property string $detail_description
 * @property string $canonical_name
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Blogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blogs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['alt_image','link'], 'string'],
            [['status', 'CB', 'UB','type'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'date'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'alt_image'=>'Image Alt TAg',
            'date' => 'Date',
            'status' => 'Status',
            'type'=>'Type',
            'link'=>'Link',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
