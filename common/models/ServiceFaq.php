<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_faq".
 *
 * @property int $id
 * @property int $service_id
 * @property string $question
 * @property string $answer
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class ServiceFaq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'question', 'answer'], 'required'],
            [['service_id', 'sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['answer'], 'string'],
            [['DOC', 'DOU'], 'safe'],
            [['question'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'question' => 'Question',
            'answer' => 'Answer',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}