<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "package_enquiry".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $message
 * @property string $package
 * @property string $date
 */
class PackageEnquiry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_enquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message',    ], 'string'],
            [['district_id','panchayat_id'],'integer'],
            [['date'],'safe'],
            [['name', 'email', 'mobile', 'package','other'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'district_id' => 'district_id',
            'panchayat_id' => 'panchayat_id',
            'message' => 'Message',
            'package' => 'Package',
            'other' => 'Other',
            'date' => 'Date',
        ];
    }
}