<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PatientsVisitors;

/**
 * PatientsVisitorsSearch represents the model behind the search form about `common\models\PatientsVisitors`.
 */
class PatientsVisitorsSearch extends PatientsVisitors
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'CB', 'UB'], 'integer'],
            [['main_title', 'sub_caption', 'sub_title1', 'sub_title1_description', 'sub_title2', 'sub_title2_description', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PatientsVisitors::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'main_title', $this->main_title])
            ->andFilterWhere(['like', 'sub_caption', $this->sub_caption])
            ->andFilterWhere(['like', 'sub_title1', $this->sub_title1])
            ->andFilterWhere(['like', 'sub_title1_description', $this->sub_title1_description])
            ->andFilterWhere(['like', 'sub_title2', $this->sub_title2])
            ->andFilterWhere(['like', 'sub_title2_description', $this->sub_title2_description]);

        return $dataProvider;
    }
}
