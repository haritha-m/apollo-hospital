<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sanjeevani_common_content".
 *
 * @property int $id
 * @property string $title
 * @property string $caption
 * @property string $description
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class SanjeevaniCommonContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sanjeevani_common_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'caption',], 'required'],
            [['description'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'caption'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'caption' => 'Caption',
            'description' => 'Description',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
