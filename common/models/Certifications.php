<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "certifications".
 *
 * @property int $id
 * @property string $title
 * @property string $sub_title1
 * @property string $sub_title2
 * @property string $description
 * @property string $image
 * @property string $image_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Certifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'certifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
            [['sort_order', 'status', 'CB', 'UB','verify_display_mode'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'sub_title1', 'sub_title2', 'image_alt'], 'string', 'max' => 255],
                        [['title', 'sub_title1', 'sub_title2', 'image_alt','verify_link'], 'string'],

            [['image'], 'required', 'on' => 'create'],
            [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'sub_title1' => 'Sub Title1',
            'sub_title2' => 'Sub Title2',
            'description' => 'Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'verify_link'=>'Verification Link',
            'verify_display_mode'=>'Verify Display Mode',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
