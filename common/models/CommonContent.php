<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "common_content".
 *
 * @property int $id
 * @property string $medical_care_quality_title
 * @property string $medical_care_quality_sub_title
 * @property string $image
 * @property string $image_alt
 * @property string $department_title
 * @property string $department_sub_title
 * @property string $department_sub_description
 * @property string $hospital_nation_title
 * @property string $hospital_nation_sub_title
 * @property string $image1
 * @property string $image1_alt
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class CommonContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'common_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medical_care_quality_title', 'medical_care_quality_sub_title', 'department_sub_title', 'department_sub_description', 'hospital_nation_title', 'hospital_nation_sub_title','home_title','home_description','ip_title','ip_caption','package_title','package_description','specialities_sub_title','specialities_description',], 'required'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['specialities_title','specialities_sub_title','specialities_description','medical_care_quality_title', 'medical_care_quality_sub_title', 'image_alt', 'department_title', 'department_sub_title', 'hospital_nation_title', 'hospital_nation_sub_title', 'image1_alt','image2_alt'], 'string', 'max' => 255],
            [['image','image1','image2'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],
            [['brochre'],'file','extensions'=>'pdf'],

            [['department_sub_description'], 'string', 'max' => 400],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'medical_care_quality_title' => 'Medical Care Quality Title',
            'medical_care_quality_sub_title' => 'Medical Care Quality Sub Title',
            'image' => 'Image(Medical Care Quality)',
            'image_alt' => 'Image Alt(Medical Care Quality)',
            'department_title' => 'Department Title',
            'department_sub_title' => 'Department Sub Title',
            'department_sub_description' => 'Department Sub Description',
            'hospital_nation_title' => 'Hospital Nation Title(After Offers & Packages Section)',
            'hospital_nation_sub_title' => 'Hospital Nation Sub Title(After Offers & Packages Section)',
            'image1' => 'Image(Hospital Nation)(After Offers & Packages Section)',
            'image1_alt' => 'Image Alt(Hospital Nation)(After Offers & Packages Section)',
            'home_title' => 'Title(Home-International Patientss)',
            'home_description' => 'Description(Home-International Patients)',
            'image2' => 'Image(Home-International Patients)',
            'image2_alt' => 'Image Alt(Home-International Patients)',
            'ip_title' => 'Title(Main Page-International Patientss)',
            'ip_caption' => 'Caption(Main Page-International Patients)',
            'package_title' => 'Title(Main Page-Heath Packages)',
            'package_description' => 'Description(Main Page-Heath Packages)',
            'brochre' => 'brochure',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
