<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property int $category_id
 * @property string $short_description
 * @property string $image
 * @property string $image_alt
 * @property string $image1
 * @property string $image1_alt
 * @property string $image2
 * @property string $image2_alt
 * @property string $image3
 * @property string $image3_alt
 * @property string $brochure
 * @property string $brochure_title
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'category_id', 'short_description', 'brochure_title','sub_category_id'], 'required'],
            [['category_id', 'sort_order', 'status', 'CB', 'UB','sub_category_id'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'short_description', 'image_alt', 'image1_alt', 'image2_alt', 'image3_alt', 'brochure_title'], 'string', 'max' => 255],
            [['image', 'image1', 'image2', 'image3','brochure'], 'required', 'on' => 'create'],
            [['image', 'image1', 'image2', 'image3'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
            [['brochure'], 'file', 'extensions' => 'doc, pdf'],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'category_id' => 'Product Category',
            'sub_category_id'=>'Product Sub Category',
            'short_description' => 'Short Description(Detail Page)',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'image1' => 'Detail Page Image',
            'image1_alt' => 'Detail Page Image Alt',
            'image2' => 'Description Image1',
            'image2_alt' => 'Description Image1 Alt',
            'image3' => 'Description Image2',
            'image3_alt' => 'Description Image2 Alt',
            'brochure' => 'Brochure',
            'brochure_title' => 'Brochure Title',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
