<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "packages".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $small_description
 * @property string $image
 * @property string $image_alt
 * @property int $amount
 * @property string $sub_title1
 * @property string $sub_title1_description
 * @property string $image1
 * @property string $image2_alt
 * @property string $sub_title2
 * @property string $sub_title2_description
 * @property string $banner_title
 * @property string $banner_description
 * @property string $banner_image
 * @property string $banner_alt
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $other_meta_tags
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Packages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'packages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'small_description','amount', 'sub_title1',  'sub_title2','book_appointment_link'], 'required'],
            [['small_description', 'sub_title1_description', 'sub_title2_description', 'banner_description', 'meta_title', 'meta_description', 'meta_keyword', 'other_meta_tags'], 'string'],
            [['amount', 'CB', 'UB','sort_order','status'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'image_alt', 'sub_title1', 'image2_alt', 'sub_title2', 'banner_title', 'banner_alt'], 'string', 'max' => 255],
             [['image','image1'], 'required', 'on' => 'create'],
            [['image','image1','banner_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'small_description' => 'Small Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'amount' => 'Amount',
            'sub_title1' => 'Sub Title1',
            'sub_title1_description' => 'Sub Title1 Description',
            'image1' => 'Sub Title1 Image',
            'image2_alt' => 'Sub Title1 Image Alt',
            'sub_title2' => 'Sub Title2',
            'sub_title2_description' => 'Sub Title2 Description',
            'banner_title' => 'Banner Title',
            'banner_description' => 'Banner Description',
            'banner_image' => 'Banner Image',
            'banner_alt' => 'Banner Alt',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'other_meta_tags' => 'Other Meta Tags',
            'book_appointment_link'=>'Book an  Appointment',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
            'sort_order'=>'Sort Order',
            'status'=>'Status'
        ];
    }
}
