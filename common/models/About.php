<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $main_title
 * @property string $main_description
 * @property string $image
 * @property string $image_alt
 * @property string $count1_title
 * @property int $count1
 * @property string $count2_title
 * @property int $count2
 * @property string $count3_title
 * @property int $count3
 * @property string $count4_title
 * @property int $count4
 * @property string $common_title
 * @property string $common_description
 * @property string $image1
 * @property string $image1_alt
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_title', 'main_description', 'count1_title', 'count1', 'count2_title', 'count2', 'count3_title', 'count3', 'count4_title', 'count4', 'common_title', 'common_description'], 'required'],
            [['main_description'], 'string'],
            [['count1', 'count2', 'count3','CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['main_title', 'image_alt', 'count1_title', 'count2_title', 'count3_title', 'count4_title', 'common_title', 'common_description', 'image1_alt'], 'string', 'max' => 255],
              [['image','image1'], 'file', 'extensions' => 'jpg, png, jpeg, webp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_title' => 'Main Title',
            'main_description' => 'Main Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'count1_title' => 'Count1 Title',
            'count1' => 'Count1',
            'count2_title' => 'Count2 Title',
            'count2' => 'Count2',
            'count3_title' => 'Count3 Title',
            'count3' => 'Count3',
            'count4_title' => 'Count4 Title',
            'count4' => 'Count4',
            'common_title' => 'Common Title',
            'common_description' => 'Common Description',
            'image1' => 'Common Image',
            'image1_alt' => 'Common Image Alt',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
