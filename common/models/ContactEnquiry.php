<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact_enquiry".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $country
 * @property string $message
 * @property string $date
 */
class ContactEnquiry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_enquiry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'phone', 'message', 'date'], 'required'],
            [['district_id','panchayat_id'],'integer'],
            [['message'], 'string'],
            [['date'], 'safe'],
            [['name', 'email', 'phone',], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'country' => 'Country',
            'message' => 'Message',
            'date' => 'Date',
        ];
    }
}
