<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "brochure".
 *
 * @property int $id
 * @property string|null $pdf
 * @property string|null $pdf_title
 * @property int $status
 * @property int $sort_order
 * @property int $CB
 * @property int $UB
 * @property string|null $DOC
 * @property string|null $DOU
 */
class Brochure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brochure';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pdf_title'], 'string'],
            [['pdf_title', 'sort_order',], 'required'],
            [['pdf'],'required','on'=>'create'],
            [['status', 'sort_order', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['pdf'],'file', 'extensions'=>'pdf'],
            [['pdf'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pdf' => 'Pdf',
            'pdf_title' => 'Pdf Title',
            'status' => 'Status',
            'sort_order' => 'Sort Order',
            'CB' => 'Cb',
            'UB' => 'Ub',
            'DOC' => 'Doc',
            'DOU' => 'Dou',
        ];
    }
}
