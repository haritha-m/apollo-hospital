<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "conference".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $small_description
 * @property string $image
 * @property string $image_alt
 * @property string $date
 * @property int $status
 * @property string $detail_description
 * @property string $image1
 * @property string $image1_alt
 * @property string $image2
 * @property string $image2_alt
 * @property string $banner_title
 * @property string $banner_description
 * @property string $banner_image
 * @property string $banner_alt
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $other_meta_tags
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Conference extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conference';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'small_description','date', 'detail_description'], 'required'],
            [['status', 'CB', 'UB'], 'integer'],
            [['detail_description', 'meta_title', 'meta_description', 'meta_keyword', 'other_meta_tags'], 'string'],
            [['date', 'DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'small_description', 'image_alt',  'image1_alt', 'image2_alt', 'banner_title', 'banner_description', 'banner_alt'], 'string', 'max' => 255],
             [['image','image1'], 'required', 'on' => 'create'],
            [['image', 'image1', 'image2', 'banner_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],




        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'small_description' => 'Small Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'date' => 'Date',
            'status' => 'Status',
            'detail_description' => 'Description(Detail Page)',
            'image1' => 'Image(Detail Page)',
            'image1_alt' => 'Image Alt(Detail Page)',
            'image2' => 'Image2(Registration Section)',
            'image2_alt' => 'Image Alt(Registration Section)',
            'banner_title' => 'Banner Title',
            'banner_description' => 'Banner Description',
            'banner_image' => 'Banner Image',
            'banner_alt' => 'Banner Alt',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'other_meta_tags' => 'Other Meta Tags',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
