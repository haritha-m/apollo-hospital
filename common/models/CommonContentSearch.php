<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CommonContent;

/**
 * CommonContentSearch represents the model behind the search form about `common\models\CommonContent`.
 */
class CommonContentSearch extends CommonContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'CB', 'UB'], 'integer'],
            [['medical_care_quality_title', 'medical_care_quality_sub_title', 'image', 'image_alt', 'department_title', 'department_sub_title', 'department_sub_description', 'hospital_nation_title', 'hospital_nation_sub_title', 'image1', 'image1_alt', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommonContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'medical_care_quality_title', $this->medical_care_quality_title])
            ->andFilterWhere(['like', 'medical_care_quality_sub_title', $this->medical_care_quality_sub_title])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'image_alt', $this->image_alt])
            ->andFilterWhere(['like', 'department_title', $this->department_title])
            ->andFilterWhere(['like', 'department_sub_title', $this->department_sub_title])
            ->andFilterWhere(['like', 'department_sub_description', $this->department_sub_description])
            ->andFilterWhere(['like', 'hospital_nation_title', $this->hospital_nation_title])
            ->andFilterWhere(['like', 'hospital_nation_sub_title', $this->hospital_nation_sub_title])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image1_alt', $this->image1_alt]);

        return $dataProvider;
    }
}
