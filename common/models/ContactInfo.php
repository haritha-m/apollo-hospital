<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact_info".
 *
 * @property int $id
 * @property string $title
 * @property string $caption
 * @property string $email
 * @property string $conatct_number
 * @property string $address
 * @property string $international_patients
 * @property string $enquiry_title
 * @property string $image
 * @property string $image_alt
 * @property string $near_by_place_title
 * @property string $near_by_place_caption
 * @property string $map_link
 * @property string $whatsap_number
 * @property string $footer_conatct_number
 * @property string $footer_email
 * @property string $footer_caption
 * @property string $footer_get_title
 * @property string $footer_get_caption
 * @property string $facebook_link
 * @property string $twitter_linlk
 * @property string $instagram_link
 * @property string $linkedin_link
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class ContactInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'caption', 'email', 'conatct_number', 'address', 'international_patients', 'enquiry_title', 'near_by_place_title', 'near_by_place_caption', 'map_link', 'whatsap_number', 'footer_conatct_number', 'footer_email', 'footer_caption', 'footer_get_title', 'footer_get_caption', 'facebook_link', 'twitter_linlk', 'instagram_link', 'linkedin_link'], 'required'],
            [['map_link', 'facebook_link', 'twitter_linlk', 'instagram_link', 'linkedin_link', 'address', 'international_patients'], 'string'],
            [['CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'caption', 'email', 'conatct_number', 'enquiry_title', 'image_alt', 'near_by_place_title', 'near_by_place_caption', 'whatsap_number', 'footer_conatct_number', 'footer_email', 'footer_caption', 'footer_get_title', 'footer_get_caption','sidemenu_get_in_touch_phone_number'], 'string', 'max' => 255],
                        [['image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'caption' => 'Caption',
            'email' => 'Email',
            'conatct_number' => 'Conatct Number',
            'address' => 'Address',
            'international_patients' => 'International Patients',
            'enquiry_title' => 'Enquiry Title',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'near_by_place_title' => 'Near By Place Title',
            'near_by_place_caption' => 'Near By Place Caption',
            'map_link' => 'Map Link',
            'whatsap_number' => 'Whatsap Number',
            'footer_conatct_number' => 'Footer Conatct Number',
            'footer_email' => 'Footer Email',
            'footer_caption' => 'Footer Caption',
            'footer_get_title' => 'Footer  Title(Get In Touch)',
            'footer_get_caption' => 'Footer Caption(Get In Touch)',
            'facebook_link' => 'Facebook Link',
            'twitter_linlk' => 'Twitter Linlk',
            'instagram_link' => 'Instagram Link',
            'linkedin_link' => 'Linkedin Link',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
