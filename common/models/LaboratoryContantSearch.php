<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LaboratoryContant;

/**
 * LaboratoryContantSearch represents the model behind the search form about `common\models\LaboratoryContant`.
 */
class LaboratoryContantSearch extends LaboratoryContant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'CB', 'UB'], 'integer'],
            [['section1_title', 'section1_discription', 'section1_image', 'section1_image_alt', 'section2_title', 'section2_image', 'section2_image_alt', 'section3_title', 'section3_discription', 'section3_email', 'section3_phone_number1', 'section3_phone_number2', 'section3_address', 'section3_inp', 'DOC', 'DOU'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LaboratoryContant::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'CB' => $this->CB,
            'UB' => $this->UB,
            'DOC' => $this->DOC,
            'DOU' => $this->DOU,
        ]);

        $query->andFilterWhere(['like', 'section1_title', $this->section1_title])
            ->andFilterWhere(['like', 'section1_discription', $this->section1_discription])
            ->andFilterWhere(['like', 'section1_image', $this->section1_image])
            ->andFilterWhere(['like', 'section1_image_alt', $this->section1_image_alt])
            ->andFilterWhere(['like', 'section2_title', $this->section2_title])
            ->andFilterWhere(['like', 'section2_image', $this->section2_image])
            ->andFilterWhere(['like', 'section2_image_alt', $this->section2_image_alt])
            ->andFilterWhere(['like', 'section3_title', $this->section3_title])
            ->andFilterWhere(['like', 'section3_discription', $this->section3_discription])
            ->andFilterWhere(['like', 'section3_email', $this->section3_email])
            ->andFilterWhere(['like', 'section3_phone_number1', $this->section3_phone_number1])
            ->andFilterWhere(['like', 'section3_phone_number2', $this->section3_phone_number2])
            ->andFilterWhere(['like', 'section3_address', $this->section3_address])
            ->andFilterWhere(['like', 'section3_inp', $this->section3_inp]);

        return $dataProvider;
    }
}
