<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cme_gallery".
 *
 * @property int $id
 * @property int $cme_id
 * @property int $type
 * @property string $image
 * @property string $image_alt
 * @property string $video
 * @property string $image1
 * @property string $image1_alt
 * @property int $sort_order
 * @property int $status
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class CmeGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cme_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cme_id', 'type', 'sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['type'], 'required'],
            [['DOC', 'DOU'], 'safe'],
 [['image','image1'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],
  [['video'], 'file', 'extensions' => 'mp4'],

              [['image_alt', 'image1_alt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cme_id' => 'Cme ID',
            'type' => 'Type',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'video' => 'Video',
            'image1' => 'Thumb Image',
            'image1_alt' => 'Thumb Image Alt',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
