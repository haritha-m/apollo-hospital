<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sanjeevani".
 *
 * @property int $id
 * @property string $title
 * @property string $canonical_name
 * @property string $value
 * @property string $value_text
 * @property string $small_description
 * @property string $image
 * @property string $image_alt
 * @property string $detail_title
 * @property string $detail_description
 * @property string $gallery
 * @property int $sort_order
 * @property int $status
 * @property string $banner_title
 * @property string $banner_description
 * @property string $banner_image
 * @property string $banner_alt
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $other_meta_tags
 * @property int $CB
 * @property int $UB
 * @property string $DOC
 * @property string $DOU
 */
class Sanjeevani extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sanjeevani';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'canonical_name', 'value', 'value_text', 'detail_description','small_description'], 'required'],
            [['detail_description', 'meta_title', 'meta_description', 'meta_keyword', 'other_meta_tags'], 'string'],
            [['sort_order', 'status', 'CB', 'UB'], 'integer'],
            [['DOC', 'DOU'], 'safe'],
            [['title', 'canonical_name', 'value', 'value_text', 'small_description', 'image_alt', 'detail_title', 'banner_title', 'banner_description', 'banner_alt'], 'string', 'max' => 255],
              [['image'], 'required', 'on' => 'create'],
            [['image','banner_image'], 'file', 'extensions' => 'jpg, png, jpeg, webp,svg'],



            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'canonical_name' => 'Canonical Name',
            'value' => 'Value',
            'value_text' => 'Value Sub Text',
            'small_description' => 'Small Description',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'detail_title' => 'Sub Title(Detail Page)',
            'detail_description' => 'Description(Detail Page)',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
            'banner_title' => 'Banner Title',
            'banner_description' => 'Banner Description',
            'banner_image' => 'Banner Image',
            'banner_alt' => 'Banner Alt',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'other_meta_tags' => 'Other Meta Tags',
            'CB' => 'C B',
            'UB' => 'U B',
            'DOC' => 'D O C',
            'DOU' => 'D O U',
        ];
    }
}
