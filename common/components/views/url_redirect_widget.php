<?php

$action = Yii::$app->controller->action->id;
$url = 'campusworld.net' . $_SERVER['REQUEST_URI'];

//if ($action == 'study-abroad-details') {
//    Yii::$app->response->redirect(['site/study-abroad-details', 'title' => Yii::$app->getRequest()->getQueryParam('title')]);
//}

if ($url == 'campusworld.net/guide-for-indians-to-study-in-ireland/' || $url == 'campusworld.net/guide-for-indians-to-study-in-ireland') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'guide-for-indians-to-study-in-ireland']);
}

if ($url == 'campusworld.net/overseas-education-consultants-in-kochi-for-a-brighter-career-path/' || $url == 'campusworld.net/overseas-education-consultants-in-kochi-for-a-brighter-career-path') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'overseas-education-consultants-in-kochi-for-a-brighter-career-path']);
}

if ($url == 'campusworld.net/how-to-pay-for-college-study-abroad-programs/' || $url == 'campusworld.net/how-to-pay-for-college-study-abroad-programs') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'how-to-pay-for-college-study-abroad-programs']);
}

if ($url == 'campusworld.net/best-study-in-germany-consultants-kochi/' || $url == 'campusworld.net/best-study-in-germany-consultants-kochi') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'best-study-in-germany-consultants-kochi']);
}

if ($url == 'campusworld.net/the-necessities-of-guidance-from-study-abroad-consultants-in-cochin/' || $url == 'campusworld.net/the-necessities-of-guidance-from-study-abroad-consultants-in-cochin') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'the-necessities-of-guidance-from-study-abroad-consultants-in-cochin']);
}

if ($url == 'campusworld.net/abroad-studies-in-malaysia/' || $url == 'campusworld.net/abroad-studies-in-malaysia') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'abroad-studies-in-malaysia']);
}

if ($url == 'campusworld.net/study-abroad-checklist-2021/' || $url == 'campusworld.net/study-abroad-checklist-2021') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-abroad-checklist-2021']);
}

if ($url == 'campusworld.net/why-you-should-consider-study-abroad-consultants/' || $url == 'campusworld.net/why-you-should-consider-study-abroad-consultants') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'why-you-should-consider-study-abroad-consultants']);
}

if ($url == 'campusworld.net/why-study-in-new-zealand/' || $url == 'campusworld.net/why-study-in-new-zealand') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'why-study-in-new-zealand']);
}

if ($url == 'campusworld.net/top-5-courses-to-study-abroad/' || $url == 'campusworld.net/top-5-courses-to-study-abroad') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'top-5-courses-to-study-abroad']);
}

if ($url == 'campusworld.net/top-5-universities-to-study-abroad/' || $url == 'campusworld.net/top-5-universities-to-study-abroad/') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'top-5-universities-to-study-abroad']);
}

if ($url == 'campusworld.net/top-5-destinations-to-study-abroad/' || $url == 'campusworld.net/top-5-destinations-to-study-abroad') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'top-5-destinations-to-study-abroad']);
}

if ($url == 'campusworld.net/why-we-should-approach-overseas-education-consultants-to-study-abroad/' || $url == 'campusworld.net/why-we-should-approach-overseas-education-consultants-to-study-abroad') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'why-we-should-approach-overseas-education-consultants-to-study-abroad']);
}

if ($url == 'campusworld.net/possibilities-of-abroad-studies-in-2020/' || $url == 'campusworld.net/possibilities-of-abroad-studies-in-2020') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'possibilities-of-abroad-studies-in-2020']);
}

if ($url == 'campusworld.net/5-reasons-to-study-abroad/' || $url == 'campusworld.net/5-reasons-to-study-abroad') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => '5-reasons-to-study-abroad']);
}

if ($url == 'campusworld.net/scholarships-for-indian-women/' || $url == 'campusworld.net/scholarships-for-indian-women') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'scholarships-for-indian-women']);
}

if ($url == 'campusworld.net/7-good-reasons-to-study-in-canada/' || $url == 'campusworld.net/7-good-reasons-to-study-in-canada') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => '7-good-reasons-to-study-in-canada']);
}

if ($url == 'campusworld.net/top-reasons-to-study-in-austria/' || $url == 'campusworld.net/top-reasons-to-study-in-austria') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'top-reasons-to-study-in-austria']);
}

if ($url == 'campusworld.net/study-in-latvia/' || $url == 'campusworld.net/study-in-latvia') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-in-latvia']);
}

if ($url == 'campusworld.net/scholarships-for-indian-students-in-canada/' || $url == 'campusworld.net/scholarships-for-indian-students-in-canada') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'scholarships-for-indian-students-in-canada']);
}

if ($url == 'campusworld.net/why-study-in-europe/' || $url == 'campusworld.net/why-study-in-europe') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'why-study-in-europe']);
}

if ($url == 'campusworld.net/study-in-germany/' || $url == 'campusworld.net/study-in-germany') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-in-germany']);
}

if ($url == 'campusworld.net/study-in-australia-with-scholarship/' || $url == 'campusworld.net/study-in-australia-with-scholarship') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-in-australia-with-scholarship']);
}

if ($url == 'campusworld.net/scholarships-for-indian-women/' || $url == 'campusworld.net/scholarships-for-indian-women') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'scholarships-for-indian-women']);
}

if ($url == 'campusworld.net/finance-your-studies-abroad/' || $url == 'campusworld.net/finance-your-studies-abroad') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'finance-your-studies-abroad']);
}

if ($url == 'campusworld.net/best-country-to-study-for-indian-students/' || $url == 'campusworld.net/best-country-to-study-for-indian-students') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'best-country-to-study-for-indian-students']);
}

if ($url == 'campusworld.net/overseas-education-scholarship/' || $url == 'campusworld.net/overseas-education-scholarship') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'overseas-education-scholarship']);
}

if ($url == 'campusworld.net/germany-scholarship/' || $url == 'campusworld.net/germany-scholarship') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'germany-scholarship']);
}

if ($url == 'campusworld.net/study-in-usa-with-scholarship-campus-world/' || $url == 'campusworld.net/study-in-usa-with-scholarship-campus-world') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-in-usa-with-scholarship-campus-world']);
}

if ($url == 'campusworld.net/uk-scholarships/' || $url == 'campusworld.net/uk-scholarships') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'uk-scholarships']);
}

if ($url == 'campusworld.net/study-in-sweden/' || $url == 'campusworld.net/study-in-sweden') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-in-sweden']);
}

if ($url == 'campusworld.net/study-in-france/' || $url == 'campusworld.net/study-in-france') {
    Yii::$app->response->redirect(['site/blog-details', 'name' => 'study-in-france/']);
}
?>