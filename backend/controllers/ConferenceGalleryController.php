<?php

namespace backend\controllers;

use Yii;
use common\models\ConferenceGallery;
use common\models\ConferenceGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\helper\Helper;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * ConferenceGalleryController implements the CRUD actions for ConferenceGallery model.
 */
class ConferenceGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ConferenceGallery models.
     * @return mixed
     */
    public function actionIndex($conference_id)
    {
        $searchModel = new ConferenceGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'conference_id' => $conference_id,
        ]);
    }

    /**
     * Displays a single ConferenceGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ConferenceGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($conference_id)
    {
        $model = new ConferenceGallery();
         $model->conference_id =$conference_id;
       $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
        $image = UploadedFile::getInstance($model, 'image');
       
        if(!empty($image)){

        $model->image = Helper::slugify($model->title).'-'.yii::$app->EncryptDecrypt->encrypt('encrypt',date('y-m-d H:i:s')).'.'.$image->extension;

        if ($model->validate() && $model->save()) {
            $this->Upload($image,    $model);
            Yii::$app->session->setFlash('success', "Created successfully");
            return $this->redirect(['index','conference_id'=> $conference_id]);
    }
    }
    } else {
    }return $this->render('create', [
        'model' => $model,
        'conference_id'=>$conference_id
]);
    }

    /**
     * Updates an existing ConferenceGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_ = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? Helper::slugify($model->image_alt).'-'.Yii::$app->EncryptDecrypt->encrypt('encrypt',date('Y-m-d H:i:s')).'.'.$image->extension : $model_->image;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$model);
                Yii::$app->session->setFlash('success', "Updated successfully");
                return $this->redirect(['index','conference_id'=> $model->conference_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'conference_id'=> $model->conference_id
            
        ]); 
    }



public function Upload($image, $model) {
        
        $path = Yii::$app->basePath . '/../frontend/web/images/conference/gallery';
        FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        if (!empty($image)) {
            // $id     = Yii::$app->EncryptDecrypt->encrypt('encrypt', $model->id);
            $name   =  $model->image;
            $image->saveAs($path . '/' . $name);
        }
    }
    /**
     * Deletes an existing ConferenceGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
       Yii::$app->session->setFlash('success', "Updated successfully");
        return $this->redirect(['index','conference_id'=> $model->conference_id]);
    }

    /**
     * Finds the ConferenceGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConferenceGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ConferenceGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
