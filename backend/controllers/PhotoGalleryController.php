<?php

namespace backend\controllers;

use Yii;
use common\models\PhotoGallery;
use common\models\PhotoGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * PhotoGalleryController implements the CRUD actions for PhotoGallery model.
 */
class PhotoGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PhotoGallery models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new PhotoGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['gallery_id' => $id]);
        $dataProvider->query->orderBy([new \yii\db\Expression('-sort_order DESC, sort_order ASC')]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Creates a new PhotoGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id){
        $model = new PhotoGallery();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
        $model->gallery_id = $id;
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
                Yii::$app->session->setFlash('success', "New Photo Gallery added successfully");
                return $this->redirect(['index','id' => $id]);
            }
        }return $this->render('create', [
                    'model' => $model,
                    'id' => $id,
        ]);
    }

      public function Upload($image, $model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/media-gallery/' . $model->gallery_id . '/photos';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
            }
            Yii::$app->session->setFlash('success', "Photo Gallery updated successfully");
            return $this->redirect(['index','id' => $model->gallery_id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $path = Yii::$app->basePath . '/../frontend/web/images/media-gallery/' . $model->gallery_id . '/photos/image' . $id . '.' . $model->image;

        if ($this->findModel($id)->delete()) {
            if (file_exists($path)) {
                unlink($path);
            }
            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index','id' => $model->gallery_id]);
    }

    /**
     * Finds the PhotoGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PhotoGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PhotoGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}