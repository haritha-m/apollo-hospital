<?php

namespace backend\controllers;

use Yii;
use common\models\SanjeevaniGallery;
use common\models\SanjeevaniGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * SanjeevaniGalleryController implements the CRUD actions for SanjeevaniGallery model.
 */
class SanjeevaniGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SanjeevaniGallery models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new SanjeevaniGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['sanjeevani_id' => $id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Creates a new SanjeevaniGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new SanjeevaniGallery();
        $model->setScenario('create');
        
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $model->sanjeevani_id   = $id;
            $image                  = UploadedFile::getInstance($model, 'image');
            $model->image           = $image->extension;
            $files                  = UploadedFile::getInstances($model, 'gallery');
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $files, $model);
                Yii::$app->session->setFlash('success', "Sanjeevani Gallery added successfully");
                return $this->redirect(['index','id' => $id]);
            }
        }return $this->render('create', [
                    'model' => $model,
                    'id' => $id,
        ]);
    }

    public function Upload($image, $files, $model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/sanjeevani-gallery/' . $model->sanjeevani_id . '/' . $model->id ;
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
        if (!empty($files)) {
            $paths = Yii::$app->basePath . '/../frontend/web/images/sanjeevani-gallery/' . $model->sanjeevani_id . '/' . $model->id . '/gallery';
             $path = $this->CheckPath($paths);
             foreach ($files as $file) {
                 $name = $this->fileExists($path, $file->baseName . '.' . $file->extension, $file, 1);
                 $file->saveAs($path . '/' . $name);
             }
              return TRUE;
      }
    }
    
    public function CheckPath($paths) {
        if (!is_dir($paths)) {
            mkdir($paths);
        }
        return $paths;
    }
    
    /**
     * Function for FileExists
     */
    public function fileExists($path, $name, $file, $sufix) {
        if (file_exists($path . '/' . $name)) {

            $name = basename($path . '/' . $file->baseName, '.' . $file->extension) . '_' . $sufix . '.' . $file->extension;
            return $this->fileExists($path, $name, $file, $sufix + 1);
        } else {
            return $name;
        }
    }

    /**
     * Updates an existing SanjeevaniGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image_ = $model->image;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $files = UploadedFile::getInstances($model, 'gallery');
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $files, $model);
            }
            Yii::$app->session->setFlash('success', "Sanjeevani Gallery updated successfully");
            return $this->redirect(['index','id' => $model->sanjeevani_id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SanjeevaniGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $path                   = Yii::$app->basePath . '/../frontend/web/images/sanjeevani-gallery/' . $model->sanjeevani_id . '/' . $model->id;

        if ($this->findModel($id)->delete()) {
            if (is_dir($path)) {
                $this->deleteDir($path);
            }
            Yii::$app->session->setFlash('success', "Sanjeevani Gallery removed successfully");
        }

        return $this->redirect(['index','id' => $model->sanjeevani_id]);
    }
    /**
     * Function for deleteDir
     */
    public function deleteDir($dirPath) {
        if (is_dir($dirPath)) {
            if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                $dirPath .= '/';
            }
            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    self::deleteDir($file);
                } else {
                    unlink($file);
                }
            }
        }
        rmdir($dirPath);
        return;
    }
    /**
     * Finds the SanjeevaniGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SanjeevaniGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SanjeevaniGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveGallery() {
        if (yii::$app->request->isAjax) {
        $id = $_POST['id'];
        $model = $this->findModel($id);
        $dataval = $_POST['dataval'];
        $path = Yii::$app->basePath . '/../frontend/web/images/sanjeevani-gallery/' .  $model->sanjeevani_id  . '/' . $model->id . '/gallery/' . $dataval;
        if (file_exists($path)) {
            unlink($path);
                return 1;
       }
        }
      return $this->render('create', [
                   'model' => $model,
       ]);
   }

}