<?php

namespace backend\controllers;

use Yii;
use common\models\Sanjeevani;
use common\models\SanjeevaniGallery;
use common\models\SanjeevaniSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * SanjeevaniController implements the CRUD actions for Sanjeevani model.
 */
class SanjeevaniController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sanjeevani models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SanjeevaniSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sanjeevani model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sanjeevani model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
  public function actionCreate() {
        $model = new Sanjeevani();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            $banner_image = UploadedFile::getInstance($model, 'banner_image');

            if(!empty($banner_image))
            {
            $model->banner_image = $banner_image->extension;
        }
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$banner_image,$model);
                Yii::$app->session->setFlash('success', "New Sanjeevani added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

      public function Upload($image,$banner_image,$model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/sanjeevani';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
          if (!empty($banner_image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/sanjeevani/banner-images';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'banner' . $model->id;
            $banner_image->saveAs($path . '/' . $name . '.' . $banner_image->extension);
        }


    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $banner_image1_ = $model->banner_image;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $banner_image = UploadedFile::getInstance($model, 'banner_image');

            $model->banner_image = !empty($banner_image) ? $banner_image->extension : $banner_image1_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$banner_image,$model);
            }
            Yii::$app->session->setFlash('success', "Sanjeevani updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */


 public function CheckPath($paths) {
        if (!is_dir($paths)) {
            mkdir($paths);
        }
        return $paths;
    }

    /**
     * Function for FileExists
     */
    public function fileExists($path, $name, $file, $sufix) {
        if (file_exists($path . '/' . $name)) {

            $name = basename($path . '/' . $file->baseName, '.' . $file->extension) . '_' . $sufix . '.' . $file->extension;
            return $this->fileExists($path, $name, $file, $sufix + 1);
        } else {
            return $name;
        }
    }







    
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $transaction            = Sanjeevani::getDb()->beginTransaction();  
        $path = Yii::$app->basePath . '/../frontend/web/images/sanjeevani/image' . $id . '.' . $model->image;
        $path1 = Yii::$app->basePath . '/../frontend/web/images/sanjeevani/banner-images/banner' . $id . '.' . $model->banner_image;
        $path2 = Yii::$app->basePath . '/../frontend/web/images/sanjeevani-gallery/' . $model->id;
        try {
            if ($model->delete()) {
                if (is_dir($path2)) {
                    $this->deleteDir($path2);
                }
                if (file_exists($path)) {
                    unlink($path);
                }
                if (file_exists($path1)) {
                    unlink($path1);
                }
                if (SanjeevaniGallery::deleteAll(['gallery_id' => $model->id])) {
                    Yii::$app->session->setFlash('success', "Sanjeevani removed successfully");
                }
            }
            $transaction->commit();
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \yii\base\UserException("An error occured, Please contact the technicial team. (Error code: 001)");
        } catch (\Throwable $e) {
            //var_dump($e->getMessage());exit;
            $transaction->rollBack();
            throw new \yii\base\UserException("An error occured, Please contact the technicial team. (Error code: 002)");
        }
    }
    

    /**
     * Finds the Sanjeevani model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sanjeevani the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sanjeevani::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}