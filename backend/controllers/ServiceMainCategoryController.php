<?php

namespace backend\controllers;

use Yii;
use common\models\ServiceMainCategory;
use common\models\ServiceMainCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * ServiceMainCategoryController implements the CRUD actions for ServiceMainCategory model.
 */
class ServiceMainCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServiceMainCategory models.
     * @return mixed
     */
    public function actionIndex($category)
    {
        $searchModel = new ServiceMainCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }

    /**
     * Displays a single ServiceMainCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServiceMainCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($category)
    {
        $model = new ServiceMainCategory();
        $model->category = $category;
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post())) {
           
       
             $banner_image = UploadedFile::getInstance($model, 'banner_image');
             if(!empty($banner_image))
             {
                 $model->banner_image = $banner_image->extension;
             }
             if ($model->validate() && $model->save()) {
                 $this->Upload($banner_image,$model);
                 Yii::$app->session->setFlash('success', "New Services added successfully");
                 return $this->redirect(['index','category'=>$category]);
             }
         } else {
            return $this->render('create', [
                'model' => $model,
                'category' => $category
            ]);
        }
    }


    public function Upload($banner_image,$model) {
      

        

       if (!empty($banner_image)) {
        $path = Yii::$app->basePath . '/../frontend/web/images/services/service-main-category/banner-images';
          FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

          $name = 'banner' . $model->id;
          $banner_image->saveAs($path . '/' . $name . '.' . $banner_image->extension);
      }
       
      
  }

    /**
     * Updates an existing ServiceMainCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $banner_image_ =$model->banner_image;

        if ($model->load(Yii::$app->request->post())) {
           

            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            $model->banner_image = !empty($banner_image) ? $banner_image->extension : $banner_image_;


            if ($model->validate() && $model->save()) {
                $this->Upload($banner_image,$model);
            }
            Yii::$app->session->setFlash('success', "Services updated successfully");
            return $this->redirect(['update', 'id' => $model->id,'category'=>$model->category]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'category' => $model->category
            ]);
        }
    }

    /**
     * Deletes an existing ServiceMainCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', "Services deleted successfully");
        return $this->redirect(['index','category' =>$model->category]);
    }

    /**
     * Finds the ServiceMainCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServiceMainCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServiceMainCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
