<?php

namespace backend\controllers;

use Yii;
use common\models\Conference;
use common\models\ConferenceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * ConferenceController implements the CRUD actions for Conference model.
 */
class ConferenceController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Conference models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConferenceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conference model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Conference model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Conference();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
           
            $model->date = date('Y-m-d', strtotime($model->date));
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = $image1->extension;
            $image2 = UploadedFile::getInstance($model, 'image2');
             if(!empty($banner_image))
            {
            $model->image2 = $image2->extension;
        }
           

            if(!empty($banner_image))
            {
            $model->banner_image = $banner_image->extension;
        }
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$banner_image,$image1,$image2,$model);
                Yii::$app->session->setFlash('success', "New Conference added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

      public function Upload($image,$banner_image,$image1,$image2,$model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/conference';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
          if (!empty($banner_image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/conference/banner-images';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'banner' . $model->id;
            $banner_image->saveAs($path . '/' . $name . '.' . $banner_image->extension);
        }


       
    if (!empty($image1)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/conference';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'data' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }
       
    if (!empty($image2)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/conference';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'register' . $model->id;
            $image2->saveAs($path . '/' . $name . '.' . $image2->extension);
        }




    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $banner_image1_ = $model->banner_image;
        $image1_ = $model->image1;
        $image2_ = $model->image2;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
             $model->date = date('Y-m-d', strtotime($model->date));
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = !empty($image1) ? $image1->extension : $image1_;
            $image2 = UploadedFile::getInstance($model, 'image2');
            $model->image2 = !empty($image2) ? $image2->extension : $image2_;

            $model->banner_image = !empty($banner_image) ? $banner_image->extension : $banner_image1_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$banner_image,$image1,$image2,$model);
            }
            Yii::$app->session->setFlash('success', "Conference updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */


    /**
     * Deletes an existing Conference model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
  public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/conference/image' . $id . '.' . $model->image;
        $path1 = Yii::$app->basePath . '/../frontend/web/images/conference/banner-images/banner' . $id . '.' . $model->banner_image;
         $path2 = Yii::$app->basePath . '/../frontend/web/images/conference/data' . $id . '.' . $model->image1;
                $path3 = Yii::$app->basePath . '/../frontend/web/images/conference/register' . $id . '.' . $model->image2;


        if ($this->findModel($id)->delete()) {
           
            if (file_exists($path)) {
                unlink($path);
            }
            if (file_exists($path1)) {
                unlink($path1);
            }
               if (file_exists($path2)) {
                unlink($path2);
            }
               if (file_exists($path3)) {
                unlink($path3);
            }

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conference model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Conference the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Conference::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
