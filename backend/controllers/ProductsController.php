<?php

namespace backend\controllers;

use Yii;
use common\models\Products;
use common\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
 public function actionCreate() {
        $model = new Products();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = $image1->extension;
            $image2 = UploadedFile::getInstance($model, 'image2');
            $model->image2 = $image2->extension;
            $image3 = UploadedFile::getInstance($model, 'image3');
            $model->image3 = $image3->extension;
            $brochure = UploadedFile::getInstance($model, 'brochure');
            $model->brochure = $brochure->extension;


            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$image2,$image3,$brochure,$model);
                Yii::$app->session->setFlash('success', "New Products added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

      public function Upload($image,$image1,$image2,$image3,$brochure,$model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/products/product';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'pdt' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }

         if (!empty($image1)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/products/product';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'dt1-' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }

         if (!empty($image2)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/products/product';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'dt2-' . $model->id;
            $image2->saveAs($path . '/' . $name . '.' . $image2->extension);
        }
         if (!empty($image3)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/products/product';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'dt3-' . $model->id;
            $image3->saveAs($path . '/' . $name . '.' . $image3->extension);
        }
       if (!empty($brochure)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/products/brochure';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'brochure' . $model->id;
            $brochure->saveAs($path . '/' . $name . '.' . $brochure->extension);
        }

    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        $image2_ = $model->image2;
        $image3_ = $model->image2;
        $brochure_ = $model->brochure;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = !empty($image1) ? $image1->extension : $image1_;
            $image2 = UploadedFile::getInstance($model, 'image2');
            $model->image2 = !empty($image2) ? $image2->extension : $image2_;
            $image3 = UploadedFile::getInstance($model, 'image3');
            $model->image3 = !empty($image3) ? $image3->extension : $image3_;
            $brochure = UploadedFile::getInstance($model, 'brochure');
            $model->brochure = !empty($brochure) ? $brochure->extension : $brochure_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$image2,$image3,$brochure,$model);
            }
            Yii::$app->session->setFlash('success', "Products updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }





 public function actionSelectCategory() {




        if (Yii::$app->request->isAjax) {
            $category_id = $_POST['category_id'];


            $sub_category = \common\models\ProductsSubCategory::find()->where(['category_id' => $category_id])
            ->all();


          


            if (count($sub_category) > 0) {
                echo '<option value="">Select   Category</option>';
                foreach ($sub_category as $key => $val) {
                    echo "<option value='$val->id'>$val->title</option>";
                }
            } else {
                echo '<option value="">  Category not available</option>';
            }
        }
    }














    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
