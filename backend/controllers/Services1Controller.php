<?php

namespace backend\controllers;

use common\models\ServiceFaq;
use Yii;
use common\models\Services;
use common\models\ServicesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Services();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = $image1->extension;
            
            $icon = UploadedFile::getInstance($model, 'icon');
            if (!empty($icon)) {
                $model->icon = $icon->extension;
            }
            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            if(!empty($banner_image))
            {
                $model->banner_image = $banner_image->extension;
            }
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$banner_image,$icon,$model);
                Yii::$app->session->setFlash('success', "New Services added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

     
    public function Upload($image,$image1,$banner_image,$icon,$model) {
          $path = Yii::$app->basePath . '/../frontend/web/images/services';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

            if (!empty($image)) {
          
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
        if (!empty($image1)) {
          
            $name = 'data' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }

        if (!empty($icon)) {
          
            $name = 'icon' . $model->id;
            $icon->saveAs($path . '/' . $name . '.' . $icon->extension);
        }


         if (!empty($banner_image)) {
          $path = Yii::$app->basePath . '/../frontend/web/images/services/banner-images';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

            $name = 'banner' . $model->id;
            $banner_image->saveAs($path . '/' . $name . '.' . $banner_image->extension);
        }
         
        
    }


    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        $icon_ = $model->icon;
        $banner_image_ =$model->banner_image;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;

            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = !empty($image1) ? $image1->extension : $image1_;

            
            $icon = UploadedFile::getInstance($model, 'icon');
            $model->icon = !empty($icon) ? $icon->extension : $icon_;

            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            $model->banner_image = !empty($banner_image) ? $banner_image->extension : $banner_image_;


            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$banner_image,$icon,$model);
            }
            Yii::$app->session->setFlash('success', "Services updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model                  = $this->findModel($id);
        $transaction            = Services::getDb()->beginTransaction();  

        $path = Yii::$app->basePath . '/../frontend/web/images/services/image' . $id . '.' . $model->image;
        $path1 = Yii::$app->basePath . '/../frontend/web/images/services/data' . $id . '.' . $model->image;
        $path2 = Yii::$app->basePath . '/../frontend/web/images/services/icon' . $id . '.' . $model->icon;
        $path3 = Yii::$app->basePath . '/../frontend/web/images/services/banner-images/banner' . $id . '.' . $model->banner_image;


        try {
            if ($model->delete()) {
                if (file_exists($path)) {
                    unlink($path);
                }
                if (file_exists($path1)) {
                    unlink($path1);
                }    
                if (file_exists($path2)) {
                    unlink($path2);
                }    
                if (file_exists($path3)) {
                    unlink($path3);
                }  
                if (ServiceFaq::deleteAll(['service_id' => $id])) {
                    Yii::$app->session->setFlash('success', "Service and faq removed successfully");
                }
            }
            $transaction->commit();
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \yii\base\UserException("An error occured, Please contact the technicial team. (Error code: 001)");
        } catch (\Throwable $e) {
            //var_dump($e->getMessage());exit;
            $transaction->rollBack();
            throw new \yii\base\UserException("An error occured, Please contact the technicial team. (Error code: 002)");
        }
    }

    
    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}