<?php

namespace backend\controllers;

use Yii;
use common\models\CommonContent;
use common\models\CommonContentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * CommonContentController implements the CRUD actions for CommonContent model.
 */
class CommonContentController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommonContent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CommonContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommonContent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CommonContent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommonContent();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommonContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
       public function Upload($image,$image1,$image2,$brochre,$model) {
         $path = Yii::$app->basePath . '/../frontend/web/images/common-content';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        if (!empty($image)) {
           
            $name = 'medical' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
         if (!empty($image1)) {
           
            $name = 'nation' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }
        if (!empty($image2)) {
           
            $name = 'visitors' . $model->id;
            $image2->saveAs($path . '/' . $name . '.' . $image2->extension);
        }
        if (!empty($brochre)) {
           
            $name = 'brochre' . $model->id;
            $brochre->saveAs($path . '/' . $name . '.' . $brochre->extension);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        $image2_ = $model->image2;
        $brochre_ = $model->brochre;

        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = !empty($image1) ? $image1->extension : $image1_;
            $image2 = UploadedFile::getInstance($model, 'image2');
            $model->image2 = !empty($image2) ? $image2->extension : $image2_;
            $brochre = UploadedFile::getInstance($model, 'brochre');
            $model->brochre = !empty($brochre) ? $brochre->extension : $brochre_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$image2,$brochre,$model);
            }
            Yii::$app->session->setFlash('success', "Content updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }


    /**
     * Deletes an existing CommonContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CommonContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CommonContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CommonContent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
