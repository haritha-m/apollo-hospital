<?php

namespace backend\controllers;

use Yii;
use common\helper\Helper;
use common\models\Brochure;
use common\models\BrochureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * BrochureController implements the CRUD actions for Brochure model.
 */
class BrochureController extends Controller
{
    
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brochure models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrochureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brochure model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brochure model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Brochure();
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $pdf = UploadedFile::getInstance($model, 'pdf');
            if(!empty($pdf)){
    
            $model->pdf = Helper::slugify($model->pdf_title).'-'.yii::$app->EncryptDecrypt->encrypt('encrypt',date('y-m-d H:i:s')).'.'.$pdf->extension;
    
            if ($model->validate() && $model->save()) {
                $this->Upload($pdf,    $model);
                Yii::$app->session->setFlash('success', "Created successfully");
                return $this->redirect(['index',]);
        }
        }
    }
        return $this->render('create',[
            'model'=>$model,
        ]);
    }

    /**
     * Updates an existing Brochure model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_ = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $pdf = UploadedFile::getInstance($model, 'pdf');
            $model->pdf = !empty($pdf) ? Helper::slugify($model->pdf_title).'-'.Yii::$app->EncryptDecrypt->encrypt('encrypt',date('Y-m-d H:i:s')).'.'.$pdf->extension : $model_->pdf;
            if ($model->validate() && $model->save()) {
                $this->Upload($pdf,$model);
                Yii::$app->session->setFlash('success', "Updated successfully");
                return $this->redirect(['index',]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);  
    }

    /**
     * Deletes an existing Brochure model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brochure model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brochure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brochure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function Upload($pdf, $model) {
        
        $path = Yii::$app->basePath . '/../frontend/web/images/brochure/';
        FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        if (!empty($pdf)) {
            // $id     = Yii::$app->EncryptDecrypt->encrypt('encrypt', $model->id);
            $name   =  $model->pdf;
            $pdf->saveAs($path . '/' . $name);
        }
    }
}
