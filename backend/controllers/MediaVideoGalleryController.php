<?php

namespace backend\controllers;

use Yii;
use common\models\MediaVideoGallery;
use common\models\MediaVideoGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * MediaVideoGalleryController implements the CRUD actions for MediaVideoGallery model.
 */
class MediaVideoGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MediaVideoGallery models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new MediaVideoGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['media_gallery_id' => $id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Creates a new MediaVideoGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new MediaVideoGallery();
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
         $model->media_gallery_id = $id;
             if ($model->validate() && $model->save()) {
                 Yii::$app->session->setFlash('success', "New Media Video Gallery added successfully");
                 return $this->redirect(['index','id' => $id]);
             }
         }return $this->render('create', [
                     'model' => $model,
                     'id' => $id,
         ]);
    }

    /**
     * Updates an existing MediaVideoGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $video_ = $model->video;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
           
            if ($model->validate() && $model->save()) {
               
            }
            Yii::$app->session->setFlash('success', "Media Video Gallery updated successfully");
            return $this->redirect(['index','id' => $model->media_gallery_id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MediaVideoGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/mediasection-gallery/' . $model->media_gallery_id . '/videos/thumb' . $id . '.' . $model->image;
        $path1 = Yii::$app->basePath . '/../frontend/web/images/mediasection-gallery/' . $model->media_gallery_id . '/videos/video' . $id . '.' . $model->image;
       
       

        if ($this->findModel($id)->delete()) {
           
            if (file_exists($path)) {
                unlink($path);
            }
             if (file_exists($path1)) {
                unlink($path1);
            }

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index','id' => $model->media_gallery_id]);
    }

    /**
     * Finds the MediaVideoGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MediaVideoGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MediaVideoGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}