<?php

namespace backend\controllers;

use Yii;
use common\models\DoctorsVideoGallery;
use common\models\DoctorsVideoGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * DoctorsVideoGalleryController implements the CRUD actions for DoctorsVideoGallery model.
 */
class DoctorsVideoGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DoctorsVideoGallery models.
     * @return mixed
     */
    public function actionIndex($doctor_id)
    {
        $searchModel = new DoctorsVideoGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'doctor_id'=>$doctor_id
        ]);
    }

    /**
     * Displays a single DoctorsVideoGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DoctorsVideoGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($doctor_id) {
        $model = new DoctorsVideoGallery();
    //   $model->setScenario('create');
      $model->doctor_id = $doctor_id;
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            // $image = UploadedFile::getInstance($model, 'image');
            // $model->image = $image->extension;
            // $video = UploadedFile::getInstance($model, 'video');
            // $model->video = $video->extension;
            if ($model->validate() && $model->save()) {
                // $this->Upload($image,$model);
                Yii::$app->session->setFlash('success', "New Gallery added successfully");
                return $this->redirect(['index','doctor_id'=>$doctor_id]);
            }
        }return $this->render('create', [
                    'model' => $model,
                    'doctor_id'=>$doctor_id
        ]);
    }

    //   public function Upload($image,$model) {
    //     if (!empty($image)) {
    //         $path = Yii::$app->basePath . '/../frontend/web/images/doctors/gallery';
    //         FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
    //         $name = 'thumb' . $model->id;
    //         $image->saveAs($path . '/' . $name . '.' . $image->extension);
    //     }
    //      if (!empty($video)) {
    //         $path = Yii::$app->basePath . '/../frontend/web/images/doctors/gallery';
    //         FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
    //         $name = 'video' . $model->id;
    //         $video->saveAs($path . '/' . $name . '.' . $video->extension);
    //     }
    // }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        // $image_ = $model->image;
        // $video_ = $model->video;

        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            // $image = UploadedFile::getInstance($model, 'image');
            // $model->image = !empty($image) ? $image->extension : $image_;
            // $video = UploadedFile::getInstance($model, 'video');
            // $model->video = !empty($video) ? $video->extension : $video_;
            if ($model->validate() && $model->save()) {
                // $this->Upload($image,$model);
            }
            Yii::$app->session->setFlash('success', "Video updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
                    'doctor_id'=>$model->doctor_id
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/doctors/gallery/thumb' . $id . '.' . $model->image;
        $path1 = Yii::$app->basePath . '/../frontend/web/images/doctors/gallery/video' . $id . '.' . $model->image;
       

        if ($this->findModel($id)->delete()) {
           
            if (file_exists($path)) {
                unlink($path);
            }
           if (file_exists($path1)) {
                unlink($path1);
            }
           

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index','doctor_id'=>$model->doctor_id]);
    }
    
    


    /**
     * Finds the DoctorsVideoGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DoctorsVideoGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DoctorsVideoGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}