<?php

namespace backend\controllers;

use Yii;
use common\models\MediaGallery;
use common\models\MediaGallerySearch;
use common\models\MediaPhotoGallery;
use common\models\MediaVideoGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * MediaGalleryController implements the CRUD actions for MediaGallery model.
 */
class MediaGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MediaGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new MediaGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MediaGallery();
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
             $image = UploadedFile::getInstance($model, 'image');
             $model->image = $image->extension;
             if ($model->validate() && $model->save()) {
                 $this->Upload($image, $model);
                 Yii::$app->session->setFlash('success', "Media Gallery added successfully");
                 return $this->redirect(['index']);
             }
         }return $this->render('create', [
                     'model' => $model,
         ]);
    }
    public function Upload($image, $model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/mediasection';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
    }
    /**
     * Updates an existing MediaGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image_ = $model->image;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
            }
            Yii::$app->session->setFlash('success', "Media Gallery updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MediaGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model                  = $this->findModel($id);
        $transaction            = MediaGallery::getDb()->beginTransaction();  
        $path                   = Yii::$app->basePath . '/../frontend/web/images/mediasection/' . $model->id;
        $path1                  = Yii::$app->basePath . '/../frontend/web/images/mediasection-gallery/image' . $id . '.' . $model->image;
        
        try {
            if ($model->delete()) {
                if (is_dir($path)) {
                    $this->deleteDir($path);
                }
                if (file_exists($path1)) {
                    unlink($path1);
                }
                if (MediaPhotoGallery::deleteAll(['media_gallery_id' => $model->id]) && MediaVideoGallery::deleteAll(['media_gallery_id' => $model->id])) {
                    Yii::$app->session->setFlash('success', "Media Gallery removed successfully");
                }
            }
            $transaction->commit();
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new \yii\base\UserException("An error occured, Please contact the technicial team. (Error code: 001)");
        } catch (\Throwable $e) {
            //var_dump($e->getMessage());exit;
            $transaction->rollBack();
            throw new \yii\base\UserException("An error occured, Please contact the technicial team. (Error code: 002)");
        }
    }

    /**
     * Finds the MediaGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MediaGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MediaGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}