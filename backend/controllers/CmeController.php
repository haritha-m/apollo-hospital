<?php

namespace backend\controllers;

use Yii;
use common\models\Cme;
use common\models\CmeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * CmeController implements the CRUD actions for Cme model.
 */
class CmeController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CmeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Cme();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
        
            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            if(!empty($banner_image))
            {
            $model->banner_image = $banner_image->extension;
        }
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$banner_image,$model);
                Yii::$app->session->setFlash('success', "New CME added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

     
    public function Upload($image,$banner_image,$model) {
          $path = Yii::$app->basePath . '/../frontend/web/images/cme';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

            if (!empty($image)) {
          
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
        
          if (!empty($banner_image)) {
             $path = Yii::$app->basePath . '/../frontend/web/images/cme/banner-images';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

          
            $name = 'banner' . $model->id;
            $banner_image->saveAs($path . '/' . $name . '.' . $banner_image->extension);
        }
        
    }


    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $banner_image_ = $model->banner_image;


        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;

       


            $banner_image = UploadedFile::getInstance($model, 'banner_image');
            $model->banner_image = !empty($banner_image) ? $banner_image->extension : $banner_image_;

            if ($model->validate() && $model->save()) {
                $this->Upload($image,$banner_image,$model);
            }
            Yii::$app->session->setFlash('success', "CME updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/cme/image' . $id . '.' . $model->image;
        $path2 = Yii::$app->basePath . '/../frontend/web/images/cmee/banner-images/banner' . $id . '.' . $model->banner_image;


       

        if ($this->findModel($id)->delete()) {
           
            // if (file_exists($path)) {
            //     unlink($path);
            // }
            //  if (file_exists($path1)) {
            //     unlink($path1);
            // }
            //   if (file_exists($path2)) {
            //     unlink($path2);
            // }
           

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index']);
    }
    

    /**
     * Finds the Cme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
