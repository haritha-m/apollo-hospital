<?php

namespace backend\controllers;

use Yii;
use common\models\VisionMission;
use common\models\VisionMissionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * VisionMissionController implements the CRUD actions for VisionMission model.
 */
class VisionMissionController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VisionMission models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisionMissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VisionMission model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VisionMission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VisionMission();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VisionMission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function Upload($image,$image1,$image2,$image3,$model) {
         $path = Yii::$app->basePath . '/../frontend/web/images/vision';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        if (!empty($image)) {
            $name = 'first' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }

         if (!empty($image1)) {
            $name = 'second' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }

         if (!empty($image2)) {
            $name = 'third' . $model->id;
            $image2->saveAs($path . '/' . $name . '.' . $image2->extension);
        }
         if (!empty($image3)) {
            $name = 'common' . $model->id;
            $image3->saveAs($path . '/' . $name . '.' . $image3->extension);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        $image2_ = $model->image2;
        $image3_ = $model->image3;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = !empty($image1) ? $image1->extension : $image1_;
            $image2 = UploadedFile::getInstance($model, 'image2');
            $model->image2 = !empty($image2) ? $image2->extension : $image2_;
            $image3 = UploadedFile::getInstance($model, 'image3');
            $model->image3 = !empty($image3) ? $image3->extension : $image3_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$image2,$image3,$model);
            }
            Yii::$app->session->setFlash('success', "Content updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VisionMission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VisionMission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VisionMission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VisionMission::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
