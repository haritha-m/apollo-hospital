<?php

namespace backend\controllers;

use Yii;
use common\models\QualitySafetyFiles;
use common\models\QualitySafetyFilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * QualitySafetyFilesController implements the CRUD actions for QualitySafetyFiles model.
 */
class QualitySafetyFilesController extends Controller
{

    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QualitySafetyFiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QualitySafetyFilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single QualitySafetyFiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QualitySafetyFiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QualitySafetyFiles();

        $model->setScenario('create');
    
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
       
             $files = UploadedFile::getInstance($model, 'files');
             $model->files = $files->extension;
             if ($model->validate() && $model->save()) {
                 $this->Upload($files, $model);
                 Yii::$app->session->setFlash('success', "File added successfully");
                 return $this->redirect(['index']);
             }
         } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function Upload($files, $model) {
        if (!empty($files)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/quality-safety';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'files' . $model->id;
            $files->saveAs($path . '/' . $name . '.' . $files->extension);
        }
    }

    /**
     * Updates an existing QualitySafetyFiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $files_ = $model->files;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $files = UploadedFile::getInstance($model, 'files');
            $model->files = !empty($files) ? $files->extension : $files_;
            if ($model->validate() && $model->save()) {
                $this->Upload($files, $model);
            }
            Yii::$app->session->setFlash('success', "File updated successfully");
            return $this->redirect(['update','id'=>$model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing QualitySafetyFiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', "File deleted successfully");
        return $this->redirect(['index']);
    }

    /**
     * Finds the QualitySafetyFiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QualitySafetyFiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QualitySafetyFiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
