<?php

namespace backend\controllers;

use Yii;
use common\models\CmeGallery;
use common\models\CmeGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * CmeGalleryController implements the CRUD actions for CmeGallery model.
 */
class CmeGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmeGallery models.
     * @return mixed
     */
    public function actionIndex($cme_id)
    {
        $searchModel = new CmeGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cme_id'=>$cme_id
        ]);
    }

    /**
     * Displays a single CmeGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmeGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate($cme_id) {
        $model = new CmeGallery();
        $model->cme_id =$cme_id;
       if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if(!empty($image))
            {
            $model->image = $image->extension;
        }
        
        
            $image1 = UploadedFile::getInstance($model, 'image1');
            if(!empty($image1))
            {
            $model->image1 = $image1->extension;
        }
        
        
        
        
            $video = UploadedFile::getInstance($model, 'video');
            if(!empty($vieo))
            {
            $model->video = $video->extension;
        }
        
        
        
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$video,$model);
                Yii::$app->session->setFlash('success', "New Gallery added successfully");
                return $this->redirect(['index','cme_id'=>$cme_id]);
            }
        }return $this->render('create', [
                    'model' => $model,
                    'cme_id'=>$cme_id
        ]);
    }

      public function Upload($image,$image1,$video,$model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/cme/gallery';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'image' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
         if (!empty($video)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/cme/gallery';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'video' . $model->id;
            $video->saveAs($path . '/' . $name . '.' . $video->extension);
        }
        
         if (!empty($image1)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/cme/gallery';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'thumb' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        $video_ = $model->video;


        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
             if(!empty($image))
            {
            $model->image = !empty($image) ? $image->extension : $image_;
        }
         $image1 = UploadedFile::getInstance($model, 'image1');
             if(!empty($image1))
            {
            $model->image1 = !empty($image1) ? $image1->extension : $image_;
        }


           $video = UploadedFile::getInstance($model, 'video');
             if(!empty($video))
            {
            $model->video = !empty($video) ? $video->extension : $video_;
        }
        
        
        
        
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$video,$model);
            }
            Yii::$app->session->setFlash('success', "Gallery updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
                    'cme_id'=>$model->cme_id
        ]);
    }


    /**
     * Deletes an existing CmeGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', "Gallery Delete successfully");
        return $this->redirect(['index', 'cme_id'=>$model->cme_id ]);
    }

    /**
     * Finds the CmeGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmeGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmeGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
