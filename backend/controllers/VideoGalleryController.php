<?php

namespace backend\controllers;

use Yii;
use common\models\VideoGallery;
use common\models\VideoGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * VideoGalleryController implements the CRUD actions for VideoGallery model.
 */
class VideoGalleryController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VideoGallery models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new VideoGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['gallery_id' => $id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
        ]);
    }

    /**
     * Creates a new VideoGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id){
        $model = new VideoGallery();
    //   $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
        $model->gallery_id = $id;
    //         $image = UploadedFile::getInstance($model, 'image');
    //         $model->image = $image->extension;
            if ($model->validate() && $model->save()) {
    //             $this->Upload($image,$model);
                Yii::$app->session->setFlash('success', "New Video Gallery added successfully");
                return $this->redirect(['index','id' => $id]);
            }
        }return $this->render('create', [
                    'model' => $model,
                    'id' => $id,
        ]);
    }

    //   public function Upload($image,$model) {
    //      $path = Yii::$app->basePath . '/../frontend/web/images/media-gallery/' . $model->gallery_id . '/videos';
    //         FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
    //     if (!empty($image)) {
           
    //         $name = 'thumb' . $model->id;
    //         $image->saveAs($path . '/' . $name . '.' . $image->extension);
    //     }
    // }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        // $image_ = $model->image;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            // $image = UploadedFile::getInstance($model, 'image');
            // $model->image = !empty($image) ? $image->extension : $image_;
            if ($model->validate() && $model->save()) {
                // $this->Upload($image,$model);
            }
            Yii::$app->session->setFlash('success', "Video Gallery updated successfully");
            return $this->redirect(['index','id' => $model->gallery_id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/media-gallery/' . $model->gallery_id . '/videos/thumb' . $id . '.' . $model->image;
        $path1 = Yii::$app->basePath . '/../frontend/web/images/media-gallery/' . $model->gallery_id . '/videos/video' . $id . '.' . $model->image;
       
       

        if ($this->findModel($id)->delete()) {
           
            if (file_exists($path)) {
                unlink($path);
            }
             if (file_exists($path1)) {
                unlink($path1);
            }

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index','id' => $model->gallery_id]);
    }
    /**
     * Finds the VideoGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VideoGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VideoGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}