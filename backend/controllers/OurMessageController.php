<?php

namespace backend\controllers;

use Yii;
use common\models\OurMessage;
use common\models\OurMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * OurMessageController implements the CRUD actions for OurMessage model.
 */
class OurMessageController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OurMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OurMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OurMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OurMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OurMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OurMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
      public function Upload($image,$image1,$image2,$model) {
         $path = Yii::$app->basePath . '/../frontend/web/images/our-message';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        if (!empty($image)) {
            $name = 'cho' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }

         if (!empty($image1)) {
            $name = 'cht' . $model->id;
            $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }

         if (!empty($image2)) {
            $name = 'chc' . $model->id;
            $image2->saveAs($path . '/' . $name . '.' . $image2->extension);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        $image2_ = $model->image2;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            $image1 = UploadedFile::getInstance($model, 'image1');
            $model->image1 = !empty($image1) ? $image1->extension : $image1_;
            $image2 = UploadedFile::getInstance($model, 'image2');
            $model->image2 = !empty($image2) ? $image2->extension : $image2_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1,$image2,$model);
            }
            Yii::$app->session->setFlash('success', "Content updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }
    /**
     * Deletes an existing OurMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OurMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OurMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OurMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
