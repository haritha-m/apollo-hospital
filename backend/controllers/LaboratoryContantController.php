<?php

namespace backend\controllers;

use Yii;
use common\models\LaboratoryContant;
use common\models\LaboratoryContantSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LaboratoryContantController implements the CRUD actions for LaboratoryContant model.
 */
class LaboratoryContantController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LaboratoryContant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LaboratoryContantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LaboratoryContant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LaboratoryContant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LaboratoryContant();

        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
        $section1_image = UploadedFile::getInstance($model, 'section1_image');
    if(!empty($section1_image)){

        $model->section1_image = Helper::slugify($model->title).'-'.yii::$app->EncryptDecrypt->encrypt('encrypt',date('y-m-d H:i:s')).'.'.$section1_image->extension;
    }
    $section2_image= UploadedFile::getInstance($model, 'section2_image');
    if(!empty($section2_image)){

        $model->section2_image = Helper::slugify($model->list).'-'.yii::$app->EncryptDecrypt->encrypt('encrypt',date('y-m-d H:i:s')).'.'.$section2_image->extension;
    }
    if ($model->validate() && $model->save()) {
        $this->Upload($section1_image,$section2_image, $model);
        Yii::$app->session->setFlash('success', "Section edited successfully");
        return $this->redirect(['index']);
    }
    
    }
    return $this->render('create',[
        'model'=>$model,
    ]);
    }

    /**
     * Updates an existing LaboratoryContant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_ = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $section1_image = UploadedFile::getInstance($model, 'section1_image');
            $model->section1_image = !empty($section1_image) ? Helper::slugify($model->section1_title).'-'.Yii::$app->EncryptDecrypt->encrypt('encrypt',date('Y-m-d H:i:s')).'.'.$section1_image->extension : $model_->section1_image;
            $section2_image = UploadedFile::getInstance($model, 'section2_image');
            $model->section2_image = !empty($section2_image) ? Helper::slugify($model->section1_title).'-'.Yii::$app->EncryptDecrypt->encrypt('encrypt',date('Y-m-d H:i:s')).'.'.$section2_image->extension : $model_->section2_image;
            if ($model->validate() && $model->save()) {
                $this->Upload($section1_image,$section2_image, $model);
                Yii::$app->session->setFlash('success', "Client updated successfully");
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]); 
    }

    /**
     * Deletes an existing LaboratoryContant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LaboratoryContant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LaboratoryContant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LaboratoryContant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function Upload($section1_image,$section2_image, $model) {
        
        $path = Yii::$app->basePath . '/../frontend/web/images/appolo';
        FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
        if (!empty($section1_image)) {
            // $id     = Yii::$app->EncryptDecrypt->encrypt('encrypt', $model->id);
            $name   =  $model->section1_image;
            $section1_image->saveAs($path . '/' . $name);
        }

        if (!empty($section2_image)) {
            // $id     = Yii::$app->EncryptDecrypt->encrypt('encrypt', $model->id);
            $name   =  $model->section2_image;
            $section2_image->saveAs($path . '/' . $name);
        }
    }
}
