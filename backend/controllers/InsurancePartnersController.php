<?php

namespace backend\controllers;

use Yii;
use common\models\InsurancePartners;
use common\models\InsurancePartnersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * InsurancePartnersController implements the CRUD actions for InsurancePartners model.
 */
class InsurancePartnersController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InsurancePartners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InsurancePartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InsurancePartners model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InsurancePartners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new InsurancePartners();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
                Yii::$app->session->setFlash('success', "New Partner added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

      public function Upload($image, $model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/insurance/partners';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'partner' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
    }

    /**
     * Updates an existing InsurancePartners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
            }
            Yii::$app->session->setFlash('success', "Partner updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing InsurancePartners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/insurance/partners/partner' . $id . '.' . $model->image;
       

        if ($this->findModel($id)->delete()) {
           
            if (file_exists($path)) {
                unlink($path);
            }
           

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index']);
    }
    
    /**
     * Finds the InsurancePartners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InsurancePartners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InsurancePartners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
