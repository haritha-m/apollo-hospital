<?php

namespace backend\controllers;

use Yii;
use common\models\BannerImages;
use common\models\BannerImagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
/**
 * BannerimagesController implements the CRUD actions for BannerImages model.
 */
class BannerimagesController extends Controller
{
    /**
     * @inheritdoc
     */
     public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BannerImages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BannerImages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BannerImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   public function actionCreate() {
        $model = new BannerImages();
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image->extension;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
                Yii::$app->session->setFlash('success', "New Banner added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

      public function Upload($image, $model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/banner-images';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = 'banner-images' . $model->id;
            $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
    }

    /**
     * Updates an existing BannerImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = !empty($image) ? $image->extension : $image_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image, $model);
            }
            Yii::$app->session->setFlash('success', "Banner Images updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }


    /**
     * Deletes an existing BannerImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   public function actionDelete($id) {
        $model = $this->findModel($id);
         $path = Yii::$app->basePath . '/../frontend/web/images/banner-images/banner-images' . $id . '.' . $model->image;
         
        if ($this->findModel($id)->delete()) {
            if (file_exists($path)) {
                unlink($path);
            }
            
            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the BannerImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannerImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BannerImages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
