<?php

namespace backend\controllers;

use Yii;
use common\models\Sliders;
use common\models\SlidersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * SlidersController implements the CRUD actions for Sliders model.
 */
class SlidersController extends Controller
{
    /**
     * @inheritdoc
     */
      public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/index']);
            return false;
        }
        return true;
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sliders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SlidersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sliders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sliders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
  

 public function actionCreate() {
        $model = new Sliders();
      $model->setScenario('create');
       if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
             if (!empty($image)) {
                $model->image = 'desktop-slider-' . Yii::$app->EncryptDecrypt->encrypt('encrypt', date('Y-m-d H:i:s')) . '.' . $image->extension;
            }
            // $model->image = $image->extension;
            $image1 = UploadedFile::getInstance($model, 'image1');
             if (!empty($image1)) {
                $model->$image1 = 'mobile-slider-' . Yii::$app->EncryptDecrypt->encrypt('encrypt', date('Y-m-d H:i:s')) . '.' . $image1->extension;
            }
            // $model->image1 = $image1->extension;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1, $model);
                Yii::$app->session->setFlash('success', "New slider added successfully");
                return $this->redirect(['index']);
            }
        }return $this->render('create', [
                    'model' => $model,
        ]);
    }

      public function Upload($image,$image1, $model) {
        if (!empty($image)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/sliders';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = $model->image;
             $image->saveAs($path . '/' . $name);
            //   $name = 'slider' . $model->id;
            // $image->saveAs($path . '/' . $name . '.' . $image->extension);
        }
        if (!empty($image1)) {
            $path = Yii::$app->basePath . '/../frontend/web/images/sliders';
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
            $name = $model->image1;
             $image1->saveAs($path . '/' . $name);
            // $name = 'slider1' . $model->id;
            // $image1->saveAs($path . '/' . $name . '.' . $image1->extension);
        }
    }

    /**
     * Updates an existing Sliders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $image_ = $model->image;
        $image1_ = $model->image1;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->SetValues->Attributes($model)) {
            $image = UploadedFile::getInstance($model, 'image');
            // $model->image = !empty($image) ? $image->extension : $image_;
            if (!empty($image)) {
                $model->image = 'desktop-slider-' . Yii::$app->EncryptDecrypt->encrypt('encrypt', date('Y-m-d H:i:s')) . '.' . $image->extension;
            } else {
                $model->image = $image_;
            }
            $image1 = UploadedFile::getInstance($model, 'image1');
            if (!empty($image1)) {
                $model->image1 = 'mobile-slider-' . Yii::$app->EncryptDecrypt->encrypt('encrypt', date('Y-m-d H:i:s')) . '.' . $image1->extension;
            } else {
                $model->image1 = $image1_;
            }
            // $model->image1 = !empty($image1) ? $image1->extension : $image1_;
            if ($model->validate() && $model->save()) {
                $this->Upload($image,$image1, $model);
            }
            Yii::$app->session->setFlash('success', "Slider updated successfully");
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sliders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $path = Yii::$app->basePath . '/../frontend/web/images/sliders/slider' . $id . '.' . $model->image;
       

        if ($this->findModel($id)->delete()) {
           
            if (file_exists($path)) {
                unlink($path);
            }
           

            Yii::$app->session->setFlash('success', "Data removed successfully");
        }

        return $this->redirect(['index']);
    }
    
    

    /**
     * Finds the Sliders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sliders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sliders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
