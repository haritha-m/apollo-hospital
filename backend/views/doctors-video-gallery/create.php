<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DoctorsVideoGallery */

$this->title = 'Create Doctors Video Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Doctors Video Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Update Doctor</span>', ['/doctors/update', 'id' => $doctor_id]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Video Gallery</span>', ['/doctors-video-gallery/index','doctor_id'=>$doctor_id]);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section>   
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Doctors Video Gallery</span>', ['index','doctor_id'=>$doctor_id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="doctors-video-gallery-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

