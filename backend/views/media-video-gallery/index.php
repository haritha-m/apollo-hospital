<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MediaVideoGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media Video Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-video-gallery-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                    <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Details</span>', ['/media-gallery/update', 'id' => $id]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Media Photo Gallery</span>', ['/media-photo-gallery/index', 'id' => $id]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Media Video Gallery</span>', ['index', 'id' => $id]);
                                    ?>
                                </li>

                            </ul>
                        </div>
                    </section>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Media Video Gallery</span>', ['create', 'id' => $id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],


                        [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => function ($data) {
                                if (!empty($data->image))
                                    $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/mediasection-gallery/' . $data->media_gallery_id . '/videos/thumb' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                                return $img;
                            },
                        ],
                    
                        
                
                        [
                            'attribute' => 'status',
                            'value' => function($model, $key, $index, $column) {
                                return $model->status == 1 ? 'Enabled' : 'Disabled';
                            },
                            'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                        ],
        // 'status',
        // 'CB',
        // 'UB',
        // 'DOC',
        // 'DOU',

                    ['class' => 'yii\grid\ActionColumn','template'=>'{update}{delete}'],
                        ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>