<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MediaVideoGallery */

$this->title = 'Update Media Video Gallery: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Media Video Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Update Details</span>', ['/media-gallery/update', 'id' => $model->media_gallery_id]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Media Photo Gallery</span>', ['/media-photo-gallery/index', 'id' => $model->media_gallery_id]);
                                    ?>
                            </li>
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="active">Media Video Gallery</span>', ['index', 'id' => $model->media_gallery_id]);
                                    ?>
                            </li>

                        </ul>
                    </div>
                </section>
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Media Video Gallery</span>', ['index', 'id' => $model->media_gallery_id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="media-video-gallery-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>