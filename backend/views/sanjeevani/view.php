<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sanjeevani */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sanjeevanis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body"><div class="sanjeevani-view">
                        <p>
                            <?=  Html::a('<i class="fa fa-list"></i><span> Manage Sanjeevani</span>', ['index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                                    'id',
            'title',
            'canonical_name',
            'value',
            'value_text',
            'small_description',
            'image',
            'image_alt',
            'detail_title',
            'detail_description:ntext',
            'gallery',
            'sort_order',
            'status',
            'banner_title',
            'banner_description',
            'banner_image',
            'banner_alt',
            'meta_title:ntext',
            'meta_description:ntext',
            'meta_keyword:ntext',
            'other_meta_tags:ntext',
            'CB',
            'UB',
            'DOC',
            'DOU',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


