<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SanjeevaniSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sanjeevanis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sanjeevani-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                          <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Sanjeevani Common Content</span>', ['/sanjeevani-common-content/update', 'id' => 1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Sanjeevani</span>', ['/sanjeevani/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
              
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'image',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    if (!empty($data->image))
                                        $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/sanjeevani/image' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                                    return $img;
                                },
                            ],
                            'title',
                         
                            
                      
                            [
                                'attribute' => 'status',
                                'value' => function($model, $key, $index, $column) {
                                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                                },
                                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                            ],
            // 'small_description',
            // 'image',
            // 'image_alt',
            // 'detail_title',
            // 'detail_description:ntext',
            // 'gallery',
            // 'sort_order',
            // 'status',
            // 'banner_title',
            // 'banner_description',
            // 'banner_image',
            // 'banner_alt',
            // 'meta_title:ntext',
            // 'meta_description:ntext',
            // 'meta_keyword:ntext',
            // 'other_meta_tags:ntext',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{update}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


