<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sanjeevani */

$this->title = 'Update Sanjeevani: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sanjeevanis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Update Sanjeevani Common Content</span>', ['/sanjeevani-common-content/update', 'id' => 1]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Sanjeevani</span>', ['/sanjeevani/index']);
                                    ?>

                            </li>

                        </ul>
                    </div>
                </section>
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="active">Update details</span>', ['update', 'id' => $model->id]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Gallery Category</span>', ['/sanjeevani-gallery/index', 'id' => $model->id]);
                                    ?>

                            </li>

                        </ul>
                    </div>
                </section>
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Sanjeevani</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="sanjeevani-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>