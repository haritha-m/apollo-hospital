<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

   
/* @var $this yii\web\View */
/* @var $model common\models\Sliders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>

    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
</div>
</div>
     <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
   <?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 768*520') ?>
            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image1 = explode(',', $model->image1);
                if (!empty($image1)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/sliders/'. $model->image1 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
        </div>
        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
   <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 1920*870') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/sliders/' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
        </div>    
<div class="row">
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image1_alt')->textInput() ?>
</div>
      
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput() ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'link')->textInput() ?>
</div>
</div>
<div class="row">





        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sort_order')->textInput() ?>
</div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>






    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
