<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-category-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">


<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
 <?php
            if (!$model->isNewRecord) {
                if (isset($model->doctor_id)) {
                    $model->doctor_id = explode(',', $model->doctor_id);
                }
            }

            $doctors = ArrayHelper::map(common\models\Doctors::find()->where(['status'=> 1])->all(), 'id', 'name');

            echo $form->field($model, 'doctor_id')->dropdownlist(ArrayHelper::map(common\models\Doctors::find()->where(['status'=>1])->all(), 'id', 'name'), ['prompt' => 'Select', 'multiple' => true])
            ?>

</div>



</div>

  
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <style>
        .select2-selection__choice{
            background: #ffff !important;
            color: #000!important;
            padding: 2px !important;
        }
    </style>
    <script>


       

        $.fn.extend({
            select2_sortable: function () {
                var select = $(this);
                $(select).select2({
                    width: "100%",
                    createTag: function (params) {
                        return undefined;
                    }
                });
                var ul = $(select).next(".select2-container").first("ul.select2-selection__rendered");
                ul.sortable({
                    placeholder: "ui-state-highlight",
                    forcePlaceholderSize: true,
                    items: "li:not(.select2-search__field)",
                    tolerance: "pointer",
                    stop: function () {
                        $($(ul).find(".select2-selection__choice").get().reverse()).each(function () {
                            console.log(this);
                            var title = $(this).attr("title");
                            var option = $(select).find("option:contains(" + title + ")");
                            console.log(option);
                            $(select).prepend(option);
                        });
                    }
                });
            }
        });





        $("#promotors-doctor_id").each(function () {
            $(this).select2_sortable();
        })




    </script>




