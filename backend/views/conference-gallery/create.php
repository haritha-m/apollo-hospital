<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConferenceGallery */

$this->title = 'Create Conference Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Conference Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                                   <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Conference Common Content</span>', ['/conference-common-content/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Conference</span>', ['/conference/index']);
                                    ?>

                              
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Conference Gallery</span>', ['/conference-gallery/index','conference_id'=> $conference_id]);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Conference Gallery</span>', ['index','conference_id'=>$conference_id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="conference-gallery-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

