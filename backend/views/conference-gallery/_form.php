<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\ConferenceGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conference-gallery-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
<!--        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>-->
<!--    <?= $form->field($model, 'conference_id')->textInput() ?>-->
<!--</div>-->
   <!-- <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
        <?php
        $conference = ArrayHelper::map(\common\models\Conference::find()->where(['status' => 1])->all(), 'id', 'title');
        echo $form->field($model, 'conference_id')->widget(Select2::classname(), [
            'data'      => $conference,
            'language'  => 'en',
            'options'   => ['placeholder' => '-- Select Category --'],
        ]);
        ?>
</div> -->
    <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 460 * 730') ?> 
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/conference/gallery/' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
            <?php }
            ?>
        </div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'title')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sort_order')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>
<!--<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>-->
<!--    <?= $form->field($model, 'CB')->textInput() ?>-->
<!--</div>-->
<!--<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>-->
<!--    <?= $form->field($model, 'UB')->textInput() ?>-->
<!--</div>-->
<!--<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>-->
<!--    <?= $form->field($model, 'DOC')->textInput() ?>-->
<!--</div>-->
<!--<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>-->
<!--    <?= $form->field($model, 'DOU')->textInput() ?>-->
<!--</div>-->
    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
