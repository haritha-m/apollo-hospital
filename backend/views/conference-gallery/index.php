<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConferenceGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conference Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conference-gallery-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                                                      <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Conference Common Content</span>', ['/conference-common-content/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Conference</span>', ['/conference/index']);
                                    ?>

                              
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Conference Gallery</span>', ['/conference-gallery/index','conference_id'=> $conference_id]);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Conference Gallery</span>', ['create','conference_id'=>$conference_id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',

            [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => function ($data) {
                                if (!empty($data->image))
                                $id = Yii::$app->EncryptDecrypt->encrypt('encrypt', $data->id);
                                $img = '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/conference/gallery/'. $data->image . '?' . rand() . '" />';
                                   
                                return $img;
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function($model, $key, $index, $column) {
                                return $model->status == 0 ? 'Disabled' : 'Enabled';
                            },
                            'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                        ],
            
                                    ['class' => 'yii\grid\ActionColumn','template'=>'{update}  {delete}'],

                    
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


