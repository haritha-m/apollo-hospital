<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LaboratoryServices */

$this->title = 'Create Laboratory Services';
$this->params['breadcrumbs'][] = ['label' => 'Laboratory Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                
                     <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Laboratory Content</span>', ['/laboratory-services-content/update', 'id' => 1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Laboratory Services</span>', ['/laboratory-services/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Laboratory Services</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="laboratory-services-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

