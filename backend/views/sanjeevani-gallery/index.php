<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SanjeevaniGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sanjeevani Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sanjeevani-gallery-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                    <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Sanjeevani Common Content</span>', ['/sanjeevani-common-content/update', 'id' => 1]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Sanjeevani</span>', ['/sanjeevani/index']);
                                    ?>

                                </li>

                            </ul>
                        </div>
                    </section>
                    <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update details</span>', ['/sanjeevani/update', 'id' => $id]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Gallery Category</span>', ['index', 'id' => $id]);
                                    ?>

                                </li>

                            </ul>
                        </div>
                    </section>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Sanjeevani Gallery</span>', ['create', 'id' => $id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($data) {
                    if (!empty($data->image))
                        $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/sanjeevani-gallery/'.$data->sanjeevani_id . '/' . $data->id. '/image' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                    return $img;
                },
            ],
            'sort_order',
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $column) {
                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                },
                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
            ],
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
                        ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>