<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\SanjeevaniGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sanjeevani-gallery-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'canonical_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
            <?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 1500*822') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/sanjeevani-gallery/'. $model->sanjeevani_id  . '/' . $model->id .'/image' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model,'gallery[]')->fileInput(['multiple' => 'multiple', 'accept' => 'jpg', 'jpeg', 'png', 'bmp', 'tiff', 'webp']) ?>
        </div>

        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
            <?php
            if (!$model->isNewRecord) {
                $path = Yii::getAlias('@paths') . '/sanjeevani-gallery/' . $model->sanjeevani_id  . '/' . $model->id .'/gallery';
                if (count(glob("{$path}/*")) > 0) {
                    $k = 0;
                    foreach (glob("{$path}/*") as $file) {
                        $k++;
                        $arry = explode('/', $file);
                        $img_nmee = end($arry);

                        $img_nmees = explode('.', $img_nmee);
                        if ($img_nmees['1'] != '') {
                            ?>

            <div class="col-md-2 img-box" id="gallerybox-<?= $k; ?>">
                <div class="news-img">
                    <img class="img-responsive"
                        src="<?= Yii::$app->homeUrl . '../images/sanjeevani-gallery/' . $model->sanjeevani_id . '/' . $model->id . '/gallery/' . end($arry) ?>">
                    <?= Html::a('<i class="fa fa-remove"></i>', ['#'], ['class' => 'gal-img-remove', 'data-val' => end($arry), 'id' => $model->id, 'data-id' => $k]) ?>
                </div>
            </div>


            <?php } }  } }  ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'sort_order')->textInput(['type' => 'number', 'maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
$(".gal-img-remove").click(function(e) {
    e.preventDefault();
    var div_id = $(this).attr('data-id');
    var id = $(this).attr('id');
    var dataval = $(this).attr("data-val");
    //console.log(dataval);
    $.ajax({
        url: '<?= Yii::$app->homeUrl; ?>sanjeevani-gallery/remove-gallery',
        type: "POST",
        data: {
            id: id,
            dataval: dataval
        },
        success: function(data) {
            $("#gallerybox-" + div_id).remove();
        }
    });
});
</script>
<style>
.gal-img-remove {
    position: absolute;
    top: -22px;
    right: 0;
}

.img-responsive {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

.news-img {
    height: 100px;
    overflow: hidden;
    margin-bottom: 30px;
}
</style>