<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SanjeevaniGallery */

$this->title = 'Create Sanjeevani Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Sanjeevani Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Update Sanjeevani Common Content</span>', ['/sanjeevani-common-content/update', 'id' => 1]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Sanjeevani</span>', ['/sanjeevani/index']);
                                    ?>

                            </li>

                        </ul>
                    </div>
                </section>
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Update details</span>', ['/sanjeevani/update', 'id' => $id]);
                                    ?>
                            </li>
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="active">Gallery Category</span>', ['index', 'id' => $id]);
                                    ?>

                            </li>

                        </ul>
                    </div>
                </section>
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Sanjeevani Gallery</span>', ['index', 'id' => $id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="sanjeevani-gallery-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>