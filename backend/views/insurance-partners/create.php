<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InsurancePartners */

$this->title = 'Create Insurance Partners';
$this->params['breadcrumbs'][] = ['label' => 'Insurance Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                  <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Insurance</span>', ['/insurance/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Partners</span>', ['/insurance-partners/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Insurance Partners</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="insurance-partners-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

