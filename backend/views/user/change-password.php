<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <?= Html::a('<i class="fa fa-list"></i><span> Manage User</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="user-create">
                    <div class="user-form form-inline">

                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
                                <?= $form->field($model, 'new_password')->passwordInput() ?>
                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
                                <?= $form->field($model, 'confirm_password')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-md-12 col-sm-12 col-xs-12'>
                                <div class="form-group">
                                    <?= Html::submitButton('Submit', ['class' => 'btn btn-success', 'style' => 'float:right;']) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

