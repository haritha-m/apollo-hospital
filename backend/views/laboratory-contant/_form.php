<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\LaboratoryContant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laboratory-contant-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section1_title')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section1_discription')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section2_title')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_title')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'section1_image')->fileInput(['maxlength' => true])->hint('Diamension : 1920 * 895') ?>
    <?php
            if ($model->section1_image != '' && $model->id != "") {
                $image = explode(',', $model->section1_image);
                if (!empty($section1_image)) {
                    echo '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/appolo/' . $model->section1_image . '?' . rand() . '"/>';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'section2_image')->fileInput(['maxlength' => true])->hint('Diamension : 1920 * 895') ?>
    <?php
            if ($model->section2_image != '' && $model->id != "") {
                $image = explode(',', $model->section2_image);
                if (!empty($section2_image)) {
                    echo '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/appolo/' . $model->section2_image . '?' . rand() . '"/>';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section1_image_alt')->textInput(['maxlength' => true]) ?>
</div>


<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section2_image_alt')->textInput(['maxlength' => true]) ?>
</div>

<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_discription')->widget(CKEditor::className(),[ 'options' => ['rows' => 6],
        'preset' => 'custom'])->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_email')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_phone_number1')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_phone_number2')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_address')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-12 col-sm-12  col-xs-12 left_padd'>
    <?= $form->field($model, 'section3_inp')->widget(CKEditor::className(),[ 'options' => ['rows' => 6],
        'preset' => 'custom'])->textarea(['rows' => 6]) ?>
</div>
<!-- <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'CB')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'UB')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'DOC')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'DOU')->textInput() ?>
</div> -->
    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
