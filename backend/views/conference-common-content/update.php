<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ConferenceCommonContent */

$this->title = 'Update Conference Common Content: ';
$this->params['breadcrumbs'][] = ['label' => 'Conference Common Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
                <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Update Conference Common Content</span>', ['/conference-common-content/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Conference</span>', ['/conference/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
            
                <div class="conference-common-content-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
