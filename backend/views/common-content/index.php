<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CommonContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Common Contents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="common-content-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Common Content</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                    'id',
            'medical_care_quality_title',
            'medical_care_quality_sub_title',
            'image',
            'image_alt',
            // 'brochre'
            // 'department_title',
            // 'department_sub_title',
            // 'department_sub_description',
            // 'hospital_nation_title',
            // 'hospital_nation_sub_title',
            // 'image1',
            // 'image1_alt',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


