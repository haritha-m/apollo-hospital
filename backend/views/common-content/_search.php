<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CommonContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="common-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'medical_care_quality_title') ?>

    <?= $form->field($model, 'medical_care_quality_sub_title') ?>

    <?= $form->field($model, 'image') ?>

    <?= $form->field($model, 'image_alt') ?>

    <?php // echo $form->field($model, 'department_title') ?>

    <?php // echo $form->field($model, 'department_sub_title') ?>

    <?php // echo $form->field($model, 'department_sub_description') ?>

    <?php // echo $form->field($model, 'hospital_nation_title') ?>

    <?php // echo $form->field($model, 'hospital_nation_sub_title') ?>

    <?php // echo $form->field($model, 'image1') ?>

    <?php // echo $form->field($model, 'image1_alt') ?>

    <?php // echo $form->field($model, 'CB') ?>

    <?php // echo $form->field($model, 'UB') ?>

    <?php // echo $form->field($model, 'DOC') ?>

    <?php // echo $form->field($model, 'DOU') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
