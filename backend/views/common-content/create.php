<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CommonContent */

$this->title = 'Create Common Content';
$this->params['breadcrumbs'][] = ['label' => 'Common Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Common Content</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="common-content-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

