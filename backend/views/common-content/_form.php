<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CommonContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="common-content-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'medical_care_quality_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'medical_care_quality_sub_title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 906*604') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/common-content/medical' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'department_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'department_sub_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'department_sub_description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'specialities_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'specialities_sub_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'specialities_description')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'hospital_nation_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'hospital_nation_sub_title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 1920*794') ?>
            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image = explode(',', $model->image1);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/common-content/nation' . $model->id . '.' . $model->image1 . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image1_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'home_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'home_description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image2')->fileInput()->hint('Dimensions : 65*65') ?>
            <?php
            if ($model->image2 != '' && $model->id != "") {
                $image = explode(',', $model->image2);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;background: #6e6d6d;" src="' . Yii::$app->request->baseUrl . '/../images/common-content/visitors' . $model->id . '.' . $model->image2 . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'brochre')->fileInput() ?>
            <?php
            if ($model->brochre != '' && $model->id != "") {
                $brochre = explode(',', $model->brochre);
                if (!empty($brochre)) {
                    echo '<a href="' . Yii::$app->request->baseUrl . '/../images/common-content/brochre' . $model->id . '.' . $model->brochre . '?' . rand() . '" target="_blank">View PDF</a>';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image2_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'ip_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'ip_caption')->textInput(['maxlength' => true]) ?>
        </div>

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'package_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'package_description')->textarea(['rows' => 2]) ?>
        </div>











    </div>



    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>