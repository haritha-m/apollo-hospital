<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Brochure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brochure-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'pdf')->fileInput() ?> 
            <?php
            if ($model->pdf != '' && $model->id != "") {
                $pdf = explode(',', $model->pdf);
                if (!empty($pdf)) {
                    echo '<a href="' . Yii::$app->request->baseUrl . '/../images/brochure/' . $model->pdf . '?' . rand() . '" />view</a>';
                }
                ?>
                <br>
            <?php }
            ?>
        </div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'pdf_title')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sort_order')->textInput() ?>
</div>
<!-- <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'CB')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'UB')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'DOC')->textInput() ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'DOU')->textInput() ?>
</div> -->
    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
