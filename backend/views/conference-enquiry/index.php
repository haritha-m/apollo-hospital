<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConferenceEnquirySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conference Enquiries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conference-enquiry-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <!--<?=  Html::a('<i class="fa fa-list"></i><span> Create Conference Enquiry</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>-->
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                    // 'id',
            'name',
            'email:email',
            'mobile',
            // 'country',
            // 'department',
            // 'designation',
            // 'confernce',
            // 'date',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{view}{delete}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


