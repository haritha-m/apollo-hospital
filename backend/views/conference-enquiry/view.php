<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ConferenceEnquiry */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Conference Enquiries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body"><div class="conference-enquiry-view">
                        <p>
                            <?=  Html::a('<i class="fa fa-list"></i><span> Manage Conference Enquiry</span>', ['index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>
                            <!--<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>-->
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                                    // 'id',
            'name',
            'email:email',
            'mobile',
             // 'district_id',
            [
                        'attribute' => 'district_id',
                        'filter' => \yii\helpers\ArrayHelper::map(\common\models\Districts::find()->where(['status'=>1])->all(), 'id', 'name'),
                        'value' => function ($data) {
                            if ($data->district_id != '') {
                                return \common\models\Districts::findOne($data->district_id)->name;
                            } else {
                                return '';
                            }
                        }
                    ],
            // 'panchayat_id',
             [
                        'attribute' => 'panchayat_id',
                        'filter' => \yii\helpers\ArrayHelper::map(\common\models\Panchayat::find()->where(['status'=>1])->all(), 'id', 'name'),
                        'value' => function ($data) {
                            if ($data->panchayat_id != '') {
                                return \common\models\Panchayat::findOne($data->panchayat_id)->name;
                            } else {
                                return '';
                            }
                        }
                    ],
            'department',
            'designation',
            'confernce',
            'date',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


