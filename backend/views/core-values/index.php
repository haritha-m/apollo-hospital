<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CoreValuesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Values';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-values-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                        <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Core Values Common Content</span>', ['/core-values-content/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Core Values</span>', ['/core-values/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                  
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

            'title',
              [
                                'attribute' => 'status',
                                'value' => function($model, $key, $index, $column) {
                                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                                },
                                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                            ],
        
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{update}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


