<?php

use common\models\Services;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ServiceFaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Service Faqs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-faq-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                    <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update details</span>', ['/services/update', 'id' => $id]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Faq</span>', ['index', 'id' => $id]);
                                 ?>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?=  Html::a('<i class="fa fa-list"></i><span> Manage Services</span>', ['/services/index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>
                    <?= Html::a('Create Faq', ['create', 'id' => $id], ['class' => 'btn btn-primary']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'question',
                        'answer:ntext',
                        'sort_order',
                        [
                            'attribute' => 'status',
                            'value' => function($model, $key, $index, $column) {
                                return $model->status == 0 ? 'Disabled' : 'Enabled';
                            },
                            'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                        ],
                        // 'CB',
                        // 'UB',
                        // 'DOC',
                        // 'DOU',

                        ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
                        ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>