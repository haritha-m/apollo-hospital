<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cme */

$this->title = 'Update Cme: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cmes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
                
                
                
                <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Update CME</span>', ['/cme/update', 'id' => $model->id]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">CME Gallery</span>', ['/cme-gallery/index','cme_id'=>$model->id]);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Cme</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="cme-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
