<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\QualitySafety */

$this->title = 'Update Quality Safety: ' ;
$this->params['breadcrumbs'][] = ['label' => 'Quality Safeties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
            <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Update Quality Safety</span>', ['/quality-safety/update', 'id' => 1]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Files</span>', ['/quality-safety-files/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section>
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
               
                <div class="quality-safety-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
