<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\HomeAbout */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="home-about-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sub_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
  
    <?=
            $form->field($model, 'description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>



  <?= $form->field($model, 'video')->fileInput() ?>
            <?php
            if ($model->video != '' && $model->id != "") {
                $video = explode(',', $model->video);
                if (!empty($video)) {
                    echo '<a  href="' . Yii::$app->request->baseUrl . '/../images/about/homevideo'.$model->id.'.' . $model->video . '" / target="_blank" style="color:blue" >View  video </a>';
                }
                ?>
                <br>
                <br>
            <?php }
            ?> 







</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
     <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 1458*617') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/about/homethumb' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count1_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count1')->textInput() ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count2_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count2')->textInput() ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count3_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count3')->textInput() ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count4_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'count4')->textInput() ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
