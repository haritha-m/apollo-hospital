<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomeAboutSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="home-about-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'sub_title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'video') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'image_alt') ?>

    <?php // echo $form->field($model, 'count1_title') ?>

    <?php // echo $form->field($model, 'count1') ?>

    <?php // echo $form->field($model, 'count2_title') ?>

    <?php // echo $form->field($model, 'count2') ?>

    <?php // echo $form->field($model, 'count3_title') ?>

    <?php // echo $form->field($model, 'count3') ?>

    <?php // echo $form->field($model, 'count4_title') ?>

    <?php // echo $form->field($model, 'count4') ?>

    <?php // echo $form->field($model, 'CB') ?>

    <?php // echo $form->field($model, 'UB') ?>

    <?php // echo $form->field($model, 'DOC') ?>

    <?php // echo $form->field($model, 'DOU') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
