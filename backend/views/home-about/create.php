<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HomeAbout */

$this->title = 'Create Home About';
$this->params['breadcrumbs'][] = ['label' => 'Home Abouts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Home About</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="home-about-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

