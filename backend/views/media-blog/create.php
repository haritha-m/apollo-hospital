<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MediaBlog */

$this->title = 'Create Medical Blog';
$this->params['breadcrumbs'][] = ['label' => 'Medical Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Medical Blog</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="media-blog-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

