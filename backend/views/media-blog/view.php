<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MediaBlog */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Media Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body"><div class="media-blog-view">
                        <p>
                            <?=  Html::a('<i class="fa fa-list"></i><span> Manage Media Blog</span>', ['index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                                    'id',
            'title',
            'canonical_name',
            'small_description:ntext',
            'image',
            'image_alt',
            'date',
            'detail_description:ntext',
            'image1',
            'image1_alt',
            'banner_title',
            'banner_description:ntext',
            'banner_image',
            'banner_alt',
            'meta_title:ntext',
            'meta_description:ntext',
            'meta_keyword:ntext',
            'other_meta_tags:ntext',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


