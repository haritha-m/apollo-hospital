<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConferenceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conferences';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conference-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                      <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Conference Common Content</span>', ['/conference-common-content/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Conference</span>', ['/conference/index']);
                                    ?>

                                </li> 

                                
                            </ul>
                        </div>
                    </section> 
            
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Conference</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
  [
                                'attribute' => 'image',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    if (!empty($data->image))
                                        $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/conference/image' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                                    return $img;
                                },
                            ],
            'title',
             [
                                'attribute' => 'status',
                                'value' => function($model, $key, $index, $column) {
                                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                                },
                                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                            ],
            
            // 'image_alt',
            // 'date',
            // 'status',
            // 'detail_description:ntext',
            // 'image1',
            // 'image1_alt',
            // 'image2',
            // 'image2_alt',
            // 'banner_title',
            // 'banner_description',
            // 'banner_image',
            // 'banner_alt',
            // 'meta_title:ntext',
            // 'meta_description:ntext',
            // 'meta_keyword:ntext',
            // 'other_meta_tags:ntext',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{update}{delete}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


