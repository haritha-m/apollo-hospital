<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
        use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Conference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conference-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'canonical_name')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'small_description')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 439*218') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/conference/image' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>



        <?php
        if (!$model->isNewRecord) {
            $model->date = date('d-m-Y', strtotime($model->date));
        } else {
            $model->date = date('d-m-Y');
        }
        ?>
        <?=
        $form->field($model, 'date')->widget(DatePicker::classname(), [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd-m-yyyy'
            ]
        ]);
        ?>
    </div>






<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>
<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>

     <?=
            $form->field($model, 'detail_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
</div>
</div>
  <div class="row">
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>

    <?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 364*229') ?>
            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image = explode(',', $model->image1);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/conference/data' . $model->id . '.' . $model->image1 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image1_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
 <div class="row">
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    
    <?= $form->field($model, 'image2')->fileInput()->hint('Dimensions : 821*727') ?>
            <?php
            if ($model->image2 != '' && $model->id != "") {
                $image = explode(',', $model->image2);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/conference/register' . $model->id . '.' . $model->image2 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image2_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'banner_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'banner_description')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>



    <?= $form->field($model, 'banner_image')->fileInput()->hint('Dimensions : 1500*822') ?>
            <?php
            if ($model->banner_image != '' && $model->id != "") {
                $image = explode(',', $model->banner_image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/conference/banner-images/banner' . $model->id . '.' . $model->banner_image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>



</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'banner_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'other_meta_tags')->textarea(['rows' => 6]) ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
