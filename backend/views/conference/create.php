<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Conference */

$this->title = 'Create Conference';
$this->params['breadcrumbs'][] = ['label' => 'Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                   <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Conference Common Content</span>', ['/conference-common-content/update', 'id' =>1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Conference</span>', ['/conference/index']);
                                    ?>

                                </li> 
                                      <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Conference Gallery</span>', ['/conference-gallery/index']);
                                    ?>

                                </li>
                                
                            </ul>
                        </div>
                    </section> 
            
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Conference</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="conference-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

