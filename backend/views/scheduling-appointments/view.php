<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SchedulingAppointments */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Scheduling Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body">
                    <div class="scheduling-appointments-view">
                        <p>
                            <?=  Html::a('<i class="fa fa-list"></i><span> Manage Scheduling Appointments</span>', ['index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>

                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                                 
                            'name',
                            'email:email',
                            'mobile',
                            [
                                'attribute' => 'country_id',
                                'label' => 'Country',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    if(!empty($data->country_id)){
                                        $country = \common\models\Country::findOne($data->country_id);
                                        if(isset($country)){
                                            return $country->name;
                                        }else{
                                            return '(not found)';
                                        }
                                    }else{
                                        return '(not set)';
                                    }
                                },
                            ],
                            'date',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>