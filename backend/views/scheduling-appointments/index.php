<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SchedulingAppointmentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Scheduling Appointments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheduling-appointments-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'date',
                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'date',
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                ]
                            ]),
                            'value' => function ($data) {
                                return date("d-m-Y", strtotime($data->date));
                            },
                        ],
                        'name',
                        'email:email',
                        'mobile',
                        [
                            'attribute' => 'country_id',
                            'label' => 'Country',
                            'format' => 'raw',
                            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'name'),
                            'value' => function ($data) {
                                if(!empty($data->country_id)){
                                    $country = \common\models\Country::findOne($data->country_id);
                                     if(isset($country)){
                                         return $country->name;
                                     }else{
                                         return '(not set)';
                                     }
                                }else{
                                    return '(not set)';
                                }
                            },
                        ],

                        ['class' => 'yii\grid\ActionColumn','template'=>'{view}{delete}'],
                        ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>