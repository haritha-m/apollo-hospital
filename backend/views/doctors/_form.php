<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Doctors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doctors-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'canonical_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>



            <?php
            if (!$model->isNewRecord) {
                if (isset($model->projects_category_id)) {
                    $model->department = explode(',', $model->department);
                }
            }

            $category_options = ArrayHelper::map(common\models\Services::find()->all(), 'id', 'title');
            echo $form->field($model, 'department')->widget(Select2::classname(), [
                'data' => $category_options,
                'language' => 'en',
                'options' => ['placeholder' => '--Select--'],
                
            ]);
            ?>

        </div>
          <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'view_label')->dropDownList(['1' => 'Yes', '0' => 'No']) ?>
    </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>

            <?= $form->field($model, 'appointment_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'joined_date')->textInput(['type' => 'date', 'max' => date('Y-m-d')]) ?>
        </div>





    </div>
    <div class='col-md-6 col-sm-12 col-xs-12 left_padd'>
        <?= $form->field($model, 'small_description')->textarea(['rows' => 6]) ?>
    </div>
      <div class='col-md-6 col-sm-12 col-xs-12 left_padd'>
        <?= $form->field($model, 'education')->textarea(['rows' => 6]) ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 441*372') ?>
        <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/doctors/doctor' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
        <br>
        <br>
        <?php }
            ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
    </div>
    <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
        <?=
            $form->field($model, 'detail_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>



    </div>

    <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>



        <?=
            $form->field($model, 'fellowship_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
    </div>

    <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>

        <?=
            $form->field($model, 'expertise_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>


    </div>

    <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>

        <?=
            $form->field($model, 'languages_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
    </div>

    <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>

        <?=
            $form->field($model, 'promote_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
    </div>

    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'sort_order')->textInput() ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>
    </div>
    <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'other_meta_tags')->textarea(['rows' => 6]) ?>
    </div>

</div>
<div class="row">
    <div class='col-md-12 col-sm-12 col-xs-12'>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

</div>