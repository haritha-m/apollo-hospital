<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Doctors */

$this->title = 'Update Doctors: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Doctors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

<section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Update Doctor</span>', ['/doctors/update', 'id' => $model->id]);
                                    ?>
                                </li>
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Video Gallery</span>', ['/doctors-video-gallery/index','doctor_id'=>$model->id]);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section>   


                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Doctors</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="doctors-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
