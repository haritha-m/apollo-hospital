<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceMainCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-main-category-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'canonical_name')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'caption')->textarea(['rows' => 3]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'banner_image')->fileInput()->hint('Dimensions : 1920*870') ?>
            <?php
            if ($model->banner_image != '' && $model->id != "") {
                $image = explode(',', $model->banner_image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/services/service-main-category/banner-images/banner' . $model->id . '.' . $model->banner_image . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'inner_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-12 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'form_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'doctors_list_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'service_title')->textInput(['maxlength' => true]) ?>
</div>
</div>
<div class="row">
       <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?>

</div><div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>

</div><div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
        <?= $form->field($model, 'other_meta_tags')->textarea(['rows' => 6]) ?>

</div>
    </div>
    <div class="row">
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sort_order')->textInput() ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
