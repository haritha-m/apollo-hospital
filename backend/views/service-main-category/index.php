<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ServiceMainCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Service Main Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-main-category-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="">Update category</span>', ['/service-category/update', 'id' => $category]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="active">Subcategory</span>', ['/service-main-category/index', 'category' => $category]);
                                 ?>
                            </li>
                        </ul>
                    </div>
                </section>
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Service Main Category</span>', ['create', 'category' => $category], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                   
            'title',
            'caption',
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $column) {
                    return $model->status == 0 ? 'Disabled' : 'Enabled';
                },
                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
            ],
    

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


