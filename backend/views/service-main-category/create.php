<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ServiceMainCategory */

$this->title = 'Create Service Main Category';
$this->params['breadcrumbs'][] = ['label' => 'Service Main Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
            <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="">Update category</span>', ['/service-category/update', 'id' => $category]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="active">Subcategory</span>', ['/service-main-category/index', 'category' => $category]);
                                 ?>
                            </li>
                        </ul>
                    </div>
                </section>
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>

            </div>
            <div class="panel-body">
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Service Main Category</span>', ['index', 'category' => $category], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="service-main-category-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

