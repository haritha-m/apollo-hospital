<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
$controler = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

$title = 'ApolloAdlux Hospital';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= Yii::$app->homeUrl; ?>img/favicon.ico" rel="icon" width="50px;" height="50px;">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($title) ?></title>
    <script src="<?= Yii::$app->homeUrl; ?>js/jquery.min.js"></script>
    <script type="text/javascript">
    var homeUrl = '<?= Yii::$app->homeUrl; ?>';
    </script>
    <?php $this->head() ?>
</head>

<body class="skin-blue fixed sidebar-mini sidebar-mini-expand-featuree">
    <?php $this->beginBody() ?>

    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?= yii::$app->homeUrl; ?>" class="logo">
                <span class="logo-mini">
                    <img src="<?= Yii::$app->homeUrl; ?>img/logos.png" itemprop="image">
                </span>
                <span class="logo-lg">
                    <img src="<?= Yii::$app->homeUrl; ?>img/logos.png" itemprop="image">
                </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <?php
                                echo ''
                                . Html::beginForm(['/site/logout'], 'post', ['style' => '']) . '<a>'
                                . Html::submitButton(
                                        '<i class="fa fa-sign-out" aria-hidden="true"></i> Sign out', ['class' => 'signout-btn', 'style' => '']
                                ) . '</a>'
                                . Html::endForm()
                                . '';
                                ?>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="<?= $controler == 'site' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-dashboard"></i> Dashboard', ['/site/index'], ['class' => 'title']) ?>
                    </li>


                    <li class="treeview <?= $controler == 'user' ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa fa-cog"></i>
                            <span>Administration</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Users', ['/user/index'], ['class' => 'title']) ?>
                            </li>
                        </ul>
                    </li>




                    <li class="treeview <?= $controler == 'sliders' || $controler == 'latest-news' || $controler == 'addon-package-content'  ||  $controler == 'medical-care-quality' ||
                         $controler == 'home-about' || $controler == 
                        'common-content' ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa fa-home"></i>
                            <span>Home Management</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Sliders', ['/sliders/index'], ['class' => 'title']) ?>
                            </li>

                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>About Us', ['/home-about/update','id'=>1], ['class' => 'title']) ?>
                            </li>


                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>Medical Care Quality', ['/medical-care-quality/index'], ['class' => 'title']) ?>
                            </li>



                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>Common Content', ['/common-content/update','id'=>1], ['class' => 'title']) ?>
                            </li>
                            
                        </ul>

                        <li >
                                    <?= Html::a('<i class="fa fa-circle-o"></i> Brochure', ['/brochure/index'], ['class' => 'title']) ?>
                                </li>

                    <li class="treeview <?= $controler == 'our-message' || $controler == 'core-values' || $controler =='core-values-content'|| $controler == 'about' || $controler == 'promotors' ||  $controler == 
                        'vision-mission'  ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa fa-tasks"></i>
                            <span>About Us</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">



                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> About Apollo Adlux', ['/about/update','id'=>1], ['class' => 'title']) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>Vision And Mission', ['/vision-mission/update','id'=>1], ['class' => 'title']) ?>
                            </li>

                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Our Message', ['/our-message/update','id'=>1], ['class' => 'title']) ?>
                            </li>


                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Core Values', ['/core-values/index'], ['class' => 'title']) ?>
                            </li>


                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Promotors', ['/promotors/update','id'=>1], ['class' => 'title']) ?>
                            </li>






                        </ul>

                     </li>


                    <li class="<?= $controler == 'doctors'  || $controler =='doctors-video-gallery'? 'active' : '' ?>">
                        <?= Html::a('<i class=" fa fa-stethoscope"></i>Doctors', ['/doctors/index'], ['class' => 'title']) ?>
                    </li>
                    
                     <li class="<?= $controler == 'service-category'  || $controler =='service-main-category'? 'active' : '' ?>">
                        <?= Html::a('<i class=" fa fa-stethoscope"></i>Service Category', ['/service-category/index'], ['class' => 'title']) ?>
                    </li>


                    <li class="<?= $controler == 'services' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-user-md"></i>Services', ['/services/index'], ['class' => 'title']) ?>
                    </li>


                    <li
                        class="treeview <?= $controler == 'international-patients' || $controler == 'facilities' ||  $controler == 'visa-travel-arrangements' || $controler == 'scheduling-appointments'  ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa fa-flag"></i>
                            <span>International Patients</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">



                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> International Patients', ['/international-patients/index'], ['class' => 'title']) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>Facilities', ['/facilities/index'], ['class' => 'title']) ?>
                            </li>

                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Visa Travel & Arrangements', ['/visa-travel-arrangements/update','id'=>1], ['class' => 'title']) ?>
                            </li>


                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> 
Scheduling Appointment', ['/scheduling-appointments/index'], ['class' => 'title']) ?>
                            </li>








                        </ul>

                     








                    <li
                        class="treeview <?= $controler == 'packages' || $controler == 'offers' || $controler == 'package-enquiry' || $controler == 'offer-enquiry'  ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa fa-hourglass-start"></i>
                            <span>Packages & Offers</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">



                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>Packages', ['/packages/index'], ['class' => 'title']) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i>Offers', ['/offers/index'], ['class' => 'title']) ?>
                            </li>
                            <li>

                                <?= Html::a('<i class="fa fa-info-circle"></i> Package Enquiry', ['/package-enquiry/index'], ['class' => 'title']) ?>
                            </li>
                            <li>

                                <?= Html::a('<i class="fa fa-info-circle"></i> Offer Enquiry', ['/offer-enquiry/index'], ['class' => 'title']) ?>
                            </li>

                            <li>


                        </ul>




                    <li class="<?= $controler == 'insurance' || $controler == 'insurance-partners' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-book"></i>Insurance', ['/insurance/update','id'=>1], ['class' => 'title']) ?>
                    </li>











                    <li
                        class="<?= $controler == 'sanjeevani'   || $controler == 'sanjeevani-common-content' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-tasks"></i>Sanjeevani', ['/sanjeevani/index'], ['class' => 'title']) ?>
                    </li>
                    <li
                        class="<?= $controler == 'conference'   || $controler == 'conference-common-content'|| $controler == 'conference-gallery' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-video-camera"></i>Conference', ['/conference/index'], ['class' => 'title']) ?>
                    </li>

                      <li
                        class="<?= $controler == 'conference-enquiry'   || $controler == 'conference-common-content'|| $controler == 'conference-gallery' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-video-camera"></i>Conference Enquiry', ['/conference-enquiry/index'], ['class' => 'title']) ?>
                    </li>




                    <li class="<?= $controler == 'patients-visitors' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa  fa-users"></i>Patients & Visitors', ['/patients-visitors/update','id'=>1], ['class' => 'title']) ?>
                    </li>




                    <li
                        class="<?= $controler == 'laboratory-services' ||  $controler == 'laboratory-services-content' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa  fa-users"></i>Laboratory Services', ['/laboratory-services/index'], ['class' => 'title']) ?>
                    </li>

                    <li class="<?= $controler == 'quality-safety' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa  fa-superpowers"></i>Quality Safety', ['/quality-safety/update','id'=>1], ['class' => 'title']) ?>
                    </li>






                    <li class="<?= $controler == 'home-care' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-h-square"></i>Home Care', ['/home-care/index'], ['class' => 'title']) ?>
                    </li>



                    <li class="<?= $controler == 'cme' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-cubes"></i>CME', ['/cme/index'], ['class' => 'title']) ?>
                    </li>





                    <li class="<?= $controler == 'media-blog' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-bullhorn"></i>Medical Blogs', ['/media-blog/index'], ['class' => 'title']) ?>
                    </li>

                    <li class="<?= $controler == 'news' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-newspaper-o"></i>News', ['/news/index'], ['class' => 'title']) ?>
                    </li>




                    <li
                        class="<?= $controler == 'gallery' || $controler == 'photo-gallery' || $controler == 'video-gallery' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-bullhorn"></i>Gallery', ['/gallery/index'], ['class' => 'title']) ?>
                    </li>
                    <li class="<?= $controler == 'media-gallery' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-image"></i>Media', ['/media-gallery/index'], ['class' => 'title']) ?>
                    </li>

                    <li class="<?= $controler == 'privacy-policy' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa   fa-product-hunt"></i>Privacy Policy', ['/privacy-policy/update','id'=>1], ['class' => 'title']) ?>
                    </li>






                    <li
                        class="treeview <?=  $controler == 'careers' || $controler == 'career-enquiry' || $controler =='career-common-content' ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa  fa-tasks"></i>
                            <span>Career</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">



                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Careers', ['/careers/index'], ['class' => 'title']) ?>
                            </li>



                            <li>

                                <?= Html::a('<i class="fa fa-info-circle"></i> Career Enquiry', ['/career-enquiry/index'], ['class' => 'title']) ?>
                            </li>
                            <li>

                                <?= Html::a('<i class="fa fa-info-circle"></i> Collected CVs', ['/career-enquiry/cv-collection'], ['class' => 'title']) ?>
                            </li>

                            <li>








                        </ul>

















                    <li
                        class="treeview <?=  $controler == 'contactinfo' || $controler == 'contact-enquiry' || $controler =='near-by-loactions' ? 'active' : '' ?>">
                        <a href="">
                            <i class="fa  fa-address-card"></i>
                            <span>Contact</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">



                            <li>
                                <?= Html::a('<i class="fa fa-info-circle"></i> Contact Info', ['/contactinfo/update','id'=>1], ['class' => 'title']) ?>
                            </li>



                            <li>

                                <?= Html::a('<i class="fa fa-info-circle"></i> Contact Enquiry', ['/contact-enquiry/index'], ['class' => 'title']) ?>
                            </li>

                            <li>

                                <?= Html::a('<i class="fa fa-info-circle"></i> Near By Loactions', ['/near-by-loactions/index'], ['class' => 'title']) ?>
                            </li>


                        </ul>
                     
                               

                                <li>
                                    <?= Html::a('<i class="fa fa-circle-o"></i> Feed Back', ['/feedback/index'], ['class' => 'title']) ?>
                                </li>
                         





                    <li class="<?= $controler == 'bannerimages' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-image"></i>Banner Images', ['/bannerimages/index'], ['class' => 'title']) ?>
                    </li>


                    <li class="<?= $controler == 'metatags' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-tag"></i>Meta Tags', ['/metatags/index'], ['class' => 'title']) ?>
                    </li>
                    <li class="<?= $controler == 'districts' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-map-marker"></i>Districts', ['/districts/index'], ['class' => 'title']) ?>
                    </li>

                    <li class="<?= $controler == 'panchayat' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-map-marker"></i>Panchayat', ['/panchayat/index'], ['class' => 'title']) ?>
                    </li>
                    <li class="<?= $controler == 'country' ? 'active' : '' ?>">
                        <?= Html::a('<i class="fa fa-map-marker"></i>Country', ['/country/index'], ['class' => 'title']) ?>
                    </li>
                    
                    

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <?= \common\components\AlertMessageWidget::widget() ?>
                <?= $content ?>
            </section>
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; <?= date('Y') ?>Apollo Adlux Hospital .</strong> All Rights Reserved.
        </footer>
        <div class="control-sidebar-bg"></div>
    </div>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>