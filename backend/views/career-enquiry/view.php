<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CareerEnquiry */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Career Enquiries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body"><div class="career-enquiry-view">
                        <p>
                        <?= Html::a(
    '<i class="fa fa-list"></i><span> Manage Career Enquiry</span>',
    !empty($model->job_title) ? ['index'] : ['cv-collection'],
    ['class' => 'btn btn-warning btn-icon btn-icon-standalone']
) ?>
                          
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                              
                            [
                                'attribute' => 'job_title',
                                'visible' => !empty($model->job_title), // This will hide the row if job_title is empty
                            ],
            'name',
            'email:email',
            'mobile',
            // 'country',
            [
                                    'attribute' => 'image',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        if (!empty($data->image))
                                            $img= '<a href="' . Yii::$app->request->baseUrl . '/../images/career/enquiry/profile'  . $data->id . '.' . $data->image . '?' . rand() . '"/>view</a>';
                                            // $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/career/enquiry/profile' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                                        return $img;
                                    },
                                ],
            'date',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


