<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CmeGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cme Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cme-gallery-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                    
                      
                <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update CME</span>', ['/cme/update', 'id' => $cme_id]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">CME Gallery</span>', ['/cme-gallery/index','cme_id'=>$cme_id]);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section> 
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Cme Gallery</span>', ['create','cme_id'=>$cme_id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        
            
  [
                                'attribute' => 'type',
                                'value' => function($model, $key, $index, $column) {
                                    return $model->type == 1 ? 'Image' : 'Video';
                                },
                                'filter' => [1 => 'Image', 2 => 'Video'],
                            ],              [
                                'attribute' => 'status',
                                'value' => function($model, $key, $index, $column) {
                                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                                },
                                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
                            ],

            // 'video',
            // 'image1',
            // 'image1_alt',
            // 'sort_order',
            // 'status',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{update}{delete}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


