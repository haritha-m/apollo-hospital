<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      
<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    
                <?= $form->field($model, 'type')->dropDownList(['1' => 'Image', '2' => 'Video']) ?>

</div>
</div>

    <div class="row" id="one">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
 <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 1920*1080') ?>

            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/cme/gallery/image' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row" id="two">

<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
 <?= $form->field($model, 'video')->fileInput()->hint('Dimensions : 1920*1080') ?>
 
 
 
 
  <?php
            if ($model->video != '' && $model->id != "") {
                $video = explode(',', $model->video);
                if (!empty($video)) {
                    echo '<a  href="' . Yii::$app->request->baseUrl . '/../images/cme/gallery/video'.$model->id.'.' . $model->video . '" / target="_blank" style="color:blue" >View  video </a>';
                }
                ?>
                <br>
                <br>
            <?php }
            ?> 
 
 
 
 
 
 
 
 
 
 </div>

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
 <?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 1920*1080') ?>

            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image = explode(',', $model->image1);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/cme/gallery/thumb' . $model->id . '.' . $model->image1 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image1_alt')->textInput(['maxlength' => true]) ?>
</div>



</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sort_order')->textInput() ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
$(document).ready(function() { var type = $('#cmegallery-type').val();
        if(type == 1)
        {
             $("#one").show();
             $("#two").hide();
        }
    else{

 $("#one").hide();
             $("#two").show();



                }
     var type = $('#cmegallery-type').val();
        if(type == 1)
        {
             $("#one").show();
             $("#two").hide();
        }
    else{

 $("#one").hide();
             $("#two").show();



                }
    $('#cmegallery-type').change(function() {
        var type = $('#cmegallery-type').val();
        if(type == 1)
        {
             $("#one").show();
             $("#two").hide();
        }
    else{

 $("#one").hide();
             $("#two").show();



                }
    });
});
</script>