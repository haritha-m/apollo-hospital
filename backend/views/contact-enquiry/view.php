<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactEnquiry */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Contact Enquiries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body"><div class="contact-enquiry-view">
                        <p>
                            <?=  Html::a('<i class="fa fa-list"></i><span> Manage Contact Enquiry</span>', ['index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>
                          
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                                    
            'name',
            'email:email',
            'phone',
                 [
                        'attribute' => 'district_id',
                        'filter' => \yii\helpers\ArrayHelper::map(\common\models\Districts::find()->where(['status'=>1])->all(), 'id', 'name'),
                        'value' => function ($data) {
                            if ($data->district_id != '') {
                                return \common\models\Districts::findOne($data->district_id)->name;
                            } else {
                                return '';
                            }
                        }
                    ],
             [
                        'attribute' => 'panchayat_id',
                        'filter' => \yii\helpers\ArrayHelper::map(\common\models\Panchayat::find()->where(['status'=>1])->all(), 'id', 'name'),
                        'value' => function ($data) {
                            if ($data->panchayat_id != '') {
                                return \common\models\Panchayat::findOne($data->panchayat_id)->name;
                            } else {
                                return '';
                            }
                        }
                    ],
            'message:ntext',
            'date',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


