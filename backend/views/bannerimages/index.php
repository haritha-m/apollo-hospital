<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BannerImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banner Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-images-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

             [
                                'attribute' => 'image',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    if (!empty($data->image))
                                        $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/banner-images/banner-images' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                                    return $img;
                                },
                            ],
            'page',
           // 'description:ntext',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

['class' => 'yii\grid\ActionColumn',
     'template' => '{update}',
                            ],                         ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


