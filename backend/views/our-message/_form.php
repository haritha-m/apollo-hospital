<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OurMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-message-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>

 <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 561*628') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/our-message/cho' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>


</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'name1')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'position1')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'description1')->textarea(['rows' => 6]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
   
 <?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 561*628') ?>
            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image = explode(',', $model->image1);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/our-message/cht' . $model->id . '.' . $model->image1 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>

</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image1_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>

<hr>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'common_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'common_description')->textarea(['rows' => 3]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    
 <?= $form->field($model, 'image2')->fileInput()->hint('Dimensions : 440*310') ?>
            <?php
            if ($model->image2 != '' && $model->id != "") {
                $image = explode(',', $model->image2);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/our-message/chc' . $model->id . '.' . $model->image2 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>

</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image2_alt')->textInput(['maxlength' => true]) ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
