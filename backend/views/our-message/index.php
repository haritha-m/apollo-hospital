<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OurMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Our Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-message-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Our Message</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                    'id',
            'name',
            'position',
            'description:ntext',
            'image',
            // 'image_alt',
            // 'name1',
            // 'position1',
            // 'description1:ntext',
            // 'image1',
            // 'image1_alt',
            // 'common_title',
            // 'common_description',
            // 'image2',
            // 'image2_alt',
            // 'CB',
            // 'UB',
            // 'DOC:ntext',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


