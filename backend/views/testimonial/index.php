<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TestimonialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Testimonials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonial-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Testimonial</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                   
            'name',
            [
                'attribute' => 'type',
                'value' => function($model, $key, $index, $column) {
                    return $model->type == 1 ? 'Patient' : 'Other';
                },
                'filter' => [1 => 'Patient', 0 => 'Other'],
            ],
            [
                'attribute' => 'thump_image',
                'format' => 'raw',
                'value' => function ($data) {
                    if (!empty($data->thump_image))
                        $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/testimonial/'  . $data->thump_image . '?' . rand() . '"/>';
                    return $img;
                },
            ],
          
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $column) {
                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                },
                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


