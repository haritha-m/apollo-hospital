<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Testimonial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="testimonial-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'type')->dropDownList(['1' => 'Patient', '0' => 'Other']) ?>
</div>
        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'thump_image')->fileInput()->hint('Dimensions : 349*247') ?>
            <?php
            if ($model->thump_image != '' && $model->id != "") {
                $image = explode(',', $model->thump_image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/testimonial/'. $model->thump_image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'video_type')->dropDownList(['1' => 'Youtube', '0' => 'Facebook']) ?>
</div>
<div class='col-md-12 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'video_link')->textarea(['rows' => 2]) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'sort_order')->textInput() ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
