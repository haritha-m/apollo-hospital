<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
   use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="services-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'canonical_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
            <?= $form->field($model, 'small_description')->textarea(['rows' => 6]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 800*517') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/services/image' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image_alt')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <!-- <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'book_an_ppointment_link')->textInput(['maxlength' => true]) ?>
        </div> -->
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'detail_title')->textInput(['maxlength' => true]) ?>
        </div>


        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>



            <?=
            $form->field($model, 'detail_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>






        </div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
             <?= $form->field($model, 'facility_description')->textInput() ?>
        </div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
            <?=
            $form->field($model, 'facility_content', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 800*585') ?>
            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image = explode(',', $model->image1);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/services/data' . $model->id . '.' . $model->image1 . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'image1_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'icon')->fileInput()->hint('Dimensions : 50*50') ?>
            <?php
                if ($model->icon != '' && $model->id != "") {
                    $icon = explode(',', $model->icon);
                    if (!empty($icon)) {
                        echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/services/icon' . $model->id . '.' . $model->icon . '?' . rand() . '" />';
                    }
                    ?>
            <br>
            <br>
            <?php }
                ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'icon_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
            <?=
            $form->field($model, 'faq_description', ['options' => ['class' => 'form-group']])->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'custom',
            ])
            ?>
        </div>
    </div>
    <div class="row">

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'sort_order')->textInput() ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
        </div>


        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'banner_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'banner_description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'banner_image')->fileInput()->hint('Dimensions : 1920*870') ?>
            <?php
            if ($model->banner_image != '' && $model->id != "") {
                $image = explode(',', $model->banner_image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/services/banner-images/banner' . $model->id . '.' . $model->banner_image . '?' . rand() . '" />';
                }
                ?>
            <br>
            <br>
            <?php }
            ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'banner_alt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
        </div>
        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>
        </div>

        <div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'other_meta_tags')->textarea(['rows' => 6]) ?>
        </div>
        <div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'is_booking')->dropDownList(['1' => 'Yes', '0' => 'No']) ?>
</div>
<div class='col-md-4 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'is_home')->dropDownList(['1' => 'Yes', '0' => 'No']) ?>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>