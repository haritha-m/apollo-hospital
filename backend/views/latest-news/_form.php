<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
 use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Blogs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blogs-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>



<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'type')->dropDownList(['1' => 'News &Events', '2' => 'Youtube','3'=>"linkedin"]) ?>
</div>







</div>
    <div class="row"  id="testimage">


<?php
if(Yii::$app->controller->action->id =='create')
{
?>

<div class='col-md-3 col-sm-3 col-xs-12 left_padd'>
 <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 383*257') ?>
</div>
<div class='col-md-3 col-sm-3 col-xs-12 left_padd'>

            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/blogs/blog' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?></div>

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'alt_image')->textInput(['maxlength' => true]) ?>
</div>

<?php

}
else{
    if($model->type == 1)
    {
    ?>

<div class='col-md-3 col-sm-3 col-xs-12 left_padd'>
 <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 375*299') ?>
</div>
<div class='col-md-3 col-sm-3 col-xs-12 left_padd'>

            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/blogs/blog' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?></div>

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'alt_image')->textInput(['maxlength' => true]) ?>
</div>

    <?php
}
}
?>

    </div>
        <div class="row" id="testlink">

<div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
<?php
if(Yii::$app->controller->action->id =='create')
{
?>


    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
    <?php
}
else{
    if($model->type != 1)
    {
    ?>
    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>



    <?php
}
}
?>
</div>

</div>
     


<div class="row">









<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>


<?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]);?>
        
        
    </div>

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']) ?>
</div>


    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>



<script>
  
    $(document).ready(function () {
var t = $('#blogs-type').val();
if(t == 1)
{

$("#testlink").hide();
$("#testimage").show();
}
else{
    $("#testlink").show();
    $("#testimage").hide();
    
}




        $('#blogs-type').on('change', function () {

            var type = $(this).val();


if(type == 1)
{

$("#testlink").hide();
$("#testimage").show();
}
else{
    $("#testlink").show();
    $("#testimage").hide();
    
}
               
          
        });


    });
</script>



