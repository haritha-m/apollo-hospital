<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */

$this->title = 'Update Gallery: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="active">Update Details</span>', ['update', 'id' => $model->id]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Photo Gallery</span>', ['/photo-gallery/index', 'id' => $model->id]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Video Gallery</span>', ['/video-gallery/index', 'id' => $model->id]);
                                    ?>
                            </li>

                        </ul>
                    </div>
                </section>
                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Gallery</span>', ['index'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="gallery-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>