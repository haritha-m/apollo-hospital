<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Gallery</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($data) {
                    if (!empty($data->image))
                        $img = '<img width="60" style = "border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->homeUrl . '../images/gallery/image' . $data->id . '.' . $data->image . '?' . rand() . '"/>';
                    return $img;
                },
            ],
            'sort_order',
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $column) {
                    return $model->status == 0 ? 'Disabled' : 'Enabled';
                },
                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
            ],
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
                        ],
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>