<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\QualitySafetyFilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quality Safety Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quality-safety-files-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                <section id="tabs2">
                        <div class="card1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="">
                                    <?php
                                    echo Html::a('<span class="">Update Quality Safety</span>', ['/quality-safety/update', 'id' => 1]);
                                    ?>
                                </li>
                                <li role="presentation" class="active">
                                    <?php
                                    echo Html::a('<span class="active">Files</span>', ['/quality-safety-files/index']);
                                    ?>

                                </li> 
                                
                            </ul>
                        </div>
                    </section>
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Quality Safety Files</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                   
            'title',
            
            [
                'attribute' => 'status',
                'value' => function($model, $key, $index, $column) {
                    return $model->status == 1 ? 'Enabled' : 'Disabled';
                },
                'filter' => [1 => 'Enabled', 0 => 'Disabled'],
            ],
           

                        ['class' => 'yii\grid\ActionColumn','template'=>'{update}  {delete}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


