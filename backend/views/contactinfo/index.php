<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contact Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-info-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Contact Info</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                    'id',
            'title',
            'caption',
            'email:email',
            'conatct_number',
            // 'address',
            // 'international_patients',
            // 'enquiry_title',
            // 'image',
            // 'image_alt',
            // 'near_by_place_title',
            // 'near_by_place_caption',
            // 'map_link:ntext',
            // 'whatsap_number',
            // 'footer_conatct_number',
            // 'footer_email:email',
            // 'footer_caption',
            // 'footer_get_title',
            // 'footer_get_caption',
            // 'facebook_link:ntext',
            // 'twitter_linlk:ntext',
            // 'instagram_link:ntext',
            // 'linkedin_link:ntext',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


