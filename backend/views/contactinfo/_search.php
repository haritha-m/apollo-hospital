<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContactInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'caption') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'conatct_number') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'international_patients') ?>

    <?php // echo $form->field($model, 'enquiry_title') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'image_alt') ?>

    <?php // echo $form->field($model, 'near_by_place_title') ?>

    <?php // echo $form->field($model, 'near_by_place_caption') ?>

    <?php // echo $form->field($model, 'map_link') ?>

    <?php // echo $form->field($model, 'whatsap_number') ?>

    <?php // echo $form->field($model, 'footer_conatct_number') ?>

    <?php // echo $form->field($model, 'footer_email') ?>

    <?php // echo $form->field($model, 'footer_caption') ?>

    <?php // echo $form->field($model, 'footer_get_title') ?>

    <?php // echo $form->field($model, 'footer_get_caption') ?>

    <?php // echo $form->field($model, 'facebook_link') ?>

    <?php // echo $form->field($model, 'twitter_linlk') ?>

    <?php // echo $form->field($model, 'instagram_link') ?>

    <?php // echo $form->field($model, 'linkedin_link') ?>

    <?php // echo $form->field($model, 'CB') ?>

    <?php // echo $form->field($model, 'UB') ?>

    <?php // echo $form->field($model, 'DOC') ?>

    <?php // echo $form->field($model, 'DOU') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
