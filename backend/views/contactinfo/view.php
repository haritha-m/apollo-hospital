<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactInfo */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Contact Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">

                <div class="panel-body"><div class="contact-info-view">
                        <p>
                            <?=  Html::a('<i class="fa fa-list"></i><span> Manage Contact Info</span>', ['index'], ['class' => 'btn btn-warning  btn-icon btn-icon-standalone']) ?>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                            ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                                    'id',
            'title',
            'caption',
            'email:email',
            'conatct_number',
            'address',
            'international_patients',
            'enquiry_title',
            'image',
            'image_alt',
            'near_by_place_title',
            'near_by_place_caption',
            'map_link:ntext',
            'whatsap_number',
            'footer_conatct_number',
            'footer_email:email',
            'footer_caption',
            'footer_get_title',
            'footer_get_caption',
            'facebook_link:ntext',
            'twitter_linlk:ntext',
            'instagram_link:ntext',
            'linkedin_link:ntext',
            'CB',
            'UB',
            'DOC',
            'DOU',
                        ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


