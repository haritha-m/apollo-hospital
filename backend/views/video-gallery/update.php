<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VideoGallery */

$this->title = 'Update Video Gallery: ';
$this->params['breadcrumbs'][] = ['label' => 'Video Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


            </div>
            <div class="panel-body">
                <section id="tabs2">
                    <div class="card1">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Update Details</span>', ['/gallery/update', 'id' => $model->gallery_id]);
                                    ?>
                            </li>
                            <li role="presentation" class="">
                                <?php
                                    echo Html::a('<span class="">Photo Gallery</span>', ['/photo-gallery/index', 'id' => $model->gallery_id]);
                                    ?>
                            </li>
                            <li role="presentation" class="active">
                                <?php
                                    echo Html::a('<span class="active">Video Gallery</span>', ['index', 'id' => $model->gallery_id]);
                                    ?>
                            </li>

                        </ul>
                    </div>
                </section>


                <?=  Html::a('<i class="fa fa-list"></i><span> Manage Video Gallery</span>', ['index', 'id' => $model->gallery_id], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                <div class="video-gallery-create">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>