<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VisionMission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vision-mission-form form-inline">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'mission_title')->textInput(['maxlength' => true]) ?>
</div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'mission_description')->textarea(['rows' => 6]) ?>
</div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'vision_title')->textInput(['maxlength' => true]) ?>
</div>
        <div class='col-md-12 col-sm-12 col-xs-12 left_padd'>
    <?= $form->field($model, 'vision_description')->textarea(['rows' => 6]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>

 <?= $form->field($model, 'image')->fileInput()->hint('Dimensions : 310*240') ?>
            <?php
            if ($model->image != '' && $model->id != "") {
                $image = explode(',', $model->image);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/vision/first' . $model->id . '.' . $model->image . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>




</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
<?= $form->field($model, 'image1')->fileInput()->hint('Dimensions : 416*291') ?>
            <?php
            if ($model->image1 != '' && $model->id != "") {
                $image = explode(',', $model->image1);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/vision/second' . $model->id . '.' . $model->image1 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>

</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image1_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image2')->fileInput()->hint('Dimensions : 278*203') ?>
            <?php
            if ($model->image2 != '' && $model->id != "") {
                $image = explode(',', $model->image2);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/vision/third' . $model->id . '.' . $model->image2 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>


</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image2_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'common_title')->textInput(['maxlength' => true]) ?>
</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'common_description')->textarea(['rows' => 3]) ?>
</div>
</div>
    <div class="row">

<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
   <?= $form->field($model, 'image3')->fileInput()->hint('Dimensions : 440*310') ?>
            <?php
            if ($model->image3 != '' && $model->id != "") {
                $image = explode(',', $model->image3);
                if (!empty($image)) {
                    echo '<img width="75" style="border: 2px solid #d2d2d2;margin-right:.5em;" src="' . Yii::$app->request->baseUrl . '/../images/vision/common' . $model->id . '.' . $model->image3 . '?' . rand() . '" />';
                }
                ?>
                <br>
                <br>
            <?php }
            ?>


</div>
<div class='col-md-6 col-sm-6 col-xs-12 left_padd'>
    <?= $form->field($model, 'image3_alt')->textInput(['maxlength' => true]) ?>
</div>
</div>

    </div>
    <div class="row">
        <div class='col-md-12 col-sm-12 col-xs-12'>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-block btn-success btn-sm', 'style' => 'float:right;']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
