<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VisionMissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vision Missions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vision-mission-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                    <?=  Html::a('<i class="fa fa-list"></i><span> Create Vision Mission</span>', ['create'], ['class' => 'btn btn-block btn-info btn-sm']) ?>
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                    'id',
            'mission_title',
            'mission_description:ntext',
            'vision_title',
            'vision_description:ntext',
            // 'image',
            // 'image_alt',
            // 'image1',
            // 'image1_alt',
            // 'image2',
            // 'image2_alt',
            // 'common_title',
            // 'common_description',
            // 'image3',
            // 'image3_alt',
            // 'CB',
            // 'UB',
            // 'DOC',
            // 'DOU',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


