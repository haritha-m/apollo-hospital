<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PackageEnquirySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Package Enquiries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-enquiry-index">

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>


                </div>
                <div class="panel-body">
                                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    
                 
                                                                            <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
        'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                                
            'name',
            'email:email',
            'mobile',
            // 'district_id',
            // 'message:ntext',
            // 'package',
            // 'date',

                        ['class' => 'yii\grid\ActionColumn','template'=>'{view}{delete}'],
                        ],
                        ]); ?>
                                                        </div>
            </div>
        </div>
    </div>
</div>


